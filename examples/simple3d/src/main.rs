#![deny(warnings)]

use std::time::Instant;

use nako::{
    self,
    combinators::Union,
    glam::{Quat, Vec3},
    modifiers::{Color, Transform},
    primitives::Sphere,
    stream::{PrimaryStream, SecondaryStream},
};
use nakorender::{
    backend::{Backend, LayerInfo},
    camera::Camera,
    log,
    marp::MarpBackend,
    winit::{
        event::{ElementState, Event, VirtualKeyCode, WindowEvent},
        event_loop::ControlFlow,
        window::WindowBuilder,
    },
};

//#[path = "../../layers/src/model_generator.rs"]
//mod model_generator;

const TRANS_SPEED: f32 = 1.0;

fn main() {
    simple_logger::init_with_level(log::Level::Info).unwrap();
    let event_loop = nakorender::winit::event_loop::EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();
    let mut renderer = MarpBackend::new(&window, &event_loop);

    let id = renderer.new_layer();
    renderer.set_layer_order(&[id.into()]);
    /*
    renderer.update_sdf(
        id,
        model_generator::random_cloud_in_range(1, Vec3::splat(-10.0), Vec3::splat(10.0))
    );
    */

    let mut arrow_key_states = (false, false, false, false);
    let mut wasd_key_states = (false, false, false, false);
    let mut sphere_translation = Vec3::ZERO;
    let mut last_redraw = Instant::now();

    renderer.update_sdf(id, prim(sphere_translation));

    let mut camera = Camera {
        location: Vec3::new(0.0, 0.0, -10.0),
        fov: 45.0,
        rotation: Quat::IDENTITY,
    };
    camera_update(&mut camera, arrow_key_states, 0.0);
    renderer.update_camera(id, camera);
    //left, right, up, down
    event_loop.run(move |event, _, control_flow| {
        // ControlFlow::Poll continuously runs the event loop, even if the OS hasn't
        // dispatched any events. This is ideal for games and similar applications.
        *control_flow = ControlFlow::Poll;

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                println!("The close button was pressed; stopping");
                *control_flow = ControlFlow::Exit
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(new_size),
                ..
            } => {
                println!("Resized info to: {:?}", new_size);
                let canvas_as_info = LayerInfo {
                    extent: (new_size.width, new_size.height).into(),
                    location: (0, 0).into(),
                    sampling: nakorender::backend::LayerSampling::OverSampling(2),
                };
                renderer.set_layer_info(id.into(), canvas_as_info);

                renderer.resize(&window);
            }
            Event::WindowEvent {
                event: WindowEvent::KeyboardInput { input, .. },
                ..
            } => match (input.state, input.virtual_keycode) {
                (ElementState::Released, Some(VirtualKeyCode::Escape)) => {
                    *control_flow = ControlFlow::Exit
                }

                (ElementState::Pressed, Some(VirtualKeyCode::Left)) => arrow_key_states.0 = true,
                (ElementState::Released, Some(VirtualKeyCode::Left)) => arrow_key_states.0 = false,

                (ElementState::Pressed, Some(VirtualKeyCode::Right)) => arrow_key_states.1 = true,
                (ElementState::Released, Some(VirtualKeyCode::Right)) => arrow_key_states.1 = false,

                (ElementState::Pressed, Some(VirtualKeyCode::Up)) => arrow_key_states.2 = true,
                (ElementState::Released, Some(VirtualKeyCode::Up)) => arrow_key_states.2 = false,

                (ElementState::Pressed, Some(VirtualKeyCode::Down)) => arrow_key_states.3 = true,
                (ElementState::Released, Some(VirtualKeyCode::Down)) => arrow_key_states.3 = false,

                (ElementState::Pressed, Some(VirtualKeyCode::W)) => wasd_key_states.0 = true,
                (ElementState::Released, Some(VirtualKeyCode::W)) => wasd_key_states.0 = false,

                (ElementState::Pressed, Some(VirtualKeyCode::A)) => wasd_key_states.1 = true,
                (ElementState::Released, Some(VirtualKeyCode::A)) => wasd_key_states.1 = false,

                (ElementState::Pressed, Some(VirtualKeyCode::S)) => wasd_key_states.2 = true,
                (ElementState::Released, Some(VirtualKeyCode::S)) => wasd_key_states.2 = false,

                (ElementState::Pressed, Some(VirtualKeyCode::D)) => wasd_key_states.3 = true,
                (ElementState::Released, Some(VirtualKeyCode::D)) => wasd_key_states.3 = false,

                _ => {}
            },
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                //Before redrawing, calc camera movement based on current input state
                let delta = last_redraw.elapsed().as_secs_f32();

                match wasd_key_states {
                    (true, _, _, _) => sphere_translation.y += delta * TRANS_SPEED,
                    (_, true, _, _) => sphere_translation.x -= delta * TRANS_SPEED,
                    (_, _, true, _) => sphere_translation.y -= delta * TRANS_SPEED,
                    (_, _, _, true) => sphere_translation.x += delta * TRANS_SPEED,
                    _ => {}
                }

                if arrow_key_states.0
                    || arrow_key_states.1
                    || arrow_key_states.2
                    || arrow_key_states.3
                {
                    camera_update(&mut camera, arrow_key_states, delta);
                    renderer.update_camera(id, camera);
                }

                if wasd_key_states.0 || wasd_key_states.1 || wasd_key_states.2 || wasd_key_states.3
                {
                    //renderer.update_sdf(id, prim(sphere_translation));
                }
                //renderer.update_sdf(id, prim(sphere_translation));
                last_redraw = Instant::now();
                renderer.render(&window);
            }
            _ => (),
        }
    });
}

const ROTATION_SPEED: f32 = 1.0;

fn prim(translation: Vec3) -> PrimaryStream {
    PrimaryStream::new()
        .push(
            SecondaryStream::new(Union, Sphere { radius: 1.0 })
                .push_mod(Color(Vec3::new(1.0, 0.0, 1.0)))
                .push_mod(Transform {
                    rotation: Quat::IDENTITY,
                    translation,
                })
                .build(),
        )
        .build()
}

fn camera_update(camera: &mut Camera, arrow_keys: (bool, bool, bool, bool), delta: f32) {
    //Depending on arrow keys, rotate camera then set position as offset from 0,0,0

    let mut rotation = Quat::IDENTITY;
    if arrow_keys.0 {
        rotation = Quat::from_rotation_y(ROTATION_SPEED * delta) * rotation;
    }

    if arrow_keys.1 {
        rotation = Quat::from_rotation_y(-ROTATION_SPEED * delta) * rotation;
    }

    if arrow_keys.2 {
        rotation = Quat::from_rotation_x(ROTATION_SPEED * delta) * rotation;
    }

    if arrow_keys.3 {
        rotation = Quat::from_rotation_x(-ROTATION_SPEED * delta) * rotation;
    }

    camera.rotation = rotation * camera.rotation;
    camera.location = camera.rotation.mul_vec3(Vec3::Z) * -5.0;
}

use std::time::Instant;

use nako::glam::{Quat, Vec3};
use nakorender::{
    camera::Camera,
    winit::event::{DeviceEvent, ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
};

const MOVEMENT_SPEED: f32 = 10.0;
const MOUSE_SPEED: f32 = 0.001;
///Small camera controller use by the test app.
pub struct CameraController {
    ///Tracks rotation as quaternion.
    rotation: Quat,
    ///Tracks current velocity in 1/sec relative to the forward direction. So moveing forward will always
    /// be (0,0,1), moving to the right will always be (1.0, 0.0, 0.0) etc.
    velocity: Vec3,
    ///Tracks absolute rotation in worldspace directions
    ws_velocity: Vec3,
    ///Tracks world position
    location: Vec3,
    fov: f32,

    last_update: Instant,
}

impl Default for CameraController {
    fn default() -> Self {
        CameraController {
            rotation: Quat::IDENTITY,
            velocity: Vec3::ZERO,
            ws_velocity: Vec3::ZERO,
            location: Vec3::ZERO,
            fov: 90.0,
            last_update: Instant::now(),
        }
    }
}

impl CameraController {
    pub fn as_camera(&self) -> Camera {
        Camera {
            location: self.location,
            rotation: self.rotation,
            fov: self.fov,
        }
    }

    #[allow(dead_code)]
    pub fn event(&mut self, event: &Event<()>) {
        match event {
            Event::WindowEvent {
                event: WindowEvent::KeyboardInput { input, .. },
                ..
            } => match input {
                KeyboardInput {
                    state,
                    virtual_keycode: Some(code),
                    ..
                } => {
                    let speed = match state {
                        ElementState::Pressed => MOVEMENT_SPEED,
                        ElementState::Released => 0.0,
                    };

                    match code {
                        VirtualKeyCode::W => self.velocity.z = speed,
                        VirtualKeyCode::A => self.velocity.x = -speed,
                        VirtualKeyCode::S => self.velocity.z = -speed,
                        VirtualKeyCode::D => self.velocity.x = speed,
                        VirtualKeyCode::E => self.ws_velocity.y = speed,
                        VirtualKeyCode::Q => self.ws_velocity.y = -speed,
                        _ => {}
                    }
                }
                _ => {}
            },
            Event::DeviceEvent { event, .. } => match event {
                DeviceEvent::MouseMotion { delta: (x, y) } => {
                    let right = self.rotation.mul_vec3(Vec3::new(1.0, 0.0, 0.0));
                    let rot_yaw = Quat::from_rotation_y(*x as f32 * MOUSE_SPEED);
                    let rot_pitch = Quat::from_axis_angle(right, *y as f32 * MOUSE_SPEED);

                    let to_add = rot_yaw * rot_pitch;
                    self.rotation = to_add * self.rotation;
                }
                _ => {}
            },
            _ => {}
        }
    }

    //Updates position based on current velocity and time since last update
    #[allow(dead_code)]
    pub fn update(&mut self) {
        let time = self.last_update.elapsed().as_secs_f32();
        self.last_update = Instant::now();

        self.location +=
            (self.rotation.mul_vec3(self.velocity) + self.ws_velocity) * time * MOVEMENT_SPEED;
    }
}

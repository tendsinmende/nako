#![deny(warnings)]

use nako::{
    glam::{IVec2, UVec2, Vec3},
    modifiers::Color,
};
use nako_std::{filler::FillArea, Area};
use nakorender::{
    backend::{Backend, LayerId, LayerId2d, LayerInfo},
    marp::MarpBackend,
    winit::{
        event::{Event, WindowEvent},
        event_loop::{ControlFlow, EventLoop},
        window::Window,
    },
};

const STRESS: usize = 128;

struct TestLayer {
    id: LayerId2d,
    #[allow(dead_code)]
    loc: IVec2,
    #[allow(dead_code)]
    ext: UVec2,
}

fn init_layer(renderer: &mut dyn Backend, window_ext: UVec2) -> TestLayer {
    let id = renderer.new_layer_2d();

    let loc = IVec2::new(
        (rand::random::<f32>() * window_ext.x as f32) as i32,
        (rand::random::<f32>() * window_ext.y as f32) as i32,
    );

    let ext = UVec2::new(
        (rand::random::<f32>() * window_ext.x as f32) as u32,
        (rand::random::<f32>() * window_ext.y as f32) as u32,
    )
    .max(UVec2::ONE);

    renderer.set_layer_info(
        id.into(),
        LayerInfo {
            extent: ext,
            location: loc,
            ..Default::default()
        },
    );

    FillArea {
        color: Color(Vec3::new(
            rand::random::<f32>(),
            rand::random::<f32>(),
            rand::random::<f32>(),
        )),
    }
    .fill(
        renderer,
        id,
        Area {
            from: loc.as_vec2(),
            to: loc.as_vec2() + ext.as_vec2(),
        },
    );

    TestLayer { id, ext, loc }
}

fn init(renderer: &mut dyn Backend, window_ext: UVec2) -> Vec<TestLayer> {
    let layers = (0..STRESS)
        .map(|_| init_layer(renderer, window_ext))
        .collect::<Vec<_>>();

    let order = layers
        .iter()
        .map(|layer| layer.id.into())
        .collect::<Vec<LayerId>>();
    renderer.set_layer_order(&order);

    layers
}

fn main() {
    //startup logger
    simple_logger::init_with_level(nakorender::log::Level::Trace).unwrap();

    let evl = EventLoop::new();
    let window = Window::new(&evl).unwrap();
    let mut renderer = MarpBackend::new(&window, &evl);

    let window_ext = UVec2::new(window.inner_size().width, window.inner_size().height);
    //create some layers with stuff that are moved around
    let _ = init(&mut renderer, window_ext);

    evl.run(move |event, _target, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                *control_flow = ControlFlow::Exit;
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(new),
                ..
            } => {
                let _ = init(&mut renderer, UVec2::new(new.width, new.height));
            }
            Event::MainEventsCleared => window.request_redraw(),
            Event::RedrawRequested(_) => {
                renderer.render(&window);
            }
            _ => {}
        }
    });
}

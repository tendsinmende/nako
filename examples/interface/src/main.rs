#![deny(warnings)]

use nako::{
    glam::{Vec2, Vec3},
    modifiers::Color,
};
use nako_std::{filler::FillArea, Area};
use nakorender::{
    backend::Backend,
    log,
    marp::MarpBackend,
    winit::{
        event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
        event_loop::{ControlFlow, EventLoop},
        window::WindowBuilder,
    },
};

use crate::interface::Interface;

mod interface;

fn main() {
    simple_logger::init_with_level(log::Level::Trace).unwrap();
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();
    let mut renderer = MarpBackend::new(&window, &event_loop);

    let bglayer = renderer.new_layer_2d();
    let bg_filler = FillArea {
        color: Color(Vec3::splat(0.1)),
    };
    let s = renderer.size().as_vec2();
    bg_filler.fill(
        &mut renderer,
        bglayer,
        Area {
            from: Vec2::ZERO,
            to: s,
        },
    );

    let mut interface = Interface::new(&mut renderer);

    let mut layers = vec![bglayer.into()];
    layers.append(&mut interface.get_order());
    renderer.set_layer_order(&layers);

    event_loop.run(move |event, _, control_flow| {
        // ControlFlow::Poll continuously runs the event loop, even if the OS hasn't
        // dispatched any events. This is ideal for games and similar applications.
        *control_flow = ControlFlow::Poll;

        interface.on_event(&event, &mut renderer);

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                println!("The close button was pressed; stopping");
                *control_flow = ControlFlow::Exit
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(new_size),
                ..
            } => {
                println!("Resized info to: {:?}", new_size);
                renderer.resize(&window);
                bg_filler.fill(
                    &mut renderer,
                    bglayer,
                    Area {
                        from: Vec2::ZERO,
                        to: Vec2::new(new_size.width as f32, new_size.height as f32),
                    },
                );
            }
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: ElementState::Released,
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                ..
                            },
                        ..
                    },
                ..
            } => {
                *control_flow = ControlFlow::Exit;
            }
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                renderer.render(&window);
            }
            _ => (),
        }
    });
}

use nako::{
    combinators::Union,
    glam::{const_vec3, IVec2, UVec2, Vec2, Vec3},
    modifiers::{Color, Translate},
    primitives::Circle,
    stream::{PrimaryStream2d, SecondaryStream2d},
};
use nako_std::{
    filler::{FillArea, QuadricBezierSpline},
    interface::{
        buttons::{LabeledButton, PushButton, DEFAULT_BUTTON_SIZE},
        edit::Edit,
        number_edit::{FloatEdit, IntegerEdit},
        scroll_array::{ArrayDirection, ScrollArray},
        slider::Slider,
        InterfaceElement,
    },
    text::{Label, TextLine},
    Area,
};
use nakorender::{
    backend::{Backend, LayerId, LayerId2d, LayerInfo},
    camera::Camera2d,
    winit::event::{ElementState, Event, MouseButton, WindowEvent},
};

pub const COLOR_BUTTON: Color = Color(const_vec3!([0.800, 0.141, 0.114]));
pub const COLOR_BUTTON_HOVER: Color = Color(const_vec3!([0.984, 0.286, 0.204]));

struct Buttons {
    button: PushButton<()>,
    text_button: LabeledButton<()>,
    scroll_button: ScrollArray<LabeledButton<()>>,
}

impl Buttons {
    pub fn new(renderer: &mut dyn Backend) -> Buttons {
        let mut button: PushButton<()> =
            PushButton::<()>::new(renderer).with_ctx(|_| println!("Pressed unlabeled button"), ());
        button.on_hover_color = Some(COLOR_BUTTON_HOVER);
        button.color = COLOR_BUTTON;

        let mut text_button = LabeledButton::new(renderer, "Labeled Button!");
        text_button.button.on_hover_color = Some(COLOR_BUTTON_HOVER);
        text_button.button.color = COLOR_BUTTON;
        text_button.button = text_button
            .button
            .with_ctx(|_| println!("Pressed text button"), ());

        let mut scroll_buttons = Vec::with_capacity(10);
        for i in 0..10 {
            let mut scroll_button = LabeledButton::new(renderer, &format!("Scroll Button {}!", i));
            scroll_button.button.on_hover_color = Some(COLOR_BUTTON_HOVER);
            scroll_button.button.color = COLOR_BUTTON;
            scroll_button.button = scroll_button
                .button
                .with_ctx(move |_| println!("Pressed scroll button {}", i), ());

            scroll_buttons.push(scroll_button);
        }

        let scroll_button = ScrollArray::new(renderer, scroll_buttons, ArrayDirection::Horizontal);
        Buttons {
            button,
            text_button,
            scroll_button,
        }
    }

    pub fn on_event(&mut self, event: &Event<()>, renderer: &mut dyn Backend) {
        self.button.on_event(renderer, event);
        self.text_button.on_event(renderer, event);
        self.scroll_button.on_event(renderer, event);
    }
}

struct SplineDrawing {
    spline: QuadricBezierSpline,
    lbg: LayerId2d,
    //lspline: LayerId2d,
    lctrlptr: LayerId2d,
    area: Area,
    mouse_loc: Vec2,

    layer_char: LayerId2d,
    line: TextLine,
}

impl SplineDrawing {
    fn new(renderer: &mut dyn Backend) -> Self {
        SplineDrawing {
            area: Area::ONE,
            lbg: renderer.new_layer_2d(),
            //lspline: renderer.new_layer_2d(),
            lctrlptr: renderer.new_layer_2d(),
            spline: QuadricBezierSpline {
                color: Color(Vec3::new(1.0, 0.95, 0.98)),
                points: vec![],
                width: 2.0,
            },
            mouse_loc: Vec2::ZERO,

            line: TextLine::new_from_path(
                "crates/nako_std/resources/Swansea-q3pd.ttf",
                "Test Text",
            )
            .unwrap(),
            layer_char: renderer.new_layer_2d(),
        }
    }

    fn render(&mut self, renderer: &mut dyn Backend) {
        FillArea {
            color: Color(Vec3::splat(0.3)),
        }
        .fill(renderer, self.lbg, self.area);

        //Draw ctrl points
        renderer.update_camera_2d(
            self.lctrlptr,
            Camera2d {
                extent: self.area.extent(),
                location: self.area.from,
                rotation: 0.0,
            },
        );
        renderer.set_layer_info(
            self.lctrlptr.into(),
            LayerInfo {
                extent: self.area.extent().as_uvec2(),
                location: self.area.from.as_ivec2(),
                ..Default::default()
            },
        );

        let mut stream = PrimaryStream2d::new();
        for pt in &mut self.spline.points {
            stream = stream.push(
                SecondaryStream2d::new(Union, Circle { radius: 2.0 })
                    .push_mod(COLOR_BUTTON)
                    .push_mod(Translate(*pt))
                    .build(),
            );
        }

        renderer.update_sdf_2d(self.lctrlptr, stream.build());

        self.line.set_position(Vec2::splat(0.0));
        self.line.size = 10.0;
        renderer.update_camera_2d(
            self.layer_char,
            Camera2d {
                extent: Vec2::splat(200.0),
                location: Vec2::ZERO,
                rotation: 0.0,
            },
        );
        renderer.set_layer_info(
            self.layer_char.into(),
            LayerInfo {
                extent: UVec2::splat(200),
                location: IVec2::new(200, 500),
                ..Default::default()
            },
        );

        let mut stream = PrimaryStream2d::new();
        stream = self.line.record_line(stream);
        renderer.update_sdf_2d(self.layer_char, stream.build());
    }

    fn set_area(&mut self, renderer: &mut dyn Backend, new_area: Area) {
        self.area = new_area;
        self.render(renderer);
    }

    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        match event {
            Event::WindowEvent {
                event:
                    WindowEvent::MouseInput {
                        button: MouseButton::Left,
                        state: ElementState::Pressed,
                        ..
                    },
                ..
            } => {
                //check location, if inside, add point
                if self.area.is_in(self.mouse_loc) {
                    self.spline.points.push(self.mouse_loc);
                    self.render(renderer);
                }
            }
            Event::WindowEvent {
                event: WindowEvent::CursorMoved { position, .. },
                ..
            } => {
                self.mouse_loc = Vec2::new(position.x as f32, position.y as f32);
            }
            _ => {}
        }
    }
}

///The interfaces caches meta information like the cursor position as well as some logical blocks
///For the different structures we show off.
pub struct Interface {
    buttons: Buttons,
    edit: Edit,
    int_edit: IntegerEdit,
    float_edit: FloatEdit,
    drawing: SplineDrawing,
    slider: Slider,
    label: Label,
}

impl Interface {
    pub fn new(renderer: &mut dyn Backend) -> Self {
        let interface = Interface {
            buttons: Buttons::new(renderer),
            edit: Edit::new_from_str("MyEditYo", renderer),
            int_edit: IntegerEdit::new_default(renderer, 4, 42),
            float_edit: FloatEdit::new_default(renderer, 5, 4.20),
            drawing: SplineDrawing::new(renderer),
            slider: Slider::new(renderer, 0.25),
            label: Label::new(renderer, "Interface Elements", 25.0),
        };
        interface
    }

    pub fn on_event(&mut self, event: &Event<()>, renderer: &mut dyn Backend) {
        match event {
            Event::WindowEvent {
                window_id: _,
                event: WindowEvent::Resized(_new_size),
            } => {
                let button_array_start = Vec2::new(100.0, 50.0);
                let button_area = DEFAULT_BUTTON_SIZE;
                let button_advance = Vec2::new(button_area.x, 0.0) + Vec2::new(20.0, 0.0);

                //Move buttons
                self.buttons.button.set_area(Area {
                    from: button_array_start,
                    to: button_array_start + button_area,
                });

                self.buttons.text_button.set_area(Area {
                    from: button_array_start + (1.0 * button_advance),
                    to: button_array_start + (1.0 * button_advance) + button_area,
                });

                self.buttons.scroll_button.set_area(Area {
                    from: button_array_start + (2.0 * button_advance),
                    to: button_array_start + (2.0 * button_advance) + button_area * 2.0,
                });

                self.edit.set_area(Area {
                    from: Vec2::new(100.0, 150.0),
                    to: Vec2::new(100.0, 150.0) + DEFAULT_BUTTON_SIZE * Vec2::new(2.0, 1.0),
                });

                self.int_edit.set_area(Area {
                    from: Vec2::new(100.0, 200.0),
                    to: Vec2::new(100.0, 200.0) + DEFAULT_BUTTON_SIZE * Vec2::new(2.0, 1.0),
                });

                self.float_edit.set_area(Area {
                    from: Vec2::new(100.0, 250.0),
                    to: Vec2::new(100.0, 250.0) + DEFAULT_BUTTON_SIZE * Vec2::new(2.0, 1.0),
                });

                self.slider.set_area(Area {
                    from: Vec2::new(100.0, 300.0),
                    to: Vec2::new(100.0, 300.0) + DEFAULT_BUTTON_SIZE * Vec2::new(2.0, 1.0),
                });

                self.label.set_area(Area {
                    from: Vec2::new(20.0, 10.0),
                    to: Vec2::new(250.0, 40.0),
                });

                let drawing_area_ext = Vec2::new(600.0, 400.0);
                let draw_from = self.edit.get_area().from
                    + Vec2::new(self.edit.get_area().extent().x, 0.0)
                    + Vec2::new(10.0, 0.0);
                let draw_to = draw_from + drawing_area_ext;
                self.drawing.set_area(
                    renderer,
                    Area {
                        from: draw_from,
                        to: draw_to,
                    },
                );

                println!("Value is: {}", self.int_edit.get_value());
                println!("Value is: {}", self.float_edit.get_value());
                println!("Slider is: {}", self.slider.get_value());
            }
            _ => {}
        }

        self.buttons.on_event(event, renderer);
        self.edit.on_event(renderer, event);
        self.int_edit.on_event(renderer, event);
        self.float_edit.on_event(renderer, event);
        self.drawing.on_event(renderer, event);
        self.slider.on_event(renderer, event);
        self.label.on_event(renderer, event);
    }

    pub fn get_order(&self) -> Vec<LayerId> {
        let mut layers = vec![];

        layers.append(&mut self.buttons.button.get_layers().to_vec());
        layers.append(&mut self.buttons.text_button.get_layers().to_vec());
        layers.append(&mut self.buttons.scroll_button.get_layers().to_vec());
        layers.append(&mut self.edit.get_layers().to_vec());
        layers.append(&mut self.int_edit.get_layers().to_vec());
        layers.append(&mut self.float_edit.get_layers().to_vec());
        layers.append(&mut self.slider.get_layers().to_vec());
        layers.append(&mut self.label.get_layers().to_vec());

        layers.push(self.drawing.lbg.into());
        layers.push(self.drawing.lctrlptr.into());
        layers.push(self.drawing.layer_char.into());

        layers
    }
}

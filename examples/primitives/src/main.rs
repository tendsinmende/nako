#![deny(warnings)]

use std::time::Instant;

use nako::{
    self,
    combinators::Union,
    glam::{Quat, Vec2, Vec3, Vec4},
    modifiers::{Color, ColorTrans, Round, Transform, Translate},
    primitives::{
        Circle, Cuboid, Extrusion, Line, PlanarProjection, Polygon, QuadricBezier, Revolution,
        Sphere, Triangle,
    },
    stream::{PrimaryStream, PrimaryStream2d, SecondaryStream, SecondaryStream2d},
};
use nako_std::{filler::FillArea, Area};
use nakorender::{
    backend::{Backend, LayerInfo},
    camera::{Camera, Camera2d},
    cpu::CpuBackend,
    log::Level,
    winit::{
        event::{DeviceEvent, ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
        event_loop::ControlFlow,
        window::WindowBuilder,
    },
};

const CAMERA_SPEED: f32 = 10.0;
const ROTATION_SPEED: f32 = 0.01;
struct CameraControler {
    location: Vec3,
    rotation: Quat,

    //rotation local velocity
    velocity: Vec3,
    last_update: Instant,
}

impl CameraControler {
    fn on_event(&mut self, event: &Event<()>) {
        match event {
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state,
                                virtual_keycode: Some(kc),
                                ..
                            },
                        ..
                    },
                ..
            } => match (state, kc) {
                (ElementState::Pressed, VirtualKeyCode::Left | VirtualKeyCode::D) => {
                    self.velocity.x = CAMERA_SPEED
                }
                (ElementState::Released, VirtualKeyCode::Left | VirtualKeyCode::D) => {
                    self.velocity.x = 0.0
                }
                (ElementState::Pressed, VirtualKeyCode::Right | VirtualKeyCode::A) => {
                    self.velocity.x = -CAMERA_SPEED
                }
                (ElementState::Released, VirtualKeyCode::Right | VirtualKeyCode::A) => {
                    self.velocity.x = 0.0
                }
                (ElementState::Pressed, VirtualKeyCode::Up | VirtualKeyCode::W) => {
                    self.velocity.z = CAMERA_SPEED
                }
                (ElementState::Released, VirtualKeyCode::Up | VirtualKeyCode::W) => {
                    self.velocity.z = 0.0
                }
                (ElementState::Pressed, VirtualKeyCode::Down | VirtualKeyCode::S) => {
                    self.velocity.z = -CAMERA_SPEED
                }
                (ElementState::Released, VirtualKeyCode::Down | VirtualKeyCode::S) => {
                    self.velocity.z = 0.0
                }
                (ElementState::Pressed, VirtualKeyCode::E) => self.velocity.y = CAMERA_SPEED,
                (ElementState::Released, VirtualKeyCode::E) => self.velocity.y = 0.0,
                (ElementState::Pressed, VirtualKeyCode::Q) => self.velocity.y = -CAMERA_SPEED,
                (ElementState::Released, VirtualKeyCode::Q) => self.velocity.y = 0.0,
                _ => {}
            },
            Event::DeviceEvent {
                event: DeviceEvent::MouseMotion { delta },
                ..
            } => {
                let right = self.rotation.mul_vec3(Vec3::new(1.0, 0.0, 0.0));
                let rot_yaw = Quat::from_rotation_y(delta.0 as f32 * ROTATION_SPEED);
                let rot_pitch = Quat::from_axis_angle(right, delta.1 as f32 * ROTATION_SPEED);

                let to_add = rot_yaw * rot_pitch;
                self.rotation = to_add * self.rotation;
            }
            _ => {}
        }
    }

    fn update(&mut self) -> Camera {
        self.location +=
            self.last_update.elapsed().as_secs_f32() * self.rotation.mul_vec3(self.velocity);
        self.last_update = Instant::now();

        Camera {
            fov: 90.0,
            location: self.location,
            rotation: self.rotation,
        }
    }
}

fn secondary_array() -> Vec<SecondaryStream> {
    vec![
        SecondaryStream::new(Union, Sphere { radius: 1.0 })
            .push_mod(Color(Vec3::new(1.0, 1.0, 1.0)))
            .build(),
        SecondaryStream::new(
            Union,
            Cuboid {
                extent: Vec3::splat(0.5),
            },
        )
        .push_mod(ColorTrans(Vec4::new(0.5, 1.0, 0.2, 0.1)))
        .build(),
        SecondaryStream::new(
            Union,
            Line {
                start: Vec3::splat(-0.5),
                end: Vec3::splat(0.5),
                width: 0.1,
            },
        )
        .push_mod(Color(Vec3::new(1.0, 1.0, 1.0)))
        .build(),
        SecondaryStream::new(
            Union,
            Triangle {
                vertices: [
                    Vec3::new(0.0, 0.5, 0.5),
                    Vec3::splat(0.5),
                    Vec3::splat(-0.5),
                ],
            },
        )
        .push_mod(Color(Vec3::new(1.0, 1.0, 1.0)))
        .push_mod(Round { radius: 0.05 })
        .build(),
        SecondaryStream::new(
            Union,
            Extrusion {
                length: 1.0,
                stream: PrimaryStream2d::new()
                    .push(
                        SecondaryStream2d::new(Union, Circle { radius: 0.5 })
                            .push_mod(Color(Vec3::ONE))
                            .build(),
                    )
                    .build(),
            },
        )
        .push_mod(Transform {
            rotation: Quat::from_rotation_x((45.0f32).to_radians()),
            translation: Vec3::ZERO,
        })
        .build(),
        SecondaryStream::new(
            Union,
            PlanarProjection {
                stream: PrimaryStream2d::new()
                    .push(
                        SecondaryStream2d::new(Union, Circle { radius: 0.5 })
                            .push_mod(Color(Vec3::ONE))
                            .build(),
                    )
                    .build(),
            },
        )
        .push_mod(Transform {
            rotation: Quat::from_rotation_x((45.0f32).to_radians()),
            translation: Vec3::ZERO,
        })
        .build(),
        SecondaryStream::new(
            Union,
            Revolution {
                offset: 1.0,
                stream: PrimaryStream2d::new()
                    .push(
                        SecondaryStream2d::new(Union, Circle { radius: 0.5 })
                            .push_mod(Color(Vec3::ONE))
                            .build(),
                    )
                    .build(),
            },
        )
        .push_mod(Transform {
            rotation: Quat::from_rotation_x((45.0f32).to_radians()),
            translation: Vec3::ZERO,
        })
        .build(),
    ]
}

fn streams_2d() -> Vec<PrimaryStream2d> {
    vec![
        PrimaryStream2d::new()
            .push(
                SecondaryStream2d::new(Union, Circle { radius: 1.0 })
                    .push_mod(Color(Vec3::ONE))
                    .push_mod(Translate(Vec2::splat(1.0)))
                    .build(),
            )
            .build(),
        PrimaryStream2d::new()
            .push(
                SecondaryStream2d::new(
                    Union,
                    Cuboid {
                        extent: Vec2::splat(0.5),
                    },
                )
                .push_mod(Color(Vec3::ONE))
                .push_mod(Translate(Vec2::splat(1.0)))
                .build(),
            )
            .build(),
        PrimaryStream2d::new()
            .push(
                SecondaryStream2d::new(
                    Union,
                    Line {
                        start: Vec2::ZERO,
                        end: Vec2::ONE,
                        width: 0.05,
                    },
                )
                .push_mod(Color(Vec3::ONE))
                .build(),
            )
            .build(),
        PrimaryStream2d::new()
            .push(
                SecondaryStream2d::new(
                    Union,
                    Triangle {
                        vertices: [Vec2::ZERO, Vec2::new(0.0, 1.0), Vec2::ONE],
                    },
                )
                .push_mod(Color(Vec3::ONE))
                .build(),
            )
            .build(),
        PrimaryStream2d::new()
            .push(
                SecondaryStream2d::new(
                    Union,
                    Polygon {
                        vertices: vec![
                            Vec2::ZERO,
                            Vec2::new(0.0, 1.0),
                            Vec2::new(0.5, 0.5),
                            Vec2::ONE,
                            Vec2::new(1.0, 0.0),
                        ],
                    },
                )
                .push_mod(Color(Vec3::ONE))
                .build(),
            )
            .build(),
        PrimaryStream2d::new()
            .push(
                SecondaryStream2d::new(
                    Union,
                    QuadricBezier {
                        start: Vec2::ZERO,
                        controll: Vec2::new(0.1, 3.0),
                        end: Vec2::ONE,
                        width: 0.05,
                    },
                )
                .push_mod(Color(Vec3::ONE))
                .build(),
            )
            .build(),
    ]
}

fn main() {
    simple_logger::init_with_level(Level::Info).unwrap();
    let mut camera_ctrl = CameraControler {
        location: Vec3::new(0.0, 0.0, -4.0),
        rotation: Quat::IDENTITY,
        velocity: Vec3::ZERO,
        last_update: Instant::now(),
    };

    let event_loop = nakorender::winit::event_loop::EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();
    let mut renderer = CpuBackend::new(&window, &event_loop);

    let bgid = renderer.new_layer_2d();
    FillArea {
        color: Color(Vec3::new(0.2, 0.2, 0.2)),
    }
    .fill(&mut renderer, bgid, Area::ONE);

    let id = renderer.new_layer();

    let id_2d = renderer.new_layer_2d();
    renderer.update_camera_2d(
        id_2d,
        Camera2d {
            extent: Vec2::new(5.0, 5.0),
            location: Vec2::ZERO,
            rotation: 0.0,
        },
    );
    renderer.set_layer_order(&[bgid.into(), id.into(), id_2d.into()]);
    //Setup all primitives as one big sdf stream

    renderer.update_sdf_2d(
        id_2d,
        PrimaryStream2d::new()
            .push(
                SecondaryStream2d::new(Union, Circle { radius: 1.0 })
                    .push_mod(Color(Vec3::ONE))
                    .push_mod(Translate(Vec2::splat(1.0)))
                    .build(),
            )
            .build(),
    );

    renderer.update_sdf(
        id,
        PrimaryStream::new()
            .push(
                SecondaryStream::new(Union, Sphere { radius: 1.0 })
                    .push_mod(Color(Vec3::ONE))
                    .build(),
            )
            .build(),
    );

    let mut last_update = Instant::now();
    let mut next_stream_3d = 0;
    let streams_3d = secondary_array();
    let mut next_stream_2d = 0;
    let streams_2d = streams_2d();

    event_loop.run(move |event, _, control_flow| {
        // ControlFlow::Poll continuously runs the event loop, even if the OS hasn't
        // dispatched any events. This is ideal for games and similar applications.
        *control_flow = ControlFlow::Poll;

        camera_ctrl.on_event(&event);

        if last_update.elapsed().as_secs_f32() > 2.0 {
            renderer.update_sdf(
                id,
                PrimaryStream::new()
                    .push(streams_3d[next_stream_3d].clone())
                    .build(),
            );
            last_update = Instant::now();
            next_stream_3d = (next_stream_3d + 1) % streams_3d.len();

            renderer.update_sdf_2d(id_2d, streams_2d[next_stream_2d].clone());
            next_stream_2d = (next_stream_2d + 1) % streams_2d.len();

            println!("NEW PRIM!");
        }

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                println!("The close button was pressed; stopping");
                *control_flow = ControlFlow::Exit
            }
            Event::WindowEvent {
                event: WindowEvent::Resized(new_size),
                ..
            } => {
                println!("Resized info to: {:?}", new_size);
                let canvas_as_info = LayerInfo {
                    extent: (new_size.width, new_size.height).into(),
                    location: (0, 0).into(),
                    sampling: nakorender::backend::LayerSampling::OverSampling(2),
                };

                FillArea {
                    color: Color(Vec3::new(0.2, 0.2, 0.2)),
                }
                .fill(
                    &mut renderer,
                    bgid,
                    Area {
                        from: Vec2::ZERO,
                        to: Vec2::new(new_size.width as f32, new_size.height as f32),
                    },
                );
                renderer.set_layer_info(id.into(), canvas_as_info);
                let aspect = new_size.width as f32 / new_size.height as f32;
                let new_camera = Camera2d {
                    extent: Vec2::splat(5.0) * Vec2::new(aspect, 1.0),
                    location: Vec2::ZERO,
                    rotation: 0.0,
                };
                renderer.update_camera_2d(id_2d, new_camera);
                renderer.set_layer_info(id_2d.into(), canvas_as_info);
                renderer.resize(&window);
            }
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                virtual_keycode: Some(VirtualKeyCode::Escape),
                                state: ElementState::Pressed,
                                ..
                            },
                        ..
                    },
                ..
            } => *control_flow = ControlFlow::Exit,
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                //Before redrawing, calc camera movement based on current input state
                renderer.update_camera(id, camera_ctrl.update());

                renderer.render(&window);
            }
            _ => (),
        }
    });
}

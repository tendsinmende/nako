/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::{
    fs::create_dir_all,
    io::{Read, Write},
    path::{Path, PathBuf},
};

use shader_shared::access::{
    get_glsl_location, get_shader_crate_location, get_shader_spirv_location,
};
use spirv_builder::{
    Capability, MetadataPrintout, ModuleResult, SpirvBuilder, SpirvBuilderError, SpirvMetadata,
};

///Builds the shader crate and moves all files to a location that can be found by the renderer's loader.
pub fn compile_rust_shader(name: &str) -> Result<(), SpirvBuilderError> {
    let mut shader_crate_location =
        get_shader_crate_location().map_err(|e| SpirvBuilderError::CratePathDoesntExist(e))?;
    shader_crate_location.push(name);
    println!("Building shader {:?}", shader_crate_location);

    let mut spirv_target_location = get_shader_spirv_location();
    if !spirv_target_location.exists() {
        create_dir_all(&spirv_target_location).expect("Could not create spirv directory!");
    }
    spirv_target_location = spirv_target_location
        .canonicalize()
        .expect("Could not canonicalize path!");
    println!("SpirV dir @ {:?}", spirv_target_location);
    spirv_target_location.push(format!("{}.spv", name));
    println!("SpirvFile @ {:?}", spirv_target_location);

    let compiler_result = SpirvBuilder::new(shader_crate_location, "spirv-unknown-vulkan1.2")
        .spirv_metadata(SpirvMetadata::NameVariables)
        .print_metadata(MetadataPrintout::None)
        .capability(Capability::Int8)
        .capability(Capability::Int16)
        .capability(Capability::ImageQuery)
        .build()?;

    println!("Generated following Spirv entrypoints:");
    for e in &compiler_result.entry_points {
        println!("{}", e);
    }

    let move_spirv_file = |spv_location: &Path| {
        std::fs::copy(spv_location, &spirv_target_location).expect("Failed to copy spirv file!");
    };

    match compiler_result.module {
        ModuleResult::MultiModule(modules) => {
            //Note currently ignoring entry name since all of them should be "main", just copying the
            //shader files. Later might use a more sophisticated approach.
            for (_entry, src_file) in modules {
                move_spirv_file(&src_file);
            }
        }
        ModuleResult::SingleModule(path) => {
            move_spirv_file(&path);
        }
    };

    Ok(())
}

#[allow(dead_code)]
fn glsl_include(
    file_name: &str,
    _include_type: shaderc::IncludeType,
    _target_shader: &str,
    _size: usize,
) -> core::result::Result<shaderc::ResolvedInclude, String> {
    //Load the file given in the include and load it
    let mut content = String::new();

    let mut include_path = get_glsl_location();
    include_path.push("includes");
    include_path.push(file_name);

    println!("Using include path: {:?}", include_path);

    let mut include_file =
        std::fs::File::open(&include_path).expect("Failed to open shader include file!");
    include_file
        .read_to_string(&mut content)
        .expect("Could not read include file to shader!");

    Ok(shaderc::ResolvedInclude {
        resolved_name: String::from(file_name),
        content,
    })
}

#[allow(dead_code)]
fn compile_shader(name: &str, out_name: &str, shader_type: shaderc::ShaderKind) -> PathBuf {
    let mut file_path = get_glsl_location();
    file_path.push(name);

    let mut file = std::fs::File::open(&file_path).expect("Failed to read shader!");
    let mut source = String::new();
    file.read_to_string(&mut source)
        .expect("Failed to read shader!");

    let mut compiler = shaderc::Compiler::new().expect("Failed to start shaderc compiler!");

    let mut compile_options =
        shaderc::CompileOptions::new().expect("Failed to create compile options!");
    compile_options.add_macro_definition("EP", Some("main"));
    //Always go for performance
    compile_options.set_optimization_level(shaderc::OptimizationLevel::Performance);
    compile_options.set_include_callback(glsl_include);

    let result = compiler
        .compile_into_spirv(
            &source,
            shader_type,
            name,
            "main", //always entry at main atm.
            Some(&compile_options),
        )
        .expect("Failed to compile shader!");

    if result.get_num_warnings() > 0 {
        println!("\tWarnings: {}", result.get_warning_messages());
    }

    let mut out_path = get_shader_spirv_location();
    out_path.push(out_name);
    println!("Creating glsl spv at {:?}", out_path);

    let mut out_file = std::fs::File::create(&out_path).expect("Could not create spv file!");
    let len = out_file
        .write(result.as_binary_u8())
        .expect("Failed to write out binary spv file");
    if len != result.as_binary_u8().len() {
        panic!("Failed to write out shader binary!");
    }

    PathBuf::from(out_path)
}

#[allow(dead_code)]
fn exchange_ending(src: &str, new_ending: &str) -> String {
    let (name, _old_ending) = src.rsplit_once('.').unwrap();

    let mut name = name.to_string();
    name.push_str(new_ending);
    name
}

#[allow(dead_code)]
pub fn glsl_compiler(name: &str) {
    match (
        name.ends_with(".comp"),
        name.ends_with(".glsl"),
        name.ends_with(".vs"),
        name.ends_with(".fs"),
    ) {
        (true, _, _, _) | (_, true, _, _) => {
            println!("Compiling {} as  compute shader", name);
            let spirv_name = exchange_ending(&name, ".spv");
            compile_shader(&&name, &spirv_name, shaderc::ShaderKind::Compute)
        }
        (_, _, true, _) => {
            println!("Compiling {} as vertex shader", name);
            let spirv_name = exchange_ending(&name, ".spv");
            compile_shader(&&name, &spirv_name, shaderc::ShaderKind::Vertex)
        }
        (_, _, _, true) => {
            println!("Compiling {} as fragment shader", name);
            let spirv_name = exchange_ending(&name, ".spv");
            compile_shader(&&name, &spirv_name, shaderc::ShaderKind::Fragment)
        }
        _ => {
            println!("Could not classify shader {}", name);
            return;
        }
    };
}

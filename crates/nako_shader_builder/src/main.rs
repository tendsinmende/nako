use nako_shader_builder::glsl_compiler;
use rayon::prelude::*;
use shader_shared::access::{get_glsl_location, get_shader_spirv_location};

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#[path = "lib.rs"]
mod shader_builder;

///All shaders, compiled if no argument is given.
pub const ALL_SHADERS: &'static [&'static str] = &[
    "shader_clear",
    "shader_direct_light",
    "shader_layer_combine",
    "shader_primary_2d",
    "shader_primary_3d",
    "shader_resolve",
    "shader_shading",
    "shader_query_volume",
    "shader_primary_3d_segment_tracing",
];

pub fn main() {
    let mut shaders_to_compile = ALL_SHADERS
        .iter()
        .map(|s| s.to_string())
        .collect::<Vec<_>>();
    let mut cleared_module_list = false;
    let mut compile_glsl = false;
    let mut compile_rust = true;
    //Check if we got any arguments
    let mut arg_iter = std::env::args();
    let _progname = arg_iter.next();
    loop {
        if let Some(arg) = arg_iter.next() {
            match arg.as_str() {
                "--module" | "-m" => {
                    //got a module, check if this is the first module, if yes, erase compile list. After push following argument as name
                    compile_rust = true;
                    if !cleared_module_list {
                        cleared_module_list = true;
                        shaders_to_compile.clear();
                    }

                    if let Some(module_name) = arg_iter.next() {
                        shaders_to_compile.push(module_name);
                    } else {
                        println!("Declared module, but no name followed, check help for correct command pattern!");
                        println!("Return without compiling!");
                        return;
                    }
                }
                "--no-rust-shader" | "-n" => {
                    compile_rust = false;
                }
                "--glsl" | "-g" => {
                    compile_glsl = true;
                }
                "--help" | "-h" => {
                    println!(
                        "
NAKO SHADER COMPILER

nako_shader_compiler [<argument> <option>]

arguments:
-n --no-rust-shader       : does not compile any rust shader crates
-g --glsl                 : compiles glsl shaders
-m --module <module name> : compiles a shader crate with <module name>.
-h --help                 : prints this help
"
                    );
                    return;
                }
                _ => {
                    println!(
                        "Unsupported argument: {}\nexiting without compiling shaders",
                        arg
                    );
                    return;
                }
            }
        } else {
            break;
        }
    }

    if compile_glsl | compile_rust {
        //check if the spirv directory exists, if not, create it
        if !get_shader_spirv_location().exists() {
            std::fs::create_dir_all(get_shader_spirv_location()).unwrap();
        }
    }

    if compile_rust {
        println!("Compiling shader crate(s):");
        let i: usize = shaders_to_compile
            .par_iter()
            .map(|name| {
                println!("   {}", name);
                shader_builder::compile_rust_shader(&name).expect("Failed to compile shaders");
                1
            })
            .sum();
        println!("Compiled {} shader crates", i);
    }

    if compile_glsl {
        for file in std::fs::read_dir(&get_glsl_location()).unwrap() {
            let name = file
                .unwrap()
                .file_name()
                .into_string()
                .unwrap()
                .rsplit('/')
                .last()
                .unwrap()
                .to_string();
            if !name.ends_with(".spv") {
                glsl_compiler(&name);
            }
        }
    }
}

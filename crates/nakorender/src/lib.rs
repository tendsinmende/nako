/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
//#![deny(warnings)]

#[cfg(feature = "logging")]
pub extern crate log;

pub extern crate nako;
pub use nako::glam;

pub mod backend;
///Marp based backend
pub mod marp;

#[cfg(feature = "cpu_backend")]
pub mod cpu;

///Different camera related tools and imlpementations
pub mod camera;

pub(crate) mod layer_manager;

///Reexport of winit for easy access.
pub use marp_surface_winit::winit;

///Shared crate between cpu and gpu related code. Provides some extra utilities like AABBs,
///rays and sdf query functions from streams.
pub extern crate shader_shared as nako_shared;

#[macro_export]
macro_rules! render_warn{
    ($($stream:tt)+) => {
        #[cfg(feature = "logging")]
        {
            use log::warn;
            warn!($($stream)+)
        }
    }
}

#[macro_export]
macro_rules! render_info{
    ($($stream:tt)+) => {
        #[cfg(feature = "logging")]
        {
            use log::info;
            info!($($stream)+)
        }
    }
}

#[macro_export]
macro_rules! render_error{
    ($($stream:tt)+) => {
        #[cfg(feature = "logging")]
        {
            use log::error;
            error!($($stream)+)
        }
    }
}

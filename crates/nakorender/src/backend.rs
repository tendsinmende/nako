/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use marp_surface_winit::winit::window::Window;
use nako::{
    glam::{IVec2, UVec2},
    stream::{PrimaryStream, PrimaryStream2d},
};
use slotmap::DefaultKey;

use crate::camera::{Camera, Camera2d};

///Id to any type of layer.
#[derive(Hash, Eq, PartialEq, Clone, Copy, Debug)]
pub enum LayerId {
    Planar(LayerId2d),
    Volumetric(LayerId3d),
}

impl From<LayerId2d> for LayerId {
    fn from(id: LayerId2d) -> Self {
        LayerId::Planar(id)
    }
}

impl From<LayerId3d> for LayerId {
    fn from(id: LayerId3d) -> Self {
        LayerId::Volumetric(id)
    }
}
///Id for an layer presenting an planar SDF
#[derive(Hash, Eq, PartialEq, Clone, Copy, Debug)]
pub struct LayerId2d(pub(crate) DefaultKey);
#[derive(Hash, Eq, PartialEq, Clone, Copy, Debug)]
///Id for an layer presenting an volumetric SDF
pub struct LayerId3d(pub(crate) DefaultKey);

///Describes scaling of this layer. Allows the user to render a layer either via over sampling, or// undersampling. In the latter case a fidelity-fx like upscaler is used.
#[derive(Clone, Copy, Debug, PartialEq)]
pub enum LayerSampling {
    ///Layers are render as is. Meaning no over or undersampling is done.
    None,
    ///super sampling count. If set to 1, then each unit is covered by 1 pixel. When set to 2 each unit
    /// is covered by 2x2 pixel etc.
    ///
    /// # Performance
    ///
    ///While higher values mean a better image quality values > 3 might impact the performance heavily, as special when rendering 3d scenes.
    ///
    /// # Safety
    ///
    /// A value of 0 is ignored and 1 is used instead.
    OverSampling(u32),
    ///under sampling. Similarly to OverSampling a value of 1 means each unit is covered by one pixel. However, if set to 2 each unit is only covered by quarter pixel (1/x²) and when set to 3 each
    /// pixel is covered by a 9th (1/2³).
    UnderSamping(u32),
}

///Describes how the layer is rendered on screen. This allowes you to project the output of an layer only onto a certain part of a window.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct LayerInfo {
    ///Location on screen from which the layer `extent`s (top left of the layer).
    pub location: IVec2,
    pub extent: UVec2,

    pub sampling: LayerSampling,
}

impl Default for LayerInfo {
    fn default() -> Self {
        LayerInfo {
            extent: UVec2::ONE,
            location: IVec2::ZERO,
            sampling: LayerSampling::None,
        }
    }
}

///Defines the interface to any backend.
pub trait Backend {
    fn new(
        window: &Window,
        event_loop: &marp_surface_winit::winit::event_loop::EventLoop<()>,
    ) -> Self
    where
        Self: Sized;

    ///Allocates a new 3d sdf representing layer
    fn new_layer(&mut self) -> LayerId3d;
    ///Allocates a new 2d sdf representing layer
    fn new_layer_2d(&mut self) -> LayerId2d;

    ///Tells the backend that the sdf tree has changed for the given 3d layer.
    fn update_sdf(&mut self, id: LayerId3d, new_sdf: PrimaryStream);
    ///Updates the camera of the given 3d layer
    fn update_camera(&mut self, id: LayerId3d, camera: Camera);

    ///Tells the backend that the sdf tree has changed for the given 3d layer.
    fn update_sdf_2d(&mut self, id: LayerId2d, new_sdf: PrimaryStream2d);
    ///Updates the camera of the given 3d layer
    fn update_camera_2d(&mut self, id: LayerId2d, camera: Camera2d);

    ///Sets location and extent of the given layer within the renderer.
    fn set_layer_info(&mut self, id: LayerId, info: LayerInfo);

    ///Sets the order in which the given layers are combined, where first is the lowest and last is the highest.
    fn set_layer_order(&mut self, order: &[LayerId]);

    ///Tells the renderer to render a new frame into `window`.
    fn render(&mut self, window: &Window);
    ///Resizes the renderers internal window size
    fn resize(&mut self, window: &Window);
    ///Returns the current internal window size
    fn size(&self) -> UVec2;
}

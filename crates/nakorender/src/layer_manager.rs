/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! Opinionated layer managing tool. Provides capabilities to order layers based on provided ordering, as well as scheduling prarallel combination Tasks
use nako::{
    glam::{IVec2, UVec2},
    stream::{PrimaryStream, PrimaryStream2d},
};
use slotmap::{DefaultKey, SlotMap};

use crate::{
    backend::{LayerId, LayerId2d, LayerId3d, LayerInfo, LayerSampling},
    camera::{Camera, Camera2d},
};

pub trait LayerRenderer {
    type Camera;
    type Stream;

    type TaskType;

    ///Updates the render extent being used. to render into.
    ///Sampling be used to adaptively change over / undersampling techniques on the fly
    fn update_extent(&mut self, new_extent: UVec2, sampling: LayerSampling);
    ///Updates number of buffering slots. Used for dual or tripple buffering mostly.
    fn update_number_of_slots(&mut self, num_slots: usize);
    ///Should update the inner camera of this layer.
    fn update_camera(&mut self, camera: Self::Camera);
    fn update_sdf(&mut self, sdf: Self::Stream);
    ///Records this layers action. Usually a layer either has to rerender its
    ///image, or stay the same. Extent is given by the layers internal extent, location is set by the manager via `layer_location`
    fn record(&mut self, slot: usize, layer_location: IVec2) -> Self::TaskType;
}

pub enum LayerType<TaskType> {
    L2d {
        renderer: Box<
            dyn LayerRenderer<Camera = Camera2d, Stream = PrimaryStream2d, TaskType = TaskType>
                + Send,
        >,
    },
    L3d {
        renderer: Box<
            dyn LayerRenderer<Camera = Camera, Stream = PrimaryStream, TaskType = TaskType> + Send,
        >,
    },
}

struct Layer<TaskType> {
    ///Layer info
    info: LayerInfo,

    layer: LayerType<TaskType>,
}

impl<TaskType> Layer<TaskType> {
    fn update_info(&mut self, new_info: LayerInfo) {
        //check if we have to notify layer of ext change
        if new_info.extent != self.info.extent || new_info.sampling != self.info.sampling {
            match &mut self.layer {
                LayerType::L2d { renderer } => {
                    renderer.update_extent(new_info.extent, new_info.sampling)
                }
                LayerType::L3d { renderer } => {
                    renderer.update_extent(new_info.extent, new_info.sampling)
                }
            }
        }

        self.info = new_info;
    }

    fn is_empty(&self) -> bool {
        self.info.extent == UVec2::ZERO
    }
}

pub struct LayerManager<TaskType> {
    layers: SlotMap<DefaultKey, Layer<TaskType>>,

    //insert for 2d and 3d layers
    insert_2d: Box<
        dyn Fn() -> Box<
                dyn LayerRenderer<Camera = Camera2d, Stream = PrimaryStream2d, TaskType = TaskType>
                    + Send,
            > + Send,
    >,
    insert_3d: Box<
        dyn Fn() -> Box<
                dyn LayerRenderer<Camera = Camera, Stream = PrimaryStream, TaskType = TaskType>
                    + Send,
            > + Send,
    >,

    num_slots: usize,
    order: Vec<LayerId>,
}

impl<TaskType: 'static> LayerManager<TaskType> {
    pub fn new<D2D, D3D>(
        default_2d_layer_creation: D2D,
        default_3d_layer_creation: D3D,
        num_slots: usize,
    ) -> Self
    where
        D2D: Fn() -> Box<
                dyn LayerRenderer<Camera = Camera2d, Stream = PrimaryStream2d, TaskType = TaskType>
                    + Send,
            > + Send
            + 'static,
        D3D: Fn() -> Box<
                dyn LayerRenderer<Camera = Camera, Stream = PrimaryStream, TaskType = TaskType>
                    + Send,
            > + Send
            + 'static,
    {
        LayerManager {
            layers: SlotMap::new(),

            insert_2d: Box::new(default_2d_layer_creation),
            insert_3d: Box::new(default_3d_layer_creation),
            num_slots,
            order: Vec::new(),
        }
    }

    pub fn alloc_layer_2d(&mut self) -> LayerId2d {
        let id = self.layers.insert(Layer {
            info: LayerInfo::default(),
            layer: LayerType::L2d {
                renderer: (*self.insert_2d)(),
            },
        });

        LayerId2d(id)
    }

    pub fn alloc_layer_3d(&mut self) -> LayerId3d {
        let id = self.layers.insert(Layer {
            info: LayerInfo::default(),
            layer: LayerType::L3d {
                renderer: (*self.insert_3d)(),
            },
        });

        LayerId3d(id)
    }

    pub fn set_num_slots(&mut self, num_slots: usize) {
        if num_slots == self.num_slots {
            return;
        }

        self.num_slots = num_slots;
        //Have to recreate all layers.
        //TODO only recreate used ones and tag unused for recreation if they get used again
        for (_k, l) in self.layers.iter_mut() {
            match &mut l.layer {
                LayerType::L2d { renderer } => renderer.update_number_of_slots(self.num_slots),
                LayerType::L3d { renderer } => renderer.update_number_of_slots(self.num_slots),
            }
        }
    }

    pub fn set_info(&mut self, id: LayerId, new_info: LayerInfo) {
        let id = layer_id_to_key(id);
        if let Some(l) = self.layers.get_mut(id) {
            l.update_info(new_info);
        }
    }

    pub fn set_order(&mut self, new_order: &[LayerId]) {
        #[cfg(Debug)]
        {
            for new_id in &new_order {
                assert!(
                    self.layers.contains_key(new_id),
                    "New order contains unknown id"
                );
            }
        }

        self.order = new_order.to_vec();
    }

    pub fn update_camera_2d(&mut self, id: LayerId2d, camera: Camera2d) {
        if let Some(l) = self.layers.get_mut(id.0) {
            match &mut l.layer {
                LayerType::L2d { renderer } => renderer.update_camera(camera),
                _ => debug_assert!(false, "Tried to update 3d layer with 2d camera"),
            }
        }
    }

    pub fn update_camera(&mut self, id: LayerId3d, camera: Camera) {
        if let Some(l) = self.layers.get_mut(id.0) {
            match &mut l.layer {
                LayerType::L3d { renderer } => renderer.update_camera(camera),
                _ => debug_assert!(false, "Tried updating 2d layer with 3d camera"),
            }
        }
    }

    pub fn update_sdf_2d(&mut self, id: LayerId2d, sdf: PrimaryStream2d) {
        if let Some(l) = self.layers.get_mut(id.0) {
            match &mut l.layer {
                LayerType::L2d { renderer } => renderer.update_sdf(sdf),
                _ => debug_assert!(false, "Tried updating 3d layer with 2d sdf"),
            }
        }
    }

    pub fn update_sdf(&mut self, id: LayerId3d, sdf: PrimaryStream) {
        if let Some(l) = self.layers.get_mut(id.0) {
            match &mut l.layer {
                LayerType::L3d { renderer } => renderer.update_sdf(sdf),
                _ => debug_assert!(false, "Tried updaing 2d layer with 3d sdf"),
            }
        }
    }

    pub fn record(&mut self, slot: usize) -> Vec<TaskType> {
        //Collect in order
        self.order
            .clone()
            .iter()
            .filter_map(|layer_id| {
                let id = layer_id_to_key(*layer_id);
                if !self.layers.get(id).unwrap().is_empty() {
                    let location = self.layers.get(id).unwrap().info.location;
                    Some(match &mut self.layers.get_mut(id).unwrap().layer {
                        LayerType::L2d { renderer } => renderer.record(slot, location),
                        LayerType::L3d { renderer } => renderer.record(slot, location),
                    })
                } else {
                    None
                }
            })
            .collect()
    }
}

#[inline(always)]
fn layer_id_to_key(id: LayerId) -> DefaultKey {
    match id {
        LayerId::Planar(l) => l.0,
        LayerId::Volumetric(l) => l.0,
    }
}

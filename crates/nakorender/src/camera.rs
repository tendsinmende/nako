/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use nako::glam::{self, Mat4, UVec2, Vec2, Vec3};
use nako_shared::{GpuCamera, GpuCamera2d};

///definition for a camera in 3d space
#[derive(Clone, Copy)]
pub struct Camera {
    pub location: glam::Vec3,
    pub rotation: glam::Quat,
    ///Horizontal field of view in degree.
    pub fov: f32,
}

impl Camera {
    pub fn transform(&self) -> Mat4 {
        glam::Mat4::from_rotation_translation(self.rotation, self.location)
    }

    pub fn into_gpu_cam(&self, img_width: u32, img_height: u32) -> GpuCamera {
        GpuCamera {
            location: [self.location.x, self.location.y, self.location.z],
            fov: self.fov, //TODO already calculate radiant?
            rotation: self.rotation.into(),
            img_width,
            img_height,
            pad1: [0, 0],
        }
    }
}

impl Default for Camera {
    fn default() -> Self {
        Camera {
            location: Vec3::ZERO,
            rotation: glam::Quat::IDENTITY,
            fov: 90.0,
        }
    }
}

///Definition of an camera in 2d space
#[derive(Clone, Copy)]
pub struct Camera2d {
    ///location of the Top left edge of this camera.
    pub location: Vec2,
    ///Extent of this camera in "worldspace" units. Is projected to the actual layer,
    pub extent: Vec2,
    ///Rotations around it self in degree.
    pub rotation: f32,
}

impl Camera2d {
    ///Calculates where in 2d worldspace a pixel would be
    pub fn pixel_to_worldspace(&self, coord: UVec2, extent: UVec2) -> Vec2 {
        //TODO impl rotation as well
        let pixel_size = self.extent / extent.as_vec2();

        self.location + (pixel_size * coord.as_vec2())
    }

    pub fn into_gpu_cam(&self, image_extent: UVec2) -> GpuCamera2d {
        GpuCamera2d {
            location: [self.location.x, self.location.y],
            extent: [self.extent.x, self.extent.y],
            rotation: self.rotation,
            image_size: [image_extent.x as f32, image_extent.y as f32],
            pad1: 0.0,
        }
    }
}

impl Default for Camera2d {
    fn default() -> Self {
        Camera2d {
            location: Vec2::ZERO,
            extent: Vec2::new(1.0, 1.0),
            rotation: 0.0,
        }
    }
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::sync::Arc;

use image::{Rgba, RgbaImage};
use nako::{
    glam::{IVec2, UVec2},
    stream::{PrimaryStream, PrimaryStream2d},
};

use crate::{
    backend::LayerSampling,
    camera::{Camera, Camera2d},
    layer_manager::LayerRenderer,
};

use super::rendering::{evaluate_sdf_2d, evaluate_sdf_3d};

fn mix_alpha(a: f32, b: f32, alpha: f32) -> f32 {
    let alpha = alpha.clamp(0.0, 1.0);

    a * alpha + b * (1.0 - alpha)
}

fn mix(target: Rgba<u8>, src: Rgba<u8>) -> Rgba<u8> {
    Rgba([
        mix_alpha(src.0[0] as f32, target.0[0] as f32, src.0[3] as f32).clamp(0.0, u8::MAX as f32)
            as u8,
        mix_alpha(src.0[1] as f32, target.0[1] as f32, src.0[3] as f32).clamp(0.0, u8::MAX as f32)
            as u8,
        mix_alpha(src.0[2] as f32, target.0[2] as f32, src.0[3] as f32).clamp(0.0, u8::MAX as f32)
            as u8,
        src.0[3].max(target.0[3]),
    ])
}

///Blends the two images based on the alpha of src
fn mix_image(target: &mut RgbaImage, src: &RgbaImage, src_location: IVec2, src_extent: UVec2) {
    //early return if nothing to be copied
    if (target.width() as usize) < src_location.x as usize
        || (target.height() as usize) < src_location.y as usize
    {
        return;
    }

    //Also do not copy if the layer is outside of the targets range
    if (src_location.x + src_extent.x as i32) < 0 || (src_location.y + src_extent.y as i32) < 0 {
        return;
    }

    let copy_width =
        (src_location.x + src_extent.x as i32).min(target.width() as i32) - src_location.x;
    let copy_height =
        (src_location.y + src_extent.y as i32).min(target.height() as i32) - src_location.y;

    for x in 0..copy_width {
        for y in 0..copy_height {
            *target.get_pixel_mut(
                (src_location.x + x as i32) as u32,
                (src_location.y + y as i32) as u32,
            ) = mix(
                *target.get_pixel(
                    (src_location.x + x as i32) as u32,
                    (src_location.y + y as i32) as u32,
                ),
                *src.get_pixel(x as u32, y as u32),
            );
        }
    }
}

pub(crate) fn combine(layers: Vec<CpuTask>, extent: UVec2) -> RgbaImage {
    let mut final_image = RgbaImage::new(extent.x, extent.y);
    for task in layers {
        mix_image(&mut final_image, &task.image, task.location, task.extent)
    }

    final_image
}

///Task emitted by cpu renderer's layer.
pub struct CpuTask {
    image: RgbaImage,
    location: IVec2,
    extent: UVec2,
}

pub(crate) struct CpuLayer3d {
    camera: Camera,
    stream: Arc<PrimaryStream>,
    image: RgbaImage,

    up_to_date: bool,
}

impl Default for CpuLayer3d {
    fn default() -> Self {
        CpuLayer3d {
            image: RgbaImage::new(1, 1),
            stream: Arc::new(PrimaryStream::new().build()),
            camera: Camera::default(),
            up_to_date: false,
        }
    }
}

impl LayerRenderer for CpuLayer3d {
    type Camera = Camera;
    type Stream = PrimaryStream;
    type TaskType = CpuTask;

    fn update_camera(&mut self, camera: Self::Camera) {
        self.camera = camera;
        self.up_to_date = false;
    }

    fn update_sdf(&mut self, sdf: Self::Stream) {
        self.stream = Arc::new(sdf);
        self.up_to_date = false;
    }

    fn update_extent(&mut self, new_extent: UVec2, _sampling: LayerSampling) {
        //ignore sampling for now..
        self.image = RgbaImage::new(new_extent.x, new_extent.y);
        self.up_to_date = false;
    }

    fn update_number_of_slots(&mut self, _num_slots: usize) {
        //ignore for now
    }

    fn record(&mut self, _slot: usize, layer_location: IVec2) -> Self::TaskType {
        if !self.up_to_date {
            //update image
            evaluate_sdf_3d(&mut self.image, self.stream.clone(), &self.camera);
        }

        CpuTask {
            extent: UVec2::new(self.image.width(), self.image.height()),
            image: self.image.clone(),
            location: layer_location,
        }
    }
}

pub(crate) struct CpuLayer2d {
    camera: Camera2d,
    stream: Arc<PrimaryStream2d>,
    image: RgbaImage,

    up_to_date: bool,
}

impl Default for CpuLayer2d {
    fn default() -> Self {
        CpuLayer2d {
            image: RgbaImage::new(1, 1),
            stream: Arc::new(PrimaryStream2d::new().build()),
            camera: Camera2d::default(),
            up_to_date: false,
        }
    }
}

impl LayerRenderer for CpuLayer2d {
    type Camera = Camera2d;
    type Stream = PrimaryStream2d;
    type TaskType = CpuTask;

    fn update_camera(&mut self, camera: Self::Camera) {
        self.camera = camera;
        self.up_to_date = false;
    }

    fn update_sdf(&mut self, sdf: Self::Stream) {
        self.stream = Arc::new(sdf);
        self.up_to_date = false;
    }

    fn update_extent(&mut self, new_extent: UVec2, _sampling: LayerSampling) {
        //ignore sampling for now..
        self.image = RgbaImage::new(new_extent.x, new_extent.y);
        self.up_to_date = false;
    }

    fn update_number_of_slots(&mut self, _num_slots: usize) {
        //ignore for now
    }

    fn record(&mut self, _slot: usize, layer_location: IVec2) -> Self::TaskType {
        if !self.up_to_date {
            //update image
            evaluate_sdf_2d(&mut self.image, self.stream.clone(), &self.camera);
        }

        CpuTask {
            extent: UVec2::new(self.image.width(), self.image.height()),
            image: self.image.clone(),
            location: layer_location,
        }
    }
}

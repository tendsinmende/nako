/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use crate::camera::{Camera, Camera2d};
use image::Rgba;
use image::{self, RgbaImage};
use nako::glam::{UVec2, UVec3, Vec3};
use nako::sdf::BBox;
use nako::stream::PrimaryStream;
use nako::stream::PrimaryStream2d;
use nako_shared::push_constants::QueryVolumePushConst;
use nako_shared::query_volume::QueryVoxel;
use nako_shared::ray_tracing::{segment_tracing, bound_segment_tracing, SegmentTraceResult};
use rayon::iter::IntoParallelIterator;
use rayon::iter::ParallelIterator;
use std::{sync::Arc, time::Instant};

use super::shaders::unlit_2d;

const VOXEL_RESOLUTION: u32 = 16;

pub fn evaluate_sdf_3d(img: &mut RgbaImage, code: Arc<PrimaryStream>, camera: &Camera) {
    let code = code.clone();

    println!("Code: {:?}", code.stream);
    let camera = camera.into_gpu_cam(img.width(), img.height());

    let mut is_finit = true;
    let bound = if let Some(finite_bound) = code.finite_bound {
        finite_bound
    } else {
        println!("Bound is infinite!");
        is_finit = false;
        BBox {
            min: Vec3::splat(-20.0),
            max: Vec3::splat(20.0),
        }
    };

    println!("Bound: {:?}", code.finite_bound.unwrap());

    //execute opt pass once for dispatch 1x1, which is 8x8 threads
    let mut push = QueryVolumePushConst {
        min: bound.min.into(),
        max: bound.max.into(),
        resolution: VOXEL_RESOLUTION,
        voxel_size: 0.0, //set later
        pad0: [0; 2],
        flags: if is_finit {
            QueryVolumePushConst::FLAG_WITHIN_BOUNDS
        } else {
            0
        },
        pad2: 0.0,
    };
    push.set_voxel_size();
    println!("Flag: {}", push.flags);

    //Setup query volume
    let mut query_volume = vec![
        QueryVoxel {
            bound: [0.0; 3],
            dist: 0.0
        };
        (VOXEL_RESOLUTION * VOXEL_RESOLUTION * VOXEL_RESOLUTION) as usize
    ];

    let start = Instant::now();
    for dx in 0..VOXEL_RESOLUTION {
        for dy in 0..VOXEL_RESOLUTION {
            for dz in 0..VOXEL_RESOLUTION {
                shader_query_volume::main(
                    UVec3::new(dx, dy, dz),
                    &push,
                    &code.stream,
                    &mut query_volume,
                );
            }
        }
    }
    let elapsed_sec = start.elapsed().as_secs_f32();
    println!(
        "Setup volume in: {}s, which is {}ms per voxel",
        elapsed_sec,
        elapsed_sec / (128 * 128 * 128) as f32 * 1_000.0
    );
    /*
    for x in 0..VOXEL_RESOLUTION{
        for y in 0..VOXEL_RESOLUTION{
            let max = (0..VOXEL_RESOLUTION).fold(0.0, |f, z|
                                                 query_volume[push.global_index_to_1d_index(UVec3::new(x, y, z))].dist.max(f)
            );
            print!("-{}-", max)
        }
        println!("");
    }
    */
    /*
    let pixel: Vec<(u32, u32, Rgba<f32>)> = (0..img.width())
        .into_par_iter()
        .map(|x| {
            let mut result = Vec::with_capacity(img.height() as usize);
            for y in 0..img.height() {
                let pixel_coord = UVec2::new(x as u32, y as u32);
                if pixel_coord.x >= img.width() || pixel_coord.y >= img.height() {
                    continue;
                }

                //Simple lit shader for now
                let color = lit(UVec2::new(x as u32, y as u32), &camera, &code.stream);

                //let tile_color = color;
                let color = Rgba(color.into());
                result.push((x as u32, y as u32, color));
            }
            result
        })
        .flatten()
        .collect();
    */

    let shared_volume = Arc::new(query_volume);
    let pixel: Vec<(u32, u32, Rgba<f32>)> = (0..img.width())
        .into_par_iter()
        .map(|x| {
            let mut result = Vec::with_capacity(img.height() as usize);
            for y in 0..img.height() {
                let mut pixel_coord = UVec2::new(x as u32, y as u32);
                if pixel_coord.x >= img.width() || pixel_coord.y >= img.height() {
                    continue;
                }
                pixel_coord.y = img.height() - pixel_coord.y;

                let ray = camera.primary_ray(pixel_coord);

                let (mut mint, mut maxt) = (1.0f32, 150.0f32);
                
                let ray_result = if push.sdf_enclosed(){
                    let (is_intersecting, new_min, new_max) = shader_shared::bounds::BBox{
                        min: push.min.into(),
                        max: push.max.into()
                    }.ray_intersections(&ray);

                    if !is_intersecting{
                        //println!("Infinit ts");
                        SegmentTraceResult::NO_INTERSECTION
                    }else{
                        mint = mint.max(new_min);
                        maxt = maxt.min(new_max);
                        //println!("Min: {}, max: {}", mint, maxt);
                        bound_segment_tracing(&push, &shared_volume, &code.stream, &ray, mint, maxt)
                    }                  
                    }else{
                    segment_tracing(&push, &shared_volume, &code.stream, &ray, 0.0, 150.0)
                };
                    

                //let ray_result = trace(&code.stream, &ray, 150.0);

                //let ray_result = trace(&code.stream, &ray, 150.0);
                /*
                //Sample volume
                let sample_location = UVec3::new(
                    x % VOXEL_RESOLUTION,
                    y % VOXEL_RESOLUTION,
                    y / VOXEL_RESOLUTION,
                );

                let index = push.global_index_to_1d_index(sample_location);
                let color = if index <= shared_volume.len(){
                    let result = &shared_volume[index];
                    [result.dist, result.dist, result.dist, 1.0]
                }else{
                    [0.0, 0.0, 0.0, 1.0]
                };
                 */
                let grad = colorgrad::warm();
                let color = if ray_result.has_hit {
                    //println!("Using intersection shader");
                    let col = grad.at(ray_result.result.slot_i0[0] as f64 / 64.0);
                    [col.red() as f32, col.green() as f32, col.blue() as f32, 1.0]
                    //ray_result.result.slot_v0
                } else {
                    if ray_result.is_invalid(){
                        [1.0, 0.0, 0.0, 1.0]
                    }else{
                        [0.0, 0.0, 1.0, 1.0]
                    }
                };
                //Simple lit shader for now
                //let color = lit(UVec2::new(x as u32, y as u32), &camera, &code.stream);

                let color = Rgba(color);
                result.push((x as u32, y as u32, color));
            }
            result
        })
        .flatten()
        .collect();

    println!(
        "Whole Rendering took {}ms",
        start.elapsed().as_secs_f32() * 1000.0
    );

    for (x, y, color) in pixel.into_iter() {
        let mut px = img.get_pixel_mut(x, y);
        px.0[0] = (color.0[0] * u8::MAX as f32) as u8;
        px.0[1] = (color.0[1] * u8::MAX as f32) as u8;
        px.0[2] = (color.0[2] * u8::MAX as f32) as u8;
        px.0[3] = (color.0[3] * u8::MAX as f32) as u8;
    }
}

pub fn evaluate_sdf_2d(img: &mut RgbaImage, code: Arc<PrimaryStream2d>, camera: &Camera2d) {
    let pixel: Vec<(u32, u32, Rgba<f32>)> = (0..img.width())
        .into_par_iter()
        .map(|x| {
            let mut result = Vec::with_capacity(img.height() as usize);
            let code = code.clone();

            for y in 0..img.height() {
                let color = unlit_2d(
                    UVec2::new(img.width(), img.height()),
                    UVec2::new(x, y),
                    *camera,
                    code.clone(),
                );
                result.push((x as u32, y as u32, color));
            }
            result
        })
        .flatten()
        .collect();

    //Collect pixel into image buffer
    for (x, y, color) in pixel.into_iter() {
        let mut px = img.get_pixel_mut(x, y);
        px.0[0] = (color.0[0] * u8::MAX as f32) as u8;
        px.0[1] = (color.0[1] * u8::MAX as f32) as u8;
        px.0[2] = (color.0[2] * u8::MAX as f32) as u8;
        px.0[3] = (color.0[3] * u8::MAX as f32) as u8;
    }
}

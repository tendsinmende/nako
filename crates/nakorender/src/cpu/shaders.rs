/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use std::sync::Arc;

use crate::camera::Camera2d;
use image::Rgba;
use nako::{
    glam::{UVec2, Vec3, Vec4},
    serialize::Word,
    stream::PrimaryStream2d,
};

use nako_shared::{
    evaluator::eval_2d_from_code,
    ray_tracing::{get_normal, trace},
    GpuCamera,
};

const MAX_DISTANCE: f32 = 1000.0;
const LIGHT_LOCATION: [f32; 3] = [10.0, 300.0, -105.0];
const AMBIENT_LIGHT: [f32; 3] = [0.06, 0.05, 0.05];
const LIGHT_COLOR: [f32; 3] = [1.0, 0.85, 0.95];

///renders a pixel at `coord`, based on `camera` and an sdf.
#[allow(dead_code)]
pub fn lit(mut coord: UVec2, camera: &GpuCamera, sdf: &[Word]) -> Vec4 {
    //Since we are in "vulkan space" switch y
    coord.y = camera.img_height - coord.y - 1;

    let ray = camera.primary_ray(coord);

    let primary = trace(sdf, &ray, MAX_DISTANCE);

    if primary.t < MAX_DISTANCE {
        //The location at which we have found something
        let eval_location = ray.point_on_ray(primary.t);
        let n = get_normal(sdf, eval_location);

        let ll: Vec3 = LIGHT_LOCATION.into();
        //Trace ray to light location
        let shadow_ray_direction = (ll - eval_location).normalize();
        let phong = shadow_ray_direction.dot(n).max(0.0).min(1.0);

        /*
            let (lightray, num_skips) = shadow_march(
            eval_location + (n * 1.0),
            LIGHT_LOCATION.into(),
            SHADOW_SHARPNESS,
            &code.stream,
        );

            num_iter += num_skips;
             */
        let lightness = Vec3::from(LIGHT_COLOR) * 1.0; //lightray;

        let mut light: Vec3 = (phong * lightness + (1.0 - phong) * Vec3::from(AMBIENT_LIGHT))
            * Vec3::new(
                primary.result.slot_v0[0],
                primary.result.slot_v0[1],
                primary.result.slot_v0[2],
            );

        light.x = light.x.max(0.0).min(1.0);
        light.y = light.y.max(0.0).min(1.0);
        light.z = light.z.max(0.0).min(1.0);

        light.extend(primary.result.slot_v0[3])
    } else {
        //Did not intersect
        Vec4::ZERO
    }
}

///Calculates the unlit pixel for a 2d sdf
pub fn unlit_2d(
    extent: UVec2,
    coord: UVec2,
    camera: Camera2d,
    sdf: Arc<PrimaryStream2d>,
) -> Rgba<f32> {
    let coord = camera.pixel_to_worldspace(coord, extent);
    let result = eval_2d_from_code(coord, &sdf.stream, 0, sdf.stream.len());

    if result.result < 0.0 {
        Rgba([
            result.slot_v0[0],
            result.slot_v0[1],
            result.slot_v0[2],
            result.slot_v0[3],
        ])
    } else {
        Rgba([0.0; 4])
    }
}

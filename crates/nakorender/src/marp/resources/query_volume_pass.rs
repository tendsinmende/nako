use std::sync::Arc;

use marp::{
    ash::vk::{
        DescriptorPoolSize, DescriptorType, ImageLayout, PipelineBindPoint, ShaderStageFlags,
    },
    buffer::Buffer,
    command_buffer::CommandBuffer,
    descriptor::{DescResource, DescriptorPool, DescriptorSet, PushConstant, StdDescriptorPool},
    device::Device,
    image::Image,
    pipeline::{ComputePipeline, PipelineLayout},
    shader::Stage,
};
use nako_shared::push_constants::QueryVolumePushConst;

use super::shader_compiler::ShaderLoader;

///Takes an sdf and a target buffer and executes the
/// shader to write the query_volume.
pub struct QueryBufferPass {
    pipeline: Arc<ComputePipeline>,
    descriptor_set: Arc<DescriptorSet>,
    #[allow(dead_code)]
    shader_loader: ShaderLoader,
    push_const: PushConstant<QueryVolumePushConst>,
}

impl QueryBufferPass {
    pub fn new(device: Arc<Device>, initial_sdf: Arc<Buffer>, initial_image: Arc<Image>) -> Self {
        let descriptor_pool = StdDescriptorPool::new(
            device.clone(),
            vec![
                DescriptorPoolSize::builder()
                    .ty(DescriptorType::STORAGE_IMAGE)
                    //Out: Volume texture
                    .descriptor_count(1)
                    .build(),
                DescriptorPoolSize::builder()
                    .ty(DescriptorType::STORAGE_BUFFER)
                    //Out: SDF buffer
                    .descriptor_count(1)
                    .build(),
            ]
            .as_slice(),
            1,
        )
        .unwrap();

        let mut descriptor_set = descriptor_pool.next();
        descriptor_set
            .add(DescResource::new_buffer(
                0,
                vec![initial_sdf],
                DescriptorType::STORAGE_BUFFER,
            ))
            .unwrap();

        descriptor_set
            .add(DescResource::new_image(
                1,
                vec![(initial_image, None, ImageLayout::GENERAL)],
                DescriptorType::STORAGE_IMAGE,
            ))
            .unwrap();
        let descriptor_set = descriptor_set.build().unwrap();

        let push_const = PushConstant::new(
            QueryVolumePushConst {
                max: [0.0; 3],
                min: [0.0; 3],
                pad0: [0; 2],
                pad2: 0.0,
                flags: 0,
                resolution: 1,
                voxel_size: 0.0,
            },
            ShaderStageFlags::COMPUTE,
        );

        let pipe_layout = PipelineLayout::new(
            device.clone(),
            vec![*descriptor_set.layout()],
            vec![*push_const.range()],
        )
        .unwrap();

        let mut shader_loader = ShaderLoader::new("shader_query_volume").unwrap();
        let shader = shader_loader
            .build_shader_stage(device.clone(), Stage::Compute, "main")
            .unwrap();

        let pipeline = ComputePipeline::new(device.clone(), shader, pipe_layout).unwrap();

        QueryBufferPass {
            pipeline,
            descriptor_set,
            push_const,
            shader_loader,
        }
    }

    pub(crate) fn record(
        &mut self,
        command_buffer: Arc<CommandBuffer>,
        sdf_buffer: Arc<Buffer>,
        query_volume: Arc<Image>,
        push_const: QueryVolumePushConst,
    ) -> Arc<CommandBuffer> {
        let disptach_size = voxel_resolution_to_dispatch(push_const.resolution);
        //Update push const
        *self.push_const.get_content_mut() = push_const;

        //Update descriptor set
        self.descriptor_set
            .update(DescResource::new_buffer(
                0,
                vec![sdf_buffer],
                DescriptorType::STORAGE_BUFFER,
            ))
            .unwrap();

        self.descriptor_set
            .update(DescResource::new_image(
                1,
                vec![(query_volume, None, ImageLayout::GENERAL)],
                DescriptorType::STORAGE_IMAGE,
            ))
            .unwrap();

        command_buffer
            .cmd_bind_descriptor_sets(
                PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![self.descriptor_set.clone()],
                vec![],
            )
            .unwrap();

        command_buffer
            .cmd_bind_pipeline(PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .unwrap();

        command_buffer
            .cmd_push_constants(self.pipeline.layout(), &self.push_const)
            .unwrap();

        command_buffer.cmd_dispatch(disptach_size).unwrap();

        command_buffer
    }
}

fn voxel_resolution_to_dispatch(resolution: u32) -> [u32; 3] {
    //We know that the shader uses a 4x4x4 kernel, therefore we divide by 4 ceil that and return
    let count = (resolution as f32 / 4.0).ceil() as u32;
    [count; 3]
}

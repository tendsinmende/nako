/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::sync::Arc;

#[cfg(feature = "watch_shaders")]
use std::sync::RwLock;

#[cfg(feature = "watch_shaders")]
use hotwatch::Hotwatch;

#[cfg(feature = "watch_shaders")]
use crate::{render_info, render_warn};

use crate::render_error;
use marp::{
    device::Device,
    shader::{AbstractShaderModule, ShaderModule, ShaderStage, ShaderStageError, Stage},
};
use std::fs::File;

#[allow(unused_imports)]
use nako_shared::access::get_shader_spirv_location;

///creates a constant buffer `shader_name` that includes the shader bytecode from a shader called `shader_file_name`.
///for instance
///`include_shader!(MY_COMPUTE_SHADER, "compute_shader.spv");`
#[macro_export]
macro_rules! include_shader {
    ($shader_file_name:expr) => {{
        let bytes = include_bytes!(concat!(
            "../../../../../resources/spirv/",
            $shader_file_name
        ));
        marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..]))
            .expect("Failed to read shader file")
    }};
}

#[macro_export]
macro_rules! include_rust_shader {
    ($shader_file_name:expr) => {{
        let bytes = include_bytes!(concat!(
            "../../../../../resources/spirv/",
            $shader_file_name
        ));
        marp::ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..]))
            .expect("Failed to read shader file")
    }};
}

///Responsible for loading a single type of shader. If the `watch_shaders` feature it listens for new shader versions with the given name.
pub struct ShaderLoader {
    buffer: Vec<u32>,
    #[cfg(feature = "watch_shaders")]
    ///If the file watcher registers a new version of spirv this is "some".
    send_back: Arc<RwLock<Option<Vec<u32>>>>,

    #[cfg(feature = "watch_shaders")]
    #[allow(dead_code)]
    watcher: hotwatch::Hotwatch,
}

impl ShaderLoader {
    #[allow(dead_code)]
    pub fn new_from_glsl(name: &str) -> Result<Self, std::io::Error> {
        let mut path = get_shader_spirv_location();
        path.push(format!("{}.spv", name));
        path = path.canonicalize()?;
        if !path.exists() {
            render_error!(
                "Failed to load shader {}, does not exist at {:?}",
                name,
                path
            );
            return Err(std::io::Error::new(
                std::io::ErrorKind::NotFound,
                "SpirV file does not exist",
            ));
        }

        let shader_module = marp::ash::util::read_spv(&mut File::open(path)?)?;

        #[cfg(feature = "watch_shaders")]
        render_warn!("Hotwatching glsl spirv is not implemented yet");

        Ok(ShaderLoader {
            buffer: shader_module,

            #[cfg(feature = "watch_shaders")]
            send_back: Arc::new(RwLock::new(None)),

            #[cfg(feature = "watch_shaders")]
            watcher: Hotwatch::new().expect("Failed to register empty hotwatch"),
        })
    }

    #[allow(dead_code)]
    pub fn new(name: &str) -> Result<Self, std::io::Error> {
        //trying to load shader at location, if not possible print error and return
        let mut spirv_path = get_shader_spirv_location();
        spirv_path.push(format!("{}.spv", name));

        if !spirv_path.exists() {
            render_error!(
                "Failed to load shader {}, does not exist at {:?}",
                name,
                spirv_path
            );
            return Err(std::io::Error::new(
                std::io::ErrorKind::NotFound,
                "SpirV file does not exist",
            ));
        }
        //Seems to exist. Therefore load code into buffer and return
        let shader_module = marp::ash::util::read_spv(&mut File::open(&spirv_path)?)?;

        #[cfg(feature = "watch_shaders")]
        let (send_back, watcher) = {
            let send_back = Arc::new(RwLock::new(None));

            let mut watch = Hotwatch::new().expect("Failed to register shader watch!");

            let path = spirv_path.clone();
            let sb = send_back.clone();
            watch
                .watch(&spirv_path, move |_event| {
                    render_info!("Reload shader @ {:?}", path);
                    let mut file = match File::open(&path) {
                        Ok(f) => f,
                        Err(e) => {
                            render_error!("Failed to load new spirv file from disk: {}", e);
                            return;
                        }
                    };

                    let new_module = match marp::ash::util::read_spv(&mut file) {
                        Ok(spv) => spv,
                        Err(e) => {
                            render_error!(
                                "Failed to load new spirv module as shader module: {}",
                                e
                            );
                            return;
                        }
                    };

                    //Go the new module at runtime, set sendback notification
                    *sb.write().unwrap() = Some(new_module);
                })
                .expect("Could not register spirv path in watcher!");

            (send_back, watch)
        };

        Ok(ShaderLoader {
            buffer: shader_module,
            #[cfg(feature = "watch_shaders")]
            send_back,

            #[cfg(feature = "watch_shaders")]
            watcher,
        })
    }

    pub fn build_shader_stage(
        &mut self,
        device: Arc<Device>,
        stage: Stage,
        entry_name: &str,
    ) -> Result<Arc<ShaderStage>, ShaderStageError> {
        #[cfg(feature = "watch_shaders")]
        {
            if !self.is_up_to_date() {
                if let Some(new_code) = self.send_back.write().unwrap().take() {
                    self.buffer = new_code;
                }
            }
        }

        Ok(ShaderModule::new_from_code(device, self.buffer.clone())?.to_stage(stage, entry_name))
    }

    #[cfg(feature = "watch_shaders")]
    pub fn is_up_to_date(&self) -> bool {
        self.send_back.read().unwrap().is_none()
    }
}

#[cfg(feature = "watch_shaders")]
pub mod shader_watching {

    use std::{
        sync::{Arc, Mutex},
        time::Duration,
    };

    use hotwatch::Hotwatch;
    use nako_shared::access::get_shader_crate_location;

    use crate::{render_error, render_info, render_warn};

    ///The shader watcher implements asynchronus watching of all shader files and recompilation.
    ///Whenever a spirv file changes the inner representation (that can be queried via `load(spirv_name)`) gets updated.
    pub struct ShaderWatcher {
        #[allow(dead_code)]
        worker: Hotwatch,
    }

    impl ShaderWatcher {
        ///Creates a new watcher that re-compiles any shader that changes
        pub fn new() -> Result<Self, ()> {
            //get all shaders in the shader directory and register them. Then preload spirv if possible, if not we block until the shader
            //is compiled the first time.

            let shader_crate_loactions = match get_shader_crate_location() {
                Ok(l) => l,
                Err(e) => {
                    render_error!("Shader at {:?} do not exist", e);
                    return Err(());
                }
            };

            //NOTE we are using two secs of notifying delay to not compile the shaders on every keystroke
            let mut worker =
                Hotwatch::new_with_custom_delay(Duration::from_secs(5)).map_err(|e| {
                    render_error!("Failed to create hotwatch: {}", e);
                    ()
                })?;

            if let Ok(iter) = shader_crate_loactions.read_dir() {
                for entry in iter {
                    if let Ok(entry) = entry {
                        match entry.file_type().map(|ty| ty.is_dir()) {
                            Ok(true) => {
                                //Create the pipe for this path and register in worker
                                let mut shader_path = shader_crate_loactions.clone();
                                shader_path.push(entry.file_name());

                                let shader_name = entry.file_name();
                                let is_compiling = Arc::new(Mutex::new(false));

                                render_info!("Watching shader {:?}", shader_path);
                                if let Err(e) = worker.watch(shader_path, move |_event| {
                                    //check if it is already recompiling
                                    {
                                        let mut g = is_compiling.lock().unwrap();
                                        if !*g {
                                            *g = true;
                                        } else {
                                            render_warn!("Already comiling!");
                                            return;
                                        }
                                    }

                                    render_info!(
                                        "Shader {:?} changed, recompiling now...",
                                        shader_name
                                    );

                                    //Try to execute shader compiler

                                    let res = std::process::Command::new("cargo")
                                        .arg("run")
                                        .arg("--bin")
                                        .arg("nako_shader_builder")
                                        .arg("--")
                                        .arg("--module")
                                        .arg(&shader_name)
                                        .output();

                                    //now wait for the compiler to return. After compiling the spirv
                                    //files are moved to the correct location, which should trigger
                                    //any watching shader module.
                                    match res {
                                        Ok(r) => {
                                            //Check the output for an error, otherwise end silently
                                            match std::str::from_utf8(&r.stdout) {
                                                Ok(s) => {
                                                    if s.contains("error") {
                                                        render_error!(
                                                            "Recompilation failed with:\n{}",
                                                            s
                                                        );
                                                    } else {
                                                        render_info!(
                                                            "Recompiled with exit:\n{}",
                                                            s
                                                        );
                                                    }
                                                }
                                                Err(e) => {
                                                    render_info!(
                                                        "Shader compiler exited with: {:?}",
                                                        e
                                                    );
                                                }
                                            };
                                        }
                                        Err(e) => {
                                            render_error!("Shader compiler failed with {}", e);
                                        }
                                    }

                                    //Reset compiling flag
                                    *is_compiling.lock().unwrap() = false;
                                }) {
                                    render_error!(
                                        "Failed to initiate watcher for shader {:?}: {}",
                                        entry.file_name(),
                                        e
                                    );
                                };
                            }
                            _ => {}
                        }
                    }
                }
            }

            Ok(ShaderWatcher { worker })
        }
    }
}

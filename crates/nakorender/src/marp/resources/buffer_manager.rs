/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use std::{mem::size_of, sync::Arc};

use marp::{
    ash::vk::{
        AccessFlags, CommandPoolCreateFlags, ComponentMapping, DependencyFlags, Format,
        ImageLayout, ImageTiling, PipelineStageFlags,
    },
    buffer::{Buffer, BufferUsage, SharingMode, UploadStrategy},
    command_buffer::{CommandBuffer, CommandBufferPool, CommandPool},
    device::{Device, Queue, SubmitInfo},
    image::{AbstractImage, Image, ImageInfo, ImageType, ImageUsage, MipLevel},
    memory::MemoryUsage,
    sync::QueueFence,
};
use nako::{
    glam::Vec3,
    sdf::BBox,
    serialize::Word,
    stream::{PrimaryStream, PrimaryStream2d},
};
use nako_shared::push_constants::QueryVolumePushConst;

use crate::{marp::Queues, render_error, render_warn, render_info};

use super::query_volume_pass::QueryBufferPass;

///Buffers needed for 2d layer rendering
#[derive(Clone)]
pub struct BufferStorage2d {
    pub code_buffer: Arc<Buffer>,
}

///Buffers needed for 3d layer rendering
#[derive(Clone)]
pub struct BufferStorage3d {
    pub code_buffer: Arc<Buffer>,
    pub query_volume_buffer: Arc<Image>,
    pub query_volume_descriptor: QueryVolumePushConst,
}

pub trait BufferManager {
    ///Type of storage that is maintained.
    type StorageType;
    ///Type of stream that is consumed when updating the internal storage.
    type StreamType;
    fn update_code(&mut self, new_code: Self::StreamType);
    fn get_storage(&mut self) -> &Self::StorageType;
    ///True whenever a newer buffer could be ready.
    fn is_outdated(&self) -> bool;
}

///The buffer manager manages all non-image related buffers. Currently this is the `code` buffer that stores the SDF instructions, as well as a `query_volume` buffer
/// that serves as an acceleration structure when using segment tracing.
///
/// `B` is the type of resulting buffers, `S` is the stream type handled by this manager. Both are usually decided based on the layer type (3d or 2d).
pub struct BufferManager2d {
    current_buffers: BufferStorage2d,

    //Upload queue, currently same as everywhere, but maybe that will be a transfer queue at some point?
    #[allow(dead_code)]
    queue: Arc<Queue>,
    #[allow(dead_code)]
    device: Arc<Device>,
}

///2D implementation of the buffer manager.
impl BufferManager2d {
    pub fn new(device: Arc<Device>, queues: &Queues) -> Self {
        let queue = queues.graphics_queue.clone();
        //Upload a dummy buffer and memory for now which will get updated at some point
        let code = Buffer::new(
            device.clone(),
            1 as u64,
            BufferUsage {
                storage_buffer: true,
                transfer_dst: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly,
        )
        .expect("Failed to create inital code buffer");
        let code_upload = code.copy(queue.clone(), vec![u8::MAX]).unwrap();
        code_upload.wait(u64::MAX).unwrap();

        let initial_storage = BufferStorage2d { code_buffer: code };

        BufferManager2d {
            current_buffers: initial_storage,

            queue,
            device,
        }
    }

    fn immediate_upload_stream(&mut self, mut stream: Vec<Word>) {
        //if the stream is empty, push an invalid instruction. Should be ignored by the gpu.
        if stream.len() == 0 {
            stream = vec![u32::MAX];
        }
        //Upload inlined priamry stream which should already be in perfect layout
        let new_buffer = Buffer::new(
            self.device.clone(),
            (stream.len() * size_of::<Word>()) as u64,
            BufferUsage {
                storage_buffer: true,
                transfer_dst: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly,
        )
        .expect("Failed to create inital mem buffer");
        //Upload actual data
        new_buffer
            .copy(self.queue.clone(), stream)
            .expect("Failed to upload new code")
            .wait(u64::MAX)
            .unwrap();

        self.current_buffers = BufferStorage2d {
            code_buffer: new_buffer,
        }
    }
}

impl BufferManager for BufferManager2d {
    type StorageType = BufferStorage2d;
    type StreamType = PrimaryStream2d;

    ///Updates the code by directly uploading it to the gpu
    fn update_code(&mut self, new_code: Self::StreamType) {
        //NOTE 2D buffers are currently uploaded directly, since it is not much more than a copy. However,
        //later a more sophisticated way might be implemented. Similar to how 3d buffer upload works.
        self.immediate_upload_stream(new_code.stream);
    }

    fn get_storage(&mut self) -> &Self::StorageType {
        &mut self.current_buffers
    }

    fn is_outdated(&self) -> bool {
        //always false since we are uploading in sync
        false
    }
}

struct InFlight {
    ///Storage that is being updated
    new_storage: BufferStorage3d,
    ///Fence signaling when the gpu finished writing data
    fence: QueueFence,
}

struct CodeUpdate {
    stream: PrimaryStream,
    push_const: QueryVolumePushConst,
    new_buffer: Arc<Buffer>,
    new_query_volume: Arc<Image>,
}

///The buffer manager manages all non-image related buffers. Currently this is the `code` buffer that stores the SDF instructions, as well as a `query_volume` buffer
/// that serves as an acceleration structure when using segment tracing.
///
/// `B` is the type of resulting buffers, `S` is the stream type handled by this manager. Both are usually decided based on the layer type (3d or 2d).
pub struct BufferManager3d {
    ///Some whenever code is updated. Is taken whenever a new update is scheduled for the gpu.
    new_code: Option<CodeUpdate>,

    ///Some while the gpu is uploading new buffers lazily.
    in_flight: Option<InFlight>,

    ///The command buffer that is being executed
    command_buffer: Arc<CommandBuffer>,

    current_buffers: BufferStorage3d,

    buffer_pass: QueryBufferPass,

    queue: Arc<Queue>,
    device: Arc<Device>,
}

impl BufferManager3d {
    pub const MAX_RESOLUTION: u32 = 512;
    pub const VOXELS_PER_UNIT: f32 = 16.0;
    pub const QUERY_BUFFER_FORMAT: Format = Format::R16G16B16A16_SFLOAT;

    fn query_volume_image_info(resolution: u32) -> ImageInfo {
        ImageInfo {
            component_mapping: ComponentMapping::default(),
            format: Self::QUERY_BUFFER_FORMAT,
            image_type: ImageType::Image3D {
                width: resolution,
                height: resolution,
                depth: resolution,
            },
            memory_usage: MemoryUsage::GpuOnly,
            mip_mapping: MipLevel::Specific(1),
            tiling: ImageTiling::LINEAR,
            usage: ImageUsage {
                storage: true,
                color_aspect: true,
                ..Default::default()
            },
        }
    }

    pub fn new(device: Arc<Device>, queues: &Queues) -> Self {
        let queue = queues.graphics_queue.clone();
        //Upload a dummy buffer and memory for now which will get updated at some point
        let code = Buffer::new(
            device.clone(),
            1 as u64,
            BufferUsage {
                storage_buffer: true,
                transfer_dst: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly,
        )
        .expect("Failed to create inital code buffer");
        let code_upload = code.copy(queue.clone(), vec![u8::MAX]).unwrap();
        code_upload.wait(u64::MAX).unwrap();

        let initial_storage = BufferStorage3d {
            code_buffer: code.clone(),
            query_volume_buffer: Image::new(
                device.clone(),
                Self::query_volume_image_info(1),
                SharingMode::Exclusive,
            )
            .expect("Failed to create initial volume texture"),
            query_volume_descriptor: QueryVolumePushConst::EMPTY,
        };

        //Setup inflight command buffer
        let cbpool = CommandBufferPool::new(
            device.clone(),
            queue.clone(),
            CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        )
        .unwrap();
        let command_buffer = cbpool
            .alloc(1, false)
            .expect("Failed to allocate upload cb")
            .into_iter()
            .next()
            .unwrap();

        let buffer_pass = QueryBufferPass::new(
            device.clone(),
            code.clone(),
            initial_storage.query_volume_buffer.clone(),
        );

        BufferManager3d {
            in_flight: None,
            command_buffer,
            new_code: None,
            current_buffers: initial_storage,
            buffer_pass,
            queue,
            device,
        }
    }

    ///takes a currently waiting
    fn process_upload(&mut self) {
        #[cfg(Debug)]
        if !self.in_flight.is_empty() {
            render_error!("Tried to upload, but cb is already in flight!");
            return;
        }

        match (self.new_code.take(), self.in_flight.take()) {
            (Some(waiting), Some(in_flight)) => {
                //Happens when subscription is high.
                self.new_code = Some(waiting);
                self.in_flight = Some(in_flight);
            }
            (None, Some(in_flight)) => {
                render_error!("tried to upload, but there was no buffer!");
                self.in_flight = Some(in_flight);
            }
            (None, None) => {} //nothing..
            (Some(waiting), None) => {
                //This is the correct state, start recording the command buffer and setup inflight field afterwards
                self.command_buffer
                    .begin_recording(true, false, false, None)
                    .unwrap();

                //First action is uploading the sdf buffer
                waiting
                    .new_buffer
                    .copy_sync(self.command_buffer.clone(), waiting.stream.stream)
                    .expect("Failed to schedule upload of initial buffer");
                //Now wait for the upload to finished. When done, transition the buffer from TransferWrite to to General for shader read. Transfer the volume from undefined to general as well for write.
                //then execute the query_volume shader to fill the volume with the correct data based on the supplied data.

                self.command_buffer
                    .cmd_pipeline_barrier(
                        PipelineStageFlags::ALL_COMMANDS,
                        PipelineStageFlags::COMPUTE_SHADER,
                        DependencyFlags::empty(),
                        vec![],
                        vec![
                            //Transfer sdf buffer to be readable
                            waiting.new_buffer.buffer_barrier(
                                None,
                                None,
                                Some(AccessFlags::TRANSFER_WRITE),
                                Some(AccessFlags::SHADER_READ),
                                None,
                                None,
                            ),
                        ],
                        vec![
                            //Transfer volume texture to be writeable
                            waiting.new_query_volume.new_image_barrier(
                                Some(ImageLayout::UNDEFINED),
                                Some(ImageLayout::GENERAL),
                                Some(self.queue.clone()),
                                Some(self.queue.clone()),
                                None,
                                Some(AccessFlags::SHADER_WRITE),
                                None,
                            ),
                        ],
                    )
                    .unwrap();

                //now submit volume pass
                self.command_buffer = self.buffer_pass.record(
                    self.command_buffer.clone(),
                    waiting.new_buffer.clone(),
                    waiting.new_query_volume.clone(),
                    waiting.push_const.clone(),
                );

                //Now wait for the compute pass to finish before ending cb
                self.command_buffer
                    .cmd_pipeline_barrier(
                        PipelineStageFlags::COMPUTE_SHADER,
                        PipelineStageFlags::ALL_COMMANDS,
                        DependencyFlags::empty(),
                        vec![],
                        vec![],
                        vec![
                            //Transfer volume texture to be writeable
                            waiting.new_query_volume.new_image_barrier(
                                Some(ImageLayout::GENERAL),
                                Some(ImageLayout::GENERAL),
                                None,
                                None,
                                Some(AccessFlags::SHADER_WRITE),
                                Some(AccessFlags::SHADER_READ),
                                None,
                            ),
                        ],
                    )
                    .unwrap();
                //wait for the shader to finish, then transition volume to read state as well

                self.command_buffer.end_recording().unwrap();

                //Finally submit
                let fence = self
                    .queue
                    .queue_submit(vec![SubmitInfo::new(
                        vec![],
                        vec![self.command_buffer.clone()],
                        vec![],
                    )])
                    .unwrap();

                let storage = BufferStorage3d {
                    code_buffer: waiting.new_buffer,
                    query_volume_buffer: waiting.new_query_volume,
                    query_volume_descriptor: waiting.push_const,
                };

                //Setup inflight info
                self.in_flight = Some(InFlight {
                    fence,
                    new_storage: storage,
                });
            }
        }
    }
}

impl BufferManager for BufferManager3d {
    type StorageType = BufferStorage3d;
    type StreamType = PrimaryStream;

    ///Updates the code by directly uploading it to the gpu
    fn update_code(&mut self, mut new_code: Self::StreamType) {
        //Setup new storages
        if new_code.stream.len() == 0 {
            //Catch empty streams
            new_code.stream = vec![Word::MAX];
            //NOTE this is garbage, but wont be evaluated anyways...
            new_code.infinite_bound = BBox {
                min: Vec3::ZERO,
                max: Vec3::ONE,
            };
        }

        //catch infinite bounding boxes. In that case conserve to the region around the origin.
        //TODO calculate region more intelligently. For instance by removing all infinite bounds from the stream and
        //checking those

        let mut is_sdf_enclosed = true;

        if new_code.finite_bound.is_none() {
            render_warn!(
                "Detected infinite bounding box, falling back to box +/- 20 around the origin"
            );
            new_code.finite_bound = Some(BBox {
                min: Vec3::splat(-20.0),
                max: Vec3::splat(20.0),
            });
            is_sdf_enclosed = false;
        }

        let new_buffer = Buffer::new(
            self.device.clone(),
            (new_code.stream.len() * size_of::<Word>()) as u64,
            BufferUsage {
                storage_buffer: true,
                transfer_dst: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::GpuOnly,
        )
        .expect("Failed to create code buffer");

        //Checking finite bounds, note that there are always finite bounds at this point since they are set above.
        let max_extent = new_code.finite_bound.unwrap().extent().max_element();
        let num_voxel = max_extent * Self::VOXELS_PER_UNIT;
        let mut resolution = 2u32.pow(num_voxel.log2().ceil() as u32);

        //check that the volume does not become too big.
        resolution = resolution.min(Self::MAX_RESOLUTION);
        //println!("Calculated resolution of {}, for bbox {}, {}", resolution, new_code.finite_bound.unwrap().min, new_code.finite_bound.unwrap().max);
        render_info!("VoxelBufffer resolution of: {}", resolution);
        //Now create the volumetric image for this buffer
        let query_volume_buffer = Image::new(
            self.device.clone(),
            Self::query_volume_image_info(resolution),
            SharingMode::Exclusive,
        )
        .expect("Failed to create volume texture");

        //setup flags based on gathered properties
        let mut flags = 0;
        if is_sdf_enclosed {
            flags |= QueryVolumePushConst::FLAG_WITHIN_BOUNDS;
        }

        //Setup new push constants
        let mut push_const = QueryVolumePushConst {
            min: new_code.finite_bound.unwrap().min.into(),
            max: new_code.finite_bound.unwrap().max.into(),
            resolution,
            voxel_size: 0.0,
            flags,
            pad0: [0; 2],
            pad2: 0.0,
        };
        //now calc actual voxel size
        push_const.set_voxel_size();

        //Set the "new" code.
        self.new_code = Some(CodeUpdate {
            stream: new_code,
            new_buffer,
            new_query_volume: query_volume_buffer,
            push_const,
        });

        //Now check if we can start uploading immediately.
        //If not another buffer is already in flight.
        if self.in_flight.is_none() {
            self.process_upload()
        }
    }

    ///Returns a reference to the current up to date buffers.
    ///might trigger upload of more recent buffers.
    fn get_storage(&mut self) -> &Self::StorageType {
        //check if the upload finished, if that is the case, update the "current buffer"
        if let Some(in_flight) = self.in_flight.take() {
            //something is processing, check if it finished yet.
            if let Ok(true) = in_flight.fence.get_status() {
                //has finished, update buffer and reset command buffer
                self.current_buffers = in_flight.new_storage;
                //Now reset cb for next upload
                self.command_buffer.reset().unwrap();
            } else {
                //has not yet finished. put fence back into place
                self.in_flight = Some(in_flight);
            }
        }

        //Now check if we have no fence in flight, but a buffer waiting, in that case, trigger upload
        if self.new_code.is_some() {
            self.process_upload();
        }

        &mut self.current_buffers
    }

    fn is_outdated(&self) -> bool {
        self.in_flight.is_some() || self.new_code.is_some()
    }
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
///Hosts sub passes and records everything to the command buffer.
mod buffer_manager;
pub use buffer_manager::{
    BufferManager, BufferManager2d, BufferManager3d, BufferStorage2d, BufferStorage3d,
};

///Currently compiling shaders on each startup. This is the compiler. Probably moving into a build script at some point.
pub(crate) mod shader_compiler;

pub(crate) mod query_volume_pass;

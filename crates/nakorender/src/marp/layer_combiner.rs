/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use std::sync::Arc;

use marp::{
    ash::vk::{self, PipelineStageFlags},
    command_buffer::{CommandBuffer, CommandBufferPool, CommandPool},
    device::{Device, SubmitInfo},
    image::{AbstractImage, SwapchainImage},
    sync::{QueueFence, Semaphore},
};
use nako::glam::UVec2;
use nako_shared::push_constants::LayerCombinePushConst;

use crate::marp::layer_renderer::MarpTask;

use super::{
    passes::{layer_combine::LayerCombine, pass_image_to_swapchain::ImgToSwapchain},
    Queues,
};

struct FrameInfo {
    command_buffer: Arc<CommandBuffer>,

    copy_finished: Arc<Semaphore>,
    in_flight: Option<QueueFence>,

    swapchain_image: Arc<SwapchainImage>,
}

///Takes a set of layer render informations and combines them in their order
pub struct LayerCombiner {
    frames: Vec<FrameInfo>,
    //Subpass that is able to combine any count of layers
    combiner: LayerCombine,
    window_ext: UVec2,
}

impl LayerCombiner {
    pub fn new(
        swapchain_images: Vec<Arc<SwapchainImage>>,
        device: Arc<Device>,
        queues: &Queues,
    ) -> Self {
        let num_scimgs = swapchain_images.len();
        let extent = swapchain_images[0].extent();
        let command_pool = CommandBufferPool::new(
            device.clone(),
            queues.graphics_queue.clone(),
            vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        )
        .expect("Failed to create command pool");

        let command_buffers = command_pool
            .alloc(num_scimgs, false)
            .expect("Failed to allocate command buffers");

        let frame_infos: Vec<_> = command_buffers
            .into_iter()
            .enumerate()
            .map(|(idx, cb)| FrameInfo {
                command_buffer: cb,
                copy_finished: Semaphore::new(device.clone()).unwrap(),
                in_flight: None,
                swapchain_image: swapchain_images[idx].clone(),
            })
            .collect();

        Self {
            frames: frame_infos,
            combiner: LayerCombine::new(device.clone(), extent, num_scimgs),
            window_ext: UVec2::new(
                swapchain_images[0].extent().width,
                swapchain_images[0].extent().height,
            ),
        }
    }

    pub fn get_copy_complete_semaphore(&self, slot: usize) -> Arc<Semaphore> {
        self.frames[slot].copy_finished.clone()
    }

    pub fn combine(
        &mut self,
        device: Arc<Device>,
        queues: &Queues,
        layers: Vec<MarpTask>,
        slot: usize,
        present_semaphore: Arc<Semaphore>,
    ) {
        //Filter layers 1 for layer infos for the shader, as well as the ordered set images.
        let combine_infos = layers
            .iter()
            .filter_map(|layer| match layer {
                //Unwrap layers, note that we cull layers that are outside of the current window for performance reasons
                MarpTask::Ready {
                    image,
                    extent,
                    location,
                }
                | MarpTask::Rendering {
                    image,
                    extent,
                    location,
                    sem: _,
                    stage: _,
                } => {
                    let max = *location + extent.as_ivec2();
                    if max.x < 0
                        || max.y < 0
                        || location.x > self.window_ext.x as i32
                        || location.y > self.window_ext.y as i32
                    {
                        None
                    } else {
                        let push_info = LayerCombinePushConst {
                            origin: [location.x, location.y],
                            extent: [extent.x, extent.y],
                            pad0: [0, 0],
                            pad1: [0, 0],
                        };
                        Some((image.clone(), push_info))
                    }
                }
            })
            .collect::<Vec<_>>();

        //Collect all waiting information that needs to be submitted together with this command buffer submission
        let mut wait_info: Vec<_> = layers
            .iter()
            .filter_map(|i| match i {
                MarpTask::Ready { .. } => None,
                MarpTask::Rendering { sem, stage, .. } => Some((sem.clone(), *stage)),
            })
            .collect();

        //Push the own present compleate semaphore as well, needed for the "to swapchain" copy
        wait_info.push((
            present_semaphore,
            PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
        ));

        //Wait for last copy
        if let Some(inflight) = self.frames[slot].in_flight.take() {
            inflight.wait(u64::MAX).unwrap();
        }

        //Begin copy command buffer
        //Reset command buffer
        let mut command_buffer = self.frames[slot].command_buffer.clone();
        command_buffer.reset().unwrap();
        command_buffer
            .begin_recording(true, false, false, None)
            .unwrap();

        //Schedule copy operations on cb
        command_buffer =
            self.combiner
                .record(slot, device.clone(), queues, command_buffer, combine_infos);

        //Note: The swapchain copy pass waits for the compute shader to finish

        let final_image = self.combiner.get_final_image(slot);
        //Now blit the final image to the swapchain
        command_buffer = ImgToSwapchain::record(
            command_buffer,
            final_image,
            self.frames[slot].swapchain_image.clone(),
        );

        //Finish command buffer and submit
        command_buffer.end_recording().unwrap();

        let new_inflight = queues
            .graphics_queue
            .queue_submit(vec![SubmitInfo::new(
                wait_info, //wait for nothing since we just waited for the former fence
                vec![command_buffer],
                vec![self.frames[slot].copy_finished.clone()], //Signal execution semaphore
            )])
            .unwrap();

        self.frames[slot].in_flight = Some(new_inflight);
    }
}

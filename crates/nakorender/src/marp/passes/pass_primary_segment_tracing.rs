/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use std::sync::Arc;

use marp::{
    ash::vk::{
        AccessFlags, DependencyFlags, DescriptorPoolSize, DescriptorType, Extent2D, Format,
        ImageLayout, PipelineBindPoint, PipelineStageFlags, ShaderStageFlags,
    },
    buffer::SharingMode,
    command_buffer::CommandBuffer,
    descriptor::{DescResource, DescriptorPool, DescriptorSet, PushConstant, StdDescriptorPool},
    device::Device,
    image::{AbstractImage, Image, ImageInfo, ImageType, ImageUsage, MipLevel},
    memory::MemoryUsage,
    pipeline::{ComputePipeline, PipelineLayout},
    shader::Stage,
};
use nako_shared::push_constants::{Primary3dSegTraConst, QueryVolumePushConst};

use super::pass::Pass;
use crate::{
    camera::Camera,
    marp::{
        passes::dispatch_size,
        resources::{shader_compiler::ShaderLoader, BufferStorage3d},
        Queues,
    },
    render_error,
};

use nako_shared::GpuCamera;

pub(crate) struct PSTPassData {
    pub(crate) albedo: Arc<Image>,
    pub(crate) nrm_depth: Arc<Image>,
    descriptor_set: Arc<DescriptorSet>,
    is_transitioned: bool,
}

pub(crate) struct PrimaryPassSegmentTracing {
    pub(crate) data: Vec<PSTPassData>,
    pipeline: Arc<ComputePipeline>,
    #[allow(dead_code)]
    shader_loader: ShaderLoader,
    push_descriptor: PushConstant<Primary3dSegTraConst>,
}

impl PrimaryPassSegmentTracing {
    #[allow(dead_code)]
    pub fn new(
        buffers: &BufferStorage3d,
        device: Arc<Device>,
        extent: Extent2D,
        num_slots: usize,
    ) -> Self {
        let descriptor_pool = StdDescriptorPool::new(
            device.clone(),
            vec![
                DescriptorPoolSize::builder()
                    .ty(DescriptorType::STORAGE_IMAGE)
                    .descriptor_count(num_slots as u32 * 3) //albedo and nrm/depth image, and volume texture
                    .build(),
                DescriptorPoolSize::builder()
                    .ty(DescriptorType::STORAGE_BUFFER)
                    .descriptor_count(num_slots as u32 * 1) //for code
                    .build(),
            ]
            .as_slice(),
            num_slots as u32,
        )
        .unwrap();

        let mut data = Vec::with_capacity(num_slots);
        for _i in 0..num_slots {
            let albedo = Image::new(
                device.clone(),
                ImageInfo::new(
                    ImageType::Image2D {
                        width: extent.width,
                        height: extent.height,
                        samples: 1,
                    },
                    Format::R8G8B8A8_UNORM,
                    None,
                    Some(MipLevel::Specific(1)),
                    ImageUsage {
                        transfer_src: true,
                        storage: true,
                        color_aspect: true,
                        ..Default::default()
                    },
                    MemoryUsage::GpuOnly,
                    None,
                ),
                SharingMode::Exclusive,
            )
            .unwrap();

            let nrm_depth = Image::new(
                device.clone(),
                ImageInfo::new(
                    ImageType::Image2D {
                        width: extent.width,
                        height: extent.height,
                        samples: 1,
                    },
                    Format::R32G32B32A32_SFLOAT,
                    None,
                    Some(MipLevel::Specific(1)),
                    ImageUsage {
                        transfer_src: true,
                        storage: true,
                        color_aspect: true,
                        ..Default::default()
                    },
                    MemoryUsage::GpuOnly,
                    None,
                ),
                SharingMode::Exclusive,
            )
            .unwrap();

            //Allocate the descriptor set
            let mut descriptor_set = descriptor_pool.next();

            descriptor_set
                .add(DescResource::new_buffer(
                    0,
                    vec![buffers.code_buffer.clone()],
                    DescriptorType::STORAGE_BUFFER,
                ))
                .unwrap();

            descriptor_set
                .add(DescResource::new_image(
                    1,
                    vec![(
                        buffers.query_volume_buffer.clone(),
                        None,
                        ImageLayout::GENERAL,
                    )],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            descriptor_set
                .add(DescResource::new_image(
                    2,
                    vec![(albedo.clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            descriptor_set
                .add(DescResource::new_image(
                    3,
                    vec![(nrm_depth.clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            let descriptor_set = descriptor_set.build().unwrap();

            data.push(PSTPassData {
                albedo,
                nrm_depth,
                descriptor_set,
                is_transitioned: false,
            });
        }

        //Create initial push constant
        let push_descriptor = PushConstant::new(
            Primary3dSegTraConst {
                camera: Camera::default().into_gpu_cam(extent.width, extent.height),
                volume: QueryVolumePushConst::EMPTY,
            },
            ShaderStageFlags::COMPUTE,
        );

        //Setup the compute pipeline.
        let pipe_layout = PipelineLayout::new(
            device.clone(),
            vec![*data[0].descriptor_set.layout()],
            vec![*push_descriptor.range()],
        )
        .unwrap();

        let mut shader_loader = ShaderLoader::new("shader_primary_3d_segment_tracing").unwrap();

        let shader = shader_loader
            .build_shader_stage(device.clone(), Stage::Compute, "main")
            .unwrap();

        let pipeline = ComputePipeline::new(device.clone(), shader, pipe_layout).unwrap();

        PrimaryPassSegmentTracing {
            data,
            pipeline,
            shader_loader,
            push_descriptor,
        }
    }
    
    #[allow(dead_code)]
    pub fn update_camera(&mut self, mut new_camera: GpuCamera) {
        //update image widht/height
        new_camera.img_width = self.data[0].albedo.extent().width as u32;
        new_camera.img_height = self.data[0].albedo.extent().height as u32;
        self.push_descriptor.get_content_mut().camera = new_camera;
    }

    #[allow(dead_code)]
    pub fn update_buffer(&mut self, idx: usize, buffer: BufferStorage3d) {
        //Update all buffers
        //TODO only update those that are actually changed. For instance the stack wont change that often
        self.data[idx]
            .descriptor_set
            .update(DescResource::new_buffer(
                0,
                vec![buffer.code_buffer],
                DescriptorType::STORAGE_BUFFER,
            ))
            .unwrap();

        self.data[idx]
            .descriptor_set
            .update(DescResource::new_image(
                1,
                vec![(
                    buffer.query_volume_buffer.clone(),
                    None,
                    ImageLayout::GENERAL,
                )],
                DescriptorType::STORAGE_IMAGE,
            ))
            .unwrap();
        //Update descriptor to the volume as well
        self.push_descriptor.get_content_mut().volume = buffer.query_volume_descriptor;
    }

    #[allow(dead_code)]
    fn rebuild_pipeline(&mut self, device: Arc<Device>) {
        let new_stage =
            match self
                .shader_loader
                .build_shader_stage(device.clone(), Stage::Compute, "main")
            {
                Ok(stage) => stage,
                Err(e) => {
                    render_error!(
                        "Could not build new shader stage for primary from shader: {:?}",
                        e
                    );
                    return;
                }
            };
        let pipe_layout = PipelineLayout::new(
            device.clone(),
            vec![*self.data[0].descriptor_set.layout()],
            vec![*self.push_descriptor.range()],
        )
        .unwrap();

        let new_pipeline = match ComputePipeline::new(device.clone(), new_stage, pipe_layout) {
            Ok(p) => p,
            Err(e) => {
                render_error!("Could build new pipeline for primary_shader: {:?}", e);
                return;
            }
        };

        self.pipeline = new_pipeline;
    }
}

impl Pass for PrimaryPassSegmentTracing {
    fn pre(
        &mut self,
        queues: &Queues,
        command_buffer: Arc<CommandBuffer>,
        slot_index: usize,
    ) -> Arc<CommandBuffer> {
        #[cfg(feature = "watch_shaders")]
        if !self.shader_loader.is_up_to_date() {
            self.rebuild_pipeline(queues.graphics_queue.get_device());
        }

        //Transition every resource that isn't yet in the correct layout
        if self.data[slot_index].is_transitioned {
            return command_buffer;
        }

        //We want our images to be in the general layout, so we can write to them in the primary pass
        command_buffer
            .cmd_pipeline_barrier(
                PipelineStageFlags::ALL_COMMANDS,
                PipelineStageFlags::COMPUTE_SHADER,
                DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    self.data[slot_index].albedo.new_image_barrier(
                        Some(ImageLayout::UNDEFINED),
                        Some(ImageLayout::GENERAL),
                        Some(queues.graphics_queue.clone()), //Keep being on graphics queue for now.
                        Some(queues.graphics_queue.clone()),
                        None,
                        Some(AccessFlags::SHADER_WRITE),
                        None,
                    ),
                    self.data[slot_index].nrm_depth.new_image_barrier(
                        Some(ImageLayout::UNDEFINED),
                        Some(ImageLayout::GENERAL),
                        Some(queues.graphics_queue.clone()), //Keep being on graphics queue for now.
                        Some(queues.graphics_queue.clone()),
                        None,
                        Some(AccessFlags::SHADER_WRITE),
                        None,
                    ),
                ],
            )
            .expect("Failed to transition image in primary pass");
        //Mark this slot as transitioned
        self.data[slot_index].is_transitioned = true;
        command_buffer
    }
    fn record(
        &mut self,
        command_buffer: Arc<CommandBuffer>,
        slot_index: usize,
    ) -> Arc<CommandBuffer> {
        //Bind descriptorset
        command_buffer
            .cmd_bind_descriptor_sets(
                PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![self.data[slot_index].descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind Primary pass Descriptorset");

        command_buffer
            .cmd_bind_pipeline(PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .expect("Failed to bind primary pipeline");
        //upload current camera
        command_buffer
            .cmd_push_constants(self.pipeline.layout(), &self.push_descriptor)
            .unwrap();
        command_buffer
            .cmd_dispatch(dispatch_size(&self.data[slot_index].albedo))
            .expect("Failed to schedule primary pass");

        command_buffer
    }
}

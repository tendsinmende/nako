/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use std::sync::Arc;

use marp::{
    ash::vk::{
        AccessFlags, DependencyFlags, DescriptorPoolSize, DescriptorType, Extent2D, Format,
        ImageLayout, PipelineBindPoint, PipelineStageFlags, ShaderStageFlags,
    },
    buffer::SharingMode,
    command_buffer::CommandBuffer,
    descriptor::{DescResource, DescriptorPool, DescriptorSet, PushConstant, StdDescriptorPool},
    device::Device,
    image::{AbstractImage, Image, ImageInfo, ImageType, ImageUsage, MipLevel},
    memory::MemoryUsage,
    pipeline::{ComputePipeline, PipelineLayout},
    shader::Stage,
};
use nako_shared::push_constants::{
    ResolveConst, SAMPLING_FLAG_NONE_SAMPLING, SAMPLING_FLAG_OVER_SAMPLING,
    SAMPLING_FLAG_UNDER_SAMPLING,
};

use super::pass::Pass;
use crate::{
    backend::LayerSampling,
    marp::{passes::dispatch_size, resources::shader_compiler::ShaderLoader, Queues},
    render_error,
};

pub(crate) struct ResolvePassData {
    pub(crate) resolved: Arc<Image>,
    descriptor_set: Arc<DescriptorSet>,
    is_transitioned: bool,
}

pub(crate) struct ResolvePass {
    pub(crate) data: Vec<ResolvePassData>,
    pipeline: Arc<ComputePipeline>,
    #[allow(dead_code)]
    shader_loader: ShaderLoader,
    resolve_const: PushConstant<ResolveConst>,
}

impl ResolvePass {
    pub fn new(
        device: Arc<Device>,
        extent: Extent2D, //extend of the actual final image
        resolve_mode: LayerSampling,
        num_slots: usize,
        final_pass: &impl Pass, //where we get our source image
    ) -> Self {
        let descriptor_pool = StdDescriptorPool::new(
            device.clone(),
            vec![DescriptorPoolSize::builder()
                .ty(DescriptorType::STORAGE_IMAGE)
                //In: src, Out: resolved
                .descriptor_count(num_slots as u32 * 2)
                .build()]
            .as_slice(),
            num_slots as u32,
        )
        .unwrap();

        let mut data = Vec::with_capacity(num_slots);
        for i in 0..num_slots {
            let resolved = Image::new(
                device.clone(),
                ImageInfo::new(
                    ImageType::Image2D {
                        width: extent.width,
                        height: extent.height,
                        samples: 1,
                    },
                    Format::R32G32B32A32_SFLOAT, //Unused Alpha atm, also when handling HDR input this should be u8 probably
                    None,
                    Some(MipLevel::Specific(1)),
                    ImageUsage {
                        transfer_src: true,
                        storage: true,
                        color_aspect: true,
                        ..Default::default()
                    },
                    MemoryUsage::GpuOnly,
                    None,
                ),
                SharingMode::Exclusive,
            )
            .unwrap();

            //Allocate the descriptor set
            let mut descriptor_set = descriptor_pool.next();

            descriptor_set
                .add(DescResource::new_image(
                    0,
                    vec![(
                        final_pass
                            .get_final_image(i)
                            .expect("Failed to get final image for slot of resolve pass"),
                        None,
                        ImageLayout::GENERAL,
                    )],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            descriptor_set
                .add(DescResource::new_image(
                    1,
                    vec![(resolved.clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            let descriptor_set = descriptor_set.build().unwrap();

            data.push(ResolvePassData {
                resolved,
                descriptor_set,
                is_transitioned: false,
            });
        }

        let (sampling_flags, sampling_param) = match resolve_mode {
            LayerSampling::None => (SAMPLING_FLAG_NONE_SAMPLING, 1),
            LayerSampling::OverSampling(multi) => (SAMPLING_FLAG_OVER_SAMPLING, multi.max(1)),
            LayerSampling::UnderSamping(div) => (SAMPLING_FLAG_UNDER_SAMPLING, div.max(1)),
        };

        //Create initial push constant
        let resolve_const = PushConstant::new(
            ResolveConst {
                sampling_flags,
                sampling_param,
                pad0: [0, 0],
            },
            ShaderStageFlags::COMPUTE,
        );

        //Setup the compute pipeline.
        let pipe_layout = PipelineLayout::new(
            device.clone(),
            vec![*data[0].descriptor_set.layout()],
            vec![*resolve_const.range()],
        )
        .unwrap();

        let mut shader_loader = ShaderLoader::new_from_glsl("shader_resolve").unwrap();
        let shader = shader_loader
            .build_shader_stage(device.clone(), Stage::Compute, "main")
            .unwrap();

        let pipeline = ComputePipeline::new(device.clone(), shader, pipe_layout).unwrap();

        ResolvePass {
            data,
            pipeline,
            shader_loader,
            resolve_const,
        }
    }

    #[allow(dead_code)]
    fn rebuild_pipeline(&mut self, device: Arc<Device>) {
        let new_stage =
            match self
                .shader_loader
                .build_shader_stage(device.clone(), Stage::Compute, "main")
            {
                Ok(stage) => stage,
                Err(e) => {
                    render_error!(
                        "Could not build new shader stage for direct_light from shader: {:?}",
                        e
                    );
                    return;
                }
            };
        let pipe_layout = PipelineLayout::new(
            device.clone(),
            vec![*self.data[0].descriptor_set.layout()],
            vec![*self.resolve_const.range()],
        )
        .unwrap();

        let new_pipeline = match ComputePipeline::new(device.clone(), new_stage, pipe_layout) {
            Ok(p) => p,
            Err(e) => {
                render_error!("Could build new pipeline for direct_light: {:?}", e);
                return;
            }
        };

        self.pipeline = new_pipeline;
    }

    #[allow(dead_code)]
    pub fn update_resolve_const(&mut self, new: ResolveConst) {
        //update image width/height
        *self.resolve_const.get_content_mut() = new;
    }
}

impl Pass for ResolvePass {
    fn get_final_image(&self, slot: usize) -> Option<Arc<Image>> {
        if let Some(data) = self.data.get(slot) {
            Some(data.resolved.clone())
        } else {
            None
        }
    }

    fn pre(
        &mut self,
        queues: &Queues,
        command_buffer: Arc<CommandBuffer>,
        slot_index: usize,
    ) -> Arc<CommandBuffer> {
        #[cfg(feature = "watch_shaders")]
        if !self.shader_loader.is_up_to_date() {
            self.rebuild_pipeline(queues.graphics_queue.get_device());
        }
        //Transition every resource that isn't yet in the correct layout
        if self.data[slot_index].is_transitioned {
            return command_buffer;
        }

        //We want our images to be in the general layout, so we can write to them in the primary pass
        command_buffer
            .cmd_pipeline_barrier(
                PipelineStageFlags::ALL_COMMANDS,
                PipelineStageFlags::COMPUTE_SHADER,
                DependencyFlags::empty(),
                vec![],
                vec![],
                vec![self.data[slot_index].resolved.new_image_barrier(
                    Some(ImageLayout::UNDEFINED),
                    Some(ImageLayout::GENERAL),
                    Some(queues.graphics_queue.clone()), //Keep being on graphics queue for now.
                    Some(queues.graphics_queue.clone()),
                    None,
                    Some(AccessFlags::SHADER_WRITE),
                    None,
                )],
            )
            .expect("Failed to transition image in resolve pass");
        //Mark this slot as transitioned
        self.data[slot_index].is_transitioned = true;
        command_buffer
    }

    fn record(
        &mut self,
        command_buffer: Arc<CommandBuffer>,
        slot_index: usize,
    ) -> Arc<CommandBuffer> {
        //Bind descriptorset
        command_buffer
            .cmd_bind_descriptor_sets(
                PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![self.data[slot_index].descriptor_set.clone()],
                vec![],
            )
            .expect("Failed to bind resolve Descriptorset");

        command_buffer
            .cmd_bind_pipeline(PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .expect("Failed to bind resolve pipeline");
        //upload current camera
        command_buffer
            .cmd_push_constants(self.pipeline.layout(), &self.resolve_const)
            .unwrap();

        command_buffer
            .cmd_dispatch(dispatch_size(&self.data[slot_index].resolved))
            .expect("Failed to schedule resolve pass");

        command_buffer
    }
}

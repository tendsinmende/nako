/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use std::sync::Arc;

use marp::image::{AbstractImage, Image};

///Common renderpass structure
pub(crate) mod pass;

///Simple pass that copies some image to the supplied swapchain image
pub(crate) mod pass_image_to_swapchain;

///Renderpass that generates the primary rays output
pub(crate) mod pass_primary;

///Segment tracing version of the standard primary pass. Should be faster, but might create visual glitches
pub(crate) mod pass_primary_segment_tracing;

///Primary pass as well, but 2d. Therefore only albedo and opacity information is outputted.
pub(crate) mod pass_primary_2d;

///DirectLight pass. calculates a single screen space texture containing all direct light contribution to the pixel.
pub(crate) mod direct_light;

///ShadingPass that takes several intermediate results and builds pbs shaded pixels from it.
pub(crate) mod shading;

///Combines several layers onto each other via double buffering
pub(crate) mod layer_combine;

///Resolve pass that takes an input image and applies a given resolve technique
///to resolve the image to another size.
pub(crate) mod resolve;

pub static LOCAL_SIZE: [u32; 3] = [8, 8, 1];

///Calculates the correct dispatch size for an image, assuming that the kernel uses `LOCAL_SIZE` as local dispatch size.
pub fn dispatch_size(image: &Arc<Image>) -> [u32; 3] {
    [
        (image.extent().width as f32 / LOCAL_SIZE[0] as f32).ceil() as u32,
        (image.extent().height as f32 / LOCAL_SIZE[1] as f32).ceil() as u32,
        1,
    ]
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use std::sync::Arc;

use marp::{
    ash::vk::{
        AccessFlags, DependencyFlags, DescriptorPoolSize, DescriptorType, Extent2D, Format,
        ImageLayout, PipelineBindPoint, PipelineStageFlags, ShaderStageFlags,
    },
    buffer::SharingMode,
    command_buffer::CommandBuffer,
    descriptor::{
        create_static_descriptor_set, DescResource, DescriptorPool, DescriptorSet, PushConstant,
        StdDescriptorPool,
    },
    device::Device,
    image::{AbstractImage, Image, ImageInfo, ImageType, ImageUsage, MipLevel},
    memory::MemoryUsage,
    pipeline::{ComputePipeline, PipelineLayout},
    shader::Stage,
};
use nako_shared::push_constants::LayerCombinePushConst;

use crate::{
    marp::{passes::dispatch_size, resources::shader_compiler::ShaderLoader, Queues},
    render_error,
};

struct FrameInfo {
    //Slot that holds the current final image
    final_slot: usize,
    images: [Arc<Image>; 2],

    //Descriptorsets that need to be bound for clearing each image
    clear_desc_sets: [Arc<DescriptorSet>; 2],

    //Descriptorset pool used for creating copy commands. Grows whenever a bigger
    //pool is needed.
    descriptor_set_pool: Arc<StdDescriptorPool>,
    //Tracks for how many copy commands are within this pool. Used to grow whenever a bigger batch is requested.
    pool_size: usize,

    is_transitioned: bool,
}

struct CopyCommand {
    bg_image: Arc<Image>,
    target_image: Arc<Image>,
    descset: Arc<DescriptorSet>,
    push: PushConstant<LayerCombinePushConst>,
}

pub struct LayerCombine {
    //Pipeline that clears the attached image
    clear_pipeline: Arc<ComputePipeline>,
    //Copy pipeline
    pipeline: Arc<ComputePipeline>,
    #[allow(dead_code)]
    clear_shader: ShaderLoader,
    #[allow(dead_code)]
    combine_shader: ShaderLoader,
    data: Vec<FrameInfo>,
}

impl LayerCombine {
    pub fn new(device: Arc<Device>, extent: Extent2D, num_slots: usize) -> Self {
        let mut data = Vec::with_capacity(num_slots);
        for _i in 0..num_slots {
            let target_images = [
                Image::new(
                    device.clone(),
                    ImageInfo::new(
                        ImageType::Image2D {
                            width: extent.width,
                            height: extent.height,
                            samples: 1,
                        },
                        Format::R32G32B32A32_SFLOAT,
                        None,
                        Some(MipLevel::Specific(1)),
                        ImageUsage {
                            transfer_src: true,
                            storage: true,
                            color_aspect: true,
                            ..Default::default()
                        },
                        MemoryUsage::GpuOnly,
                        None,
                    ),
                    SharingMode::Exclusive,
                )
                .unwrap(),
                Image::new(
                    device.clone(),
                    ImageInfo::new(
                        ImageType::Image2D {
                            width: extent.width,
                            height: extent.height,
                            samples: 1,
                        },
                        Format::R32G32B32A32_SFLOAT,
                        None,
                        Some(MipLevel::Specific(1)),
                        ImageUsage {
                            transfer_src: true,
                            storage: true,
                            color_aspect: true,
                            ..Default::default()
                        },
                        MemoryUsage::GpuOnly,
                        None,
                    ),
                    SharingMode::Exclusive,
                )
                .unwrap(),
            ];

            let clear_desc_sets = [
                create_static_descriptor_set(
                    device.clone(),
                    vec![DescResource::new_image(
                        0,
                        vec![(target_images[0].clone(), None, ImageLayout::GENERAL)],
                        DescriptorType::STORAGE_IMAGE,
                    )],
                )
                .unwrap(),
                create_static_descriptor_set(
                    device.clone(),
                    vec![DescResource::new_image(
                        0,
                        vec![(target_images[1].clone(), None, ImageLayout::GENERAL)],
                        DescriptorType::STORAGE_IMAGE,
                    )],
                )
                .unwrap(),
            ];

            let start_pool = StdDescriptorPool::new(
                device.clone(),
                &[DescriptorPoolSize::builder()
                    .ty(DescriptorType::STORAGE_IMAGE)
                    .descriptor_count(3) //each layer has a bg, fg and target image
                    .build()],
                1,
            )
            .unwrap();

            data.push(FrameInfo {
                final_slot: 0,
                images: target_images,
                is_transitioned: false,
                descriptor_set_pool: start_pool,
                pool_size: 1,
                clear_desc_sets,
            });
        }

        //create a fake descriptorset for our layout
        let descriptor_set = create_static_descriptor_set(
            device.clone(),
            vec![
                DescResource::new_image(
                    0,
                    vec![(data[0].images[0].clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ),
                DescResource::new_image(
                    1,
                    vec![(data[0].images[0].clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ),
                DescResource::new_image(
                    2,
                    vec![(data[0].images[0].clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ),
            ],
        )
        .unwrap();

        //Create initial push constant
        let combine_const = PushConstant::new(
            LayerCombinePushConst {
                extent: [0; 2],
                origin: [0; 2],
                pad0: [0; 2],
                pad1: [0; 2],
            },
            ShaderStageFlags::COMPUTE,
        );

        //Setup the compute pipeline.
        let pipe_layout = PipelineLayout::new(
            device.clone(),
            vec![*descriptor_set.layout()],
            vec![*combine_const.range()],
        )
        .unwrap();

        let mut combine_shader_loader = ShaderLoader::new("shader_layer_combine").unwrap();
        let combine_shader = combine_shader_loader
            .build_shader_stage(device.clone(), Stage::Compute, "main")
            .unwrap();
        let pipeline = ComputePipeline::new(device.clone(), combine_shader, pipe_layout).unwrap();

        //Create clear pipeline as well
        //Setup the compute pipeline.
        let clear_layout = PipelineLayout::new(
            device.clone(),
            vec![*data[0].clear_desc_sets[0].layout()],
            vec![],
        )
        .unwrap();
        let mut clear_shader_loader = ShaderLoader::new("shader_clear").unwrap();
        let clear_shader = clear_shader_loader
            .build_shader_stage(device.clone(), Stage::Compute, "main")
            .unwrap();
        let clear_pipeline =
            ComputePipeline::new(device.clone(), clear_shader, clear_layout).unwrap();

        LayerCombine {
            clear_pipeline,
            data,
            pipeline,
            clear_shader: clear_shader_loader,
            combine_shader: combine_shader_loader,
        }
    }

    #[allow(dead_code)]
    fn rebuild_clear_pipeline(&mut self, device: Arc<Device>) {
        let new_stage =
            match self
                .clear_shader
                .build_shader_stage(device.clone(), Stage::Compute, "main")
            {
                Ok(stage) => stage,
                Err(e) => {
                    render_error!(
                        "Could not build new shader stage for clear_shader from shader: {:?}",
                        e
                    );
                    return;
                }
            };
        let pipe_layout = PipelineLayout::new(
            device.clone(),
            vec![*self.data[0].clear_desc_sets[0].layout()],
            vec![],
        )
        .unwrap();

        let new_pipeline = match ComputePipeline::new(device.clone(), new_stage, pipe_layout) {
            Ok(p) => p,
            Err(e) => {
                render_error!("Could build new pipeline for clear: {:?}", e);
                return;
            }
        };

        self.clear_pipeline = new_pipeline;
    }

    #[allow(dead_code)]
    fn rebuild_combine_pipeline(&mut self, device: Arc<Device>) {
        //Fake descriptorset
        let descriptor_set = create_static_descriptor_set(
            device.clone(),
            vec![
                DescResource::new_image(
                    0,
                    vec![(self.data[0].images[0].clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ),
                DescResource::new_image(
                    1,
                    vec![(self.data[0].images[0].clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ),
                DescResource::new_image(
                    2,
                    vec![(self.data[0].images[0].clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ),
            ],
        )
        .unwrap();

        //Fake push const
        let combine_const = PushConstant::new(
            LayerCombinePushConst {
                extent: [0; 2],
                origin: [0; 2],
                pad0: [0; 2],
                pad1: [0; 2],
            },
            ShaderStageFlags::COMPUTE,
        );

        let new_stage =
            match self
                .combine_shader
                .build_shader_stage(device.clone(), Stage::Compute, "main")
            {
                Ok(stage) => stage,
                Err(e) => {
                    render_error!(
                        "Could not build new shader stage for combine_shader from shader: {:?}",
                        e
                    );
                    return;
                }
            };
        let pipe_layout = PipelineLayout::new(
            device.clone(),
            vec![*descriptor_set.layout()],
            vec![*combine_const.range()],
        )
        .unwrap();

        let new_pipeline = match ComputePipeline::new(device.clone(), new_stage, pipe_layout) {
            Ok(p) => p,
            Err(e) => {
                render_error!("Could build new pipeline for combine: {:?}", e);
                return;
            }
        };

        self.pipeline = new_pipeline;
    }

    fn grow_pool(&mut self, device: Arc<Device>, slot: usize, new_size: usize) {
        self.data[slot].pool_size = new_size;
        self.data[slot].descriptor_set_pool = StdDescriptorPool::new(
            device,
            &[DescriptorPoolSize::builder()
                .ty(DescriptorType::STORAGE_IMAGE)
                .descriptor_count(new_size as u32 * 3) //each layer has a bg, fg and target image
                .build()],
            new_size as u32,
        )
        .unwrap();
    }

    fn create_desciptor_sets(
        &mut self,
        device: Arc<Device>,
        slot: usize,
        copy_commands: Vec<(Arc<Image>, LayerCombinePushConst)>,
    ) -> Vec<CopyCommand> {
        //Check if we got too many copy commands, in that case, grow the pool, otherwise use again
        if self.data[slot].pool_size < copy_commands.len() {
            self.grow_pool(device.clone(), slot, copy_commands.len());
        }
        /*
                //Allocate enough descriptors for each copy command
                let descriptor_pool = StdDescriptorPool::new(
                    device.clone(),
                    &[DescriptorPoolSize::builder()
                        .ty(DescriptorType::STORAGE_IMAGE)
                        .descriptor_count(copy_commands.len() as u32 * 3) //each layer has a bg, fg and target image
                        .build()],
                    copy_commands.len() as u32,
                )
                .unwrap();
        */
        //For each copy command, create the descriptor set and the push constant
        let mut commands = Vec::with_capacity(copy_commands.len() + 1);
        let mut target_slot = 0usize;

        for (fg_image, layer_info) in copy_commands.into_iter() {
            let bg_image = self.data[slot].images[(target_slot + 1) % 2].clone();
            let target_image = self.data[slot].images[target_slot].clone();

            let mut descriptor_set = self.data[slot].descriptor_set_pool.next();
            descriptor_set
                .add(DescResource::new_image(
                    0,
                    vec![(target_image.clone(), None, ImageLayout::GENERAL)],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            descriptor_set
                .add(DescResource::new_image(
                    1,
                    vec![(
                        bg_image.clone(), //bg image is the one before
                        None,
                        ImageLayout::GENERAL,
                    )],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            //fg image supplied by copy command
            descriptor_set
                .add(DescResource::new_image(
                    2,
                    vec![(
                        fg_image.clone(), //bg image is the one before
                        None,
                        ImageLayout::GENERAL,
                    )],
                    DescriptorType::STORAGE_IMAGE,
                ))
                .unwrap();

            let descriptor_set = descriptor_set.build().unwrap();
            //Build Push Const as well
            let combine_const = PushConstant::new(layer_info, ShaderStageFlags::COMPUTE);

            //Push both
            commands.push(CopyCommand {
                bg_image,
                target_image,
                descset: descriptor_set,
                push: combine_const,
            });
            //Move to next slot for next copy
            target_slot = (target_slot + 1) % 2;
        }

        self.data[slot].final_slot = (target_slot + 1) % 2;
        commands
    }

    ///Returns the last target image on this slot
    pub fn get_final_image(&self, slot: usize) -> Arc<Image> {
        self.data[slot].images[self.data[slot].final_slot].clone()
    }

    fn record_copy(
        &self,
        command_buffer: Arc<CommandBuffer>,
        copy_command: CopyCommand,
    ) -> Arc<CommandBuffer> {
        //Assuming the bg image is still in write_mode, since it was a target before,
        //assuming the target image is still in read_mode since it was a src image before.
        //also waiting for the last compute kernel to finish
        command_buffer
            .cmd_pipeline_barrier(
                PipelineStageFlags::COMPUTE_SHADER,
                PipelineStageFlags::COMPUTE_SHADER,
                DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    copy_command.bg_image.new_image_barrier(
                        Some(ImageLayout::GENERAL),
                        Some(ImageLayout::GENERAL),
                        None,
                        None,
                        Some(AccessFlags::SHADER_WRITE),
                        Some(AccessFlags::SHADER_READ),
                        None,
                    ),
                    copy_command.target_image.new_image_barrier(
                        Some(ImageLayout::GENERAL),
                        Some(ImageLayout::GENERAL),
                        None,
                        None,
                        Some(AccessFlags::SHADER_READ),
                        Some(AccessFlags::SHADER_WRITE),
                        None,
                    ),
                ],
            )
            .unwrap();

        //Schedule copy on queue
        command_buffer
            .cmd_bind_descriptor_sets(
                PipelineBindPoint::COMPUTE,
                self.pipeline.layout(),
                0,
                vec![copy_command.descset.clone()],
                vec![],
            )
            .expect("Failed to bind ShadingPass Descriptorset");

        command_buffer
            .cmd_bind_pipeline(PipelineBindPoint::COMPUTE, self.pipeline.clone())
            .expect("Failed to bind shading pipeline");

        //upload current camera
        command_buffer
            .cmd_push_constants(self.pipeline.layout(), &copy_command.push)
            .unwrap();

        command_buffer
            .cmd_dispatch(dispatch_size(&copy_command.target_image))
            .expect("Failed to schedule shading pass");

        command_buffer
    }

    fn clear_image(
        &self,
        slot: usize,
        image_slot: usize,
        command_buffer: Arc<CommandBuffer>,
    ) -> Arc<CommandBuffer> {
        command_buffer
            .cmd_bind_descriptor_sets(
                PipelineBindPoint::COMPUTE,
                self.clear_pipeline.layout(),
                0,
                vec![self.data[slot].clear_desc_sets[image_slot].clone()],
                vec![],
            )
            .expect("Failed to bind ShadingPass Descriptorset");

        command_buffer
            .cmd_bind_pipeline(PipelineBindPoint::COMPUTE, self.clear_pipeline.clone())
            .expect("Failed to bind shading pipeline");

        command_buffer
            .cmd_dispatch(dispatch_size(&self.data[slot].images[image_slot]))
            .expect("Failed to schedule shading pass");

        command_buffer
    }

    pub fn record(
        &mut self,
        slot: usize,
        device: Arc<Device>,
        queues: &Queues,
        mut command_buffer: Arc<CommandBuffer>,
        copy_commands: Vec<(Arc<Image>, LayerCombinePushConst)>,
    ) -> Arc<CommandBuffer> {
        let images = copy_commands
            .iter()
            .map(|(i, _)| i.clone())
            .collect::<Vec<_>>();
        //First transition all incoming images to read mode, assuming they are already in general layout
        command_buffer = self.pre(command_buffer, queues, slot, &images);

        //Before adding any copy commands, clear both images of this slot
        command_buffer = self.clear_image(slot, 0, command_buffer);
        command_buffer = self.clear_image(slot, 1, command_buffer);

        //Prepare copy commands by building all needed descriptorsets, that already assign the correct images
        let copies = self.create_desciptor_sets(device, slot, copy_commands);

        for copy in copies {
            command_buffer = self.record_copy(command_buffer, copy);
        }

        command_buffer = self.post(command_buffer, &images);

        command_buffer
    }

    fn pre(
        &mut self,
        command_buffer: Arc<CommandBuffer>,
        queues: &Queues,
        slot: usize,
        src_images: &[Arc<Image>],
    ) -> Arc<CommandBuffer> {
        #[cfg(feature = "watch_shaders")]
        if !self.clear_shader.is_up_to_date() {
            self.rebuild_clear_pipeline(queues.graphics_queue.get_device());
        }

        #[cfg(feature = "watch_shaders")]
        if !self.combine_shader.is_up_to_date() {
            self.rebuild_combine_pipeline(queues.graphics_queue.get_device());
        }

        let mut barriers = src_images
            .iter()
            .map(|img| {
                //Assuming this image to be in GENERAL/SHADER_WRITE layout
                img.new_image_barrier(
                    Some(ImageLayout::GENERAL),
                    Some(ImageLayout::GENERAL),
                    None,
                    None,
                    Some(AccessFlags::SHADER_WRITE),
                    Some(AccessFlags::SHADER_READ), //Move to read for combining
                    None,
                )
            })
            .collect::<Vec<_>>();

        //Attach our own transition info as well. Otherwise should already be in GENERAL/SHADER_WRITE layout
        if !self.data[slot].is_transitioned {
            for img in &self.data[slot].images {
                barriers.push(img.new_image_barrier(
                    Some(ImageLayout::UNDEFINED),
                    Some(ImageLayout::GENERAL),
                    Some(queues.graphics_queue.clone()), //Keep being on graphics queue for now.
                    Some(queues.graphics_queue.clone()),
                    None,
                    Some(AccessFlags::SHADER_WRITE),
                    None,
                ));
            }

            self.data[slot].is_transitioned = true;
        }
        //Transition to general layout from undefined
        command_buffer
            .cmd_pipeline_barrier(
                PipelineStageFlags::ALL_COMMANDS,
                PipelineStageFlags::COMPUTE_SHADER,
                DependencyFlags::empty(),
                vec![],
                vec![],
                barriers,
            )
            .expect("Failed to transition image in combine pass");

        command_buffer
    }

    fn post(
        &mut self,
        command_buffer: Arc<CommandBuffer>,
        src_images: &[Arc<Image>],
    ) -> Arc<CommandBuffer> {
        let barriers = src_images
            .iter()
            .map(|img| {
                //Assuming this image to be in GENERAL/SHADER_WRITE layout
                img.new_image_barrier(
                    Some(ImageLayout::GENERAL),
                    Some(ImageLayout::GENERAL),
                    None,
                    None,
                    Some(AccessFlags::SHADER_READ),
                    Some(AccessFlags::SHADER_WRITE), //Move back to write
                    None,
                )
            })
            .collect::<Vec<_>>();

        //TODO add flag for final image?

        command_buffer
            .cmd_pipeline_barrier(
                PipelineStageFlags::ALL_COMMANDS,
                PipelineStageFlags::COMPUTE_SHADER,
                DependencyFlags::empty(),
                vec![],
                vec![],
                barriers,
            )
            .expect("Failed to transition image in combine pass");

        command_buffer
    }
}

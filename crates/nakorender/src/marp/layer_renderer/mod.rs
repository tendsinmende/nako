/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use std::sync::Arc;

use marp::{ash::vk::PipelineStageFlags, image::Image, sync::Semaphore};
use nako::glam::{IVec2, UVec2};

///Lits a scene with lights. In this case in 3d
pub(crate) mod lit3d;
///Renders a 2d stream directly to texture. Only keeps the albedo value
pub(crate) mod unlit2d;

pub enum MarpTask {
    ///Untouched image that just needs to be added on top
    Ready {
        image: Arc<Image>,
        location: IVec2,
        extent: UVec2,
    },
    ///Images that is currently rendering. Combiner has to wait for its semaphore
    Rendering {
        sem: Arc<Semaphore>,
        stage: PipelineStageFlags,
        image: Arc<Image>,
        location: IVec2,
        extent: UVec2,
    },
}

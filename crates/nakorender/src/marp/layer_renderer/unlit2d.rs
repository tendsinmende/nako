/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use std::sync::Arc;

use crate::{
    backend::LayerSampling,
    camera::Camera2d,
    layer_manager::LayerRenderer,
    marp::{
        passes::{pass::Pass, pass_primary_2d::PrimaryPass2d, resolve::ResolvePass},
        resources::{BufferManager, BufferManager2d, BufferStorage2d},
        Queues,
    },
    render_info,
};
use marp::{
    ash::vk::{self, AccessFlags, DependencyFlags, Extent2D, ImageLayout, PipelineStageFlags},
    buffer::Buffer,
    command_buffer::{CommandBuffer, CommandBufferPool, CommandPool},
    device::{Device, SubmitInfo},
    image::{AbstractImage, Image},
    sync::{QueueFence, Semaphore},
};
use nako::{
    combinators::Union,
    glam::{IVec2, UVec2, Vec2, Vec3},
    modifiers::Color,
    primitives::Cuboid,
    stream::{PrimaryStream2d, SecondaryStream2d},
};
use nako_shared::GpuCamera2d;

use super::MarpTask;

pub(crate) struct Unlit2d {
    //Currently rendered to extent
    current_extent: Extent2D,
    //current sampling technique
    sampling: LayerSampling,
    //Handles Sdf related buffers
    sdf_buffers: BufferManager2d,

    //Flag unset whenever sdf or camera change and the chaneg has not yet been reredndered on this slot.
    up_to_date: Vec<bool>,

    //Builds us one frame
    frame_builder: FrameBuilder,

    //vulkan context info used for creating the frame builder
    device: Arc<Device>,
    queues: Queues,
    number_of_slots: usize,

    last_camera: Camera2d,
}

impl Unlit2d {
    pub fn new(device: Arc<Device>, queues: Queues, num_slots: usize) -> Self {
        let initial_ext = Extent2D {
            width: 1,
            height: 1,
        };

        let mut initial_buffer = BufferManager2d::new(device.clone(), &queues);
        //Add initial sdf (standard cube)
        initial_buffer.update_code(
            PrimaryStream2d::new()
                .push(
                    SecondaryStream2d::new(Union, Cuboid { extent: Vec2::ONE })
                        .push_mod(Color(Vec3::ONE))
                        .build(),
                )
                .build(),
        );

        let frame_builder = FrameBuilder::new(
            &mut initial_buffer,
            device.clone(),
            &queues,
            num_slots,
            initial_ext,
            LayerSampling::None,
        );

        Unlit2d {
            current_extent: initial_ext,
            sampling: LayerSampling::None,
            sdf_buffers: initial_buffer,
            up_to_date: vec![false; num_slots],
            frame_builder,
            device,
            queues,
            number_of_slots: num_slots,
            last_camera: Camera2d::default(),
        }
    }

    fn reset_update_state(&mut self) {
        for s in &mut self.up_to_date {
            *s = false;
        }
    }

    fn recreate(&mut self) {
        //Recreate with new properties
        self.frame_builder = FrameBuilder::new(
            &mut self.sdf_buffers,
            self.device.clone(),
            &self.queues,
            self.number_of_slots,
            self.current_extent,
            self.sampling,
        );
        self.up_to_date = vec![false; self.number_of_slots];
    }
}

impl LayerRenderer for Unlit2d {
    type Camera = Camera2d;
    type Stream = PrimaryStream2d;
    type TaskType = MarpTask;

    fn update_camera(&mut self, camera: Self::Camera) {
        self.last_camera = camera;
        self.frame_builder.update_camera(camera);
        self.reset_update_state();
    }

    fn update_sdf(&mut self, sdf: Self::Stream) {
        self.sdf_buffers.update_code(sdf);
        self.frame_builder
            .update_sdf(self.sdf_buffers.get_storage().code_buffer.clone());
        self.reset_update_state();
    }

    fn update_extent(&mut self, new_extent: UVec2, sampling: LayerSampling) {
        if self.current_extent.width == new_extent.x
            && self.current_extent.height == new_extent.y
            && self.sampling == sampling
        {
            return;
        }
        self.sampling = sampling;
        self.current_extent = Extent2D {
            width: new_extent.x,
            height: new_extent.y,
        };
        self.recreate();
        self.update_camera(self.last_camera);
    }

    fn update_number_of_slots(&mut self, num_slots: usize) {
        if num_slots == self.number_of_slots {
            return;
        }
        self.number_of_slots = num_slots;
        self.recreate();
    }

    fn record(&mut self, slot: usize, layer_location: IVec2) -> Self::TaskType {
        if self.up_to_date[slot] {
            let image = self.frame_builder.final_image(slot);
            let ext = image.extent();
            return MarpTask::Ready {
                image,
                extent: (ext.width, ext.height).into(),
                location: layer_location,
            };
        }

        self.up_to_date[slot] = true;
        self.frame_builder
            .record(&self.queues, slot, layer_location)
    }
}

///Single frame info for frame builder
struct FrameInfo {
    finished_execution: Arc<Semaphore>,
    command_buffer: Arc<CommandBuffer>,
    in_flight: Option<QueueFence>,
    //If some the buffer on the descriptor needs to be updated
    update_buffer: Option<Arc<Buffer>>,
}

///Responsible for building a frame. The result will be put into the swapchain image of the given
///slot.
struct FrameBuilder {
    frame_infos: Vec<FrameInfo>,
    primary_2d: PrimaryPass2d,
    ///If resolve was set, contains the resolve pass in the correct state.
    resolve_pass: Option<ResolvePass>,
}

impl FrameBuilder {
    pub fn new(
        buffers: &mut impl BufferManager<StorageType = BufferStorage2d, StreamType = PrimaryStream2d>,
        device: Arc<Device>,
        queues: &Queues,
        num_images: usize,
        extent: Extent2D, //extent of the layer
        sampling: LayerSampling,
    ) -> Self {
        render_info!(
            "Creating Frame builder for extent={:?}, with image count={} and sampling={:?}",
            extent,
            num_images,
            sampling
        );

        #[cfg(feature = "logging")]
        {
            use log::warn;
            if extent.width == 0 || extent.height == 0 {
                warn!("Frame buffer extent is 0 on one axis: {:?}. This is not allowed, correcting to 1 on that axis!", extent);
            }

            if num_images == 0 {
                warn!("Number of images for frame is 0, correcting to 1");
            }
        }

        let layer_extent = match extent {
            Extent2D {
                width: 0,
                height: 0,
            } => Extent2D {
                width: 1,
                height: 1,
            },
            Extent2D { width: 0, height } => Extent2D { width: 1, height },
            Extent2D { width, height: 0 } => Extent2D { width, height: 1 },
            Extent2D { width, height } => Extent2D { width, height },
        };

        //Based on the sampling setting, calculate the render image extent
        let render_extent = match sampling {
            LayerSampling::None => layer_extent,
            LayerSampling::OverSampling(multiplier) => Extent2D {
                width: layer_extent.width * multiplier.max(1),
                height: layer_extent.height * multiplier.max(1),
            },
            LayerSampling::UnderSamping(divider) => Extent2D {
                width: ((layer_extent.width as f32 / divider.max(1) as f32).ceil() as u32).max(1), //make sure to have no partial pixels via ceil
                height: ((layer_extent.height as f32 / divider.max(1) as f32).ceil() as u32).max(1),
            },
        };

        let num_images = num_images.max(1);

        let command_pool = CommandBufferPool::new(
            device.clone(),
            queues.graphics_queue.clone(),
            vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        )
        .expect("Failed to create command pool");

        let command_buffers = command_pool
            .alloc(num_images, false)
            .expect("Failed to allocate command buffers");

        let frame_infos = command_buffers
            .into_iter()
            .map(|cb| FrameInfo {
                command_buffer: cb,
                finished_execution: Semaphore::new(device.clone()).unwrap(),
                in_flight: None,
                update_buffer: None,
            })
            .collect();

        let primary_2d = PrimaryPass2d::new(buffers, device.clone(), render_extent, num_images);

        //init resolve pass if needed
        let resolve_pass = if sampling != LayerSampling::None {
            Some(ResolvePass::new(
                device.clone(),
                layer_extent,
                sampling,
                num_images,
                &primary_2d,
            ))
        } else {
            None
        };

        FrameBuilder {
            frame_infos,
            primary_2d,
            resolve_pass,
        }
    }

    pub fn update_camera(&mut self, new_camera: Camera2d) {
        //Update primary_pass
        let ext = UVec2::new(
            self.primary_2d.data[0].albedo_depth.extent().width,
            self.primary_2d.data[0].albedo_depth.extent().height,
        );
        let new_camera: GpuCamera2d = new_camera.into_gpu_cam(ext);

        self.primary_2d.update_camera(new_camera.clone());
    }

    pub fn final_image(&self, slot: usize) -> Arc<Image> {
        if let Some(resolve) = &self.resolve_pass {
            resolve.get_final_image(slot).unwrap()
        } else {
            self.primary_2d.data[slot].albedo_depth.clone()
        }
    }

    fn record_pass(
        queues: &Queues,
        mut command_buffer: Arc<CommandBuffer>,
        slot: usize,
        pass: &mut impl Pass,
    ) -> Arc<CommandBuffer> {
        //Start Primary pass
        command_buffer = pass.pre(queues, command_buffer, slot);
        command_buffer = pass.record(command_buffer, slot);
        command_buffer = pass.post(queues, command_buffer, slot);

        command_buffer
    }

    pub fn update_sdf(&mut self, buffer: Arc<Buffer>) {
        for d in &mut self.frame_infos {
            d.update_buffer = Some(buffer.clone());
        }
    }

    pub fn record(&mut self, queues: &Queues, slot: usize, location: IVec2) -> MarpTask {
        //Wait for the last frame on this slot if needed
        if let Some(inflight) = self.frame_infos[slot].in_flight.take() {
            inflight.wait(u64::MAX).unwrap();
        }

        //Check if we should update the buffer
        if let Some(new_buffer) = self.frame_infos[slot].update_buffer.take() {
            self.primary_2d.update_buffer(slot, new_buffer.clone());
        }

        //Reset command buffer
        let mut command_buffer = self.frame_infos[slot].command_buffer.clone();
        command_buffer.reset().unwrap();
        command_buffer
            .begin_recording(true, false, false, None)
            .unwrap();

        //Record primary pass
        command_buffer = Self::record_pass(queues, command_buffer, slot, &mut self.primary_2d);

        //if there is a resolve involved, wait for rendering, resolve image and transition images back into correct layout
        if let Some(resolve_pass) = &mut self.resolve_pass {
            //Wait for rendering of primary pass, then transition image to read mode for resolve pass
            command_buffer
                .cmd_pipeline_barrier(
                    PipelineStageFlags::COMPUTE_SHADER,
                    PipelineStageFlags::COMPUTE_SHADER,
                    DependencyFlags::empty(),
                    vec![],
                    vec![],
                    vec![self
                        .primary_2d
                        .get_final_image(slot)
                        .unwrap()
                        .new_image_barrier(
                            Some(ImageLayout::GENERAL),
                            Some(ImageLayout::GENERAL),
                            None,
                            None,
                            Some(AccessFlags::SHADER_WRITE),
                            Some(AccessFlags::SHADER_READ),
                            None,
                        )],
                )
                .unwrap();
            //execute resolve pass
            command_buffer = Self::record_pass(queues, command_buffer, slot, resolve_pass);

            //Wait for resolve pass, transition primary back to write mode (assumed by the pass)
            command_buffer
                .cmd_pipeline_barrier(
                    PipelineStageFlags::COMPUTE_SHADER,
                    PipelineStageFlags::COMPUTE_SHADER,
                    DependencyFlags::empty(),
                    vec![],
                    vec![],
                    vec![self
                        .primary_2d
                        .get_final_image(slot)
                        .unwrap()
                        .new_image_barrier(
                            Some(ImageLayout::GENERAL),
                            Some(ImageLayout::GENERAL),
                            None,
                            None,
                            Some(AccessFlags::SHADER_READ),
                            Some(AccessFlags::SHADER_WRITE),
                            None,
                        )],
                )
                .unwrap();
        }

        //At this point the shaded image is finished
        command_buffer.end_recording().unwrap();

        //Submit new frame and let it signal our Semaphore when finished
        let new_in_flight = queues
            .graphics_queue
            .queue_submit(vec![SubmitInfo::new(
                vec![], //wait for nothing since we just waited for the former fence
                vec![command_buffer],
                vec![self.frame_infos[slot].finished_execution.clone()], //Signal execution semaphore
            )])
            .unwrap();

        self.frame_infos[slot].in_flight = Some(new_in_flight);

        //select correct image and extent based on sampling
        let (image, extent) = if let Some(resolve) = &self.resolve_pass {
            let img = resolve.get_final_image(slot).unwrap();
            let ext = img.extent();
            (img, ext)
        } else {
            let img = self.primary_2d.get_final_image(slot).unwrap();
            let ext = img.extent();
            (img, ext)
        };

        MarpTask::Rendering {
            image,
            extent: (extent.width, extent.height).into(),
            location,
            sem: self.frame_infos[slot].finished_execution.clone(),
            stage: PipelineStageFlags::COMPUTE_SHADER,
        }
    }
}

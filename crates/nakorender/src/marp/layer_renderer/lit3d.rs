/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::sync::Arc;

use crate::{
    backend::LayerSampling,
    camera::Camera,
    layer_manager::LayerRenderer,
    marp::{
        passes::{
            direct_light,
            pass::Pass,
            resolve::{self, ResolvePass},
            shading, pass_primary,
        },
        resources::{BufferManager, BufferManager3d, BufferStorage3d},
        Queues,
    },
    render_info, render_warn,
};
use marp::{
    ash::vk::{self, AccessFlags, DependencyFlags, Extent2D, ImageLayout, PipelineStageFlags},
    command_buffer::{CommandBuffer, CommandBufferPool, CommandPool},
    device::{Device, SubmitInfo},
    image::{AbstractImage, Image},
    sync::{QueueFence, Semaphore},
};
use nako::{
    combinators::Union,
    glam::{IVec2, UVec2, Vec3},
    modifiers::Color,
    primitives::Cuboid,
    stream::{PrimaryStream, SecondaryStream},
};
use nako_shared::GpuCamera;

use super::MarpTask;

pub(crate) struct Lit3d {
    //Currently rendered to extent
    current_extent: Extent2D,
    //current sampling technique
    sampling: LayerSampling,

    //Handles Sdf related buffers
    sdf_buffers: BufferManager3d,

    //Flag unset whenever sdf or camera change and the chaneg has not yet been reredndered on this slot.
    up_to_date: Vec<bool>,

    //frame builder based for a fully shaded 3d sdf
    frame_builder: FrameBuilder,

    //vulkan context info used for creating the frame builder
    device: Arc<Device>,
    queues: Queues,
    number_of_slots: usize,

    last_camera: Camera,
}

impl Lit3d {
    pub fn new(device: Arc<Device>, queues: Queues, num_slots: usize) -> Self {
        let initial_ext = Extent2D {
            width: 1,
            height: 1,
        };

        let mut initial_buffer = BufferManager3d::new(device.clone(), &queues);
        //Add initial sdf (standard cube)
        initial_buffer.update_code(
            PrimaryStream::new()
                .push(
                    SecondaryStream::new(Union, Cuboid { extent: Vec3::ONE })
                        .push_mod(Color(Vec3::ONE))
                        .build(),
                )
                .build(),
        );

        let frame_builder = FrameBuilder::new(
            initial_buffer.get_storage(),
            device.clone(),
            &queues,
            num_slots,
            initial_ext,
            LayerSampling::None,
        );

        Lit3d {
            current_extent: initial_ext,
            sampling: LayerSampling::None,
            sdf_buffers: initial_buffer,
            up_to_date: vec![false; num_slots],
            frame_builder,
            device,
            queues,
            number_of_slots: num_slots,
            last_camera: Camera::default(),
        }
    }

    fn reset_update_state(&mut self) {
        for s in &mut self.up_to_date {
            *s = false;
        }
    }

    fn recreate(&mut self) {
        //Recreate with new properties
        self.frame_builder = FrameBuilder::new(
            self.sdf_buffers.get_storage(),
            self.device.clone(),
            &self.queues,
            self.number_of_slots,
            self.current_extent,
            self.sampling,
        );
        self.up_to_date = vec![false; self.number_of_slots];
    }
}

impl LayerRenderer for Lit3d {
    type Camera = Camera;
    type Stream = PrimaryStream;
    type TaskType = MarpTask;

    fn update_camera(&mut self, camera: Self::Camera) {
        self.last_camera = camera;
        self.frame_builder.update_camera(camera);
        self.reset_update_state();
    }

    fn update_sdf(&mut self, sdf: Self::Stream) {
        self.sdf_buffers.update_code(sdf);
        render_info!("Set sdf!");
        self.reset_update_state();
    }

    fn update_extent(&mut self, new_extent: UVec2, sampling: LayerSampling) {
        if self.current_extent.width == new_extent.x
            && self.current_extent.height == new_extent.y
            && self.sampling == sampling
        {
            return;
        }

        self.sampling = sampling;
        self.current_extent = Extent2D {
            width: new_extent.x,
            height: new_extent.y,
        };
        self.recreate();
        self.update_camera(self.last_camera);
    }

    fn update_number_of_slots(&mut self, num_slots: usize) {
        if num_slots == self.number_of_slots {
            return;
        }
        self.number_of_slots = num_slots;
        self.recreate();
    }

    fn record(&mut self, slot: usize, layer_location: IVec2) -> Self::TaskType {
        //Check if there is a new sdf version available
        //FIXME: check if the sdf update could be "too fast"?
        //but in that case the update_sdf() call should have set the update state to false, which lets the
        //build check anyways...
        if self.sdf_buffers.is_outdated() {
            self.frame_builder
                .update_buffers(self.sdf_buffers.get_storage().clone());
            render_warn!("Catched outdated buffer!");
            self.reset_update_state();
        }

        //If already up to date, just copy image
        if self.up_to_date[slot] {
            let image = self.frame_builder.final_image(slot);
            let ext = image.extent();
            return MarpTask::Ready {
                image,
                extent: (ext.width as u32, ext.height as u32).into(),
                location: layer_location,
            };
        }
        self.up_to_date[slot] = true;
        self.frame_builder
            .record(&self.queues, slot, layer_location)
    }
}

///Single frame info for frame builder
struct FrameInfo {
    finished_execution: Arc<Semaphore>,
    command_buffer: Arc<CommandBuffer>,
    in_flight: Option<QueueFence>,
    //If some the buffer on the descriptor needs to be updated
    update_buffer: Option<BufferStorage3d>,
}

///Responsible for building a frame. The result will be put into the swapchain image of the given
///slot.
struct FrameBuilder {
    primary_pass: pass_primary::PrimaryPass,
    direct_light: direct_light::DirectLightPass,
    shading_pass: shading::ShadingPass,
    ///If resolve was set, contains the resolve pass in the correct state.
    resolve_pass: Option<resolve::ResolvePass>,

    frame_infos: Vec<FrameInfo>,
}

impl FrameBuilder {
    pub fn new(
        buffers: &BufferStorage3d,
        device: Arc<Device>,
        queues: &Queues,
        num_images: usize,
        extent: Extent2D, //extent of the layer
        sampling: LayerSampling,
    ) -> Self {
        render_info!(
            "Creating Frame builder for extent={:?}, with image count={} and sampling={:?}",
            extent,
            num_images,
            sampling
        );

        #[cfg(feature = "logging")]
        {
            use log::warn;
            if extent.width == 0 || extent.height == 0 {
                warn!("Frame buffer extent is 0 on one axis: {:?}. This is not allowed, correcting to 1 on that axis!", extent);
            }

            if num_images == 0 {
                warn!("Number of images for frame is 0, correcting to 1");
            }
        }

        let layer_extent = match extent {
            Extent2D {
                width: 0,
                height: 0,
            } => Extent2D {
                width: 1,
                height: 1,
            },
            Extent2D { width: 0, height } => Extent2D { width: 1, height },
            Extent2D { width, height: 0 } => Extent2D { width, height: 1 },
            Extent2D { width, height } => Extent2D { width, height },
        };
        //Based on the sampling setting, calculate the render image extent
        let render_extent = match sampling {
            LayerSampling::None => layer_extent,
            LayerSampling::OverSampling(multiplier) => Extent2D {
                width: layer_extent.width * multiplier.max(1),
                height: layer_extent.height * multiplier.max(1),
            },
            LayerSampling::UnderSamping(divider) => Extent2D {
                width: ((layer_extent.width as f32 / divider.max(1) as f32).ceil() as u32).max(1), //make sure to have no partial pixels via ceil
                height: ((layer_extent.height as f32 / divider.max(1) as f32).ceil() as u32).max(1),
            },
        };

        let num_images = num_images.max(1);

        let command_pool = CommandBufferPool::new(
            device.clone(),
            queues.graphics_queue.clone(),
            vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        )
        .expect("Failed to create command pool");

        let command_buffers = command_pool
            .alloc(num_images, false)
            .expect("Failed to allocate command buffers");

        let frame_infos = command_buffers
            .into_iter()
            .map(|cb| FrameInfo {
                command_buffer: cb,
                finished_execution: Semaphore::new(device.clone()).unwrap(),
                in_flight: None,
                update_buffer: None,
            })
            .collect();

        let primary_pass = pass_primary::PrimaryPass::new(
            buffers,
            device.clone(),
            render_extent,
            num_images,
        );

        let direct_light = direct_light::DirectLightPass::new(
            buffers,
            device.clone(),
            render_extent,
            num_images,
            &primary_pass,
        );

        let shading_pass = shading::ShadingPass::new(
            device.clone(),
            render_extent,
            num_images,
            &primary_pass,
            &direct_light,
        );

        //init resolve pass if needed
        let resolve_pass = if sampling != LayerSampling::None {
            Some(ResolvePass::new(
                device.clone(),
                layer_extent,
                sampling,
                num_images,
                &shading_pass,
            ))
        } else {
            None
        };

        FrameBuilder {
            primary_pass,
            direct_light,
            shading_pass,
            resolve_pass,
            frame_infos,
        }
    }

    pub fn update_camera(&mut self, new_camera: Camera) {
        //Update primary_pass
        let new_camera: GpuCamera = new_camera.into_gpu_cam(
            self.shading_pass.data[0].shaded.extent().width,
            self.shading_pass.data[0].shaded.extent().height,
        );
        self.primary_pass.update_camera(new_camera.clone());
        self.direct_light.update_camera(new_camera);
    }

    pub fn final_image(&self, slot: usize) -> Arc<Image> {
        if let Some(resolve) = &self.resolve_pass {
            resolve.get_final_image(slot).unwrap()
        } else {
            self.shading_pass.data[slot].shaded.clone()
        }
    }

    fn record_pass(
        queues: &Queues,
        mut command_buffer: Arc<CommandBuffer>,
        slot: usize,
        pass: &mut impl Pass,
    ) -> Arc<CommandBuffer> {
        //Start Primary pass
        command_buffer = pass.pre(queues, command_buffer, slot);
        command_buffer = pass.record(command_buffer, slot);
        command_buffer = pass.post(queues, command_buffer, slot);

        command_buffer
    }

    pub fn update_buffers(&mut self, buffer: BufferStorage3d) {
        for d in &mut self.frame_infos {
            d.update_buffer = Some(buffer.clone());
        }
    }

    pub fn record(&mut self, queues: &Queues, slot: usize, location: IVec2) -> MarpTask {
        //Wait for the last frame on this slot if needed
        if let Some(inflight) = self.frame_infos[slot].in_flight.take() {
            inflight.wait(u64::MAX).unwrap();
        }

        //Reset command buffer
        let mut command_buffer = self.frame_infos[slot].command_buffer.clone();
        command_buffer.reset().unwrap();
        command_buffer
            .begin_recording(true, false, false, None)
            .unwrap();

        //Check if we should update the buffer. Note that the buffers are already in the correct
        //layout after upload by the buffer manager.
        if let Some(new_buffer) = self.frame_infos[slot].update_buffer.take() {
            self.primary_pass.update_buffer(slot, new_buffer.clone());
            self.direct_light.update_buffer(slot, new_buffer);
        }

        //Start by generating the primary pass calls
        command_buffer = Self::record_pass(queues, command_buffer, slot, &mut self.primary_pass);

        //Wait for primary to finish
        command_buffer
            .cmd_pipeline_barrier(
                PipelineStageFlags::COMPUTE_SHADER,
                PipelineStageFlags::COMPUTE_SHADER,
                DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    //Move primary to read only
                    self.primary_pass.data[slot].albedo.new_image_barrier(
                        Some(ImageLayout::GENERAL),
                        Some(ImageLayout::GENERAL), //Do not change layout
                        None,
                        None, //Do not change queue
                        Some(AccessFlags::SHADER_WRITE),
                        Some(AccessFlags::SHADER_READ),
                        None,
                    ),
                    self.primary_pass.data[slot].nrm_depth.new_image_barrier(
                        Some(ImageLayout::GENERAL),
                        Some(ImageLayout::GENERAL), //Do not change layout
                        None,
                        None, //Do not change queue
                        Some(AccessFlags::SHADER_WRITE),
                        Some(AccessFlags::SHADER_READ),
                        None,
                    ),
                ],
            )
            .unwrap();

        //Start Directl light pass
        command_buffer = Self::record_pass(queues, command_buffer, slot, &mut self.direct_light);

        //Wait for the direct light pass to finish. Then Transition the direct light image to write mode
        command_buffer
            .cmd_pipeline_barrier(
                PipelineStageFlags::COMPUTE_SHADER,
                PipelineStageFlags::COMPUTE_SHADER,
                DependencyFlags::empty(),
                vec![],
                vec![],
                vec![self.direct_light.data[slot].direct_light.new_image_barrier(
                    Some(ImageLayout::GENERAL),
                    Some(ImageLayout::GENERAL), //Do not change layout
                    None,
                    None, //Do not change queue
                    Some(AccessFlags::SHADER_READ),
                    Some(AccessFlags::SHADER_WRITE),
                    None,
                )],
            )
            .unwrap();

        command_buffer = Self::record_pass(queues, command_buffer, slot, &mut self.shading_pass);

        //Move remaining images back to write layout.
        command_buffer
            .cmd_pipeline_barrier(
                PipelineStageFlags::COMPUTE_SHADER,
                PipelineStageFlags::COMPUTE_SHADER,
                DependencyFlags::empty(),
                vec![],
                vec![],
                vec![
                    //Move primary to write only
                    self.primary_pass.data[slot].albedo.new_image_barrier(
                        Some(ImageLayout::GENERAL),
                        Some(ImageLayout::GENERAL), //Do not change layout
                        None,
                        None, //Do not change queue
                        Some(AccessFlags::SHADER_READ),
                        Some(AccessFlags::SHADER_WRITE),
                        None,
                    ),
                    self.primary_pass.data[slot].nrm_depth.new_image_barrier(
                        Some(ImageLayout::GENERAL),
                        Some(ImageLayout::GENERAL), //Do not change layout
                        None,
                        None, //Do not change queue
                        Some(AccessFlags::SHADER_READ),
                        Some(AccessFlags::SHADER_WRITE),
                        None,
                    ),
                    self.direct_light.data[slot].direct_light.new_image_barrier(
                        Some(ImageLayout::GENERAL),
                        Some(ImageLayout::GENERAL), //Do not change layout
                        None,
                        None, //Do not change queue
                        Some(AccessFlags::SHADER_READ),
                        Some(AccessFlags::SHADER_WRITE),
                        None,
                    ),
                ],
            )
            .unwrap();

        //if there is a resolve involved, wait for rendering, resolve image and transition images back into correct layout
        if let Some(resolve_pass) = &mut self.resolve_pass {
            //Wait for rendering of primary pass, then transition image to read mode for resolve pass
            command_buffer
                .cmd_pipeline_barrier(
                    PipelineStageFlags::COMPUTE_SHADER,
                    PipelineStageFlags::COMPUTE_SHADER,
                    DependencyFlags::empty(),
                    vec![],
                    vec![],
                    vec![self
                        .shading_pass
                        .get_final_image(slot)
                        .unwrap()
                        .new_image_barrier(
                            Some(ImageLayout::GENERAL),
                            Some(ImageLayout::GENERAL),
                            None,
                            None,
                            Some(AccessFlags::SHADER_WRITE),
                            Some(AccessFlags::SHADER_READ),
                            None,
                        )],
                )
                .unwrap();

            //execute resolve pass
            command_buffer = Self::record_pass(queues, command_buffer, slot, resolve_pass);

            //Wait for resolve pass, transition primary back to write mode (assumed by the pass)
            command_buffer
                .cmd_pipeline_barrier(
                    PipelineStageFlags::COMPUTE_SHADER,
                    PipelineStageFlags::COMPUTE_SHADER,
                    DependencyFlags::empty(),
                    vec![],
                    vec![],
                    vec![self
                        .shading_pass
                        .get_final_image(slot)
                        .unwrap()
                        .new_image_barrier(
                            Some(ImageLayout::GENERAL),
                            Some(ImageLayout::GENERAL),
                            None,
                            None,
                            Some(AccessFlags::SHADER_READ),
                            Some(AccessFlags::SHADER_WRITE),
                            None,
                        )],
                )
                .unwrap();
        }

        //At this point the shaded image is finished
        command_buffer.end_recording().unwrap();

        //Submit new frame and let it signal our Semaphore when finished
        let new_in_flight = queues
            .graphics_queue
            .queue_submit(vec![SubmitInfo::new(
                vec![], //wait for direct light as well as primary pass
                vec![command_buffer],
                vec![self.frame_infos[slot].finished_execution.clone()], //Signal execution semaphore
            )])
            .unwrap();

        self.frame_infos[slot].in_flight = Some(new_in_flight);

        //select correct image and extent based on sampling
        let (image, extent) = if let Some(resolve) = &self.resolve_pass {
            let img = resolve.get_final_image(slot).unwrap();
            let ext = img.extent();
            (img, ext)
        } else {
            let img = self.shading_pass.get_final_image(slot).unwrap();
            let ext = img.extent();
            (img, ext)
        };

        MarpTask::Rendering {
            image,
            extent: (extent.width, extent.height).into(),
            location,
            sem: self.frame_infos[slot].finished_execution.clone(),
            stage: PipelineStageFlags::COMPUTE_SHADER,
        }
    }
}

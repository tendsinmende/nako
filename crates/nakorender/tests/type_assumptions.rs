//!Contains some compile tests that should be kept between versions.

use nakorender::cpu::CpuBackend;
use nakorender::marp::MarpBackend;
use static_assertions::assert_impl_all;

#[test]
fn backends_sendable() {
    assert_impl_all!(CpuBackend: Send);
    assert_impl_all!(MarpBackend: Send);
}

use std::path::PathBuf;

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

fn main() {
    // Tell Cargo that if the given file changes, to rerun this build script.
    //println!("cargo:rerun-if-changed=../");
    //NOTE: currently checking if the spirv directory exists and is non empty.
    //Later probably should verify that the correct shader files exist.
    //however, while transforming the renderer often that's not really useful.

    let spirv_location = shader_shared::access::get_shader_spirv_location();
    let should_build = {
        if spirv_location.exists() {
            match std::fs::read_dir(&spirv_location) {
                Ok(dir) => {
                    //Check if anything is inside (later check for file names)
                    if dir.collect::<Vec<_>>().len() > 0 {
                        //Exists and is not empty
                        false
                    } else {
                        //is empty, need to recompile
                        println!("cargo:warning=spirv directory is empty, recompiling");
                        true
                    }
                }
                Err(e) => {
                    println!("cargo:warning=could not read spirv dir: \n{}\nTrying recompilation of shaders!", e);
                    true
                }
            }
        } else {
            //Directory does not exist
            println!("cargo:warning=spirv directory does not exist, recompiling");
            true
        }
    };

    //If should rebuild, build rust and glsl shaders
    if should_build {
        println!("cargo:warning=recompiling shaders, if this is the first time this might take up to 10 min. !");

        let execution_path = PathBuf::from(env!("CARGO_MANIFEST_DIR"))
            .join("../../")
            .canonicalize()
            .unwrap();

        println!("cargo:warning=compiling @ {:?}!", execution_path);
        //Excute command that builds all rust and glsl shaders
        let res = std::process::Command::new("cargo")
            .current_dir(execution_path)
            .arg("run")
            .arg("--bin")
            .arg("nako_shader_builder")
            .arg("--release")
            .arg("--")
            .arg("--glsl")
            .output();

        if res.is_err() {
            panic!(
                "Could not build shaders, compiler exited with:\n\n{:?}",
                res
            );
        } else {
            let res = res.unwrap();
            let out_str = core::str::from_utf8(&res.stdout).unwrap();
            let err_str = core::str::from_utf8(&res.stderr).unwrap();
            //Check for errors in ouput
            //HACKY: This is hacky, but currently the best we got :D
            if out_str.contains("error")
                || out_str.contains("Error")
                || err_str.contains("error")
                || err_str.contains("Error")
            {
                panic!(
                    "Failed to build nako shaders with:\nstdout: {}\nstderr: {}",
                    out_str, err_str
                );
            }
            println!(
                "cargo:warning=finished successfuly with:\n------\n {} \n------",
                out_str
            );
        }

        println!("cargo::warning=finished compilation of shaders!");
    } else {
        println!("cargo:warning=not compiling shaders!");
    }
}

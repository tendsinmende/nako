/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![deny(warnings)]

use glam::{Mat4, Vec2, Vec3};

#[cfg(feature = "serialize")]
use serde::{Deserialize, Serialize};

pub use nako_instruction::NakoResult;

///Boundingbox, might contain infinit ranges. For instance a plane on xy is only bound in hight, but not in width or length.
///
///Note that for 2d calculations only x/y axis are considered.

#[cfg(feature = "serialize")]
#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Debug)]
pub struct BBox {
    pub min: Vec3,
    pub max: Vec3,
}

impl BBox {
    pub fn new_2d(min: Vec2, max: Vec2) -> Self {
        BBox {
            min: min.extend(0.0),
            max: max.extend(0.0),
        }
    }

    ///Returns true if min and maxs z axis have the same value
    #[inline]
    pub fn is_2d(&self) -> bool {
        self.min.z == self.max.z
    }

    pub fn union(self, other: Self) -> Self {
        BBox {
            min: self.min.min(other.min),
            max: self.max.max(other.max),
        }
    }

    ///Returns the intersection volume if there is any.
    pub fn intersection(self, other: Self) -> Option<Self> {
        let new = BBox {
            min: Vec3::new(
                self.min.x.max(other.min.x),
                self.min.y.max(other.min.y),
                self.min.z.max(other.min.z),
            ),
            max: Vec3::new(
                self.max.x.min(other.max.x),
                self.max.y.min(other.max.y),
                self.max.z.min(other.max.z),
            ),
        };

        if new.min.x > new.max.x || new.min.y > new.max.y || new.min.z > new.max.z {
            None
        } else {
            Some(new)
        }
    }

    ///Sphere | point intersection
    pub fn is_point_intersecting(&self, location: Vec3) -> bool {
        location.x > self.min.x
            && location.y > self.min.y
            && location.z > self.min.z
            && location.x < self.max.x
            && location.y < self.max.y
            && location.z < self.max.z
    }

    ///Sphere | Box intersection test
    pub fn is_sphere_intersecting(&self, location: Vec3, radius: f32) -> bool {
        let closest = self.min.max(location.min(self.max));
        let dist = ((closest.x - location.x) * (closest.x - location.x)
            + (closest.y - location.y) * (closest.y - location.y)
            + (closest.z - location.z) * (closest.z - location.z))
            .sqrt();

        dist < radius
    }

    ///Grows self by subtracting `addition` from min, and adding it to max.
    pub fn grow_even(&mut self, addition: Vec3) {
        self.min -= addition;
        self.max += addition;
    }

    pub fn is_intersecting(&self, other: &Self) -> bool {
        self.intersection(*other).is_some()
    }

    pub fn extent(&self) -> Vec3 {
        self.max - self.min
    }

    pub fn center(&self) -> Vec3 {
        self.min + (self.extent() / 2.0)
    }

    pub fn transform(&mut self, trans: Mat4) {
        let nmin = trans * self.min.extend(1.0);
        let nmax = trans * self.max.extend(1.0);

        self.min = Vec3::new(nmin.x.min(nmax.x), nmin.y.min(nmax.y), nmin.z.min(nmax.z));
        self.max = Vec3::new(nmin.x.max(nmax.x), nmin.y.max(nmax.y), nmin.z.max(nmax.z));
    }

    pub fn is_finite(&self) -> bool {
        self.min.is_finite() && self.max.is_finite()
    }
}

impl Default for BBox {
    fn default() -> Self {
        BBox {
            min: Vec3::ZERO,
            max: Vec3::ZERO,
        }
    }
}

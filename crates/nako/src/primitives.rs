/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//!Contains basic primitives that define, generic over any return type, how an surface is found.
use crate::{
    sdf::BBox,
    serialize::{OpTy, StreamSerializer, Word},
    stream::{PrimaryStream2d, Primitive, Primitive2d},
};
use glam::{Vec2, Vec3};

///Sphere with its center at (0,0,0) and the given radius.
#[derive(Clone, Copy, Debug)]
pub struct Sphere {
    pub radius: f32,
}
///A Sphere in 2D is also referred to as a Circle. Therefore the type alias. However, internally they are treated the same.
pub type Circle = Sphere;
impl Primitive for Sphere {
    fn bound(&self) -> BBox {
        BBox {
            min: Vec3::new(-self.radius, -self.radius, -self.radius),
            max: Vec3::new(self.radius, self.radius, self.radius),
        }
    }

    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Sphere as u32);
        ser.push_f32(self.radius);
    }
}

impl Primitive2d for Sphere {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Sphere as Word);
        ser.push_f32(self.radius);
    }

    fn bound(&self) -> BBox {
        BBox::new_2d(
            Vec2::new(-self.radius, -self.radius),
            Vec2::new(self.radius, self.radius),
        )
    }
}

///Simple box (cuboid) laying at (0,0)/(0,0,0) going from -extent to +extent
#[derive(Clone, Copy)]
pub struct Cuboid<T> {
    pub extent: T,
}

impl Primitive for Cuboid<Vec3> {
    fn bound(&self) -> BBox {
        BBox {
            max: self.extent.abs(),
            min: -1.0 * self.extent.abs(),
        }
    }

    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Cuboid as u32);
        ser.push_vec3(self.extent.abs());
    }
}

impl Primitive2d for Cuboid<Vec2> {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Cuboid as Word);
        ser.push_f32(self.extent.x);
        ser.push_f32(self.extent.y);
    }

    fn bound(&self) -> BBox {
        BBox::new_2d(self.extent.abs() * -1.0, self.extent.abs())
    }
}

///Infinit plane facing `normal`. Placed at `hight` on this normal in space. Has an infinit bounding
///box, execpt if one axis on `normal` is 1 and the rest is 0.
#[derive(Clone, Copy)]
pub struct Plane<N, H> {
    ///Vec3 denoting normal of this plane
    pub normal: N,
    ///location on the y axis in space
    pub height: H,
}

impl Plane<Vec3, f32> {
    pub fn bounding_box(&self) -> BBox {
        //For a plane there are two cases. Either the plane has an ideal direction into +/- x,y,z
        //Or is somehow oriented. In the former case it has no hight on its normal axis, and infinit width/length
        //on its plane axis.

        //In the second case it is infinit in each direction

        //Note matching on floats, however, we can only have a plane on one of the axis aligned planes
        //if two values are zero. So this shouldnt be a problem in floating point land.
        match (
            self.normal.x == 0.0,
            self.normal.y == 0.0,
            self.normal.z == 0.0,
        ) {
            (false, true, true) => {
                //Plane on yz
                BBox {
                    min: Vec3::new(self.height, f32::NEG_INFINITY, f32::NEG_INFINITY),
                    max: Vec3::new(self.height, f32::INFINITY, f32::INFINITY),
                }
            }
            (true, false, true) => {
                //Plane on xz
                BBox {
                    min: Vec3::new(f32::NEG_INFINITY, self.height, f32::NEG_INFINITY),
                    max: Vec3::new(f32::INFINITY, self.height, f32::INFINITY),
                }
            }
            (true, true, false) => {
                //Plane on xy
                BBox {
                    min: Vec3::new(f32::NEG_INFINITY, f32::NEG_INFINITY, self.height),
                    max: Vec3::new(f32::INFINITY, f32::INFINITY, self.height),
                }
            }
            _ => {
                //Something else
                BBox {
                    min: Vec3::new(f32::NEG_INFINITY, f32::NEG_INFINITY, f32::NEG_INFINITY),
                    max: Vec3::new(f32::INFINITY, f32::INFINITY, f32::INFINITY),
                }
            }
        }
    }
}

impl Primitive for Plane<Vec3, f32> {
    fn bound(&self) -> BBox {
        self.bounding_box()
    }

    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Plane as u32);
        ser.push_vec3(self.normal.normalize());
        ser.push_f32(self.height.abs());
    }
}

///Line from a to b with a start and end point.
#[derive(Clone, Copy)]
pub struct Line<T> {
    pub start: T,
    pub end: T,
    pub width: f32,
}

impl Primitive for Line<Vec3> {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Line as Word);
        ser.push_vec3(self.start);
        ser.push_vec3(self.end);
        ser.push_f32(self.width);
    }

    fn bound(&self) -> BBox {
        let width = Vec3::new(self.width, self.width, self.width);
        BBox {
            min: (self.start - width).min(self.end - width),
            max: (self.start + width).max(self.end + width),
        }
    }
}

impl Primitive2d for Line<Vec2> {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Line as Word);
        ser.push_f32(self.start.x);
        ser.push_f32(self.start.y);
        ser.push_f32(self.end.x);
        ser.push_f32(self.end.y);
        ser.push_f32(self.width);
    }

    fn bound(&self) -> BBox {
        BBox::new_2d(
            Vec2::new(self.start.x.min(self.end.x), self.start.y.min(self.end.y)),
            Vec2::new(self.start.x.max(self.end.x), self.start.y.max(self.end.y)),
        )
    }
}

///Triangle made from the given vertices.
#[derive(Clone, Copy)]
pub struct Triangle<T> {
    pub vertices: [T; 3],
}
impl Primitive for Triangle<Vec3> {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Triangle as Word);
        ser.push_vec3(self.vertices[0]);
        ser.push_vec3(self.vertices[1]);
        ser.push_vec3(self.vertices[2]);
    }

    fn bound(&self) -> BBox {
        BBox {
            min: self.vertices[0].min(self.vertices[1]).min(self.vertices[2]),
            max: self.vertices[0].max(self.vertices[1]).max(self.vertices[2]),
        }
    }
}

impl Primitive2d for Triangle<Vec2> {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Triangle as Word);
        ser.push_vec2(self.vertices[0]);
        ser.push_vec2(self.vertices[1]);
        ser.push_vec2(self.vertices[2]);
    }

    fn bound(&self) -> BBox {
        BBox::new_2d(
            self.vertices[0].min(self.vertices[1]).min(self.vertices[2]),
            self.vertices[0].max(self.vertices[1]).max(self.vertices[2]),
        )
    }
}

///Polygone made from vertices. Is always closed.
#[derive(Clone)]
pub struct Polygon<T> {
    pub vertices: Vec<T>,
}

impl Primitive2d for Polygon<Vec2> {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Polygon as Word);
        ser.push_word(self.vertices.len() as Word);
        for v in &self.vertices {
            ser.push_f32(v.x);
            ser.push_f32(v.y);
        }
    }

    fn bound(&self) -> BBox {
        let mut min = Vec2::new(f32::INFINITY, f32::INFINITY);
        let mut max = Vec2::new(f32::NEG_INFINITY, f32::NEG_INFINITY);

        for vert in &self.vertices {
            min = min.min(*vert);
            max = max.max(*vert);
        }

        BBox::new_2d(min, max)
    }
}

#[derive(Clone, Copy)]
pub struct QuadricBezier<T> {
    pub start: T,
    ///Controll point through which the line travels from start to end
    pub controll: T,
    pub end: T,
    pub width: f32,
}

impl Primitive2d for QuadricBezier<Vec2> {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::QuadricBezier as Word);
        ser.push_f32(self.start.x);
        ser.push_f32(self.start.y);

        ser.push_f32(self.controll.x);
        ser.push_f32(self.controll.y);

        ser.push_f32(self.end.x);
        ser.push_f32(self.end.y);

        ser.push_f32(self.width);
    }

    //TODO lazy, therfore too big.
    fn bound(&self) -> BBox {
        let min = Vec2::new(
            self.start.x.min(self.controll.x).min(self.end.x) - self.width,
            self.start.y.min(self.controll.y).min(self.end.y) - self.width,
        );

        let max = Vec2::new(
            self.start.x.max(self.controll.x).max(self.end.x) + self.width,
            self.start.y.max(self.controll.y).max(self.end.y) + self.width,
        );

        BBox::new_2d(min, max)
    }
}

///Projects some 2d planar sdf onto a plane in space.
#[derive(Clone)]
pub struct PlanarProjection {
    pub stream: PrimaryStream2d,
}

impl Primitive for PlanarProjection {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::PlanarProjection as Word);
        ser.push_word(self.stream.stream.len() as Word);
        ser.clone_stream(&self.stream.stream);
    }

    fn bound(&self) -> BBox {
        //Note that the projection is on the x/z plane
        BBox {
            min: Vec3::new(self.stream.bound.min.x, 0.0, self.stream.bound.min.y),
            max: Vec3::new(self.stream.bound.max.x, 0.0, self.stream.bound.max.y),
        }
    }
}

#[derive(Clone)]
pub struct Extrusion {
    pub stream: PrimaryStream2d,
    pub length: f32,
}

impl Primitive for Extrusion {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Extrude as Word);
        ser.push_f32(self.length);
        ser.push_word(self.stream.stream.len() as u32);
        ser.clone_stream(&self.stream.stream);
    }

    fn bound(&self) -> BBox {
        let mut b = self.stream.bound;
        b.max.z = self.length;
        b.min.z = 0.0; // I guess?
        b
    }
}

#[derive(Clone)]
pub struct Revolution {
    pub stream: PrimaryStream2d,
    ///Offset for the revolutions center. Must be positive
    pub offset: f32,
}

impl Primitive for Revolution {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Revolution as Word);
        ser.push_f32(self.offset);
        ser.push_word(self.stream.stream.len() as Word);
        ser.clone_stream(&self.stream.stream);
    }

    fn bound(&self) -> BBox {
        //Half bound @ offset = 0.0, otherwise adding offset
        BBox {
            min: Vec3::new(
                self.stream.bound.min.x - self.offset, //Winding the x min around 0.0
                self.stream.bound.min.y, //The y extend on the xy plane of this object is our hight
                self.stream.bound.min.x - self.offset, //Sames as x since we are winding around 0,0 on the xy plane.
            ),
            max: Vec3::new(
                self.stream.bound.max.x + self.offset, //Winding the x min around 0.0
                self.stream.bound.max.y, //The y extend on the xy plane of this object is our hight
                self.stream.bound.max.x + self.offset, //Sames as x since we are winding around 0,0 on the xy plane.
            ),
        }
    }
}

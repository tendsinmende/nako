/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//!
//! # Nako
//!
//! The main crate of nako has two focus points. The definition of signed distance functions (sdf), and compiling them
//! into a instruction stream that can be interpreted by some backend/evaluator.
//!
//! # Operations
//! The supported operations are within the `operations` module. They are split in `planar` for 2d operations, as well
//! as `volumetric` for 3d. 2d operations can be projected into 3d via `PlanarProjection`, `Extrusion` and `Revolution`.
//!
//! There are three different types of operations.
//!
//! 1. Primitives: Some function that defines a sdf. For instance a box or a sphere.
//! 2. Modifier: Modiefies a result withn a sdf. This could change the color of some box, or translate it in space.
//! 3. Combinator: Combines some Primitive with applied modifiers with the rest of the scene. For insance by adding it,
//! or by subtracting it.
//!
//! # Serializing
//! When combining the sdf functions a set of Builder types is used. In general an sdf consists of a `PrimaryStream` that is
//! build from many `SecondaryStream`s. A Secondary stream is any Primitive, a set of modifiers and an Combinator that is used
//! to combine this secondary stream with the scene.
//!
//! ```rust
//! //TODO write example
//! ```
#[deny(warnings)]
///Linear algebra crate used
pub extern crate glam;
///Main definition of operations as well as the sdf itself.
pub mod sdf;

///Operations that can be used to build an sdf tree
//pub mod operations;

///Serialization related primitives. Contains the OpCodes as well as the internaly used Serializer.
pub mod serialize;

///Contains the stream builders. Used for easy building a serialized primary stream out of secondary streams.
pub mod stream;

///Contains all primitives that can be added to a secondary stream.
pub mod primitives;

///Set of modifiers that can be applied to any `SecondaryStream`
pub mod modifiers;

///All combinators that can be used to combine a secondary stream with a primary stream.
pub mod combinators;

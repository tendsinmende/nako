/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use glam::Vec3;

use crate::{
    sdf::BBox,
    serialize::{OpTy, StreamSerializer, Word},
    stream::{Combinator, Combinator2d},
};

///The skip operator signals the bound of the next `skip_to` words. If the currently queried object, for instance a ray, lies not
///within this bound, the instruction pointer can safely skip `skip_to` instructions.
#[derive(Clone, Copy)]
pub struct SkipOperator {
    pub bound: BBox,
    pub skip_to: u32,
}

//Note: implemented as combinator, since those operate on the highest instruction set level.
impl Combinator for SkipOperator {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Skip as u32);
        ser.push_vec3(self.bound.min);
        ser.push_vec3(self.bound.max);
        ser.push_word(self.skip_to);
    }
}

///Composes two expressions by taking the minimum
#[derive(Clone, Copy)]
pub struct Union;
impl Combinator for Union {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Union as Word);
    }
}
impl Combinator2d for Union {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Union as Word);
    }
}

///Composes two expressions by taking the minimum, smoothes fields in the given radius.
#[derive(Clone, Copy)]
pub struct SmoothUnion(pub f32);
impl Combinator for SmoothUnion {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothUnion as Word);
        ser.push_f32(self.0);
    }

    fn bound_modifier(&self, secondary_bound: &mut BBox) {
        //Need to grow the bound since the radius in which we are changed grows as well
        secondary_bound.grow_even(Vec3::new(self.0, self.0, self.0));
    }
}
impl Combinator2d for SmoothUnion {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothUnion as Word);
        ser.push_f32(self.0);
    }
}

///Composes two expressions by taking the minimum
#[derive(Clone, Copy)]
pub struct Subtraction;
impl Combinator for Subtraction {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Subtraction as Word);
    }
    fn bound_modifier(&self, _secondary_bound: &mut BBox) {
        //TODO implement
    }
}
impl Combinator2d for Subtraction {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Subtraction as Word);
    }
}

///Composes two expressions by taking the minimum smoothing fields within the given radius.
#[derive(Clone, Copy)]
pub struct SmoothSubtraction(pub f32);
impl Combinator for SmoothSubtraction {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothSubtraction as Word);
        ser.push_f32(self.0);
    }

    fn bound_modifier(&self, _secondary_bound: &mut BBox) {
        //TODO Implement modifier, however all we know at this point is, that the bound isnt getting bigger...
    }
}
impl Combinator2d for SmoothSubtraction {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothSubtraction as Word);
        ser.push_f32(self.0);
    }
}

///Composes two expressions by taking the minimum
#[derive(Clone, Copy)]
pub struct Intersect;
impl Combinator for Intersect {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Intersection as Word);
    }

    fn bound_modifier(&self, _secondary_bound: &mut BBox) {
        //TODO implement anything?
    }
}
impl Combinator2d for Intersect {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Intersection as Word);
    }
}

///Composes two expressions by taking the minimum, smoothes fields in the given radius.
#[derive(Clone, Copy)]
pub struct SmoothIntersect(pub f32);
impl Combinator for SmoothIntersect {
    fn serialize(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothIntersection as Word);
        ser.push_f32(self.0);
    }

    fn bound_modifier(&self, secondary_bound: &mut BBox) {
        secondary_bound.grow_even(Vec3::new(self.0, self.0, self.0));
    }
}
impl Combinator2d for SmoothIntersect {
    fn ser(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::SmoothIntersection as Word);
        ser.push_f32(self.0);
    }
}

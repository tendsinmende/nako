/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! TODO: Explain why we are not using serde. Mostly because at runtime we don't know what to expect, so the serialized data has to know which types to build.
//!

use crate::{sdf::BBox, stream::TipInfo};
use glam::{Mat4, Vec3};
use glam::{Vec2, Vec4};

///Reexport of the instruction codes
pub use nako_instruction::OpTy;
///Reexport of a single instruction word.
pub use nako_instruction::Word;

pub struct StreamSerializer {
    pub bbox: BBox,
    pub buffer: Vec<Word>,
}

impl StreamSerializer {
    pub fn new() -> Self {
        StreamSerializer {
            bbox: BBox {
                min: Vec3::ZERO,
                max: Vec3::ZERO,
            },
            buffer: Vec::new(),
        }
    }

    pub fn push_word(&mut self, word: Word) {
        self.buffer.push(word);
    }

    pub fn push_f32(&mut self, f: f32) {
        self.push_word(f.to_bits());
    }

    pub fn push_vec2(&mut self, v: Vec2) {
        self.push_f32(v.x);
        self.push_f32(v.y);
    }

    pub fn push_vec3(&mut self, v: Vec3) {
        self.push_f32(v.x);
        self.push_f32(v.y);
        self.push_f32(v.z);
    }

    pub fn push_vec4(&mut self, v: Vec4) {
        self.push_f32(v.x);
        self.push_f32(v.y);
        self.push_f32(v.z);
        self.push_f32(v.w);
    }

    ///Pushes a 4x4 matrix in col order
    pub fn push_matrix(&mut self, m: Mat4) {
        let ordered = m.to_cols_array();
        for v in &ordered {
            self.push_f32(*v);
        }
    }

    ///Appends the words of `other` directly. Note that it does not check the bounds.
    pub(crate) fn append_stream(&mut self, mut other: Vec<Word>) {
        self.buffer.append(&mut other)
    }

    pub(crate) fn clone_stream(&mut self, other: &Vec<Word>) {
        self.buffer.append(&mut other.clone())
    }

    pub fn consume(self) -> (TipInfo, Vec<Word>) {
        (TipInfo { bbox: self.bbox }, self.buffer)
    }

    pub fn on_bbox<F>(&mut self, f: F)
    where
        F: FnOnce(&mut BBox),
    {
        f(&mut self.bbox);
    }
}

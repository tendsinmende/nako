/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::{
    combinators::SkipOperator,
    sdf::BBox,
    serialize::{StreamSerializer, Word},
};

use glam::Vec3;
#[cfg(feature = "serialize")]
use serde::{Deserialize, Serialize};

///Must be implemented by any modifier, that assumes an already present Primitve in the stream.
pub trait Modifier {
    ///Used to enqueue instructions before calling the childs instructions
    fn pre(&self, _ser: &mut StreamSerializer) {}
    ///Used to enqueue instuctions after calling the childs instructions
    fn post(&self, _ser: &mut StreamSerializer) {}
}

pub trait Primitive {
    fn bound(&self) -> BBox;
    fn serialize(&self, ser: &mut StreamSerializer);
}

pub trait Combinator {
    fn serialize(&self, ser: &mut StreamSerializer);
    ///Is called after assembling the secondary stream. Might be used to adjust the bound based on the
    ///combinator
    fn bound_modifier(&self, _secondary_bound: &mut BBox) {}
}

#[cfg(feature = "serialize")]
#[derive(Serialize, Deserialize, Clone)]
pub struct TipInfo {
    pub bbox: BBox,
}

///Secondary stream. Is build from a linear stream of secondary operations. Mostly primitives and attributes, like translation or color.
#[cfg(feature = "serde")]
#[derive(Serialize, Deserialize, Clone)]
pub struct SecondaryStream {
    bound: BBox,
    substream: Vec<Word>,
}

impl SecondaryStream {
    pub fn new<C, P>(combinator: C, primitive: P) -> SecondaryStreamBuilder
    where
        C: Combinator + 'static,
        P: Primitive + 'static,
    {
        SecondaryStreamBuilder {
            primitive: Box::new(primitive),
            combinator: Box::new(combinator),
            mod_stream: Vec::new(),
        }
    }
}

pub struct SecondaryStreamBuilder {
    ///Base primitive for this secondary stream
    primitive: Box<dyn Primitive + 'static>,
    ///ombinator used for thi secondary stream to be added to the primary stream,
    combinator: Box<dyn Combinator + 'static>,
    ///All modifiers in order, where [0] is the first after the primitve and [len] is the last modifier applied, before
    ///merging with the primary stream.
    mod_stream: Vec<Box<dyn Modifier + 'static>>,
}

impl SecondaryStreamBuilder {
    pub fn push_mod<M>(mut self, attr: M) -> Self
    where
        M: Modifier + 'static,
    {
        self.mod_stream.push(Box::new(attr));
        self
    }

    pub fn build(self) -> SecondaryStream {
        let mut ser = StreamSerializer::new();

        for m in self.mod_stream.iter().rev() {
            m.pre(&mut ser);
        }

        //setup initial bounding box
        ser.on_bbox(|bbox| *bbox = self.primitive.bound());

        self.primitive.serialize(&mut ser);
        for m in self.mod_stream.iter() {
            m.post(&mut ser);
        }

        //let the combinator modify the tip info
        self.combinator.bound_modifier(&mut ser.bbox);
        //Now serialize the combinator as well
        self.combinator.serialize(&mut ser);
        //consume serialize, read bounds, prepare skip operator and prepend whole
        //sub stream with skip operator
        let (tip_info, substream) = ser.consume();

        let mut ser = StreamSerializer::new();
        ser.bbox = tip_info.bbox;
        SkipOperator {
            bound: tip_info.bbox,
            skip_to: substream.len() as u32,
        }
        .serialize(&mut ser);
        ser.append_stream(substream);

        let (tip_info, substream) = ser.consume();

        SecondaryStream {
            bound: tip_info.bbox,
            substream,
        }
    }
}

#[derive(Clone)]
pub struct PrimaryStreamBuilder {
    primary_stream: Vec<SecondaryStream>,
}

impl PrimaryStreamBuilder {
    pub fn push(mut self, secondary_stream: SecondaryStream) -> Self {
        self.primary_stream.push(secondary_stream);
        self
    }

    ///We build the stream by prepending each SecondaryStream with a skip operation pointing to the next secondary stream.
    ///We also keep track of the global bounds. Those are prepended to the whole stream which
    ///makes it possible for any implementation to skip the whole stream completely when not querying
    ///"inside" the volume.
    pub fn build(self) -> PrimaryStream {
        let (global_stream, infinite_bound, finite_bound) = {
            //catch empty streams. in that case, make a minimal bounding box that is serialized
            //and return
            if self.primary_stream.len() == 0 {
                let mut ser = StreamSerializer::new();
                SkipOperator {
                    bound: BBox {
                        min: Vec3::splat(f32::NEG_INFINITY),
                        max: Vec3::splat(f32::NEG_INFINITY),
                    },
                    skip_to: 0,
                }
                .serialize(&mut ser);

                let (tip, stream) = ser.consume();
                (stream, tip.bbox, None)
            } else {
                let (alloc, infinite_bound, finite_bound): (usize, BBox, Option<BBox>) =
                    self.primary_stream.iter().fold(
                        (
                            0,
                            BBox {
                                min: Vec3::splat(f32::INFINITY),
                                max: Vec3::splat(f32::NEG_INFINITY),
                            },
                            None,
                        ),
                        |(size, infinite_bbox, finite_bbox), stream| {
                            //Check if the bound is finite.
                            if stream.bound.is_finite() {
                                //If we already got a finite one, union both,
                                //otherwise create a new one
                                if let Some(finite_bbox) = finite_bbox {
                                    (
                                        size + stream.substream.len(),
                                        infinite_bbox.union(stream.bound),
                                        Some(finite_bbox.union(stream.bound)),
                                    )
                                } else {
                                    (
                                        size + stream.substream.len(),
                                        infinite_bbox.union(stream.bound),
                                        Some(stream.bound),
                                    )
                                }
                            } else {
                                //just update the infinite part
                                (
                                    size + stream.substream.len(),
                                    infinite_bbox.union(stream.bound),
                                    finite_bbox,
                                )
                            }
                        },
                    );

                let mut inlined_stream = Vec::with_capacity(alloc);
                inlined_stream = self.primary_stream.into_iter().fold(
                    inlined_stream,
                    |mut instream, mut stream| {
                        instream.append(&mut stream.substream);
                        instream
                    },
                );

                (inlined_stream, infinite_bound, finite_bound)
            }
        };

        PrimaryStream {
            stream: global_stream,
            finite_bound,
            infinite_bound,
        }
    }
}

#[cfg(feature = "serialize")]
#[derive(Serialize, Deserialize, Clone)]
pub struct PrimaryStream {
    ///Gpu uploadable buffer. Contains a mixture of BVH nodes and instuction streams.
    pub stream: Vec<Word>,
    ///The bound of all finite primitives. This bound might be different from `infinit_bound` if the stream contains infinit
    /// secondary streams, like a plane.
    ///
    ///There might not be such a bound if the sdf contains no bound primitives.
    pub finite_bound: Option<BBox>,
    pub infinite_bound: BBox,
}

impl PrimaryStream {
    pub fn new() -> PrimaryStreamBuilder {
        PrimaryStreamBuilder {
            primary_stream: Vec::new(),
        }
    }

    pub fn empty() -> Self {
        PrimaryStream {
            stream: vec![],
            finite_bound: None,
            infinite_bound: BBox {
                min: Vec3::splat(f32::INFINITY),
                max: Vec3::splat(f32::NEG_INFINITY),
            },
        }
    }
}

pub trait Primitive2d {
    fn ser(&self, _ser: &mut StreamSerializer);
    fn bound(&self) -> BBox;
}

pub trait Combinator2d {
    fn ser(&self, _ser: &mut StreamSerializer);
    fn bound_modifier(&self, _bound: &mut BBox) {}
}

pub trait Modifier2d {
    fn pre(&self, _ser: &mut StreamSerializer) {}
    fn post(&self, _ser: &mut StreamSerializer) {}
}

///Same as the normal secondary stream, but only for 2d primitives and mods
#[derive(Clone)]
pub struct SecondaryStream2d {
    pub stream: Vec<Word>,
    pub bound: BBox,
}

impl SecondaryStream2d {
    pub fn new<C, P>(combinator: C, primitive: P) -> Secondary2dBuilder
    where
        C: Combinator2d + 'static,
        P: Primitive2d + 'static,
    {
        Secondary2dBuilder {
            primitive: Box::new(primitive),
            combinator: Box::new(combinator),
            modifier: vec![],
        }
    }

    pub fn code_len(&self) -> usize {
        self.stream.len()
    }
}

pub struct Secondary2dBuilder {
    primitive: Box<dyn Primitive2d>,
    combinator: Box<dyn Combinator2d>,
    modifier: Vec<Box<dyn Modifier2d>>,
}

impl Secondary2dBuilder {
    pub fn push_mod<M>(mut self, modifier: M) -> Self
    where
        M: Modifier2d + 'static,
    {
        self.modifier.push(Box::new(modifier));
        self
    }

    pub fn build(self) -> SecondaryStream2d {
        let mut ser = StreamSerializer::new();
        for m in &self.modifier {
            m.pre(&mut ser);
        }
        self.primitive.ser(&mut ser);
        for m in &self.modifier {
            m.post(&mut ser);
        }
        self.combinator.bound_modifier(&mut ser.bbox);
        self.combinator.ser(&mut ser);

        let (tipinfo, stream) = ser.consume();

        SecondaryStream2d {
            stream,
            bound: tipinfo.bbox,
        }
    }
}

///Primary stream for planar sdfs
#[derive(Clone)]
pub struct PrimaryStream2d {
    //The whole 2d stream
    pub stream: Vec<Word>,
    pub bound: BBox,
}

impl PrimaryStream2d {
    pub fn new() -> Primary2dBuilder {
        Primary2dBuilder { subs: Vec::new() }
    }
}

pub struct Primary2dBuilder {
    subs: Vec<SecondaryStream2d>,
}

impl Primary2dBuilder {
    pub fn push(mut self, s: SecondaryStream2d) -> Self {
        self.subs.push(s);
        self
    }

    pub fn build(self) -> PrimaryStream2d {
        let mut stream = Vec::with_capacity(self.subs.iter().fold(0, |l, c| l + c.code_len()));

        let mut bound = if let Some(first_stream) = self.subs.first() {
            first_stream.bound
        } else {
            BBox::default()
        };

        for mut s in self.subs {
            bound = bound.union(s.bound);
            stream.append(&mut s.stream);
        }

        PrimaryStream2d { stream, bound }
    }
}

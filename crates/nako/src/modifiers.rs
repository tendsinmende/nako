/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::{
    serialize::{OpTy, StreamSerializer, Word},
    stream::{Modifier, Modifier2d},
};
use glam::{Mat3, Mat4, Quat, Vec2, Vec3, Vec4};

///Translates the inner sdf (by translating the queried point by the inverse)
#[derive(Clone, Copy)]
pub struct Translate<T>(pub T);

impl Modifier for Translate<Vec3> {
    fn pre(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::CoordAdd as u32);
        ser.push_vec3(-self.0);
    }

    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::CoordAdd as u32);
        ser.push_vec3(self.0);
        ser.on_bbox(|bbox| {
            bbox.min += self.0;
            bbox.max += self.0;
        });
    }
}
impl Modifier2d for Translate<Vec2> {
    fn pre(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::CoordAdd as Word);
        let inv = -self.0;
        ser.push_f32(inv.x);
        ser.push_f32(inv.y);
    }
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::CoordAdd as Word);
        ser.push_f32(self.0.x);
        ser.push_f32(self.0.y);
        ser.on_bbox(|bbox| {
            bbox.min += self.0.extend(0.0);
            bbox.max += self.0.extend(0.0);
        })
    }
}

///Transformation of in space for some primitive.
#[derive(Clone, Copy)]
pub struct Transform<T, R> {
    pub translation: T,
    pub rotation: R,
}

impl Transform<Vec3, Quat> {
    pub fn to_matrix(&self) -> Mat4 {
        Mat4::from_rotation_translation(self.rotation, self.translation)
    }
}
impl Transform<Vec2, f32> {
    pub fn to_matrix(&self) -> Mat3 {
        Mat3::from_quat(Quat::from_rotation_z(self.rotation))
            * Mat3::from_translation(self.translation)
    }
}

impl Modifier for Transform<Vec3, Quat> {
    fn pre(&self, ser: &mut StreamSerializer) {
        let mat = self.to_matrix();
        let mat_inv = mat.inverse();
        ser.push_word(OpTy::CoordMatMul as Word);
        ser.push_matrix(mat_inv);
    }

    fn post(&self, ser: &mut StreamSerializer) {
        let mat = self.to_matrix();
        ser.push_word(OpTy::CoordMatMul as Word);
        ser.push_matrix(mat);
        ser.on_bbox(|bbox| {
            bbox.transform(mat);
        });
    }
}
impl Modifier2d for Transform<Vec2, f32> {
    fn pre(&self, ser: &mut StreamSerializer) {
        let mat = self.to_matrix();
        let mat_inv = mat.inverse();
        ser.push_word(OpTy::CoordMatMul as Word);
        let colmat = mat_inv.to_cols_array_2d();
        ser.push_vec3(colmat[0].into());
        ser.push_vec3(colmat[1].into());
        ser.push_vec3(colmat[2].into());
    }

    fn post(&self, ser: &mut StreamSerializer) {
        let mat = self.to_matrix();
        ser.push_word(OpTy::CoordMatMul as Word);
        let colmat = mat.to_cols_array_2d();
        ser.push_vec3(colmat[0].into());
        ser.push_vec3(colmat[1].into());
        ser.push_vec3(colmat[2].into());
    }
}

///Rounds the expression with given radius
#[derive(Clone, Copy)]
pub struct Round {
    pub radius: f32,
}
impl Modifier for Round {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Round as Word);
        ser.push_f32(self.radius);
    }
}
impl Modifier2d for Round {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Round as Word);
        ser.push_f32(self.radius);
    }
}

///Replaces a volume with a volume that represents the former sdfs walls (f(x) = 0) with a
/// `thickness` thick hull.
#[derive(Clone, Copy)]
pub struct Onion {
    pub thickness: f32,
}
impl Modifier for Onion {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Onion as Word);
        ser.push_f32(self.thickness);
    }
}

impl Modifier2d for Onion {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::Onion as Word);
        ser.push_f32(self.thickness);
    }
}

pub const SHADING_FLAG_PBR: Word = 0x1;
pub const SHADING_FLAG_SIMPLE_COLOR: Word = 0x2;

///Physical based shading parameters for an object
#[derive(Clone, Copy)]
pub struct PhysicalBasedShading {
    albedo: Vec3,
    ///Describes how translucent the object is. 0.0 is Completely "see through", 1.0 is completely opaque.
    translucency: f32,
    roughness: f32,
    metallic: f32,
}

impl Default for PhysicalBasedShading {
    fn default() -> Self {
        PhysicalBasedShading {
            albedo: Vec3::ONE,
            metallic: 0.0,
            roughness: 1.0,
            translucency: 1.0,
        }
    }
}

impl Modifier for PhysicalBasedShading {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::ShadingModel as Word);
        ser.push_word(SHADING_FLAG_PBR);
        ser.push_word(OpTy::MaterialSlotV0 as Word);
        ser.push_vec4(self.albedo.extend(self.translucency));
        ser.push_word(OpTy::MaterialSlotV1 as Word);
        ser.push_vec4(Vec4::new(self.roughness, self.metallic, 0.0, 0.0))
    }
}

///Simple Color assignment for an object
#[derive(Clone, Copy)]
pub struct Color(pub Vec3);
impl Modifier for Color {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::ShadingModel as Word);
        ser.push_word(SHADING_FLAG_SIMPLE_COLOR);
        ser.push_word(OpTy::MaterialSlotV0 as Word);
        ser.push_vec4(self.0.extend(1.0));
    }
}
impl Modifier2d for Color {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::ShadingModel as Word);
        ser.push_word(SHADING_FLAG_SIMPLE_COLOR);
        ser.push_word(OpTy::MaterialSlotV0 as Word);
        ser.push_vec4(self.0.extend(1.0));
    }
}

///Color assignment for an object including transparency
#[derive(Clone, Copy)]
pub struct ColorTrans(pub Vec4);
impl Modifier for ColorTrans {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::ShadingModel as Word);
        ser.push_word(SHADING_FLAG_SIMPLE_COLOR);
        ser.push_word(OpTy::MaterialSlotV0 as Word);
        ser.push_vec4(self.0);
    }
}
impl Modifier2d for ColorTrans {
    fn post(&self, ser: &mut StreamSerializer) {
        ser.push_word(OpTy::ShadingModel as Word);
        ser.push_word(SHADING_FLAG_SIMPLE_COLOR);
        ser.push_word(OpTy::MaterialSlotV0 as Word);
        ser.push_vec4(self.0);
    }
}

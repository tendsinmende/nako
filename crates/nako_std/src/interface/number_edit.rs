/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nakorender::{
    backend::Backend,
    winit::event::{Event, WindowEvent},
};

use crate::text::DEFAULT_FONT;

use super::{edit::Edit, InterfaceElement};

///Only allows for numeric values as well as a negative sign in front of the number string.
pub struct IntegerEdit {
    edit: Edit,
}

impl IntegerEdit {
    ///Creates a IntegerEdit from the specified edit field. Allows you to setup most of the edit's behaviour yourself.
    pub fn new(mut edit: Edit, max_number_of_digits: usize, default_value: isize) -> Self {
        edit.max_character_count = Some(max_number_of_digits);
        edit.edit_line.line.set_str(&format!("{}", default_value));

        IntegerEdit { edit }
    }

    ///Creates a IntegerEdit with a default font, color etc. Only lets you set number of digits and an initial value
    pub fn new_default(
        backend: &mut dyn Backend,
        max_number_of_digits: usize,
        default_value: isize,
    ) -> Self {
        let edit = Edit::new(DEFAULT_FONT.to_vec(), "", backend);
        Self::new(edit, max_number_of_digits, default_value)
    }

    pub fn get_value(&self) -> isize {
        self.edit.edit_line.line.get_str().parse::<isize>().unwrap()
    }

    ///Sets the internal value, note that this methode does not check wether the value fulfils the max_digit_count
    pub fn set_value(&mut self, value: isize) {
        self.edit.edit_line.line.set_str(&format!("{}", value));
    }
}

impl InterfaceElement for IntegerEdit {
    fn get_layers(&self) -> &[nakorender::backend::LayerId] {
        self.edit.get_layers()
    }

    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        //This is the main work we have to do. We check here that recieved characters are of the right type. All other events
        //get passed on as usual
        let mut ignore_event = false;
        match event {
            Event::WindowEvent {
                event: WindowEvent::ReceivedCharacter(c),
                ..
            } => {
                if c.is_numeric() || c.is_control() {
                    //is correct
                } else {
                    //There is one case where alphanumerics are allowed, when the cursor is in front, the first symbol is not "-", and "-" is passed in (negating a value).
                    //checking this here
                    let first_is_not_minus =
                        if let Some(element) = self.edit.edit_line.line.characters.get(0) {
                            element.character.character != '-'
                        } else {
                            true
                        };

                    if self.edit.edit_line.cursor == Some(0) && *c == '-' && first_is_not_minus {
                        //allow
                    } else {
                        ignore_event = true;
                    }
                }
            }
            _ => {}
        }

        if !ignore_event {
            self.edit.on_event(renderer, event)
        }
    }

    fn set_area(&mut self, area: crate::Area) {
        self.edit.set_area(area)
    }
}

///Allows only for numerics, a negative sign in the front, and a single decimal dot.
pub struct FloatEdit {
    edit: Edit,
}

impl FloatEdit {
    ///Creates a IntegerEdit from the specified edit field. Allows you to setup most of the edit's behaviour yourself.
    pub fn new(mut edit: Edit, max_number_of_digits: usize, default_value: f32) -> Self {
        edit.max_character_count = Some(max_number_of_digits);
        edit.edit_line.line.set_str(&format!("{}", default_value));

        FloatEdit { edit }
    }

    ///Creates a IntegerEdit with a default font, color etc. Only lets you set number of digits and an initial value
    pub fn new_default(
        backend: &mut dyn Backend,
        max_number_of_digits: usize,
        default_value: f32,
    ) -> Self {
        let edit = Edit::new(DEFAULT_FONT.to_vec(), "", backend);
        Self::new(edit, max_number_of_digits, default_value)
    }

    pub fn get_value(&self) -> f32 {
        self.edit.edit_line.line.get_str().parse::<f32>().unwrap()
    }

    ///Sets the internal value, note that this methode does not check wether the value fulfils the max_digit_count
    pub fn set_value(&mut self, value: f32) {
        self.edit.edit_line.line.set_str(&format!("{}", value));
    }
}

impl InterfaceElement for FloatEdit {
    fn get_layers(&self) -> &[nakorender::backend::LayerId] {
        self.edit.get_layers()
    }

    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        //This is the main work we have to do. We check here that recieved characters are of the right type. All other events
        //get passed on as usual
        let mut ignore_event = false;
        match event {
            Event::WindowEvent {
                event: WindowEvent::ReceivedCharacter(c),
                ..
            } => {
                if c.is_numeric() || c.is_control() {
                    //is correct
                } else {
                    //There are two case where alphanumerics are allowed,
                    // 1st. when the cursor is in front, the first symbol is not "-", and "-" is passed in (negating a value).
                    // 2nd. the edits string does not yet contain a "." symbol
                    //checking this here
                    if *c == '-' {
                        let first_is_not_minus =
                            if let Some(element) = self.edit.edit_line.line.characters.get(0) {
                                element.character.character != '-'
                            } else {
                                true
                            };

                        if self.edit.edit_line.cursor == Some(0) && first_is_not_minus {
                            //allow
                        } else {
                            ignore_event = true;
                        }
                    } else if *c == '.' {
                        if self.edit.edit_line.line.get_str().contains(|c| c == '.') {
                            ignore_event = true;
                        }
                    } else {
                        ignore_event = true;
                    }
                }
            }
            _ => {}
        }

        if !ignore_event {
            self.edit.on_event(renderer, event)
        }
    }

    fn set_area(&mut self, area: crate::Area) {
        self.edit.set_area(area)
    }
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Interface
//! The crate provides several interface elements that might be of use, like buttons, editing fields or the ability to draw
//! Arbitrary lines of Bezier splines.

use nako::glam::Vec2;
use nakorender::{
    backend::{Backend, LayerId},
    winit::event::Event,
};

use crate::Area;

///button related widgets
pub mod buttons;
///provides a generic edit field, as well as abstractions like number edits.
pub mod edit;

pub mod number_edit;

pub mod scroll_box;

pub mod scroll_array;

pub mod slider;

///Common trait used by all elements that manage them selfes
pub trait InterfaceElement {
    ///Returns the layer needed to render this element in order
    fn get_layers(&self) -> &[LayerId];
    ///Called for each event, basically the working callback for most elements. Note that there is a "RedrawRequested" event, which is synonymouse with "per frame" on most platforms.
    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>);
    ///Sets the area of the interface element. The element is expected to update as soon as possible to prevent interface lags.
    fn set_area(&mut self, area: Area);
}

///A trait that can be implemented whenever a object can be masked within an area. This makes it possible for instance to implement `Scrollable` genericaly
pub trait Maskable: InterfaceElement {
    ///Defines the area within the object can be displayed. This might not be the area that was defined via `set_area()`. In that
    /// case parts of the element might be "masked away".
    fn mask(&mut self, area: Area);
}

///When implemented allows the object to be placed in a `ScrollBox`
pub trait Scrollable: Maskable {
    ///Returns the extent the scrollable "would like" to cover ideally.
    fn extent_hint(&self) -> Vec2;
}

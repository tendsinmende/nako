/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako::{
    glam::{Vec2, Vec3},
    modifiers::Color,
};
use nakorender::{
    backend::{Backend, LayerId, LayerId2d},
    winit::event::{ElementState, Event, MouseButton, WindowEvent},
};

use crate::{filler::FillAreaRound, Area, Orientation2d};

use super::InterfaceElement;

///Slider that modifiers a value between 0 and 1. Can be used to scale any other scalar
pub struct Slider {
    area: Area,

    ///draws the main scale
    layer_scale: LayerId2d,
    ///draws the handle that's being moved
    layer_handle: LayerId2d,

    layers: Vec<LayerId>,

    ///The actual current value
    value: f32,

    has_changed: bool,

    //true while the sliders handle is being dragged
    is_dragging: bool,
    cursor_pos: Vec2,

    ///Maximum width of the scale on which the handle moves.
    ///
    /// This means not the length, which is determined by the area.
    pub scale_width: f32,
    ///Main color of the scale
    pub scale_color: Color,
    ///If a border is set, the borders color
    pub scale_border_color: Color,
    ///Border width of the scale. Is clamped by the scales width
    pub scale_border_width: f32,
    ///Sets the radius the scales area has.
    pub scale_radius: f32,

    ///Color of the handle that's being dragged
    pub handle_color: Color,
    ///Max width of the handle, height is determined by the areas height
    pub handle_width: f32,
    ///Radius of the handles area
    pub handle_radius: f32,

    pub orientation: Orientation2d,
}

impl Slider {
    pub fn new(renderer: &mut dyn Backend, initial_value: f32) -> Self {
        let mut slider = Slider {
            area: Area::ONE,
            layer_handle: renderer.new_layer_2d(),
            layer_scale: renderer.new_layer_2d(),

            layers: Vec::with_capacity(2),

            has_changed: true, //Note: will trigger rerender on first event
            is_dragging: false,
            cursor_pos: Vec2::ZERO,
            value: initial_value.clamp(0.0, 1.0),

            scale_width: 10.0,
            scale_color: Color(Vec3::ZERO),
            scale_border_width: 2.0,
            scale_border_color: Color(Vec3::ONE),
            scale_radius: 4.0,

            handle_color: Color(Vec3::ONE),
            handle_width: 10.0,
            handle_radius: 4.0,

            orientation: Orientation2d::Horizontal,
        };

        //Setup initial order
        slider.layers.push(slider.layer_scale.into());
        slider.layers.push(slider.layer_handle.into());

        slider
    }

    ///Returns the current value of slider. Is always in the range [0..1]
    pub fn get_value(&self) -> f32 {
        self.value
    }

    ///Sets the value. Is clamped between 0 and 1
    pub fn set_value(&mut self, new_value: f32) {
        self.value = new_value.clamp(0.0, 1.0);
        self.has_changed = true;
    }

    //depending on current area and current value, calculates the area that's occupied by the handle
    fn handle_area(&self) -> Area {
        match self.orientation {
            Orientation2d::Horizontal => {
                //sliding on the horizontal, therfore get our point on that axis
                let handle_midpoint = self.area.from
                    + Vec2::new(
                        (self.area.extent().x - self.handle_width) * self.value, //Note the scale length is reduced by the handles width
                        self.area.extent().y / 2.0,
                    )
                    + Vec2::new(self.handle_width / 2.0, 0.0);

                let handle_height = self.area.extent().y / 2.0;

                Area {
                    from: handle_midpoint - Vec2::new(self.handle_width, handle_height),
                    to: handle_midpoint + Vec2::new(self.handle_width, handle_height),
                }
            }
            Orientation2d::Vertical => {
                let handle_midpoint = self.area.from
                    + Vec2::new(
                        self.area.extent().x / 2.0,
                        (self.area.extent().y - self.handle_width) * self.value,
                    )
                    + Vec2::new(0.0, self.handle_width / 2.0);

                let handle_width = self.area.extent().x / 2.0;

                Area {
                    from: handle_midpoint - Vec2::new(handle_width, self.handle_width),
                    to: handle_midpoint + Vec2::new(handle_width, self.handle_width),
                }
            }
        }
    }

    fn scale_area(&self) -> Area {
        match self.orientation {
            Orientation2d::Horizontal => {
                let mid = self.area.center();
                Area {
                    from: mid
                        - Vec2::new(
                            (self.area.extent().x - self.handle_width) / 2.0,
                            self.scale_width / 2.0,
                        ),
                    to: mid
                        + Vec2::new(
                            (self.area.extent().x - self.handle_width) / 2.0,
                            self.scale_width / 2.0,
                        ),
                }
            }
            Orientation2d::Vertical => {
                let mid = self.area.center();
                Area {
                    from: mid
                        - Vec2::new(
                            self.scale_width / 2.0,
                            (self.area.extent().x - self.handle_width) / 2.0,
                        ),
                    to: mid
                        + Vec2::new(
                            self.scale_width / 2.0,
                            (self.area.extent().x - self.handle_width) / 2.0,
                        ),
                }
            }
        }
    }

    ///Updates inner value based on a cursor position somewhere on screen
    fn update_value(&mut self, cursor_position: Vec2) {
        match self.orientation {
            Orientation2d::Horizontal => {
                //map position onto our scale
                let scale = self.scale_area();
                let relative = cursor_position.x - scale.from.x;
                let new_val = relative / scale.extent().x;
                self.set_value(new_val);
            }
            Orientation2d::Vertical => {
                //map position onto our scale
                let scale = self.scale_area();
                let relative = cursor_position.y - scale.from.y;
                let new_val = relative / scale.extent().y;
                self.set_value(new_val);
            }
        }

        self.has_changed = true;
    }

    fn update_layout(&mut self, renderer: &mut dyn Backend) {
        let scale = self.scale_area();
        let handle = self.handle_area();
        FillAreaRound {
            color: self.scale_color,
            radius: self.scale_radius,
        }
        .fill(renderer, self.layer_scale, scale);
        FillAreaRound {
            color: self.handle_color,
            radius: self.handle_radius,
        }
        .fill(renderer, self.layer_handle, handle);
    }
}

impl InterfaceElement for Slider {
    fn get_layers(&self) -> &[LayerId] {
        &self.layers
    }

    fn set_area(&mut self, area: Area) {
        self.area = area;
        self.has_changed = true;
    }

    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        match event {
            Event::WindowEvent {
                event:
                    WindowEvent::MouseInput {
                        state,
                        button: MouseButton::Left,
                        ..
                    },
                ..
            } => match state {
                ElementState::Pressed => {
                    if self.handle_area().is_in(self.cursor_pos) {
                        self.is_dragging = true;
                    }
                }
                ElementState::Released => self.is_dragging = false,
            },
            Event::WindowEvent {
                event: WindowEvent::CursorMoved { position, .. },
                ..
            } => {
                self.cursor_pos = Vec2::new(position.x as f32, position.y as f32);
                if self.is_dragging {
                    self.update_value(self.cursor_pos);
                }
            }
            _ => {}
        }

        if self.has_changed {
            self.update_layout(renderer);
        }
    }
}

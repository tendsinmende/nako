/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako::{
    glam::{const_vec3, IVec2, UVec2, Vec2},
    modifiers::Color,
    stream::PrimaryStream2d,
};
use nakorender::{
    backend::{Backend, LayerId, LayerId2d, LayerInfo},
    camera::Camera2d,
    winit::event::{Event, MouseScrollDelta, WindowEvent},
};

use crate::{filler::FillArea, Area};

use super::{InterfaceElement, Scrollable};

///Allows an scrollable object `T` to declare any size via `Scrollable::size_hint()`. The box will add scrollbars depending on the needed extent.
pub struct ScrollBox<T: Scrollable> {
    ///Area this widget can actually cover.
    area: Area,
    pub(crate) inner: T,

    ///Color used for the scrollbar background.
    pub color_background: Color,
    ///Color used for the scroll bar,
    pub color_bar: Color,
    ///current inner offset for the element
    offset: Vec2,

    cursor_location: Vec2,

    ///whether scrolling vertically is possible
    is_vertical: bool,
    ///whether scrolling horizontal is possible, not as usual as the vertical variant.
    is_horizontal: bool,
    is_changed: bool,
    //Last known extent of inner. Used to determine when we have to recalculate the area and the scrollability flags
    //for the inner content.
    last_extent: Vec2,

    layer_bar_h_bg: LayerId2d,
    layer_bar_h_fg: LayerId2d,
    layer_bar_v_bg: LayerId2d,
    layer_bar_v_fg: LayerId2d,

    layers: Vec<LayerId>,
}

impl<T: Scrollable> ScrollBox<T> {
    pub const SCROLL_BOX_WIDTH: f32 = 10.0;
    pub const COLOR_BACKGROUND: Color = Color(const_vec3!([0.2, 0.2, 0.2]));
    pub const COLOR_BAR: Color = Color(const_vec3!([0.8, 0.8, 0.8]));

    pub fn new(renderer: &mut dyn Backend, element: T) -> Self {
        let mut scbox = ScrollBox {
            area: Area::ONE,
            inner: element,
            offset: Vec2::ZERO,

            color_background: Self::COLOR_BACKGROUND,
            color_bar: Self::COLOR_BAR,

            cursor_location: Vec2::ZERO,

            is_horizontal: false,
            is_vertical: false,
            is_changed: true,
            last_extent: Vec2::ZERO,

            layer_bar_h_bg: renderer.new_layer_2d(),
            layer_bar_h_fg: renderer.new_layer_2d(),
            layer_bar_v_bg: renderer.new_layer_2d(),
            layer_bar_v_fg: renderer.new_layer_2d(),
            layers: vec![],
        };
        scbox.update_layers();
        scbox
    }

    pub(crate) fn update_layers(&mut self) {
        self.layers = Vec::with_capacity(4);

        self.layers.append(&mut self.inner.get_layers().to_vec());
        self.layers.push(self.layer_bar_h_bg.into());
        self.layers.push(self.layer_bar_h_fg.into());
        self.layers.push(self.layer_bar_v_bg.into());
        self.layers.push(self.layer_bar_v_fg.into());
    }

    pub fn set_inner(&mut self, new: T) {
        self.inner = new;
        self.update_layers();
    }

    #[inline]
    fn is_inner_unfit(&self) -> bool {
        let new_ext = self.inner.extent_hint();
        new_ext.x != self.last_extent.x || new_ext.y != self.last_extent.y
    }

    fn update_layout(&mut self, renderer: &mut dyn Backend) {
        //update extent
        self.last_extent = self.inner.extent_hint();
        //update scrollable flags

        self.is_horizontal = self.last_extent.x > self.area.extent().x;
        self.is_vertical = self.last_extent.y > self.area.extent().y;

        //Check if we have to reduce the inner offset
        // (for instance if the content shrunk)
        let outer_pt = self.area.from - self.offset + self.last_extent;
        if outer_pt.x < self.area.to.x {
            self.offset.x = (self.last_extent.x - self.area.extent().x).max(0.0);
        }
        if outer_pt.y < self.area.to.y {
            self.offset.y = (self.last_extent.y - self.area.extent().y).max(0.0);
        }

        //Based on current state, set mask and offset for inner element

        //the area in which inner will be displayed. Later used as mask area and reference when offset
        let inner_area = Area {
            from: self.area.from,
            to: self.area.to
                - Vec2::new(
                    if self.is_vertical {
                        Self::SCROLL_BOX_WIDTH
                    } else {
                        0.0
                    },
                    if self.is_horizontal {
                        Self::SCROLL_BOX_WIDTH
                    } else {
                        0.0
                    },
                ),
        };
        //set new mask
        self.inner.mask(inner_area);
        //set area by offseting our own areas origin via offset, then extenting that point by the hinted size
        let offseted = self.area.from - self.offset;
        self.inner.set_area(Area {
            from: offseted,
            to: offseted + self.last_extent,
        });

        //draw bars
        if self.is_horizontal {
            let hbar_area_bg = Area {
                from: Vec2::new(self.area.from.x, inner_area.to.y),
                to: self.area.to,
            };

            renderer.set_layer_info(
                self.layer_bar_h_bg.into(),
                LayerInfo {
                    extent: hbar_area_bg.extent().as_uvec2(),
                    location: hbar_area_bg.from.as_ivec2(),
                    ..Default::default()
                },
            );
            renderer.update_camera_2d(
                self.layer_bar_h_bg,
                Camera2d {
                    extent: hbar_area_bg.extent(),
                    location: Vec2::ZERO,
                    rotation: 0.0,
                },
            );
            FillArea {
                color: self.color_background,
            }
            .fill(renderer, self.layer_bar_h_bg, hbar_area_bg);
        } else {
            //clear
            renderer.set_layer_info(
                self.layer_bar_h_bg.into(),
                LayerInfo {
                    extent: UVec2::ZERO,
                    location: IVec2::ZERO,
                    ..Default::default()
                },
            );
            renderer.update_sdf_2d(self.layer_bar_h_bg, PrimaryStream2d::new().build());
        }
        if self.is_vertical {
            let vbar_area_bg = Area {
                from: Vec2::new(inner_area.to.x, self.area.from.y),
                to: self.area.to,
            };

            renderer.set_layer_info(
                self.layer_bar_v_bg.into(),
                LayerInfo {
                    extent: vbar_area_bg.extent().as_uvec2(),
                    location: vbar_area_bg.from.as_ivec2(),
                    ..Default::default()
                },
            );
            renderer.update_camera_2d(
                self.layer_bar_v_bg,
                Camera2d {
                    extent: vbar_area_bg.extent(),
                    location: Vec2::ZERO,
                    rotation: 0.0,
                },
            );
            FillArea {
                color: self.color_background,
            }
            .fill(renderer, self.layer_bar_v_bg, vbar_area_bg);
        } else {
            //clear
            renderer.set_layer_info(
                self.layer_bar_v_bg.into(),
                LayerInfo {
                    extent: UVec2::ZERO,
                    location: IVec2::ZERO,
                    ..Default::default()
                },
            );
            renderer.update_sdf_2d(self.layer_bar_v_bg, PrimaryStream2d::new().build());
        }
    }
}

impl<T: Scrollable> InterfaceElement for ScrollBox<T> {
    fn set_area(&mut self, area: Area) {
        self.area = area;
        self.is_changed = true;
    }

    fn get_layers(&self) -> &[LayerId] {
        &self.layers
    }

    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        let mut has_changed = match event {
            Event::WindowEvent {
                event: WindowEvent::MouseWheel { delta, .. },
                ..
            } => {
                if self.area.is_in(self.cursor_location) {
                    let delta = match (delta, self.is_horizontal, self.is_vertical) {
                        (MouseScrollDelta::LineDelta(x, y), true, true) => {
                            Vec2::new(*x * 10.0, *y * 10.0)
                        }
                        (MouseScrollDelta::LineDelta(_x, y), true, false) => {
                            Vec2::new(*y * 10.0, 0.0)
                        }
                        (MouseScrollDelta::LineDelta(_x, y), false, true) => {
                            Vec2::new(0.0, *y * 10.0)
                        }

                        (MouseScrollDelta::PixelDelta(delta), true, true) => {
                            Vec2::new(delta.x as f32, delta.y as f32)
                        }
                        (MouseScrollDelta::PixelDelta(delta), true, false) => {
                            Vec2::new(delta.y as f32, 0.0)
                        }
                        (MouseScrollDelta::PixelDelta(delta), false, true) => {
                            Vec2::new(0.0, delta.y as f32)
                        }
                        _ => Vec2::ZERO,
                    };

                    self.offset += delta;
                    self.offset = self.offset.max(Vec2::ZERO);
                    //return true if anything changed
                    true
                } else {
                    false
                }
            }
            Event::WindowEvent {
                event: WindowEvent::CursorMoved { position, .. },
                ..
            } => {
                self.cursor_location = Vec2::new(position.x as f32, position.y as f32);
                false
            }
            _ => false,
        };

        has_changed |= self.is_changed;
        has_changed |= self.is_inner_unfit();

        if has_changed {
            self.is_changed = false;
            self.update_layout(renderer);
        }
        //After setting new mask n stuff, pass down to inner
        self.inner.on_event(renderer, event);
    }
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako::glam::Vec2;
use nakorender::{
    backend::{Backend, LayerId},
    winit::event::Event,
};

use crate::Area;

use super::{scroll_box::ScrollBox, InterfaceElement, Maskable, Scrollable};

pub enum ArrayDirection {
    Vertical,
    Horizontal,
}

///Inner abstraction that builds the horizontal or vertical list that is being displayed
struct ScrollList<T: Scrollable> {
    direction: ArrayDirection,

    area: Area,
    mask: Option<Area>,

    has_changed: bool,

    values: Vec<T>,
    layers: Vec<LayerId>,
}

impl<T: Scrollable> ScrollList<T> {
    fn update_layout(&mut self) {
        //set new areas based on current location. Mask is set in Masked implementation, should be applied regardless
        let mut offset = self.area.from;
        match self.direction {
            ArrayDirection::Horizontal => {
                for element in &mut self.values {
                    let element_ext = element.extent_hint().min(self.area.extent());
                    let element_area = Area {
                        from: offset,
                        to: offset + element_ext,
                    };
                    element.set_area(element_area);
                    offset.x += element_ext.x;
                }
            }
            ArrayDirection::Vertical => {
                for element in &mut self.values {
                    let element_ext = element.extent_hint().min(self.area.extent());
                    let element_area = Area {
                        from: offset,
                        to: offset + element_ext,
                    };
                    element.set_area(element_area);
                    offset.y += element_ext.y;
                }
            }
        }
    }

    fn update_layer(&mut self) {
        self.layers = self.values.iter().fold(Vec::new(), |mut col, el| {
            col.append(&mut el.get_layers().to_vec());
            col
        });
    }
}

impl<T: Scrollable> InterfaceElement for ScrollList<T> {
    fn set_area(&mut self, area: Area) {
        self.area = area;
        self.has_changed = true;
    }

    fn get_layers(&self) -> &[LayerId] {
        &self.layers
    }

    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        if self.has_changed {
            self.has_changed = false;
            self.update_layout();
        }
        for ele in &mut self.values {
            ele.on_event(renderer, event);
        }
    }
}

impl<T: Scrollable> Maskable for ScrollList<T> {
    fn mask(&mut self, area: Area) {
        self.mask = Some(area);
        for e in &mut self.values {
            e.mask(area);
        }
        self.has_changed = true;
    }
}

impl<T: Scrollable> Scrollable for ScrollList<T> {
    fn extent_hint(&self) -> Vec2 {
        match self.direction {
            ArrayDirection::Horizontal => self.values.iter().fold(Vec2::ZERO, |com, ele| {
                Vec2::new(com.x + ele.extent_hint().x, com.y.max(ele.extent_hint().y))
            }),
            ArrayDirection::Vertical => self.values.iter().fold(Vec2::ZERO, |com, ele| {
                Vec2::new(com.x.max(ele.extent_hint().x), com.y + ele.extent_hint().y)
            }),
        }
    }
}

///Allows to place any number of objects of type `T: Scrollable` to be placed in a horizontal or vertical list.
pub struct ScrollArray<T: Scrollable> {
    inner: ScrollBox<ScrollList<T>>,
}

impl<T: Scrollable> ScrollArray<T> {
    pub fn new(renderer: &mut dyn Backend, values: Vec<T>, direction: ArrayDirection) -> Self {
        let mut list = ScrollList {
            area: Area::ONE,
            direction,
            has_changed: true,
            layers: vec![],
            mask: None,
            values,
        };
        list.update_layer();
        ScrollArray {
            inner: ScrollBox::new(renderer, list),
        }
    }

    fn update_layers(&mut self) {
        self.inner.inner.update_layer();
        self.inner.update_layers();
    }

    pub fn get_elements(&self) -> &[T] {
        &self.inner.inner.values
    }

    pub fn get_elements_mut(&mut self) -> &mut [T] {
        &mut self.inner.inner.values
    }

    pub fn set_elements(&mut self, elements: Vec<T>) {
        self.inner.inner.values = elements;
        self.update_layers();
        self.inner.inner.has_changed = true;
    }

    pub fn push(&mut self, element: T) {
        self.inner.inner.values.push(element);
        self.update_layers();
        self.inner.inner.has_changed = true;
    }

    pub fn remove(&mut self, index: usize) -> Option<T> {
        let ret = if index < self.inner.inner.values.len() {
            Some(self.inner.inner.values.remove(index))
        } else {
            None
        };

        self.update_layers();
        self.inner.inner.has_changed = true;

        ret
    }
}

impl<T: Scrollable> InterfaceElement for ScrollArray<T> {
    fn set_area(&mut self, area: Area) {
        self.inner.set_area(area);
    }

    fn get_layers(&self) -> &[LayerId] {
        self.inner.get_layers()
    }

    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        self.inner.on_event(renderer, event);
    }
}

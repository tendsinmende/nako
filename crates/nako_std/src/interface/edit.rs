/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako::{
    combinators::{Subtraction, Union},
    glam::{Vec2, Vec3},
    modifiers::{Color, Translate},
    primitives::Cuboid,
    stream::{PrimaryStream2d, SecondaryStream2d},
};
use nakorender::{
    backend::{Backend, LayerId, LayerId2d, LayerInfo},
    camera::Camera2d,
    winit::event::{ElementState, Event, MouseButton, WindowEvent},
};

use crate::{
    text::{EditableLine, HorizontalAlignment, VerticalAlignment, DEFAULT_FONT},
    Area,
};

use super::InterfaceElement;

///Text field that allows editing a utf-8 string. About the edited string is made.
pub struct Edit {
    //area in which the element is drawn
    area: Area,

    pub color_frame: Color,
    pub color_background: Color,
    pub color_text: Color,

    pub border_width: f32,

    layer_frame: LayerId2d,
    layer_background: LayerId2d,

    pub(crate) edit_line: EditableLine,
    //caches layer ids in correct order. Those should not changed over time
    layers: Vec<LayerId>,

    //tracks local mouse position
    cursor_pos: Vec2,

    //flags that is set whenever the whole edit needs a rerender. Usually whenever
    //a resize occured. Otherwise only the text gets edited
    is_changed: bool,

    ///Horizontal text alignment, vertical alignment is always centered.
    pub alignment: HorizontalAlignment,
    ///If set ignores any input if the max_character_count was reached already
    pub max_character_count: Option<usize>,
}

impl Edit {
    pub fn new(font: Vec<u8>, value: &str, renderer: &mut dyn Backend) -> Self {
        let mut edit = Edit {
            area: Area::ONE,

            color_frame: Color(Vec3::ONE),
            color_background: Color(Vec3::ZERO),
            color_text: Color(Vec3::ONE),

            border_width: 5.0,

            layer_frame: renderer.new_layer_2d(),
            layer_background: renderer.new_layer_2d(),

            edit_line: EditableLine::new(font, value, renderer),

            cursor_pos: Vec2::ZERO,

            layers: Vec::with_capacity(3),
            is_changed: true,
            alignment: HorizontalAlignment::Left,
            max_character_count: None,
        };

        //post push layers in order
        edit.layers.push(edit.layer_background.into());
        edit.layers
            .append(&mut edit.edit_line.get_layers().to_vec());
        edit.layers.push(edit.layer_frame.into());

        edit
    }

    ///Creates edit with default font
    pub fn new_from_str(string: &str, renderer: &mut dyn Backend) -> Self {
        Self::new(DEFAULT_FONT.to_vec(), string, renderer)
    }

    //rerenders the whole widget based on current internal state
    fn update_layout(&mut self, renderer: &mut dyn Backend) {
        let area_info = LayerInfo {
            extent: self.area.extent().as_uvec2(),
            location: self.area.from.as_ivec2(),
            ..Default::default()
        };
        let mut text_area = Area {
            from: self.area.from + self.border_width + Vec2::ONE,
            to: self.area.to - self.border_width - Vec2::ONE,
        };

        if text_area.extent().min_element() < 1.0 {
            //make sure the text area is at least one pixel big
            text_area.to = text_area.to.max(text_area.from + Vec2::ONE);
        }
        //Set new text area for line
        self.edit_line
            .set_alignment(self.alignment, VerticalAlignment::Middle);
        self.edit_line.set_area(text_area);

        renderer.set_layer_info(self.layer_background.into(), area_info);
        renderer.set_layer_info(self.layer_frame.into(), area_info);

        //layer local camera so we can draw in normal pixel space terms
        let camera = Camera2d {
            extent: self.area.extent(),
            location: Vec2::ZERO,
            rotation: 0.0,
        };
        renderer.update_camera_2d(self.layer_background, camera);
        renderer.update_camera_2d(self.layer_frame, camera);

        renderer.update_sdf_2d(
            self.layer_background,
            PrimaryStream2d::new()
                .push(
                    SecondaryStream2d::new(
                        Union,
                        Cuboid {
                            extent: self.area.extent() / 2.0,
                        },
                    )
                    .push_mod(Translate(self.area.extent() / 2.0))
                    .push_mod(self.color_background)
                    .build(),
                )
                .build(),
        );

        //Frame, works by drawin square over everything, then removing the inner part where the background will be revealed
        renderer.update_sdf_2d(
            self.layer_frame,
            PrimaryStream2d::new()
                .push(
                    SecondaryStream2d::new(
                        Union,
                        Cuboid {
                            extent: self.area.extent() / 2.0,
                        },
                    )
                    .push_mod(Translate(self.area.extent() / 2.0))
                    .push_mod(self.color_frame)
                    .build(),
                )
                .push(
                    SecondaryStream2d::new(
                        Subtraction,
                        Cuboid {
                            extent: (self.area.extent() / 2.0) - Vec2::splat(self.border_width),
                        },
                    )
                    .push_mod(Translate(self.area.extent() / 2.0))
                    .build(),
                )
                .build(),
        );
    }

    pub fn get_area(&self) -> Area {
        self.area
    }
}

impl InterfaceElement for Edit {
    fn get_layers(&self) -> &[LayerId] {
        &self.layers
    }

    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        let mut ignore_event = false;
        match event {
            Event::WindowEvent {
                event: WindowEvent::ReceivedCharacter(c),
                ..
            } => {
                if let Some(char_count) = &mut self.max_character_count {
                    //If this has the potentual to add to the line length, check if this is still allowed
                    if c.is_alphanumeric() {
                        //If char count was reached, ignore char, except for delete character
                        if *char_count < self.edit_line.line.characters.len() + 1 {
                            ignore_event = true;
                        }
                    }
                }
            }
            Event::WindowEvent {
                event: WindowEvent::CursorMoved { position, .. },
                ..
            } => {
                self.cursor_pos = Vec2::new(position.x as f32, position.y as f32);
            }
            Event::WindowEvent {
                event: WindowEvent::MouseInput { button, state, .. },
                ..
            } => {
                if self.area.is_in(self.cursor_pos) {
                    match (button, state) {
                        (MouseButton::Left, ElementState::Pressed) => {
                            self.edit_line.set_focused(true, self.cursor_pos)
                        }
                        _ => {}
                    }
                } else {
                    match (button, state) {
                        (MouseButton::Left, ElementState::Pressed) => {
                            self.edit_line.set_focused(false, self.cursor_pos)
                        }
                        _ => {}
                    }
                }
            }
            _ => {}
        }

        if !ignore_event {
            self.edit_line.on_event(renderer, event);
        }
        if self.is_changed {
            self.is_changed = false;
            self.update_layout(renderer);
        }
    }

    fn set_area(&mut self, area: Area) {
        self.area = area;
        self.edit_line.set_area(Area {
            from: self.area.from + Vec2::splat(self.border_width),
            to: self.area.to - Vec2::splat(self.border_width),
        });
        self.is_changed = true;
    }
}

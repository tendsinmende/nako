/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako::{
    glam::{const_vec2, Vec2, Vec3},
    modifiers::Color,
};
use nakorender::{
    backend::{Backend, LayerId, LayerId2d},
    winit::event::{ElementState, Event, MouseButton, WindowEvent},
};

use crate::{filler::FillAreaRound, text::Label, Area};

use super::{InterfaceElement, Maskable, Scrollable};

pub const DEFAULT_BUTTON_SIZE: Vec2 = const_vec2!([100.0, 50.0]);

///Simple Button that executes an event when pushed.
///It also allows you to execute code when hovering and the like.
pub struct PushButton<C> {
    ///Area this button covers on screen
    area: Area,
    mask: Option<Area>,

    pub border_radius: f32,
    pub color: Color,
    pub border_color: Color,
    pub on_hover_color: Option<Color>,

    pub context: C,
    ///Function that should be executed when pressing on the button. Gets the context variable of this button
    pub press_event: Box<dyn Fn(&mut C)>,

    cursor_over_button: bool,
    state_changed: bool,

    lid: [LayerId2d; 1],
    ids: [LayerId; 1],
}

impl PushButton<()> {
    pub fn new(renderer: &mut dyn Backend) -> PushButton<()> {
        let lid = [renderer.new_layer_2d()];
        let ids = [lid[0].into()];
        PushButton {
            area: Area::ONE,
            mask: None,

            border_radius: 5.0,
            color: Color(Vec3::splat(0.2)),
            border_color: Color(Vec3::splat(0.3)),
            on_hover_color: Some(Color(Vec3::splat(0.6))),

            context: (),
            press_event: Box::new(|_| {}),

            cursor_over_button: false,
            state_changed: true,
            lid,
            ids,
        }
    }
}

impl<C> PushButton<C> {
    pub fn with_ctx<NC>(
        self,
        new_function: impl Fn(&mut NC) + 'static,
        new_context: NC,
    ) -> PushButton<NC> {
        PushButton {
            context: new_context,
            press_event: Box::new(new_function),

            area: self.area,
            mask: self.mask,
            border_color: self.border_color,
            border_radius: self.border_radius,
            color: self.color,
            cursor_over_button: self.cursor_over_button,
            on_hover_color: self.on_hover_color,
            state_changed: true,

            lid: self.lid,
            ids: self.ids,
        }
    }

    pub fn with_area(mut self, area: Area) -> Self {
        self.set_area(area);
        self
    }

    pub fn state_changed(&self) -> bool {
        self.state_changed
    }

    fn update_layout(&mut self, renderer: &mut dyn Backend) {
        let filler = FillAreaRound {
            color: if self.cursor_over_button {
                self.on_hover_color.unwrap_or(self.color)
            } else {
                self.color
            },
            radius: self.border_radius,
        };
        if let Some(mask) = &self.mask {
            filler.fill_masked(renderer, self.lid[0], self.area, *mask);
        } else {
            filler.fill(renderer, self.lid[0], self.area);
        }
    }
}

impl<C> InterfaceElement for PushButton<C> {
    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        match event {
            Event::RedrawRequested(_) => {
                if self.state_changed {
                    self.update_layout(renderer);
                    //mark as unchanged again
                    self.state_changed = false;
                }
            }
            Event::WindowEvent {
                event: WindowEvent::CursorMoved { position, .. },
                ..
            } => {
                let pos = Vec2::new(position.x as f32, position.y as f32);
                if self.area.is_in(pos) && !self.cursor_over_button {
                    self.cursor_over_button = true;
                    self.state_changed = true;
                } else if !self.area.is_in(pos) && self.cursor_over_button {
                    self.cursor_over_button = false;
                    self.state_changed = true;
                }
            }
            Event::WindowEvent {
                event: WindowEvent::MouseInput { button, state, .. },
                ..
            } => match (button, state) {
                (MouseButton::Left, ElementState::Pressed) => {
                    if self.cursor_over_button {
                        (*self.press_event)(&mut self.context)
                    }
                }
                _ => {}
            },
            _ => {}
        }
    }

    fn set_area(&mut self, area: Area) {
        self.area = area;
        self.cursor_over_button = false; //reset state, cannot know
        self.state_changed = true;
    }

    fn get_layers(&self) -> &[LayerId] {
        &self.ids
    }
}

impl<C> Maskable for PushButton<C> {
    fn mask(&mut self, area: Area) {
        self.mask = Some(area);
        self.state_changed = true;
    }
}

impl<C> Scrollable for PushButton<C> {
    fn extent_hint(&self) -> Vec2 {
        DEFAULT_BUTTON_SIZE
    }
}

pub struct LabeledButton<C> {
    pub button: PushButton<C>,
    pub label: Label,
    ids: Vec<LayerId>,
}

impl LabeledButton<()> {
    pub fn new(renderer: &mut dyn Backend, text: &str) -> Self {
        let button = PushButton::new(renderer);
        let label = Label::new(renderer, text, 10.0);

        let mut button = LabeledButton {
            label,
            button,
            ids: Vec::new(),
        };

        button.ids.append(&mut button.button.get_layers().to_vec());
        button.ids.append(&mut button.label.get_layers().to_vec());

        button
    }
}

impl<C> LabeledButton<C> {
    ///Changes the context and the on_press function
    pub fn with_context<CTX>(
        self,
        new_function: impl Fn(&mut CTX) + 'static,
        new_contex: CTX,
    ) -> LabeledButton<CTX> {
        LabeledButton {
            button: self.button.with_ctx(new_function, new_contex),
            ids: self.ids,
            label: self.label,
        }
    }

    pub fn from_button_label(label: Label, button: PushButton<C>) -> Self {
        let mut button = LabeledButton {
            button,
            label,
            ids: vec![],
        };

        button.ids.append(&mut button.button.get_layers().to_vec());
        button.ids.append(&mut button.label.get_layers().to_vec());

        button
    }
}

impl<C> InterfaceElement for LabeledButton<C> {
    fn get_layers(&self) -> &[LayerId] {
        &self.ids
    }

    fn set_area(&mut self, area: Area) {
        self.button.set_area(area);
        self.label.set_area(area);
    }

    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        self.button.on_event(renderer, event);
        self.label.on_event(renderer, event);
    }
}

impl<C> Maskable for LabeledButton<C> {
    fn mask(&mut self, area: Area) {
        self.button.mask(area);
        self.label.mask(area);
    }
}

impl<C> Scrollable for LabeledButton<C> {
    fn extent_hint(&self) -> Vec2 {
        DEFAULT_BUTTON_SIZE
    }
}

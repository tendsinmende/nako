/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::{fs::File, io::Read, path::Path, sync::Arc};

use font_kit::{error::FontLoadingError, font::Font};

use nako::{
    glam::{Vec2, Vec3},
    modifiers::Color,
    stream::Primary2dBuilder,
};
use rustybuzz::{shape, Face, UnicodeBuffer};

use crate::Area;

use super::{Character, HorizontalAlignment, VerticalAlignment};

///Single character on a line of text. Wraps position information.
pub(crate) struct LineCharacter {
    pub(crate) character: Character,
    ///Base offset when scaling is 1.0. Used to calculate correct offset when scaling the line
    pub(crate) offset: Vec2,
}

///A line of text based on a string laied out by rustybuzz. Note that linebreaks are *NOT* supported. Those will lead to problems when laying out the characters.
pub struct TextLine {
    content: String,
    pub(crate) characters: Vec<LineCharacter>,
    //The font being used. Probably not the best to save that here, but works for now.
    //Should maybe only be an accessor to system fonts or something.
    font: Arc<Vec<u8>>,
    pub position: Vec2,
    pub size: f32,
    pub color: Color,
}

impl TextLine {
    pub fn new_from_path(
        font_path: impl AsRef<Path>,
        string: &str,
    ) -> Result<Self, FontLoadingError> {
        let file = File::open(font_path)?;
        Ok(Self::new_from_file(file, string))
    }

    pub fn new_from_file(mut font_file: File, string: &str) -> Self {
        //Load font in both font-kit and rustybuzz by first loading its bytes and the passing them to both
        let mut font_data = Vec::new();
        font_file.read_to_end(&mut font_data).unwrap();
        Self::new(font_data, string)
    }

    ///Shapes a new text line from the given font as bytes. Should be either a ttf or otf, otherwise this panics.
    pub fn new(font: Vec<u8>, string: &str) -> Self {
        let mut line = TextLine {
            content: String::new(),
            characters: vec![],
            font: Arc::new(font),
            position: Vec2::ZERO,
            size: 1.0,
            color: Color(Vec3::ONE),
        };

        line.set_str(string);
        line
    }

    pub fn set_str(&mut self, string: &str) {
        //Could be used to select a sub font. However, currently not implemented.
        let font_index = 0;

        let fontkit = Font::from_bytes(self.font.clone(), font_index).unwrap();
        let font_face = Face::from_slice(&self.font, font_index).unwrap();

        let add = fontkit.metrics().descent.abs();
        let size_div = add + fontkit.metrics().ascent;

        let mut buffer = UnicodeBuffer::new();
        buffer.push_str(string);

        //TODO at this point we could set stuff like direction etc. However that is currently not configureable.

        let glyphs = shape(&font_face, &[], buffer);

        let (_, characters) = glyphs
            .glyph_positions()
            .iter()
            .zip(glyphs.glyph_infos().iter())
            .zip(string.chars())
            .fold(
                (0.0, Vec::new()),
                |(offset, mut glyphs), ((pos, info), ch)| {
                    glyphs.push(LineCharacter {
                        character: Character::from_glyph_id(&fontkit, info.glyph_id, ch),
                        offset: Vec2::new(offset, 0.0),
                    });

                    (offset + (pos.x_advance as f32 / size_div), glyphs)
                },
            );

        self.content = string.to_string();
        self.characters = characters;
    }

    pub fn with_size(mut self, size: f32) -> Self {
        self.size = size;
        self
    }

    pub fn with_position(mut self, position: Vec2) -> Self {
        self.position = position;
        self
    }

    pub fn set_position(&mut self, pos: Vec2) {
        self.position = pos;
    }

    pub fn get_position(&self) -> Vec2 {
        self.position
    }

    pub fn with_color(mut self, color: Color) -> Self {
        self.color = color;
        self
    }

    ///Checks if `loc` is on any char, otherwise returns first or last index of the character vector, depending on the
    ///position that was queried.
    pub(crate) fn location_to_char_idx(&self, loc: Vec2) -> usize {
        //fast path, querries if we are "before" / "above" first element.
        let local_query_location = loc - self.position;
        if local_query_location.min_element() < 0.0 {
            return 0;
        }

        //iterate through all characters.
        let mut last_idx = 0;
        for (idx, c) in self.characters.iter().enumerate() {
            //create bbox. The boxes high is fitted to the internal size. Othewise clicking small characters like o, or ¹ might be difficult.
            let norm_area = c.character.get_area();
            let bbox = Area {
                from: Vec2::new(c.offset.x * self.size, 0.0),
                to: Vec2::new(
                    (c.offset.x * self.size) + (norm_area.extent().x * self.size),
                    self.size,
                ),
            };

            if bbox.is_in(local_query_location) {
                return idx;
            }

            last_idx = idx;
        }

        last_idx + 1
    }

    ///returns the positioned and scaled area of a character. Note that `char_idx` means the actual char, not unicode code point.
    pub fn get_character_area(&self, char_idx: usize) -> Option<Area> {
        if let Some(ch) = self.characters.get(char_idx) {
            //scale and position area
            let mut area = ch.character.get_area();
            area.from = (area.from * self.size) + (ch.offset * self.size) + self.position;
            area.to = (area.to * self.size) + (ch.offset * self.size) + self.position;
            Some(area)
        } else {
            None
        }
    }

    pub fn get_character_selection_area(&self, char_idx: usize) -> Option<Area> {
        if let Some(ch) = self.characters.get(char_idx) {
            let norm_area = ch.character.get_area();
            Some(Area {
                from: Vec2::new(ch.offset.x * self.size, 0.0) + self.position,
                to: Vec2::new(
                    (ch.offset.x * self.size) + norm_area.extent().x * self.size,
                    self.size,
                ) + self.position,
            })
        } else {
            None
        }
    }

    pub fn get_str(&self) -> &str {
        &self.content
    }

    pub fn get_area(&self) -> Area {
        if self.characters.len() == 0 {
            return Area {
                from: self.position,
                to: self.position,
            };
        }
        self.characters.iter().fold(
            Area {
                from: Vec2::new(f32::INFINITY, f32::INFINITY),
                to: Vec2::new(f32::NEG_INFINITY, f32::NEG_INFINITY),
            },
            |mut com, ch| {
                let mut char_area = ch.character.get_area();
                //offset area into our space
                char_area.from =
                    (char_area.from * self.size) + self.position + (ch.offset * self.size);
                char_area.to = (char_area.to * self.size) + self.position + (ch.offset * self.size);

                com.from = com.from.min(char_area.from);
                com.to = com.to.max(char_area.to);

                com
            },
        )
    }

    ///Sets inner position in a way that the text line fulfils the alignment requirements
    pub fn align_in_area(
        &mut self,
        horizontal_alignment: HorizontalAlignment,
        vertical_alignment: VerticalAlignment,
        area: Area,
    ) {
        let horizontal = match horizontal_alignment {
            HorizontalAlignment::Left => area.from.x,
            HorizontalAlignment::Middle => area.center().x - (self.get_area().extent().x / 2.0),
            HorizontalAlignment::Right => area.to.x - self.get_area().extent().x,
        };

        let vertical = match vertical_alignment {
            VerticalAlignment::Top => area.from.y,
            VerticalAlignment::Middle => area.center().y - (self.get_area().extent().y / 2.0),
            VerticalAlignment::Bottom => area.to.y - self.get_area().extent().y,
        };

        self.set_position(Vec2::new(horizontal, vertical))
    }

    ///Sets inner position and size in a way that the line fits within the `area` perfectly.
    pub fn fit_to_area(&mut self, area: Area) {
        //we do this by first finding the correct size.
        //after setting that we can just position the line in the middle of this area
        let max_size = area
            .extent()
            .y
            .min(area.extent().x / self.characters.len() as f32);

        self.size = max_size;
        let new_own_area = self.get_area();
        self.set_position(area.center() - (new_own_area.extent() / 2.0));
    }

    ///Creates a Primary stream that contains the characters of this text line. Note that the
    ///sdf is already offseted based on self.position.
    pub fn record_line(&mut self, mut stream: Primary2dBuilder) -> Primary2dBuilder {
        for c in &mut self.characters {
            stream = c.character.record_modifier_character(
                stream,
                (c.offset * self.size) + self.position,
                self.size,
                self.color,
            );
        }
        stream
    }
}

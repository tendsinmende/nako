/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako::{
    glam::{UVec2, Vec2, Vec3},
    modifiers::Color,
    stream::PrimaryStream2d,
};
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
use nakorender::{
    backend::{Backend, LayerId, LayerId2d, LayerInfo, LayerSampling},
    camera::Camera2d,
    winit::event::{ElementState, Event, KeyboardInput, VirtualKeyCode, WindowEvent},
};

use super::{HorizontalAlignment, TextLine, VerticalAlignment};
use crate::{filler::FillArea, interface::InterfaceElement, Area};
use log::error;

///Text lines, but can receive input. Note that, contrary to the generic `TextLine` this is a
///widget. The caller has to ensure that the line only receives input when
///it is selected.
///Helper methods to signal input state are provided via `set_focused()`
///
///The edits text size will always be fitted to its area. If the size should stay static, avoid scaling.
///Generally the text size depends on the area's height.
pub struct EditableLine {
    area: Area,

    layer_text: LayerId2d,
    layer_cursor: LayerId2d,
    layers: Vec<LayerId>,

    pub(crate) line: TextLine,

    has_changed: bool,

    ///If some, the cursor location. Should be read as "cursor is before char n".
    pub(crate) cursor: Option<usize>,
    is_focused: bool,

    alignment: (HorizontalAlignment, VerticalAlignment),
}

impl EditableLine {
    pub fn new(font: Vec<u8>, value: &str, renderer: &mut dyn Backend) -> Self {
        let mut line = EditableLine {
            area: Area::ONE,
            layer_cursor: renderer.new_layer_2d(),
            layer_text: renderer.new_layer_2d(),
            layers: Vec::with_capacity(2),

            line: TextLine::new(font, value),

            has_changed: true,
            cursor: None,
            is_focused: false,

            alignment: (HorizontalAlignment::Middle, VerticalAlignment::Middle),
        };

        line.layers.push(line.layer_cursor.into());
        line.layers.push(line.layer_text.into());
        line
    }

    ///Inserts the char at the current location. Assumes `assert!(c.is_alphanumeric())`
    //NOTE This is not really performant code, but it works and is more robust than working within the string buffer by hand.
    fn insert_char(&mut self, c: char) {
        //insert char at idx, collect string and set string
        if let Some(cursor_idx) = self.cursor {
            //Special case if the current idx is at the end. In that case the search loop stops one index before. In that case just append to the end and update
            let new_string = if cursor_idx == self.line.characters.len() {
                let mut new = self.line.get_str().to_string();
                new.push(c);
                new
            } else {
                //search and insert
                let mut new_string = String::with_capacity(self.line.get_str().len() + 1);
                for (i, oc) in self.line.characters.iter().enumerate() {
                    if i == cursor_idx {
                        new_string.push(c);
                    }

                    new_string.push(oc.character.character);
                }
                new_string
            };
            self.line.set_str(&new_string);
            self.cursor = Some(cursor_idx + 1);
            self.has_changed = true;
        }
    }

    ///removes a character. If `in_front` is set the char in front of the cursor is removed, otherwise the char behind.
    fn remove_char(&mut self, in_front: bool) {
        //TODO remove idx from line char vec, collect that vec into string and use set string methode
        if let Some(cursor_idx) = self.cursor {
            //Cannot remove in front, if already at front
            if cursor_idx == 0 && in_front {
                return;
            }
            //also cannot remove at back if already at back
            if cursor_idx == self.line.characters.len() && !in_front {
                return;
            }

            let mut new_string = String::with_capacity(self.line.get_str().len());
            let target_idx = if in_front { cursor_idx - 1 } else { cursor_idx };

            for (i, oc) in self.line.characters.iter().enumerate() {
                if i == target_idx {
                    continue;
                } else {
                    new_string.push(oc.character.character);
                }
            }

            self.line.set_str(&new_string);
            self.cursor = Some(target_idx);
            self.has_changed = true;
        }
    }

    pub fn set_focused(&mut self, is_focused: bool, focus_pos: Vec2) {
        if !is_focused {
            self.has_changed = true; //mark as changed, probably have to hide cursor
            self.cursor = None;
            self.is_focused = false;
        } else {
            self.is_focused = true;
            self.cursor = Some(self.line.location_to_char_idx(focus_pos - self.area.from));
            self.has_changed = true;
        };
    }

    fn update_layout(&mut self, renderer: &mut dyn Backend) {
        //Update line positon and size based on current area
        self.line.size = self.area.extent().y;

        //set inner alignment
        self.line.align_in_area(
            self.alignment.0,
            self.alignment.1,
            Area {
                from: Vec2::ZERO,
                to: self.area.extent(),
            },
        );

        let area_as_info = LayerInfo {
            extent: self.area.extent().as_uvec2(),
            location: self.area.from.as_ivec2(),
            sampling: LayerSampling::OverSampling(2),
            ..Default::default()
        };

        renderer.set_layer_info(self.layer_text.into(), area_as_info);
        renderer.update_camera_2d(
            self.layer_text,
            Camera2d {
                extent: self.area.extent(),
                location: Vec2::ZERO,
                rotation: 0.0,
            },
        );

        let stream = self.line.record_line(PrimaryStream2d::new());
        renderer.update_sdf_2d(self.layer_text, stream.build());

        //now check where we have to draw the cursor
        if let Some(cursor_idx) = self.cursor {
            if let Some(char_area) = self.line.get_character_selection_area(cursor_idx) {
                FillArea {
                    color: Color(Vec3::ONE),
                }
                .fill(
                    renderer,
                    self.layer_cursor,
                    Area {
                        from: Vec2::new(char_area.from.x - 2.0, 0.0) + self.area.from,
                        to: Vec2::new(char_area.from.x + 2.0, self.area.extent().y)
                            + self.area.from,
                    },
                );
            } else {
                //sanitize and put behind
                let cursor_idx = self.line.characters.len();
                self.cursor = Some(cursor_idx);

                let draw_area = if self.line.characters.len() > 0 {
                    let char_area = self
                        .line
                        .get_character_selection_area(self.line.characters.len() - 1)
                        .unwrap();
                    Area {
                        from: Vec2::new(char_area.to.x, 0.0) + self.area.from,
                        to: Vec2::new(char_area.to.x + 4.0, self.area.extent().y) + self.area.from,
                    }
                } else {
                    Area {
                        from: self.area.from - Vec2::new(2.0, 0.0),
                        to: Vec2::new(self.area.from.x + 2.0, self.area.to.y),
                    }
                };

                //Now put behind last character
                FillArea {
                    color: Color(Vec3::ONE),
                }
                .fill(renderer, self.layer_cursor, draw_area);
            }
        } else {
            //make sure no cursor is drawn
            renderer.set_layer_info(
                self.layer_cursor.into(),
                LayerInfo {
                    location: self.area.from.as_ivec2(),
                    extent: UVec2::ONE,
                    ..Default::default()
                },
            );
        }
    }

    //Sets inner alignment of this text line within its area
    pub fn set_alignment(&mut self, horizontal: HorizontalAlignment, vertical: VerticalAlignment) {
        self.alignment = (horizontal, vertical);
        self.has_changed = true;
    }
}

impl InterfaceElement for EditableLine {
    fn set_area(&mut self, area: Area) {
        self.area = area;
        self.has_changed = true;
    }

    fn get_layers(&self) -> &[LayerId] {
        &self.layers
    }

    fn on_event(&mut self, renderer: &mut dyn Backend, event: &Event<()>) {
        if self.is_focused {
            match event {
                Event::WindowEvent {
                    event: WindowEvent::ReceivedCharacter(c),
                    ..
                } => {
                    //Check if this is a char, or some control character. If its a char, insert, if not, handle control sequence.
                    //Not sure tho if this is the actual best way to handle this.
                    if c.is_control() {
                        match c {
                            '\u{8}' => {
                                self.remove_char(true);
                            }
                            '\u{7f}' => {
                                self.remove_char(false);
                            }
                            _ => error!("Unhandled control sequence {:?}", c),
                        }
                    } else {
                        self.insert_char(*c);
                    }
                }
                Event::WindowEvent {
                    event:
                        WindowEvent::KeyboardInput {
                            input:
                                KeyboardInput {
                                    virtual_keycode: Some(scan_code),
                                    state,
                                    ..
                                },
                            ..
                        },
                    ..
                } => match (scan_code, state) {
                    (VirtualKeyCode::Left, ElementState::Pressed) => {
                        if let Some(c) = &mut self.cursor {
                            if *c > 0 {
                                *c -= 1;
                                self.has_changed = true;
                            }
                        }
                    }
                    (VirtualKeyCode::Right, ElementState::Pressed) => {
                        if let Some(c) = &mut self.cursor {
                            if *c < self.line.characters.len() {
                                *c += 1;
                                self.has_changed = true;
                            }
                        }
                    }
                    _ => {}
                },
                _ => {}
            }
        }

        if self.has_changed {
            self.has_changed = false;
            self.update_layout(renderer);
        }
    }
}

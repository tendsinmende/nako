/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Text
//! Provides everything you need related to text. We use font_kit to retriev fonts and rustybuzz to translate fonts and
//! text into usable signed distance fields functions.

//Contains helper common to all character realated tasks.
pub(crate) mod helper;

mod editable_line;
pub use editable_line::EditableLine;
mod character;
pub use character::Character;
mod line;
pub use line::TextLine;

mod label;
pub use label::Label;

pub const DEFAULT_FONT: &'static [u8] = include_bytes!("../../resources/Swansea-q3pd.ttf");

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum HorizontalAlignment {
    Left,
    Middle,
    Right,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum VerticalAlignment {
    Top,
    Middle,
    Bottom,
}

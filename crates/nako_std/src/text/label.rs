/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako::{glam::Vec2, modifiers::Color, stream::PrimaryStream2d};
use nakorender::{
    backend::{Backend, LayerId, LayerId2d, LayerInfo},
    camera::Camera2d,
};

use crate::{
    interface::{InterfaceElement, Maskable, Scrollable},
    Area,
};

use super::{TextLine, DEFAULT_FONT};

///Simple self contained label
pub struct Label {
    area: Area,
    mask: Option<Area>,

    is_changed: bool,

    text: TextLine,
    layer_text: LayerId2d,
    layers: Vec<LayerId>,
}

impl Label {
    ///Creates a new label with the given text and text `size`
    pub fn new(renderer: &mut dyn Backend, text: &str, size: f32) -> Self {
        let mut label = Label {
            area: Area::ONE,
            mask: None,
            is_changed: true,
            layer_text: renderer.new_layer_2d(),
            layers: Vec::with_capacity(1),
            text: TextLine::new(DEFAULT_FONT.to_vec(), text),
        };

        label.layers.push(label.layer_text.into());
        label.text.size = size;

        label
    }

    pub fn set_size(&mut self, size: f32) {
        self.is_changed = true;
        self.text.size = size;
    }

    pub fn set_text(&mut self, text: &str) {
        self.is_changed = true;
        self.text.set_str(text);
    }

    pub fn set_color(&mut self, color: Color) {
        self.text.color = color;
        self.is_changed = true;
    }

    fn update_layout(&mut self, renderer: &mut dyn Backend) {
        let mask = if let Some(mask) = self.mask {
            mask
        } else {
            self.area
        };

        self.text.align_in_area(
            super::HorizontalAlignment::Middle,
            super::VerticalAlignment::Middle,
            self.area,
        );

        let label_area = self.text.get_area();

        let draw_area = Area {
            from: label_area.from.max(mask.from),
            to: label_area.to.min(mask.to),
        };

        //Note: we need to offset the cameras "location" by the amount we are masking away
        let camera_location = label_area.from - (self.area.from - mask.from).min(Vec2::ZERO);

        let mut stream = PrimaryStream2d::new();
        stream = self.text.record_line(stream);

        renderer.set_layer_info(
            self.layer_text.into(),
            LayerInfo {
                extent: draw_area.extent().as_uvec2(),
                location: draw_area.from.as_ivec2(),
                sampling: nakorender::backend::LayerSampling::OverSampling(2),
            },
        );

        renderer.update_camera_2d(
            self.layer_text,
            Camera2d {
                extent: draw_area.extent(),
                location: camera_location,
                ..Default::default()
            },
        );

        renderer.update_sdf_2d(self.layer_text, stream.build());
    }
}

impl InterfaceElement for Label {
    fn get_layers(&self) -> &[LayerId] {
        &self.layers
    }

    fn set_area(&mut self, area: Area) {
        self.area = area;
        self.is_changed = true;
    }

    fn on_event(
        &mut self,
        renderer: &mut dyn Backend,
        _event: &nakorender::winit::event::Event<()>,
    ) {
        if self.is_changed {
            self.is_changed = false;
            self.update_layout(renderer);
        }
    }
}

impl Maskable for Label {
    fn mask(&mut self, area: Area) {
        self.is_changed = true;
        self.mask = Some(area);
    }
}

impl Scrollable for Label {
    fn extent_hint(&self) -> Vec2 {
        self.text.get_area().extent()
    }
}

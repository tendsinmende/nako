/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use font_kit::outline::{Contour, PointFlags};
use lyon::{
    geom::{CubicBezierSegment, QuadraticBezierSegment},
    math::point,
    path::{traits::PathBuilder, Event, Path},
};
use nako::{glam::Vec2, primitives::Polygon};

use crate::Area;

use super::character::CharacterShape;

///Tesselates the contoure. Returns the boundery vertices of this conture.
pub fn tesselate_contour(contour: &Contour, tollerance: f32) -> CharacterShape {
    assert!(
        contour.positions.len() >= 2,
        "Conture does not have enough points!"
    );

    let mut path = Path::builder().flattened(tollerance);

    //For each contur we have a compleate spline based on several parts
    let points = contour
        .positions
        .iter()
        .zip(contour.flags.iter())
        .collect::<Vec<_>>();

    let mut verts = Vec::new();

    //Reader index
    let mut r = 1;
    path.begin(point(points[0].0.x(), points[0].0.y()));
    while r < points.len() {
        let r1_flag = if let Some(r1) = points.get(r) {
            *r1.1
        } else {
            //There can't even be a line. In that case break
            break;
        };

        if r1_flag == PointFlags::CONTROL_POINT_0 {
            //Is either a quadric or cubic spline
            let r2_flag = points
                .get(r + 1)
                .expect("Expected at either flagged point, or end point")
                .1;
            if r2_flag == &PointFlags::CONTROL_POINT_1 {
                //Is cubic bezier
                path.cubic_bezier_to(
                    point(points[r + 0].0.x(), points[r + 0].0.y()),
                    point(points[r + 1].0.x(), points[r + 1].0.y()),
                    point(points[r + 2].0.x(), points[r + 2].0.y()),
                );
                r += 3;
            } else {
                //Is quadric bezier
                path.quadratic_bezier_to(
                    point(points[r + 0].0.x(), points[r + 0].0.y()),
                    point(points[r + 1].0.x(), points[r + 1].0.y()),
                );
                r += 2;
            }
        } else {
            //is a line
            path.line_to(point(points[r].0.x(), points[r].0.y()));
            r += 1;
        }
    }

    path.end(true);
    let path = path.build();

    for e in path.iter() {
        match e {
            Event::Begin { at } => {
                //Do not track the beginning,
                verts.push(Vec2::new(at.x, at.y));
            }
            Event::End {
                first,
                last: _,
                close: _,
            } => {
                verts.push(Vec2::new(first.x, first.y));
            }
            Event::Cubic {
                from,
                ctrl1,
                ctrl2,
                to,
            } => {
                //Cubic bezier spline
                let b = CubicBezierSegment {
                    from,
                    ctrl1,
                    ctrl2,
                    to,
                };

                for v in b.flattened(tollerance) {
                    verts.push(Vec2::new(v.x, v.y));
                }
            }
            Event::Quadratic { from, ctrl, to } => {
                let b = QuadraticBezierSegment { from, ctrl, to };
                for v in b.flattened(tollerance) {
                    verts.push(Vec2::new(v.x, v.y));
                }
            }
            Event::Line { from: _, to } => {
                verts.push(Vec2::new(to.x, to.y));
            }
        }
    }

    let area = Area {
        from: verts
            .iter()
            .fold(Vec2::new(f32::INFINITY, f32::INFINITY), |com, v| {
                com.min(*v)
            }),
        to: verts
            .iter()
            .fold(Vec2::new(f32::NEG_INFINITY, f32::NEG_INFINITY), |com, v| {
                com.max(*v)
            }),
    };

    CharacterShape {
        is_subtract: false,
        shape: Polygon { vertices: verts },
        area,
    }
}

pub fn eval_polygone(poly: &Polygon<Vec2>, coord: Vec2) -> f32 {
    let mut d = (coord - poly.vertices[0]).dot(coord - poly.vertices[0]);
    let mut s = 1.0;

    let mut prev_vec = *poly.vertices.last().unwrap();
    let mut this_vec;
    for i in 0..poly.vertices.len() {
        this_vec = poly.vertices[i];
        //distance
        let e = prev_vec - this_vec;
        let w = coord - this_vec;
        let b = w - e * (w.dot(e) / e.dot(e)).clamp(0.0, 1.0);
        d = d.min(b.dot(b));

        //Based on winding number, might change sign
        match (
            coord.y >= this_vec.y,
            coord.y < prev_vec.y,
            e.x * w.y > e.y * w.x,
        ) {
            (true, true, true) | (false, false, false) => s *= -1.0,
            _ => {}
        }

        prev_vec = this_vec;
    }

    s * d.sqrt()
}

///Sets the correct subtraction/addition flags based on the scanline algorithm.
///Also orders them by pushing the "add" polgones to the front
pub fn scanline_contoure(mut contours: Vec<CharacterShape>) -> Vec<CharacterShape> {
    //For each conture check, check from the middl how many "bigger" contures there are. If odd, then we are a hole.
    //Note that we are checking for the smallest distance from the middl of each conture.
    //This actually does only work for convex polygons, but its okay for now i guess. Actual scan line takes too long

    for i in 0..contours.len() {
        let mid = contours[i]
            .shape
            .vertices
            .iter()
            .fold(Vec2::ZERO, |com, v| com + *v);
        let mid = mid / contours[i].shape.vertices.len() as f32;

        let self_d = eval_polygone(&contours[i].shape, mid);
        let mut num_bigger = 0;
        for si in 0..contours.len() {
            if si == i {
                continue;
            }

            let this_d = eval_polygone(&contours[si].shape, mid);
            if this_d < self_d {
                //Make sure that the object is not fully disc
                num_bigger += 1;
            }
        }

        if (num_bigger % 2) == 0 {
            contours[i].is_subtract = false;
        } else {
            contours[i].is_subtract = true;
        }
    }

    //Now order so that adder are drawn first
    contours.sort_by_key(|c| c.is_subtract);

    contours
}

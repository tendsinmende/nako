/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::path::Path;

use font_kit::{
    error::FontLoadingError,
    font::Font,
    hinting::HintingOptions,
    outline::{Outline, OutlineBuilder},
};

use nako::{
    combinators::{Subtraction, Union},
    glam::Vec2,
    modifiers::{Color, Transform},
    primitives::Polygon,
    stream::{Primary2dBuilder, Secondary2dBuilder, SecondaryStream2d},
};

use crate::{text::helper::scanline_contoure, Area};

use super::helper::tesselate_contour;

///Native character shape information
pub struct CharacterShape {
    ///If conture should be subtracted or unioned to the already existing shape
    pub is_subtract: bool,
    ///polygone approximating the shape
    pub shape: Polygon<Vec2>,
    ///area covered by this shape
    pub area: Area,
}

//TODO
// Therfore we should use rustybuzz for shaping the glyphs, the use fontkit for get the correct outlines,
// give those to lyon for tesselation and then build the vector of tesselated triangles, or better a single
// big "outline" polygone from which the holes are cut via Subtraction within the sdf.

///A single character based on some font. The character is guaranteed to have at max a size of `1x1`.
///However the actual area covered by this character might be smaller, depending on the char and the font.
///This is a thin primitive like type, layer handling, scaling, translation etc. have to be handled by the caller.
pub struct Character {
    area: Area,
    ///The character represented by this shape
    pub(crate) character: char,
    ///Characters outlines, used for retesselating when changing size.
    //TODO not sure if this should be cached or not. Maybe a lot of data in here.
    outline: Outline,
    ///Shape parts that make up this character.
    pub shapes: Vec<CharacterShape>,
    ///Factor used when re tesselating a character on size change. Increase if you are getting performance problems with a lot of text.
    tess_factor: f32,
}

impl Character {
    pub const DEFAULT_TESSELATION_FACTOR: f32 = 0.05;

    pub fn from_file(file: impl AsRef<Path>, character: char) -> Result<Self, FontLoadingError> {
        let font = Font::from_path(file, 0)?;
        Ok(Self::from_font(&font, character))
    }

    ///Loads the character from the font. Have a look at `font-kit` on how to load such a font.
    ///There are ways to dynamically determin a correct font on the system based on parameters, of by specifying a file.
    ///
    /// # Panics
    /// Note that the function will panic if the font contains quadric bezier splines (for now). Only Cubic bezier splines are supported at the moment.
    pub fn from_font(font: &Font, character: char) -> Self {
        let id = if let Some(glyph_id) = font.glyph_for_char(character) {
            glyph_id
        } else {
            panic!("No glyph for char in font");
        };

        Self::from_glyph_id(font, id, character)
    }

    pub fn from_glyph_id(font: &Font, id: u32, character: char) -> Self {
        let add = font.metrics().descent.abs();
        let div = add + font.metrics().ascent;

        let mut outline_builder = OutlineBuilder::new();
        font.outline(id, HintingOptions::None, &mut outline_builder)
            .unwrap();

        let mut outline = outline_builder.into_outline();

        //Prepare outline infos for
        for c in &mut outline.contours {
            for p in &mut c.positions {
                p.set_x(p.x() / div);
                p.set_y(1.0 - (p.y() + add) / div); //flip y
            }
        }

        let mut c = Character {
            area: Area::ONE, //updated on retesselation.
            character,
            outline,
            shapes: vec![], //initialized in next step
            tess_factor: Self::DEFAULT_TESSELATION_FACTOR,
        };

        c.retesselate();
        c
    }

    ///re tesselates self based on current size
    fn retesselate(&mut self) {
        let tesselation_factor = self.tess_factor;
        let shapes = self
            .outline
            .contours
            .iter()
            .map(|c| tesselate_contour(c, tesselation_factor))
            .collect::<Vec<_>>();

        let shapes = scanline_contoure(shapes);
        self.shapes = shapes;
        //update inner area again.
        self.update_area();
    }

    pub fn with_tesselation_factor(mut self, factor: f32) -> Self {
        self.tess_factor = factor;
        self.retesselate();
        self
    }

    pub fn as_char(&self) -> &char {
        &self.character
    }

    ///recalculates current area from current shapes
    fn update_area(&mut self) {
        //There is an edge case with empty characters that would return an infinity bbox. Catch that and return an empty bbox instead
        if self.shapes.len() == 0 {
            self.area = Area::ZERO;
        }

        self.area = Area {
            from: self
                .shapes
                .iter()
                .fold(Vec2::new(f32::INFINITY, f32::INFINITY), |com, s| {
                    com.min(s.area.from)
                }),
            to: self
                .shapes
                .iter()
                .fold(Vec2::new(f32::NEG_INFINITY, f32::NEG_INFINITY), |com, s| {
                    com.max(s.area.to)
                }),
        }
    }

    pub fn get_area(&self) -> Area {
        self.area
    }

    ///Records the character to the stream. `scale`s and `offset`s character.
    ///
    ///Note that the standard scale is 1.0. When wanting to lay out characters at `size = 10px` simple scaling via `10` is enough.
    ///Similarly the standard position (of the lower left corner) of each character is `0.0`. Therefore, the scaled area of each character can be takes as reference when laying out character by hand.
    ///
    /// Modification of the build stream is done through the `modifier` function, which gets called on each shapes secondary stream.
    /// Information like color, offset scaling etc can be attached here.
    ///
    ///However, when working with several character `TextLine` should be used instead. It internally uses rustybuzz to do actual layouting.
    pub fn record_character<MOD>(
        &self,
        mut stream: Primary2dBuilder,
        scaling: f32,
        modifier: MOD,
    ) -> Primary2dBuilder
    where
        MOD: Fn(Secondary2dBuilder) -> Secondary2dBuilder,
    {
        for s in &self.shapes {
            let mut poly = s.shape.clone();

            //Scale on demand
            //TODO remove the scaling and use a inversely scale area instead
            for v in &mut poly.vertices {
                *v *= scaling;
            }

            let mut sub = if s.is_subtract {
                SecondaryStream2d::new(Subtraction, poly)
            } else {
                SecondaryStream2d::new(Union, poly)
            };

            sub = modifier(sub);

            stream = stream.push(sub.build());
        }
        stream
    }

    ///simplification of `record_character` that handles the most common usecase (scaled, colored rendering at a location) for you.
    pub fn record_modifier_character(
        &self,
        stream: Primary2dBuilder,
        translation: Vec2,
        scale: f32,
        color: Color,
    ) -> Primary2dBuilder {
        self.record_character(stream, scale, |shape| {
            shape.push_mod(color).push_mod(Transform {
                translation,
                rotation: 0.0,
            })
        })
    }
}

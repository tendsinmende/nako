/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! #Nako Standard Library
//!
//! This crate contains common data structures one might need when working with nako.

#![deny(warnings)]
use nako::{glam::Vec2, modifiers::Translate, primitives::Cuboid};

///Interface rendering related structures
pub mod interface;
///Provides the capabilities to generate character SDFs from provided fonts.
///Also provides text layouting capabilities to lay out those characters correctly as text.
#[cfg(feature = "fonts")]
pub mod text;

///Provides several "filler" types, that just fill an area on a layer with some
///pattern.
pub mod filler;

///Defines some 2d area. The domain is chosen by the application, however,
///this probably is always either "pixels" or in the [0-1] domain.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Area {
    pub from: Vec2,
    pub to: Vec2,
}

impl Area {
    ///Area from (0,0) to (1,1)
    pub const ONE: Area = Area {
        from: Vec2::ZERO,
        to: Vec2::ONE,
    };
    pub const ZERO: Area = Area {
        from: Vec2::ZERO,
        to: Vec2::ZERO,
    };

    pub fn is_in(&self, position: Vec2) -> bool {
        self.from.x < position.x
            && self.from.y < position.y
            && self.to.x >= position.x
            && self.to.y >= position.y
    }

    #[inline]
    pub fn extent(&self) -> Vec2 {
        self.to - self.from
    }

    #[inline]
    pub fn center(&self) -> Vec2 {
        self.to - (self.extent() / 2.0)
    }
    /// returns self as a Box2d primitive including its transformation into worldspace
    pub fn as_box_2d(&self) -> (Cuboid<Vec2>, Translate<Vec2>) {
        (
            Cuboid {
                extent: self.extent() / 2.0,
            },
            Translate(self.center()),
        )
    }
}

///Orientation of some object. Depends on the context it is used in.
pub enum Orientation2d {
    Horizontal,
    Vertical,
}

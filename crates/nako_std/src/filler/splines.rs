/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use log::{error, warn};
use nako::{glam::Vec2, modifiers::Color, primitives::QuadricBezier};
use nakorender::backend::{Backend, LayerId2d};

use crate::Area;

///Draws an spline that smoothly passes `points` in their order.
pub struct QuadricBezierSpline {
    ///Control points, interpreted as the coordinates of the layers camera space.
    pub points: Vec<Vec2>,
    ///Color in which the spline is drawn
    pub color: Color,
    ///width of the spline
    pub width: f32,
}

impl QuadricBezierSpline {
    ///Draws the splines into `area`, interpreting area's from and to vectors as pixels, and the inner
    /// `points` as pixel locations as well.
    pub fn record_to_layer(&self, _layer_id: LayerId2d, _renderer: &mut dyn Backend, _area: Area) {
        let _ = self.into_segments();
    }

    ///Converts the spline into Bezier2d segments that can be rendered by nako.
    fn into_segments(&self) -> Vec<QuadricBezier<Vec2>> {
        //iterativly build segments by taking triple of points, each interpreting as start, ctrl, end.
        //at each iteration take a new end point, and retire the old start point.
        let mut segments = Vec::with_capacity(self.points.len() / 3);
        let mut ptr_iter = self.points.iter();

        let mut triple = (ptr_iter.next(), ptr_iter.next(), ptr_iter.next());

        loop {
            //Depending on what information we have, create a line
            let line = match triple {
                (Some(start), Some(ctrl), Some(end)) => {
                    //Got full information
                    QuadricBezier {
                        start: *start,
                        end: *end,
                        controll: *ctrl,
                        width: self.width,
                    }
                }
                (Some(_start), Some(_ctrl), None) => {
                    error!("Spline with two points unimplemented!");
                    QuadricBezier {
                        controll: Vec2::ZERO,
                        end: Vec2::ZERO,
                        start: Vec2::ZERO,
                        width: self.width,
                    }
                }
                (Some(_start), None, None) => {
                    error!("Spline with single point unimplemented!");
                    QuadricBezier {
                        controll: Vec2::ZERO,
                        end: Vec2::ZERO,
                        start: Vec2::ZERO,
                        width: self.width,
                    }
                }
                _ => {
                    warn!("Malformed spline, tripel was: {:?}", triple);
                    break;
                }
            };

            //move points
            triple = (triple.1, triple.0, ptr_iter.next());

            //push line into segments
            segments.push(line);

            //check if we've got to end
            if let (None, None, None) = triple {
                break;
            }
        }

        segments
    }
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako::{
    combinators::Union,
    glam::Vec2,
    modifiers::{Color, Round, Translate},
    primitives::{Cuboid, Line},
    stream::{PrimaryStream2d, SecondaryStream2d},
};
use nakorender::{
    backend::{Backend, LayerId2d, LayerInfo},
    camera::Camera2d,
};

use crate::Area;

pub mod splines;
pub use splines::QuadricBezierSpline;

///Fills an area on a layer with just a color
pub struct FillArea {
    pub color: Color,
}

impl FillArea {
    ///Fillst `area` on screen with the given color on the layer. note that those pixel are therefore interpreted as pixels.
    ///The layer might be changed to fit best to the area.
    pub fn fill(&self, renderer: &mut dyn Backend, id: LayerId2d, area: Area) {
        renderer.set_layer_info(
            id.into(),
            LayerInfo {
                location: area.from.as_ivec2(),
                extent: area.extent().as_uvec2(),
                ..Default::default()
            }, //NOTE we use application wide the convention that the layers extent is synonymouse with the pixel width/height
        );

        renderer.update_sdf_2d(
            id,
            PrimaryStream2d::new()
                .push(
                    SecondaryStream2d::new(
                        Union,
                        Cuboid {
                            extent: Vec2::splat(0.5),
                        },
                    )
                    .push_mod(self.color)
                    .push_mod(Translate(Vec2::splat(0.5)))
                    .build(),
                )
                .build(),
        );

        renderer.update_camera_2d(
            id,
            Camera2d {
                extent: Vec2::ONE,
                location: Vec2::ZERO,
                rotation: 0.0,
            },
        );
    }
}

pub struct FillAreaRound {
    pub color: Color,
    pub radius: f32,
}

impl FillAreaRound {
    ///Fillst `area` on screen with the given color on the layer. note that those pixel are therefore interpreted as pixels.
    ///The layer might be changed to fit best to the area.
    ///
    /// Also note that, if the radius is bigger then any extent (x or y) it will be shrunken to
    /// that extent
    pub fn fill(&self, renderer: &mut dyn Backend, id: LayerId2d, area: Area) {
        let radius = self.radius.clamp(0.0, area.extent().min_element().max(0.0));

        renderer.set_layer_info(
            id.into(),
            LayerInfo {
                location: area.from.as_ivec2(),
                extent: area.extent().as_uvec2(),
                ..Default::default()
            }, //NOTE we use application wide the convention that the layers extent is synonymouse with the pixel width/height
        );

        renderer.update_sdf_2d(
            id,
            PrimaryStream2d::new()
                .push(
                    SecondaryStream2d::new(
                        Union,
                        Cuboid {
                            extent: (area.extent() / 2.0) - Vec2::splat(radius),
                        },
                    )
                    .push_mod(Round { radius })
                    .push_mod(self.color)
                    .build(),
                )
                .build(),
        );

        renderer.update_camera_2d(
            id,
            Camera2d {
                extent: area.extent(),
                location: -area.extent() / 2.0,
                rotation: 0.0,
            },
        );
    }

    pub fn fill_masked(&self, renderer: &mut dyn Backend, id: LayerId2d, area: Area, mask: Area) {
        let masked_area = Area {
            from: area.from.max(mask.from),
            to: area.to.min(mask.to),
        };

        let radius = self.radius.clamp(0.0, area.extent().min_element().max(0.0));

        renderer.set_layer_info(
            id.into(),
            LayerInfo {
                location: masked_area.from.as_ivec2(),
                extent: masked_area.extent().as_uvec2(),
                ..Default::default()
            }, //NOTE we use application wide the convention that the layers extent is synonymouse with the pixel width/height
        );

        renderer.update_sdf_2d(
            id,
            PrimaryStream2d::new()
                .push(
                    SecondaryStream2d::new(
                        Union,
                        Cuboid {
                            extent: (area.extent() / 2.0) - Vec2::splat(radius),
                        },
                    )
                    .push_mod(Round { radius })
                    .push_mod(self.color)
                    .build(),
                )
                .build(),
        );

        renderer.update_camera_2d(
            id,
            Camera2d {
                extent: area.extent(),
                location: -area.extent() / 2.0,
                rotation: 0.0,
            },
        );
    }
}

pub struct LineDrawer {
    pub color: Color,
    pub lines: Vec<Line<Vec2>>,
}

impl LineDrawer {
    ///Draws the `lines` by interpreting the lines coordinates as pixel locations.
    pub fn draw(&self, renderer: &mut dyn Backend, id: LayerId2d) {
        //find min and max values to generate bbox of our drawing
        let mut draw_area = self.lines.iter().fold(
            Area {
                from: Vec2::splat(f32::INFINITY),
                to: Vec2::splat(f32::NEG_INFINITY),
            },
            |mut area, line| {
                area.from = area
                    .from
                    .min(line.start - line.width)
                    .min(line.end - line.width);
                area.to = area
                    .to
                    .max(line.start + line.width)
                    .max(line.end + line.width);
                area
            },
        );
        //forbit negative values

        draw_area.from = draw_area.from.max(Vec2::ZERO);
        draw_area.to = draw_area.to.max(Vec2::ZERO);

        //setup canvas
        renderer.set_layer_info(
            id.into(),
            LayerInfo {
                extent: draw_area.extent().as_uvec2(),
                location: draw_area.from.as_ivec2(),
                ..Default::default()
            },
        );

        //Setup camera to see 0-1 region, in that case we can just normalize the line coords and are good to go
        renderer.update_camera_2d(
            id,
            Camera2d {
                extent: draw_area.extent(),
                location: draw_area.from,
                rotation: 0.0,
            },
        );

        let sdf = self
            .lines
            .iter()
            .fold(PrimaryStream2d::new(), |mut stream, line| {
                stream = stream.push(
                    SecondaryStream2d::new(Union, *line)
                        .push_mod(self.color)
                        .build(),
                );
                stream
            })
            .build();

        renderer.update_sdf_2d(id, sdf);
    }
}

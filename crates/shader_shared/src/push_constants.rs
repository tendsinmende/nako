/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::{GpuCamera, GpuCamera2d};
use spirv_std::glam::{UVec3, Vec3};

#[cfg(target_arch = "spirv")]
use spirv_std::num_traits::Float;

pub const SAMPLING_FLAG_NONE_SAMPLING: u32 = 0b00000000_00000000_00000000_00000001;
pub const SAMPLING_FLAG_OVER_SAMPLING: u32 = 0b00000000_00000000_00000000_00000010;
pub const SAMPLING_FLAG_UNDER_SAMPLING: u32 = 0b00000000_00000000_00000000_00000100;

#[derive(Clone)]
#[repr(C)]
pub struct LayerCombinePushConst {
    pub origin: [i32; 2],
    pub pad0: [i32; 2],
    pub extent: [u32; 2],
    pub pad1: [u32; 2],
}

#[derive(Clone)]
#[repr(C)]
pub struct ResolveConst {
    pub sampling_param: u32,
    ///Flags combination from SAMPLING_FLAG_*
    pub sampling_flags: u32,
    pub pad0: [u32; 2],
}

#[derive(Clone)]
#[repr(C)]
pub struct Primary3dConst {
    pub camera: GpuCamera,
}

#[derive(Clone)]
#[repr(C)]
pub struct Primary2dConst {
    pub camera: GpuCamera2d,
}

#[derive(Clone)]
#[repr(C)]
pub struct DirectLightConst {
    pub camera: GpuCamera,
}

#[derive(Clone)]
#[repr(C)]
pub struct ShadingPushConst {
    pub gamma: f32,
    pub pad0: [f32; 3],
}

#[derive(Clone)]
#[repr(C)]
pub struct QueryVolumePushConst {
    ///resolution of the attached volume
    pub resolution: u32,
    ///Volume flags.
    /// currently only flags if the volume encloses the whole sdf, or only a part.
    ///
    /// ID, Meaning
    /// 0:  True if the whole sdf is within the volumes bounds
    pub flags: u32,

    pub pad0: [u32; 2],

    ///Bound min
    pub min: [f32; 3],
    ///extent of a single voxel in worldspace
    pub voxel_size: f32,
    ///bound max
    pub max: [f32; 3],
    pub pad2: f32,
}

impl QueryVolumePushConst {
    pub const FLAG_WITHIN_BOUNDS: u32 = 0b0000_0000_0000_0000_0000_0000_0000_0001;
    ///Descriptor for an empty query volume
    pub const EMPTY: Self = QueryVolumePushConst {
        min: [0.0; 3],
        max: [0.0; 3],
        flags: 0b1,
        resolution: 1,
        voxel_size: 1.0,
        pad0: [0; 2],
        pad2: 0.0,
    };

    ///Returns true if the whole sdf is enclosed within the volumes bound
    #[inline]
    pub fn sdf_enclosed(&self) -> bool {
        (self.flags & Self::FLAG_WITHIN_BOUNDS) == Self::FLAG_WITHIN_BOUNDS
    }

    ///Converts a global invocation index into the world location of this voxel.
    ///Note that the bounds are not checked. Therefore sampling outside the array could occure.
    pub fn global_index_to_location(&self, index: UVec3) -> Vec3 {
        Vec3::from(self.min) + index.as_vec3() * Vec3::splat(self.voxel_size)
    }

    ///Converts a global invocation index to a voxel index. Note that the index bounds are not checked for the query volume.
    ///
    ///Indexing is in order x,y,z. Meaning the index increases monotonic like this:
    ///
    ///```rust
    /// let mut index = 0;
    /// for x in 0..self.resolution{
    ///     for y in 0..resolution{
    ///         for z in 0..resolution{
    ///             index += 1;
    ///         }
    ///     }
    /// }
    ///```
    pub fn global_index_to_1d_index(&self, index: UVec3) -> usize {
        (index.x * (self.resolution * self.resolution) + index.y * self.resolution + index.z)
            as usize
    }

    ///Generates the voxelsize from the internal resolution and bounds.
    ///Should not be called on a per kernel-invocation basis, but on the cpu when setting up
    /// the shader call.
    pub fn set_voxel_size(&mut self) {
        let extent = Vec3::from(self.max) - Vec3::from(self.min);
        //NOTE we use the size of the biggest axis to be sure to cover the whole bound. This
        // becomes ineffective fast wen having non-cubic scenes though.
        self.voxel_size = extent.max_element() / (self.resolution as f32);
    }

    ///Takes `pos` within the volumes bound and returns the voxel coordinate associated with this position.
    ///Returns `UVec3::splat(u32::MAX)` if the location would be outside the bounds.
    ///
    /// note `pos` is considered "worldspace".
    pub fn pos_to_index(&self, mut pos: Vec3) -> UVec3 {
        //first move the pos into our space
        pos -= Vec3::from(self.min);
        //Now check index. We are rounding each axis to the next whole voxel index
        let coord = pos / Vec3::splat(self.voxel_size);
        //Check if the coord is in bound, otherwise return NAN
        if coord.max_element().round() >= (self.resolution as f32)
            || coord.min_element().round() < 0.0
        {
            return UVec3::splat(u32::MAX);
        }

        UVec3::new(
            coord.x.round() as u32,
            coord.y.round() as u32,
            coord.z.round() as u32,
        )
    }
}

#[derive(Clone)]
#[repr(C)]
pub struct Primary3dSegTraConst {
    pub camera: GpuCamera,
    pub volume: QueryVolumePushConst,
}

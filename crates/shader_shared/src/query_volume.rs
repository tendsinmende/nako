///NOTE later this is encoded in a Volume texture for faster fetches.
#[derive(Clone)]
#[repr(C)]
pub struct QueryVoxel {
    ///local lipschitz bound
    pub bound: [f32; 3],
    ///distance to next surface
    pub dist: f32,
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

///Gpu special imports
#[cfg(target_arch = "spirv")]
use spirv_std::num_traits::Float;

use crate::glam::{Vec2, Vec3};

use super::fabs;

///Glsl like sign function. Returns -1 if negative, 0 when 0 and 1 if positive.
#[inline]
pub fn sign(i: f32) -> f32 {
    if i == 0.0 {
        0.0
    } else if i > 0.0 {
        1.0
    } else {
        -1.0
    }
}

#[inline]
pub fn dot2_vec3(i: Vec3) -> f32 {
    i.dot(i)
}

///Creates dot product with self (dot2(a) == a.dot(a))
#[inline]
pub fn dot2(i: Vec2) -> f32 {
    i.dot(i)
}

pub fn sphere(coord: Vec3, radius: f32) -> f32 {
    coord.length() - radius
}

pub fn box_exact(coord: Vec3, extent: Vec3) -> f32 {
    let q = Vec3::new(fabs(coord.x), fabs(coord.y), fabs(coord.z)) - extent;
    (q.max(Vec3::ZERO)).length() + q.max_element().min(0.0)
    //(coord.abs() - self.extent).max_element()
}

pub fn plane(coord: Vec3, normal: Vec3, height: f32) -> f32 {
    coord.dot(normal.normalize()) - height
}

pub fn line3d(coord: Vec3, from: Vec3, to: Vec3, width: f32) -> f32 {
    let a = from;
    let b = to;

    let pa = coord - a;
    let ba = b - a;
    let h = (pa.dot(ba) / ba.dot(ba)).clamp(0.0, 1.0);
    (pa - ba * h).length() - width
}

pub fn triangle(coord: Vec3, verts: [Vec3; 3]) -> f32 {
    //Find edges
    let ba = verts[1] - verts[0];
    let pa = coord - verts[0];

    let cb = verts[2] - verts[1];
    let pb = coord - verts[1];

    let ac = verts[0] - verts[2];
    let pc = coord - verts[2];

    let norm = ba.cross(ac);

    (if sign(ba.cross(norm).dot(pa)) + sign(cb.cross(norm).dot(pb)) + sign(ac.cross(norm).dot(pc))
        < 2.0
    {
        dot2_vec3(ba * (ba.dot(pa) / dot2_vec3(ba)).clamp(0.0, 1.0) - pa)
            .min(dot2_vec3(
                cb * (cb.dot(pb) / dot2_vec3(cb)).clamp(0.0, 1.0) - pb,
            ))
            .min(dot2_vec3(
                ac * (ac.dot(pc) / dot2_vec3(ac)).clamp(0.0, 1.0) - pc,
            ))
    } else {
        norm.dot(pa) * norm.dot(pa) / dot2_vec3(norm)
    })
    .sqrt()
}

pub fn triangle_2d(coord: Vec2, verts: [Vec2; 3]) -> f32 {
    let e0 = verts[1] - verts[0];
    let e1 = verts[2] - verts[1];
    let e2 = verts[0] - verts[2];

    let v0 = coord - verts[0];
    let v1 = coord - verts[1];
    let v2 = coord - verts[2];

    let pq0 = v0 - e0 * (v0.dot(e0) / e0.dot(e0)).clamp(0.0, 1.0);
    let pq1 = v1 - e1 * (v1.dot(e1) / e1.dot(e1)).clamp(0.0, 1.0);
    let pq2 = v2 - e2 * (v2.dot(e2) / e2.dot(e2)).clamp(0.0, 1.0);
    let s = sign(e0.x * e2.y - e0.y * e2.x);

    let d = Vec2::new(pq0.dot(pq0), s * (v0.x * e0.y - v0.y * e0.x))
        .min(Vec2::new(pq1.dot(pq1), s * (v1.x * e1.y - v1.y * e1.x)))
        .min(Vec2::new(pq2.dot(pq2), s * (v2.x * e2.y - v2.y * e2.x)));

    -d.x.sqrt() * sign(d.y)
}

///Calculates berzier distance from parameters.
#[inline]
pub fn bezier2d(p: Vec2, start: Vec2, ctrl: Vec2, end: Vec2, width: f32) -> f32 {
    let a = ctrl - start;
    let b = start - 2.0 * ctrl + end;
    let c = a * 2.0;
    let d = start - p;

    let kk = 1.0 / b.dot(b);
    let kx = kk * a.dot(b);
    let ky = kk * (2.0 * a.dot(a) + d.dot(b)) / 3.0;
    let kz = kk * d.dot(a);

    let p = ky - kx * kx;
    let p3 = p * p * p;
    let q = kx * (2.0 * kx * kx - 3.0 * ky) + kz;
    let h = q * q + 4.0 * p3;

    let res = if h >= 0.0 {
        //first root
        let h = h.sqrt();
        let x = (Vec2::new(h, -h) - Vec2::new(q, q)) / 2.0;
        let uv = Vec2::new(sign(x.x), sign(x.y))
            * Vec2::new(fabs(x.x).powf(1.0 / 3.0), fabs(x.y).powf(1.0 / 3.0));
        let t = (uv.x + uv.y - kx).clamp(0.0, 1.0);
        dot2(d + (c + b * t) * t)
    } else {
        //3rd root
        let z = (-p).sqrt();
        let v = (q / (p * z * 2.0)).acos() / 3.0;
        let m = v.cos();
        let n = v.sin() * 1.732050808;
        let t = (Vec3::new(m + m, -n - m, n - m) * z - Vec3::new(kx, kx, kx))
            .clamp(Vec3::ZERO, Vec3::ONE);
        dot2(d + (c + b * t.x) * t.x).min(dot2(d + (c + b * t.y) * t.y))
    };

    res.sqrt() - width
}

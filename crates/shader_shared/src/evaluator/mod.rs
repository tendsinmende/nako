/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use spirv_std::glam::Vec4Swizzles;
///Gpu special imports
#[cfg(target_arch = "spirv")]
use spirv_std::num_traits::Float;

use crate::{
    glam::{Mat3, Mat4, Vec2, Vec3, Vec4},
    BBox,
};
use nako_instruction::{NakoResult, OpTy, Word};

use self::primitives::{bezier2d, box_exact, line3d, plane, sphere, triangle, triangle_2d};

pub mod primitives;

#[inline]
pub fn as_f32(s: u32) -> f32 {
    f32::from_bits(s)
}

#[inline]
pub fn pop_vec2(code: &[Word], offset: usize) -> Vec2 {
    Vec2::new(as_f32(code[offset + 0]), as_f32(code[offset + 1]))
}

//Note current walkaround methode for abs since the actual abs generates 64bit instructions that we dont want
#[inline]
pub fn fabs(f: f32) -> f32 {
    if f < 0.0 {
        f * -1.0
    } else {
        f
    }
}

#[inline]
pub fn pop_vec3(code: &[Word], offset: usize) -> Vec3 {
    Vec3::new(
        as_f32(code[offset + 0]),
        as_f32(code[offset + 1]),
        as_f32(code[offset + 2]),
    )
}

#[inline]
pub fn pop_vec4(code: &[Word], offset: usize) -> Vec4 {
    Vec4::new(
        as_f32(code[offset + 0]),
        as_f32(code[offset + 1]),
        as_f32(code[offset + 2]),
        as_f32(code[offset + 3]),
    )
}

//FIXME: This function is garbage, should cast a slice of 16 words into the matrix
//
#[inline]
pub fn pop_mat4(code: &[Word], offset: usize) -> Mat4 {
    Mat4::from_cols_array(&[
        as_f32(code[offset + 0]),
        as_f32(code[offset + 1]),
        as_f32(code[offset + 2]),
        as_f32(code[offset + 3]),
        as_f32(code[offset + 4]),
        as_f32(code[offset + 5]),
        as_f32(code[offset + 6]),
        as_f32(code[offset + 7]),
        as_f32(code[offset + 8]),
        as_f32(code[offset + 9]),
        as_f32(code[offset + 10]),
        as_f32(code[offset + 11]),
        as_f32(code[offset + 12]),
        as_f32(code[offset + 13]),
        as_f32(code[offset + 14]),
        as_f32(code[offset + 15]),
    ])
}

/*
pub fn eval_from_code(coord: Vec3, code: &[Word]) -> NakoResult{

    let mut is = true;
    is &= code[0] == 0;
    is &= code[1] == 3212836864;
    is &= code[2] == 3212836864;
    is &= code[3] == 3212836864;
    is &= code[4] == 1065353216;
    is &= code[5] == 1065353216;
    is &= code[6] == 1065353216;
    is &= code[7] == 7;
    is &= code[8] == 16;
    is &= code[9] == 1065353216;
    is &= code[10] == 15;
    is &= code[11] == 1065353216;
    is &= code[12] == 1061997773;
    is &= code[13] == 1058642330;
    is &= code[14] == 1;

    NakoResult::default()
}
*/

#[inline]
pub fn eval_from_code(coord: Vec3, code: &[Word]) -> NakoResult {
    if code.len() == 0 {
        return NakoResult::INVALID;
    }

    let mut ptr = 0;
    let len = code.len();
    let mut coord = coord;
    let mut primary = NakoResult::default();
    let mut secondary = NakoResult::default();

    while ptr < len {
        let op: OpTy = code[ptr].into();
        match op {
            OpTy::Skip => {
                if ptr + 7 >= len {
                    return NakoResult::INVALID;
                }
                let bound = BBox {
                    min: Vec3::new(
                        as_f32(code[ptr + 1]),
                        as_f32(code[ptr + 2]),
                        as_f32(code[ptr + 3]),
                    ),
                    max: Vec3::new(
                        as_f32(code[ptr + 4]),
                        as_f32(code[ptr + 5]),
                        as_f32(code[ptr + 6]),
                    ),
                };

                if !bound.is_sphere_intersecting(coord, primary.result) {
                    let skip_to = code[ptr + 7];
                    ptr = ptr + skip_to as usize;
                }

                ptr += 8;
                //We can safely skip when the the current primary result is nearer then the whole "to be skipped" bound
            }

            //Combinators
            OpTy::Union => {
                primary = primary.union(&secondary);

                //Reset secondary
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothUnion => {
                if ptr + 1 >= len {
                    return NakoResult::invalid_with_color([1.0, 0.0, 0.0, 1.0]);
                }
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_union(&secondary, smoothness);
                //Reset secondary
                secondary = NakoResult::default();
                ptr += 2;
            }
            OpTy::Intersection => {
                primary = primary.intersection(&secondary);
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothIntersection => {
                if ptr + 1 >= len {
                    return NakoResult::invalid_with_color([1.0, 0.0, 0.0, 1.0]);
                }
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_intersection(&secondary, smoothness);
                secondary = NakoResult::default();
                ptr += 2;
            }
            OpTy::Subtraction => {
                primary = primary.subtraction(&secondary);
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothSubtraction => {
                if ptr + 1 >= len {
                    return NakoResult::invalid_with_color([1.0, 0.0, 0.0, 1.0]);
                }
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_subtraction(&secondary, smoothness);
                secondary = NakoResult::default();
                ptr += 2;
            }
            //Volumetric effects
            OpTy::Round => {
                secondary.result -= as_f32(code[ptr + 1]);
                ptr += 2;
            }

            OpTy::Onion => {
                secondary.result = fabs(secondary.result) - as_f32(code[ptr + 1]);
                ptr += 2;
            }

            //NOTE currently ignoring projections
            OpTy::PlanarProjection => {
                if ptr + 2 >= len {
                    return NakoResult::invalid_with_color([0.0, 1.0, 0.0, 1.0]);
                }

                let code_len = code[ptr + 1] as usize;
                ptr += 2 + code_len;
            }

            OpTy::Revolution => {
                if ptr + 2 >= len {
                    return NakoResult::invalid_with_color([0.0, 1.0, 0.0, 1.0]);
                }
                let codelen = code[ptr + 2] as usize;

                ptr += 3 + codelen;
            }

            OpTy::Extrude => {
                if ptr + 2 >= len {
                    return NakoResult::invalid_with_color([0.0, 1.0, 0.0, 1.0]);
                }
                let codelen = code[ptr + 2] as usize;

                ptr += 3 + codelen;
            }
            /*
                        //2d -> 3d Projections
                        OpTy::PlanarProjection => {
                            if ptr + 2 >= len {
                                return NakoResult::INVALID;
                            }
                            let code_len = code[ptr + 1] as usize;

                            //Currently just projecting xy, TODO find actual worldspace mapping based on UV
                            let coord2d = coord.xz();

                            let res2d = eval_2d_from_code(coord2d, code, ptr + 2, ptr + 2 + code_len);

                            if res2d.result.is_nan() {
                                return NakoResult::INVALID;
                            }

                            //If the projection point is inside the form, return just the length, otherwise the distance to the next edge
                            if res2d.result >= 0.0 {
                                //Calculate distance relative to the intersection distance
                                let on_plane_coord = Vec3::new(coord.x, 0.0, coord.z) + res2d.result * Vec3::X;
                                secondary.result = (coord - on_plane_coord).length();
                            } else {
                                secondary.result = fabs(coord.y);
                            }

                            secondary.slot_v0 = res2d.slot_v0;
                            secondary.slot_v1 = res2d.slot_v1;
                            secondary.slot_i0 = res2d.slot_i0;
                            secondary.shading_model = res2d.shading_model;

                            ptr += 2 + code_len;
                        }
                        OpTy::Revolution => {
                            if ptr + 2 >= len {
                                return NakoResult::INVALID;
                            }
                            let codelen = code[ptr + 2] as usize;

                            let offset = as_f32(code[ptr + 1]);
                            //Move into 2d space
                            let q = Vec2::new(coord.xz().length() - offset, coord.y);
                            secondary = eval_2d_from_code(q, code, ptr + 3, ptr + 3 + codelen);

                            ptr += 3 + codelen;
                        }
                        OpTy::Extrude => {
                            if ptr + 2 >= len {
                                return NakoResult::INVALID;
                            }
                            let codelen = code[ptr + 2] as usize;

                            let h = as_f32(code[ptr + 1]);
                            let newres = eval_2d_from_code(coord.xy(), code, ptr + 3, ptr + 3 + codelen);
                            let w = Vec2::new(newres.result, fabs(coord.z) - h);
                            secondary.result = w.max_element().min(0.0) + w.max(Vec2::ZERO).length();

                            secondary.slot_v0 = newres.slot_v0;
                            secondary.slot_v1 = newres.slot_v1;
                            secondary.slot_i0 = newres.slot_i0;
                            secondary.shading_model = newres.shading_model;

                            ptr += 3 + codelen;
                        }
            */
            //Coord manipulation
            OpTy::CoordAdd => {
                if ptr + 3 >= len {
                    return NakoResult::invalid_with_color([0.0, 0.0, 1.0, 1.0]);
                }
                let to_add = Vec3::new(
                    as_f32(code[ptr + 1]),
                    as_f32(code[ptr + 2]),
                    as_f32(code[ptr + 3]),
                );

                coord += to_add;
                ptr += 4;
            }
            OpTy::CoordMul => {
                if ptr + 3 >= len {
                    return NakoResult::invalid_with_color([0.0, 0.0, 1.0, 1.0]);
                }
                let to_mul = Vec3::new(
                    as_f32(code[ptr + 1]),
                    as_f32(code[ptr + 2]),
                    as_f32(code[ptr + 3]),
                );

                coord *= to_mul;
                ptr += 4;
            }
            OpTy::CoordMatMul => {
                if ptr + 16 >= len {
                    return NakoResult::invalid_with_color([0.0, 0.0, 1.0, 1.0]);
                }
                let matrix = pop_mat4(code, ptr + 1);
                let new = matrix * coord.extend(1.0);
                coord = new.xyz() / new.w;
                ptr += 17;
            }

            //Result Modifiers
            OpTy::ShadingModel => {
                if ptr + 1 >= len {
                    return NakoResult::invalid_with_color([1.0, 1.0, 0.0, 1.0]);
                }
                secondary.shading_model = code[ptr + 1];
                ptr += 2;
            }

            OpTy::MaterialSlotV0 => {
                if ptr + 4 >= len {
                    return NakoResult::invalid_with_color([1.0, 1.0, 0.0, 1.0]);
                }
                secondary.slot_v0 = pop_vec4(code, ptr + 1).into();
                ptr += 5;
            }
            OpTy::MaterialSlotV1 => {
                if ptr + 4 >= len {
                    return NakoResult::invalid_with_color([1.0, 1.0, 0.0, 1.0]);
                }
                secondary.slot_v1 = pop_vec4(code, ptr + 1).into();
                ptr += 5;
            }
            OpTy::MaterialSlotI0 => {
                if ptr + 4 >= len {
                    return NakoResult::invalid_with_color([1.0, 1.0, 0.0, 1.0]);
                }
                secondary.slot_i0 = [code[ptr + 1], code[ptr + 2], code[ptr + 3], code[ptr + 4]];
                ptr += 5;
            }

            //Primitives
            OpTy::Sphere => {
                if ptr + 1 >= len {
                    return NakoResult::invalid_with_color([1.0, 0.0, 1.0, 1.0]);
                }
                secondary.result = sphere(coord, as_f32(code[ptr + 1]));
                ptr += 2;
            }
            OpTy::Cuboid => {
                if ptr + 3 >= len {
                    return NakoResult::invalid_with_color([1.0, 0.0, 1.0, 1.0]);
                }
                secondary.result = box_exact(coord, pop_vec3(code, ptr + 1));

                ptr += 4;
            }
            OpTy::Plane => {
                if ptr + 4 >= len {
                    return NakoResult::invalid_with_color([1.0, 0.0, 1.0, 1.0]);
                }
                secondary.result = plane(coord, pop_vec3(code, ptr + 1), as_f32(code[ptr + 4]));
                ptr += 5;
            }
            OpTy::Line => {
                if ptr + 7 >= len {
                    return NakoResult::invalid_with_color([1.0, 0.0, 1.0, 1.0]);
                }
                let from = pop_vec3(code, ptr + 1);
                let to = pop_vec3(code, ptr + 4);
                let width = as_f32(code[ptr + 7]);

                secondary.result = line3d(coord, from, to, width);

                ptr += 8;
            }
            OpTy::Triangle => {
                if ptr + 9 >= len {
                    return NakoResult::invalid_with_color([1.0, 0.0, 1.0, 1.0]);
                }
                let verts = [
                    pop_vec3(code, ptr + 1),
                    pop_vec3(code, ptr + 4),
                    pop_vec3(code, ptr + 7),
                ];
                secondary.result = triangle(coord, verts);

                ptr += 10;
            }
            OpTy::Polygon => {
                //Not implemented yet
                return NakoResult::invalid_with_color([0.5, 0.5, 0.5, 1.0]);
            }
            OpTy::QuadricBezier => {
                //Not implemented yet
                return NakoResult::invalid_with_color([0.5, 0.5, 0.5, 1.0]);
            }
            OpTy::CubicBezier => {
                //Not implemented yet
                return NakoResult::invalid_with_color([0.5, 0.5, 0.5, 1.0]);
            }

            _ => return NakoResult::invalid_with_color([1.0, 1.0, 1.0, 1.0]), //since we can't panic, return invalid result
        }
    }

    primary
}

#[inline]
pub fn eval_2d_from_code(coord: Vec2, code: &[Word], offset: usize, length: usize) -> NakoResult {
    if code.len() == 0 {
        return NakoResult::INVALID;
    }

    let mut primary = NakoResult::default();
    let mut secondary = NakoResult::default();
    let mut ptr = offset;
    let mut coord = coord;

    while ptr < length {
        let op: OpTy = OpTy::from(code[ptr]);
        match op {
            //NoSkip for now
            //Combinators
            OpTy::Union => {
                primary = primary.union(&secondary);

                //Reset secondary
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothUnion => {
                if ptr + 1 >= length {
                    return NakoResult::INVALID;
                }
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_union(&secondary, smoothness);
                //Reset secondary
                secondary = NakoResult::default();
                ptr += 2;
            }
            OpTy::Intersection => {
                primary = primary.intersection(&secondary);
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothIntersection => {
                if ptr + 1 >= length {
                    return NakoResult::INVALID;
                }
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_intersection(&secondary, smoothness);
                secondary = NakoResult::default();
                ptr += 2;
            }
            OpTy::Subtraction => {
                primary = primary.subtraction(&secondary);
                secondary = NakoResult::default();
                ptr += 1;
            }
            OpTy::SmoothSubtraction => {
                if ptr + 1 >= length {
                    return NakoResult::INVALID;
                }
                let smoothness = as_f32(code[ptr + 1]);
                primary = primary.smooth_subtraction(&secondary, smoothness);
                secondary = NakoResult::default();
                ptr += 2;
            }

            OpTy::Round => {
                if ptr + 1 >= length {
                    return NakoResult::INVALID;
                }
                secondary.result -= as_f32(code[ptr + 1]);
                ptr += 2;
            }
            OpTy::Onion => {
                if ptr + 1 >= length {
                    return NakoResult::INVALID;
                }
                secondary.result = fabs(secondary.result) - as_f32(code[ptr + 1]);
                ptr += 2;
            }

            //Coord manipulation
            OpTy::CoordAdd => {
                if ptr + 2 >= length {
                    return NakoResult::INVALID;
                }
                let to_add = Vec2::new(as_f32(code[ptr + 1]), as_f32(code[ptr + 2]));
                coord += to_add;
                ptr += 3;
            }
            OpTy::CoordMul => {
                if ptr + 2 >= length {
                    return NakoResult::INVALID;
                }
                let to_mul = Vec2::new(as_f32(code[ptr + 1]), as_f32(code[ptr + 2]));

                coord *= to_mul;
                ptr += 3;
            }
            OpTy::CoordMatMul => {
                if ptr + 9 >= length {
                    return NakoResult::INVALID;
                }
                let matrix = Mat3::from_cols(
                    pop_vec3(code, ptr + 1),
                    pop_vec3(code, ptr + 4),
                    pop_vec3(code, ptr + 7),
                );

                coord = matrix.transform_point2(coord);
                ptr += 10;
            }

            //Result Modifiers
            OpTy::ShadingModel => {
                if ptr + 1 >= length {
                    return NakoResult::INVALID;
                }
                secondary.shading_model = code[ptr + 1];
                ptr += 2;
            }

            OpTy::MaterialSlotV0 => {
                if ptr + 4 >= length {
                    return NakoResult::INVALID;
                }
                secondary.slot_v0 = pop_vec4(code, ptr + 1).into();
                ptr += 5;
            }
            OpTy::MaterialSlotV1 => {
                if ptr + 4 >= length {
                    return NakoResult::INVALID;
                }
                secondary.slot_v1 = pop_vec4(code, ptr + 1).into();
                ptr += 5;
            }
            OpTy::MaterialSlotI0 => {
                if ptr + 4 >= length {
                    return NakoResult::INVALID;
                }
                secondary.slot_i0 = [code[ptr + 1], code[ptr + 2], code[ptr + 3], code[ptr + 4]];
                ptr += 5;
            }

            OpTy::Sphere => {
                if ptr + 1 >= length {
                    return NakoResult::INVALID;
                }
                secondary.result = coord.length() - as_f32(code[ptr + 1]);
                ptr += 2;
            }
            OpTy::Cuboid => {
                if ptr + 2 >= length {
                    return NakoResult::INVALID;
                }
                let d = Vec2::new(fabs(coord.x), fabs(coord.y))
                    - Vec2::new(as_f32(code[ptr + 1]), as_f32(code[ptr + 2]));
                secondary.result = d.max(Vec2::ZERO).length() + (d.max_element()).min(0.0);
                ptr += 3;
            }
            OpTy::Plane => {
                return NakoResult::INVALID;
            }
            OpTy::Line => {
                if ptr + 5 >= length {
                    return NakoResult::INVALID;
                }
                let a = pop_vec2(code, ptr + 1);
                let b = pop_vec2(code, ptr + 3);
                let width = as_f32(code[ptr + 5]);

                let pa = coord - a;
                let ba = b - a;
                let h = (pa.dot(ba) / ba.dot(ba)).clamp(0.0, 1.0);
                let d = (pa - ba * h).length();

                secondary.result = d - width;

                ptr += 6;
            }
            OpTy::Triangle => {
                if ptr + 6 >= length {
                    return NakoResult::INVALID;
                }
                let verts = [
                    pop_vec2(code, ptr + 1),
                    pop_vec2(code, ptr + 3),
                    pop_vec2(code, ptr + 5),
                ];
                secondary.result = triangle_2d(coord, verts);

                ptr += 7;
            }
            OpTy::Polygon => {
                if ptr + 2 >= length {
                    return NakoResult::INVALID;
                }
                let num_points = code[ptr + 1] as usize;

                let offset = ptr + 2;

                let firstv = pop_vec2(code, offset);
                let mut d = (coord - firstv).dot(coord - firstv);
                let mut s = 1.0;

                let mut prev_vec = pop_vec2(code, offset + ((num_points - 1) * 2));
                let mut this_vec;
                for i in 0..num_points {
                    this_vec = pop_vec2(code, offset + (i * 2));
                    //distance
                    let e = prev_vec - this_vec;
                    let w = coord - this_vec;
                    let b = w - e * (w.dot(e) / e.dot(e)).clamp(0.0, 1.0);
                    d = d.min(b.dot(b));

                    //Based on winding number, might change sign
                    match (
                        coord.y >= this_vec.y,
                        coord.y < prev_vec.y,
                        e.x * w.y > e.y * w.x,
                    ) {
                        (true, true, true) | (false, false, false) => s *= -1.0,
                        _ => {}
                    }

                    prev_vec = this_vec;
                }

                secondary.result = s * d.sqrt();
                ptr += 2 + num_points * 2;
            }
            OpTy::QuadricBezier => {
                if ptr + 7 >= length {
                    return NakoResult::INVALID;
                }
                let start = pop_vec2(code, ptr + 1);
                let ctrl = pop_vec2(code, ptr + 3);
                let end = pop_vec2(code, ptr + 5);
                let width = as_f32(code[ptr + 7]);

                secondary.result = bezier2d(coord, start, ctrl, end, width);

                ptr += 8;
            }
            OpTy::CubicBezier => {
                return NakoResult::INVALID;
            }
            _ => return NakoResult::INVALID, //since we can't panic, return invalid result
        }
    }

    primary
}

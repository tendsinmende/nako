use std::path::{Path, PathBuf};

///Returns the shader resource location, or an error if the path was not found
pub fn get_shader_crate_location() -> Result<PathBuf, PathBuf> {
    if let Ok(shader_path) = std::env::var("NAKO_SHADER_CRATE") {
        if !Path::new(&shader_path).exists() {
            println!("NAKO_SHADER_CRATE @ {} does not exist!", shader_path);
            return Err(PathBuf::from(shader_path));
        }
        Ok(PathBuf::from(shader_path))
    } else {
        let path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));

        let path = path.join("../nako_shader");

        if !path.exists() {
            println!("Error: Default nako shader crates does not exist!");
            return Err(path);
        }

        Ok(path)
    }
}

pub fn get_shader_spirv_location() -> PathBuf {
    if let Ok(shader_path) = std::env::var("NAKO_SHADER_CRATE") {
        if !Path::new(&shader_path).exists() {
            panic!("NAKO_SHADER_CRATE @ {} does not exist!", shader_path);
        }
        let mut path = PathBuf::from(shader_path);
        path.push("spirv");

        path
    } else {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path = path.join(format!("../../resources/spirv"));
        path
    }
}

pub fn get_glsl_location() -> PathBuf {
    get_shader_spirv_location().join("../glsl/")
}

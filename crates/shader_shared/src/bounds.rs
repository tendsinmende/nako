/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use spirv_std::glam::const_vec3;
///Gpu special imports
#[cfg(target_arch = "spirv")]
use spirv_std::num_traits::Float;

use crate::{
    glam::{Mat4, Vec2, Vec3},
    Ray,
};

#[derive(PartialEq, Clone)]
pub struct BBox {
    pub min: Vec3,
    pub max: Vec3,
}

impl BBox {
    ///Minimal bound that extents from INF to NEG_INF. Therefore, any defined BBox is "bigger".
    ///Can be used as start bound when combining several BBoxes into a common union.
    pub const MIN_BOUND: Self = BBox {
        min: const_vec3!([f32::INFINITY, f32::INFINITY, f32::INFINITY]),
        max: const_vec3!([f32::NEG_INFINITY, f32::NEG_INFINITY, f32::NEG_INFINITY]),
    };

    pub fn new_2d(min: Vec2, max: Vec2) -> Self {
        BBox {
            min: min.extend(0.0),
            max: max.extend(0.0),
        }
    }

    ///Returns true if min and maxs z axis have the same value
    #[inline]
    pub fn is_2d(&self) -> bool {
        self.min.z == self.max.z
    }

    pub fn union(&self, other: &Self) -> Self {
        BBox {
            min: self.min.min(other.min),
            max: self.max.max(other.max),
        }
    }

    ///Returns the intersection volume if there is any.
    pub fn intersection(&self, other: &Self) -> Self {
        let new = BBox {
            min: Vec3::new(
                self.min.x.max(other.min.x),
                self.min.y.max(other.min.y),
                self.min.z.max(other.min.z),
            ),
            max: Vec3::new(
                self.max.x.min(other.max.x),
                self.max.y.min(other.max.y),
                self.max.z.min(other.max.z),
            ),
        };

        if new.min.x > new.max.x || new.min.y > new.max.y || new.min.z > new.max.z {
            BBox::default() //empty bbox otherwise
        } else {
            new
        }
    }

    ///Sphere | point intersection
    pub fn is_point_intersecting(&self, location: Vec3) -> bool {
        location.x > self.min.x
            && location.y > self.min.y
            && location.z > self.min.z
            && location.x < self.max.x
            && location.y < self.max.y
            && location.z < self.max.z
    }

    ///Sphere | Box intersection test
    pub fn is_sphere_intersecting(&self, location: Vec3, radius: f32) -> bool {
        let closest = self.min.max(location.min(self.max));
        let dist = ((closest.x - location.x) * (closest.x - location.x)
            + (closest.y - location.y) * (closest.y - location.y)
            + (closest.z - location.z) * (closest.z - location.z))
            .sqrt();

        dist < radius
    }

    pub fn is_intersecting(&self, other: &Self) -> bool {
        self.intersection(other) != BBox::default()
    }

    pub fn is_ray_intersecting(&self, ray: &Ray) -> bool {
        //taken from here: https://tavianator.com/2015/ray_box_nan.html
        //slightly modified
        let inv_dir = 1.0 / ray.direction;

        let t1 = (self.min - ray.origin) * inv_dir;
        let t2 = (self.max - ray.origin) * inv_dir;

        let tmin = t1.min(t2);
        let tmax = t1.max(t2);

        let tmin = tmin.max_element();
        let tmax = tmax.min_element();

        tmax > tmin.max(0.0)
    }

    ///finds closes intersection point on ray.
    ///returns INF otherwise
    pub fn closest_ray_intersection(&self, ray: &Ray) -> f32 {
        let (is_intersecting, tmin, _tmax) = self.ray_intersections(ray);
        if is_intersecting{
            tmin
        }else{
            f32::INFINITY
        }
    }

    ///Intersects `ray` with `self`. Returns both intersection points. Note that those intersections
    ///could be "behind" the ray. This is the case if a value is negative.
    pub fn ray_intersections(&self, ray: &Ray) -> (bool, f32, f32) {
        let inv_dir = 1.0 / ray.direction;

        let t1 = (self.min - ray.origin) * inv_dir;
        let t2 = (self.max - ray.origin) * inv_dir;

        let tmin = t1.min(t2);
        let tmax = t1.max(t2);

        let tmin = tmin.max_element();
        let tmax = tmax.min_element();

        if tmax > tmin.max(0.0){
            (true, tmin, tmax)
        }else{
            (false, f32::INFINITY, f32::INFINITY)
        }
    }

    ///Grows self by subtracting `addition` from min, and adding it to max.
    pub fn grow_even(&mut self, addition: Vec3) {
        self.min -= addition;
        self.max += addition;
    }

    pub fn extent(&self) -> Vec3 {
        self.max - self.min
    }

    pub fn center(&self) -> Vec3 {
        self.min + (self.extent() / 2.0)
    }

    pub fn transform(&mut self, trans: Mat4) {
        let nmin = trans * self.min.extend(1.0);
        let nmax = trans * self.max.extend(1.0);

        self.min = Vec3::new(nmin.x.min(nmax.x), nmin.y.min(nmax.y), nmin.z.min(nmax.z));
        self.max = Vec3::new(nmin.x.max(nmax.x), nmin.y.max(nmax.y), nmin.z.max(nmax.z));
    }

    pub fn ray_intersecting(&self, ray: &Ray) -> bool {
        let f = (self.min - ray.origin) / ray.direction;
        let n = (self.max - ray.origin) / ray.direction;

        let tmax = f.max(n);
        let tmin = f.min(n);

        let t1 = tmax.min_element().min(f32::INFINITY);
        let t0 = tmin.max_element().max(0.0);

        t1 >= t0
    }
}

impl Default for BBox {
    fn default() -> Self {
        BBox {
            min: Vec3::ZERO,
            max: Vec3::ZERO,
        }
    }
}

#[derive(Clone)]
pub struct BBox2d {
    pub min: Vec2,
    pub max: Vec2,
}

impl BBox2d {
    pub fn is_intersecting(&self, other: &Self) -> bool {
        self.min.x < other.max.x
            && self.max.x > other.min.x
            && self.min.y < other.max.y
            && self.max.y > other.min.y
    }
}

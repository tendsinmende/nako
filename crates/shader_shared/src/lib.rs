/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#![cfg_attr(target_arch = "spirv", no_std, feature(lang_items))]
#![deny(warnings)]

use glam::{Mat4, Vec3};
pub use nako_instruction;
pub use spirv_std;
pub use spirv_std::glam;

///Evaluator code that takes a stream of words and outputs a result.
pub mod evaluator;

///Bounding primitives
pub mod bounds;
pub use bounds::{BBox, BBox2d};

///Multiple ray tracing algorithms specialised for different purposes.
pub mod ray_tracing;

///Collects public push contant types;
pub mod push_constants;

///Query volume related functions. A QueryVolume is voxel volume where each voxel stores
/// the local lipshitz bound as well as a pointer into an additional structure designating
/// the start for local sdf evaluation.
pub mod query_volume;

#[cfg(not(target_arch = "spirv"))]
pub mod access;

use spirv_std::glam::{Quat, UVec2, Vec2};

//On Gpu we have to import Numtraits for some of those functions
#[cfg(target_arch = "spirv")]
use spirv_std::num_traits::Float;

pub const FAR_PLANE: f32 = 100.0;
pub const NEAR_PLANE: f32 = 0.01;

///Gpu aligned version of `camera`.
#[repr(C)]
#[derive(Clone)]
pub struct GpuCamera {
    ///location on x,y,z axis
    pub location: [f32; 3],
    ///field of view angle in degree
    pub fov: f32,
    ///Rotation as Quaternion (x,y,z,w)
    pub rotation: [f32; 4],
    pub img_width: u32,
    pub img_height: u32,
    pub pad1: [u32; 2],
}

impl GpuCamera {
    fn aspect_ratio(&self) -> f32 {
        self.img_width as f32 / self.img_height as f32
    }

    ///moves a pixel coordinate to camera space ([-1 .. 1] on camera plane)
    pub fn pixel_to_cameraspace(&self, coord: Vec2) -> Vec2 {
        let mut pixel =
            (coord / Vec2::new(self.img_width as f32, self.img_height as f32)) * 2.0 - 1.0;
        pixel = pixel * Vec2::splat((self.fov / 2.0).to_radians().tan());
        pixel.x = pixel.x * self.aspect_ratio();

        pixel
    }

    pub fn primary_ray(&self, pixel_coord: UVec2) -> Ray {
        let pixel = self.pixel_to_cameraspace(pixel_coord.as_vec2() + Vec2::splat(0.5));

        //primary ray is in cameraspace, therefore transform into worldspace
        let transform: Mat4 = self.transform_mat();

        let origin = transform.project_point3(Vec3::ZERO);
        let direction = transform
            .transform_vector3(Vec3::new(pixel.x, pixel.y, 1.0))
            .normalize();

        Ray { direction, origin }
    }

    ///Calculates the transform matrix based on the inner location and rotation.
    pub fn transform_mat(&self) -> Mat4 {
        Mat4::from_rotation_translation(
            Quat::from_xyzw(
                self.rotation[0],
                self.rotation[1],
                self.rotation[2],
                self.rotation[3],
            ),
            Vec3::from(self.location),
        )
    }
}

///2d Camera used when querying a 2d sdf.
#[repr(C)]
#[derive(Clone)]
pub struct GpuCamera2d {
    pub location: [f32; 2],
    pub extent: [f32; 2],
    pub image_size: [f32; 2],
    pub pad1: f32,
    pub rotation: f32,
}

impl GpuCamera2d {
    ///Calculates where in 2d worldspace a pixel would be
    pub fn pixel_to_worldspace(&self, coord: UVec2) -> Vec2 {
        let ext: Vec2 = Vec2::new(self.image_size[0], self.image_size[1]);
        //TODO impl rotation as well
        let pixel_size: Vec2 = Vec2::new(self.extent[0], self.extent[1]) / ext;

        Vec2::new(self.location[0], self.location[1]) + (pixel_size * coord.as_vec2())
    }
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct Ray {
    pub origin: Vec3,
    pub direction: Vec3,
}

impl Ray {
    pub fn point_on_ray(&self, t: f32) -> Vec3 {
        self.origin + self.direction * t
    }

    pub fn transform(&mut self, transform: &Mat4) {
        self.origin = transform.transform_point3(self.origin);
        self.direction = transform.transform_vector3(self.direction);
        self.direction = self.direction.normalize();
    }

    ///Bounds the ray via `ray_length` and returns the bounding box
    pub fn bound(&self, ray_length: f32) -> BBox {
        let a = self.origin;
        let b = self.point_on_ray(ray_length);

        let min = a.min(b);
        let max = a.max(b);
        BBox { min, max }
    }
}

///Converts a 2d `coord` access to a 1d indices. Uses `width` to decide after how many elements
/// a "line break" occures.
#[inline(always)]
pub fn access_1d(coord: UVec2, width: u32) -> u32 {
    coord.y * width + coord.x
}

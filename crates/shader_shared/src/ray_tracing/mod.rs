/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako_instruction::Word;
use spirv_std::glam::{const_vec2, Vec2, Vec2Swizzles, Vec3};

use crate::evaluator::eval_from_code;

mod segment_tracing;
pub use segment_tracing::{segment_tracing, bound_segment_tracing, SegmentTraceResult};

mod sphere_tracing;
pub use sphere_tracing::{any_hit_trace, trace, trace_soft_shadow, SphereTraceResult};

///Delta use for `get_normal`'s thetrahedron construction.
pub const NRM_DELTA: f32 = 0.005;

///Calculates the minimum distance at which a ray tracing implementation can stop searching.
///This basically grows the allowed "EPSILON" based on the distance on the ray
pub fn min_trace(dist: f32, abs_min: f32, grow: f32) -> f32 {
    let min_delta = (dist / grow) * abs_min;
    min_delta.max(abs_min)
}

///Calculates the epsilon for `t`, if epsilon is `eps` at `t==1`
pub fn eps_relative(eps: f32, t: f32) -> f32 {
    eps * t
}

///Uses "Tetrahedron technique" from (inigo quilez blog post)[https://iquilezles.org/www/articles/normalsSDF/normalsSDF.htm] to calculate the normal at any point in a sdf.
pub fn get_normal(code: &[Word], location: Vec3) -> Vec3 {
    const K: Vec2 = const_vec2!([1.0, -1.0]);
    //Diamond sampling for normals with just 4 lookups
    (K.xyy() * eval_from_code(location + K.xyy() * NRM_DELTA, code).result
        + K.yyx() * eval_from_code(location + K.yyx() * NRM_DELTA, code).result
        + K.yxy() * eval_from_code(location + K.yxy() * NRM_DELTA, code).result
        + K.xxx() * eval_from_code(location + K.xxx() * NRM_DELTA, code).result)
        .normalize()
}

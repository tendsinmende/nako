/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako_instruction::{NakoResult, Word};
use spirv_std::glam::{const_vec3, Vec3};

use crate::{evaluator::eval_from_code, Ray, NEAR_PLANE};

use super::{eps_relative, min_trace};

pub const MAX_SPHERE_TRACE_COUNT: u32 = 512;
pub const MAX_SHADOW_TRACE: f32 = 2000.0;
pub const SHADOW_MIN: f32 = 0.01;
pub const TRACE_MIN: f32 = 0.001;

pub fn any_hit_trace(code: &[Word], ray: Ray, max_distance: f32) -> bool {
    let mut t = 1.0;
    let mut counter: u32 = 0;

    while t < max_distance && counter < 1 {
        counter += 1;

        let p = ray.point_on_ray(t);
        let eval = eval_from_code(p, code);
        //Catch invalid eval
        if eval.is_invalid() {
            return false;
        }
        //If intersected, return true
        if eval.result < eps_relative(TRACE_MIN, t) {
            return true;
        } else {
            t += eval.result;
        }
    }
    false
}

pub fn trace_soft_shadow(
    code: &[Word],
    origin: Vec3,
    light_location: Vec3,
    shadow_sharpness: f32,
) -> f32 {
    let mut res: f32 = 1.0;

    let dir = light_location - origin;
    let max_t = dir.length().min(MAX_SHADOW_TRACE);
    let dir = dir.normalize();

    let mut t = 0.0;
    let mut count = 0;
    while t < max_t && count < 512 {
        count += 1;

        let p = origin + dir * t;
        let eval = eval_from_code(p, code);
        if eval.is_invalid() {
            //Got something invalid
            return 1.0;
        }

        let min_delta = min_trace((p - origin).length(), SHADOW_MIN, 3.0);

        if eval.result < min_delta {
            return 0.0;
        }

        res = res.min(shadow_sharpness * eval.result / t);
        t += eval.result;
    }

    res
}

pub struct SphereTraceResult {
    pub hitloc: Vec3,
    pub t: f32,
    pub result: NakoResult,
    pub has_hit: bool,
}

impl SphereTraceResult {
    pub const INVALID: Self = SphereTraceResult {
        has_hit: false,
        hitloc: const_vec3!([f32::INFINITY, f32::INFINITY, f32::INFINITY]),
        result: NakoResult::INVALID,
        t: f32::INFINITY,
    };

    pub const NO_INTERSECTION: Self = SphereTraceResult {
        has_hit: false,
        hitloc: const_vec3!([f32::INFINITY, f32::INFINITY, f32::INFINITY]),
        result: NakoResult::INVALID,
        t: f32::INFINITY,
    };

    pub fn invalid_with_color(color: Vec3) -> SphereTraceResult {
        let mut res = Self::INVALID;
        res.result.slot_v0 = [color.x, color.y, color.z, 1.0];
        res
    }

    pub fn is_invalid(&self) -> bool {
        self.result.is_invalid()
    }
}

pub fn trace(code: &[Word], ray: &Ray, max_distance: f32) -> SphereTraceResult {
    let mut t = NEAR_PLANE;
    let mut tcount = 0;

    while t < max_distance && tcount < MAX_SPHERE_TRACE_COUNT {
        tcount += 1;
        let p = ray.point_on_ray(t);
        let mut result = eval_from_code(p, code);

        if result.is_invalid() {
            let mut ret_res = SphereTraceResult::INVALID;
            ret_res.result.slot_v0 = result.slot_v0;
            return ret_res;
        }

        if result.result < eps_relative(TRACE_MIN, t) {
            result.slot_i0[0] = tcount;
            return SphereTraceResult {
                has_hit: true,
                hitloc: p,
                result,
                t,
            };
        } else {
            t += result.result;
        }
    }

    let mut res = SphereTraceResult::NO_INTERSECTION;
    res.result.slot_i0[0] = tcount;
    res
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use nako_instruction::{NakoResult, Word};
use spirv_std::glam::{const_vec3, UVec3, Vec3};

#[cfg(target_arch = "spirv")]
use spirv_std::{
    glam::{Vec4, Vec4Swizzles},
    num_traits::Float,
    Image,
};

#[cfg(not(target_arch = "spirv"))]
use crate::query_volume::QueryVoxel;

use crate::{evaluator::eval_from_code, push_constants::QueryVolumePushConst, Ray};

use super::eps_relative;

const EPS: f32 = 0.0001;
const GLOBAL_K: f32 = 1.25;
const LIPSCHITZ_GRID_SIZE: f32 = 0.3;
const MAX_SEGMENT_TRACE_ITERATIONS: u32 = 256;
const SEGTRACE_MIN: f32 = 0.001;

pub struct SegmentTraceResult {
    pub result: NakoResult,
    pub hitloc: Vec3,
    pub t: f32,
    pub has_hit: bool,
}

impl SegmentTraceResult {
    pub const INVALID: Self = SegmentTraceResult {
        has_hit: false,
        hitloc: const_vec3!([f32::INFINITY, f32::INFINITY, f32::INFINITY]),
        result: NakoResult::INVALID,
        t: f32::INFINITY,
    };

    pub const NO_INTERSECTION: Self = SegmentTraceResult {
        has_hit: false,
        hitloc: const_vec3!([f32::INFINITY, f32::INFINITY, f32::INFINITY]),
        result: NakoResult::INVALID,
        t: f32::INFINITY,
    };

    pub fn is_invalid(&self) -> bool {
        self.result.is_invalid()
    }
}

const ERROR: (f32, f32) = (0.0, f32::INFINITY);

///Calculates lambda and distance
#[cfg(not(target_arch = "spirv"))]
pub fn scene_k(
    context: &QueryVolumePushConst,
    query_volume: &[QueryVoxel],
    start: Vec3,
    end: Vec3,
    dir: Vec3,
) -> (f32, f32) {
    let start_index = context.pos_to_index(start);
    if start_index == UVec3::splat(u32::MAX) {
        //println!("{} start oob", start);
        return ERROR;
    }
    let start_index = context.global_index_to_1d_index(start_index);
    if start_index > query_volume.len() {
        //println!("{} end oob", start);
        return ERROR;
    }
    let end_index = context.pos_to_index(end);
    if end_index == UVec3::splat(u32::MAX) {
        return ERROR;
    }
    let end_index = context.global_index_to_1d_index(end_index);
    if end_index > query_volume.len() {
        return ERROR;
    }

    let dist = query_volume[start_index].dist;
    let gs = Vec3::from(query_volume[start_index].bound);
    let ge = Vec3::from(query_volume[end_index].bound);

    let fds = gs.dot(dir).abs();
    let fde = ge.dot(dir).abs();
    let lam = fds.max(fde);

    (lam, dist)
}

///Calculates lambda and distance
#[cfg(target_arch = "spirv")]
pub fn scene_k(
    context: &QueryVolumePushConst,
    query_volume: &Image!(3D, format=rgba16f, sampled=false),
    start: Vec3,
    end: Vec3,
    dir: Vec3,
) -> (f32, f32) {
    let start_index = context.pos_to_index(start);
    if start_index == UVec3::splat(u32::MAX) {
        return ERROR;
    }
    let end_index = context.pos_to_index(end);
    if end_index == UVec3::splat(u32::MAX) {
        return ERROR;
    }

    let (gs, dist) = {
        let read: Vec4 = query_volume.read(start_index);
        (read.xyz(), read.w)
    };

    let ge = {
        let read: Vec4 = query_volume.read(start_index);
        read.xyz()
    };

    let fds = gs.dot(dir).abs();
    let fde = ge.dot(dir).abs();
    let lam = fds.max(fde);

    (lam, dist)
}

#[allow(dead_code)]
#[cfg(not(target_arch = "spirv"))]
fn calc_gradient(code: &[Word], pos: Vec3) -> Vec3 {
    (Vec3::new(
        eval_from_code(pos + Vec3::X * EPS, code).result,
        eval_from_code(pos + Vec3::Y * EPS, code).result,
        eval_from_code(pos + Vec3::Z * EPS, code).result,
    ) - Vec3::new(
        eval_from_code(pos - Vec3::X * EPS, code).result,
        eval_from_code(pos - Vec3::Y * EPS, code).result,
        eval_from_code(pos - Vec3::Z * EPS, code).result,
    )) / (2.0 * EPS)
}

pub fn calc_gradient_dir_abs(code: &[Word], pos: Vec3, dir: Vec3) -> f32 {
    (eval_from_code(pos + dir * EPS, code).result - eval_from_code(pos - dir * EPS, code).result)
        .abs()
        / (2.0 * EPS)
}

pub fn scene_k_from_code(code: &[Word], start: Vec3, end: Vec3, dir: Vec3) -> (f32, NakoResult) {
    let res = eval_from_code(start, code);

    //let gs = calc_gradient(code, start);
    //let ge = calc_gradient(code, end);
    //let fds = gs.dot(dir).abs();
    //let fde = ge.dot(dir).abs();

    let fds = calc_gradient_dir_abs(code, start, dir);
    let fde = calc_gradient_dir_abs(code, end, dir);

    let lam = fds.max(fde);

    (lam, res)
}

///Custom version of segment tracing that uses a VoxelVolume containing local lipschitz and dist values.
///We step through this volume until we found an intersection smaller then the voxels volume. In that case we start
///normal sdf evaluation.
///
/// Before that step however we can use the local lipschitz bounds to make big steps. Also, at each step we only have to fetch
/// a texture, which should make this whole process pretty fast.
///
/// This version can fallback to code evaluation whenever sampling happens outside the query_volume. This allows segment tracing for infinite
/// SDFs as well as SDFs that are too big for a query volume.
pub fn segment_tracing(
    context: &QueryVolumePushConst,

    #[cfg(not(target_arch = "spirv"))] query_volume: &[QueryVoxel],

    #[cfg(target_arch = "spirv")] query_volume: &Image!(3D, format=rgba16f, sampled=false),

    code: &[Word],
    ray: &Ray,
    min_t: f32,
    max_t: f32,
) -> SegmentTraceResult {
    let mut t = min_t;
    let c = 2.0;
    let mut ts: f32 = max_t - min_t;
    ts = ts.min(context.voxel_size);

    //It can happen that we sample outside the volume because our "first t" calculation is a little off,
    //therefore, count retries, if we exceed the retries return.
    //this is not really the nicest way, but we also don't want to fall back to code interpretation too fast.

    let mut recent_res = NakoResult::INVALID;
    let mut i = 0;
    while i < MAX_SEGMENT_TRACE_ITERATIONS  && t < max_t{
        let pt = ray.point_on_ray(t);
        let pts = ray.point_on_ray(t + ts);
        //Query volume for stepping distnace

        let (mut lambda, mut dist) = scene_k(context, query_volume, pt, pts, ray.direction);
        /*
        let (lambda, dist) = scene_k_from_code(
            code,
            pt,
            pts,
            ray.direction
        );
         */

        //if we got an error back , or the distance value is too small to be used from sampling, update results with standard code-evaluation
        //based calculation
        if (lambda, dist) == ERROR || dist < context.voxel_size {
            let (new_lambda, new_res) = scene_k_from_code(code, pt, pts, ray.direction);
            recent_res = new_res;
            lambda = new_lambda;
            dist = new_res.result;
        }

        //At this point we are sure that the value is "correct". Therefore check if we are intersecting.
        //FIXME use distance based break condition
        if dist < eps_relative(SEGTRACE_MIN, t) {
            //Found the actual hit, return early

            //need to evaluate once to carry the correct eval data
            if recent_res.is_invalid() {
                recent_res = eval_from_code(pt, code);
            }

            //flag for i
            recent_res.slot_i0[0] = i;
            //#[cfg(not(target_arch = "spirv"))]
            //println!("Did intersect at {} with color: {:?}", t, recent_res.slot_v0);

            return SegmentTraceResult {
                has_hit: true,
                hitloc: ray.point_on_ray(t),
                result: recent_res,
                t,
            };
        }

        //Did not intersect, keep searching
        let mut tk = dist.abs() / lambda.max(0.01);
        tk = (dist.abs() / GLOBAL_K).max(tk.min(ts));
        //ts = tk;
        if tk >= 0.0 {
            t += tk.max(EPS);
        }
        ts = tk * c;
        ts = ts.min(LIPSCHITZ_GRID_SIZE);
        if t > max_t {
            break;
        }
        i += 1;
    }

    let mut res = SegmentTraceResult::NO_INTERSECTION;
    res.result.slot_v0 = [0.0, 1.0, 0.0, 1.0];
    res.result.slot_i0[0] = i;
    res
}

///Specialised version of `segment_tracing` that assumes the sdf fully inside the `query_volume`.
///It therefore does not try to resolve queries outside the volume, but instead returns if it did not found
///any intersection within the volume.
pub fn bound_segment_tracing(
    context: &QueryVolumePushConst,

    #[cfg(not(target_arch = "spirv"))] query_volume: &[QueryVoxel],

    #[cfg(target_arch = "spirv")] query_volume: &Image!(3D, format=rgba16f, sampled=false),

    code: &[Word],
    ray: &Ray,
    min_t: f32,
    max_t: f32,
) -> SegmentTraceResult{
    let mut t = min_t;
    let c = 2.0;
    let mut ts: f32 = max_t - min_t;
    ts = ts.min(context.voxel_size);

    let mut i = 0;
    let mut has_intersected_once = false;
    while i < MAX_SEGMENT_TRACE_ITERATIONS && t < max_t{
        let pt = ray.point_on_ray(t);
        let pts = ray.point_on_ray(t + ts);
        //Query volume for stepping distnace

        let (lambda, dist) = scene_k(context, query_volume, pt, pts, ray.direction);

        //if we got an error back , or the distance value is too small to be used from sampling, update results with standard code-evaluation
        //based calculation
        if (lambda, dist) == ERROR && has_intersected_once{
            let mut res = SegmentTraceResult::INVALID;
            res.result.slot_i0[0] = i;
            return res;
        }else if (lambda, dist) == ERROR && !has_intersected_once{
            t += context.voxel_size;
            continue;
        }

        has_intersected_once = true;
        
        //At this point we are sure that the value is "correct". Therefore check if we are intersecting.
        //FIXME use distance based break condition
        if dist < eps_relative(SEGTRACE_MIN, t) {
            //Found the actual hit, return early
            let mut res = eval_from_code(pt, code);

            //flag for i
            res.slot_i0[0] = i;
            //#[cfg(not(target_arch = "spirv"))]
            //println!("Did intersect at {} with color: {:?}", t, recent_res.slot_v0);

            return SegmentTraceResult {
                has_hit: true,
                hitloc: ray.point_on_ray(t),
                result: res,
                t,
            };
        }

        //Did not intersect, keep searching
        let mut tk = dist.abs() / lambda.max(0.01);
        tk = (dist.abs() / GLOBAL_K).max(tk.min(ts));
        //ts = tk;
        if tk >= 0.0 {
            t += tk.max(EPS);
        }
        ts = tk * c;
        ts = ts.min(LIPSCHITZ_GRID_SIZE);
        if t > max_t {
            break;
        }
        i += 1;
    }

    let mut res = SegmentTraceResult::NO_INTERSECTION;
    res.result.slot_v0 = [0.0, 1.0, 0.0, 1.0];
    res.result.slot_i0[0] = i;
    res
}

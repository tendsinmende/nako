/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Nako Instruction
//!
//! This crate contains only the instructions used by nako. It is used by any crate that needs a understanding of the
//! basic nako instructions. These are usualy nako itself (that breaks Primary and secondary instructions down to word streams),
//! and any rendering implementation that needs to decode those instructions and executes them.

#![no_std]
#![feature(inline_const_pat)]

///Definition of one word in the instruction stream
pub type Word = u32;

///A single instruction code. Each code is followed by some meta data before the next instruction. Make sure to have a look at the
///encoding functions within nako.
#[derive(Clone, Copy)]
pub enum OpTy {
    Skip = 0,

    Union = 1,
    SmoothUnion = 2,

    Intersection = 3,
    SmoothIntersection = 4,

    Subtraction = 5,
    SmoothSubtraction = 6,

    ///Moves to a higher iso line. Note that the bound of the object will changes when doing so.
    Round = 7,
    ///"Onions" the sdf. Aka removes everything except for a certain thickness around the iso-line.
    Onion = 8,

    ///Projections from 2d into 3d.
    ///
    ///This projects a plante into space onto which the 2d
    ///forms are presented
    PlanarProjection = 9,
    ///Uses the 2d sdf as basis to revolute it around the coordiante systems origin
    Revolution = 10,
    ///Extrudes the given 2d sdf, extrudes it down the z axis.
    Extrude = 11,

    ///Adds the following vec3 to the evaluation coordinate used
    CoordAdd = 12,
    ///Multiplies the given coordiante to the evaluation coordinate
    CoordMul = 13,
    ///Multiplies the given matrix (Mat3) to the coordinate.
    CoordMatMul = 14,

    //Material/shading parameters
    /// # Description
    ///
    ///Single `Word1 that signals the shading model. The first 16bit are exclusive flags indicating the shading model.
    ///the last 16bit are left open for flag combinations not yet used.
    ///
    /// # Flags
    ///
    /// - 0x1: Physical Based Shading via albedo, roughness, metallic etc.
    /// - 0x2: Simple Color (only a simple rgb color)
    ShadingModel = 15,
    ///A material slot with an interpolatable `Vec4`
    MaterialSlotV0 = 16,
    ///A material slot with an interpolatable `Vec4`
    MaterialSlotV1 = 17,
    ///A material slot with a non-interpolatable `IVec4`. Can be used to set flag for instance. Whenever merging of values occurs the "newer" one is prefered.
    MaterialSlotI0 = 18,

    //primitives
    /// # Description
    ///
    ///Hyper-Sphere in its domain. In 2d this is a disk/circle in 3d this is a sphere.
    ///
    /// # Parameters
    ///
    /// `radius: f32`
    Sphere = 19,

    /// # Description
    ///
    ///Cube in its domain. Has a parameter `half_extent`. The cube extends fro -half_extend to +half_extend on each axis.
    ///
    /// # Parameters
    ///
    /// `half_extent: Vec2 / Vec3`: half_extent from its center to each of the bounding planes.
    Cuboid = 20,

    /// # Description
    ///
    ///Hyper-Plane in its domain. In 2d this is a line vertically on `normal`, offseted on the normal vector by `height`. In 3d the same parameters apply to a infinite plane in 3d spacex
    ///
    /// # Parameters
    ///
    /// `normal: Vec2 / Vec3`
    ///
    /// `height: f32`: local offset on normal vector
    Plane = 21,

    /// # Description
    ///
    ///Line in its domain with a given width.
    ///
    /// # Parameters
    ///
    /// `start: Vec2 / Vec3`
    ///
    /// `end: Vec2 / Vec3`
    ///
    /// `width: f32`
    Line = 22,

    /// # Description
    ///
    ///Triangle in its domain. In 3d this triangle has no thickness by default. However, `Round` can be used to move up the Iso-Line which adds a thickness.
    ///
    /// # Parameters
    ///
    /// `v0: Vec2 / Vec3`: first vertex
    ///
    /// `v1: Vec2 / Vec3`: second vertex
    ///
    /// `v2: Vec2 / Vec3`: third vertex
    Triangle = 23,

    /// # Description
    ///
    ///Polygon made from a set of vertices in its domain. In 3d this polygone has no thickness by default. However, `Round` can be used to move up the Iso-Line which adds a thickness.
    ///
    /// # Parameters
    ///
    /// `number_of_vertices: usize`: number of vertices
    ///
    /// `vertices: Vec<Vec2> / Vec<Vec3>`: Vector of all vertices with the length of `number_of_vertices`.
    Polygon = 24,

    /// # Description
    ///
    /// Quadric bezier spline made from three control points
    ///
    /// # Parameters
    ///
    /// `v0: Vec2 / Vec3`: first vertex
    ///
    /// `v1: Vec2 / Vec3`: second vertex
    ///
    /// `v2: Vec2 / Vec3`: third vertex
    ///
    /// `thickness: f32`: thickness of the resulting line
    QuadricBezier = 25,
    /// # Description
    ///
    /// Cubic bezier spline made from four control points
    ///
    /// # Parameters
    ///
    /// `v0: Vec2 / Vec3`: first vertex
    ///
    /// `v1: Vec2 / Vec3`: second vertex
    ///
    /// `v2: Vec2 / Vec3`: third vertex
    ///
    /// `v3: Vec2 / Vec3`: fourth vertex
    ///
    /// `thickness: f32`: thickness of the resulting line
    CubicBezier = 26,

    ///Invalid instruction placeholder
    Invalid = u32::MAX as isize,
}

impl From<u32> for OpTy {
    fn from(t: u32) -> Self {
        match t {
            const { OpTy::Skip as u32 } => OpTy::Skip,

            const { OpTy::Union as u32 } => OpTy::Union,
            const { OpTy::SmoothUnion as u32 } => OpTy::SmoothUnion,

            const { OpTy::Intersection as u32 } => OpTy::Intersection,
            const { OpTy::SmoothIntersection as u32 } => OpTy::SmoothIntersection,

            const { OpTy::Subtraction as u32 } => OpTy::Subtraction,
            const { OpTy::SmoothSubtraction as u32 } => OpTy::SmoothSubtraction,

            const { OpTy::Round as u32 } => OpTy::Round,
            const { OpTy::Onion as u32 } => OpTy::Onion,

            const { OpTy::PlanarProjection as u32 } => OpTy::PlanarProjection,
            const { OpTy::Revolution as u32 } => OpTy::Revolution,
            const { OpTy::Extrude as u32 } => OpTy::Extrude,

            const { OpTy::CoordAdd as u32 } => OpTy::CoordAdd,
            const { OpTy::CoordMul as u32 } => OpTy::CoordMul,
            const { OpTy::CoordMatMul as u32 } => OpTy::CoordMatMul,

            const { OpTy::ShadingModel as u32 } => OpTy::ShadingModel,
            const { OpTy::MaterialSlotV0 as u32 } => OpTy::MaterialSlotV0,
            const { OpTy::MaterialSlotV1 as u32 } => OpTy::MaterialSlotV1,
            const { OpTy::MaterialSlotI0 as u32 } => OpTy::MaterialSlotI0,

            const { OpTy::Sphere as u32 } => OpTy::Sphere,
            const { OpTy::Cuboid as u32 } => OpTy::Cuboid,
            const { OpTy::Plane as u32 } => OpTy::Plane,
            const { OpTy::Line as u32 } => OpTy::Line,
            const { OpTy::Triangle as u32 } => OpTy::Triangle,
            const { OpTy::Polygon as u32 } => OpTy::Polygon,
            const { OpTy::QuadricBezier as u32 } => OpTy::QuadricBezier,
            const { OpTy::CubicBezier as u32 } => OpTy::CubicBezier,

            _ => OpTy::Invalid,
        }
    }
}

#[inline]
pub fn mix(a: f32, b: f32, alpha: f32) -> f32 {
    let alpha = alpha.clamp(0.0, 1.0);
    (a * alpha) + (b * (1.0 - alpha))
}

///Standard result of nako which is supported for all normal Nako operations.
#[repr(C)]
#[derive(Clone, Copy)]
pub struct NakoResult {
    ///Sdf evaluation result
    pub result: f32,
    pub slot_v0: [f32; 4],
    pub slot_v1: [f32; 4],
    pub slot_i0: [u32; 4],
    pub shading_model: Word,
}

impl Default for NakoResult {
    fn default() -> Self {
        NakoResult {
            result: f32::INFINITY,
            slot_i0: [0, 0, 0, 0],
            slot_v0: [0.0, 0.0, 0.0, 0.0],
            slot_v1: [0.0, 0.0, 0.0, 0.0],
            shading_model: 0,
        }
    }
}

impl NakoResult {
    ///Invalid result
    pub const INVALID: NakoResult = NakoResult {
        result: f32::NAN,
        slot_i0: [0, 0, 0, 0],
        slot_v0: [0.0, 0.0, 0.0, 0.0],
        slot_v1: [0.0, 0.0, 0.0, 0.0],
        shading_model: 0,
    };

    #[inline]
    pub fn is_invalid(&self) -> bool {
        self.result == f32::NAN
    }

    pub fn union(&self, other: &Self) -> Self {
        if self.result < other.result {
            *self
        } else {
            *other
        }
    }

    pub fn invalid_with_color(color: [f32; 4]) -> Self {
        let mut res = Self::INVALID;
        res.slot_v0 = color;
        res
    }

    pub fn smooth_union(&self, other: &Self, radius: f32) -> Self {
        let h = (0.5 + 0.5 * (other.result - self.result) / radius).clamp(0.0, 1.0);

        NakoResult {
            result: mix(self.result, other.result, h) - (radius * h * (1.0 - h)),
            slot_v0: [
                mix(self.slot_v0[0], other.slot_v0[0], h),
                mix(self.slot_v0[1], other.slot_v0[1], h),
                mix(self.slot_v0[2], other.slot_v0[2], h),
                mix(self.slot_v0[3], other.slot_v0[3], h),
            ],
            slot_v1: [
                mix(self.slot_v1[0], other.slot_v1[0], h),
                mix(self.slot_v1[1], other.slot_v1[1], h),
                mix(self.slot_v1[2], other.slot_v1[2], h),
                mix(self.slot_v1[3], other.slot_v1[3], h),
            ],
            slot_i0: other.slot_i0,
            shading_model: other.shading_model,
        }
    }

    pub fn intersection(&self, other: &Self) -> Self {
        if self.result > other.result {
            *self
        } else {
            *other
        }
    }

    pub fn smooth_intersection(&self, other: &Self, radius: f32) -> Self {
        let h = (0.5 - 0.5 * (other.result - self.result) / radius).clamp(0.0, 1.0);
        NakoResult {
            result: mix(self.result, other.result, h) + radius * h * (1.0 - h),
            slot_v0: [
                mix(self.slot_v0[0], other.slot_v0[0], h),
                mix(self.slot_v0[1], other.slot_v0[1], h),
                mix(self.slot_v0[2], other.slot_v0[2], h),
                mix(self.slot_v0[3], other.slot_v0[3], h),
            ],
            slot_v1: [
                mix(self.slot_v1[0], other.slot_v1[0], h),
                mix(self.slot_v1[1], other.slot_v1[1], h),
                mix(self.slot_v1[2], other.slot_v1[2], h),
                mix(self.slot_v1[3], other.slot_v1[3], h),
            ],
            slot_i0: other.slot_i0,
            shading_model: other.shading_model,
        }
    }

    pub fn subtraction(&self, other: &Self) -> Self {
        if (-other.result) > self.result {
            let mut other = *other;
            other.result *= -1.0;

            if other.result < 0.0 {
                other.slot_v0 = self.slot_v0;
                other.slot_v1 = self.slot_v1;
                other.slot_i0 = self.slot_i0;
                other.shading_model = self.shading_model;
            }

            other
        } else {
            *self
        }
    }

    pub fn smooth_subtraction(&self, other: &Self, radius: f32) -> Self {
        let h = (0.5 - 0.5 * (other.result + self.result) / radius).clamp(0.0, 1.0);

        NakoResult {
            result: mix(other.result * -1.0, self.result, h) + (radius * h * (1.0 - h)),
            slot_v0: [
                mix(self.slot_v0[0], other.slot_v0[0], h),
                mix(self.slot_v0[1], other.slot_v0[1], h),
                mix(self.slot_v0[2], other.slot_v0[2], h),
                mix(self.slot_v0[3], other.slot_v0[3], h),
            ],
            slot_v1: [
                mix(self.slot_v1[0], other.slot_v1[0], h),
                mix(self.slot_v1[1], other.slot_v1[1], h),
                mix(self.slot_v1[2], other.slot_v1[2], h),
                mix(self.slot_v1[3], other.slot_v1[3], h),
            ],
            slot_i0: other.slot_i0,
            shading_model: other.shading_model,
        }
    }
}

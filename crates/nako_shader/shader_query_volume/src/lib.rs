/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![cfg_attr(
    target_arch = "spirv",
    feature(register_attr),
    register_attr(spirv),
    no_std
)]
// HACK(eddyb) can't easily see warnings otherwise from `spirv-builder` builds.
#![deny(warnings)]

use shader_shared::{
    evaluator::eval_from_code,
    nako_instruction::Word,
    push_constants::QueryVolumePushConst,
    spirv_std::glam::{UVec3, Vec3},
};

#[cfg(not(target_arch = "spirv"))]
use shader_shared::query_volume::QueryVoxel;

//Note this is needed to compile on cpu
#[cfg(not(target_arch = "spirv"))]
use shader_shared::spirv_std::macros::spirv;

#[cfg(target_arch = "spirv")]
use shader_shared::spirv_std;
#[cfg(target_arch = "spirv")]
use shader_shared::spirv_std::glam::Vec4;
#[cfg(target_arch = "spirv")]
use shader_shared::spirv_std::Image;

const EPS: f32 = 0.0001;
///Calculates local lipschitz for `code` at `pos`.
fn local_lipschitz(code: &[Word], pos: Vec3) -> Vec3 {
    //TODO calculate eps based on voxel properties?
    //TODO currently most naive nrm calculation. Could use the tetrahedron version, or tab multiple
    //times for more precision.
    Vec3::new(
        eval_from_code(pos + Vec3::X * EPS, code).result,
        eval_from_code(pos + Vec3::Y * EPS, code).result,
        eval_from_code(pos + Vec3::Z * EPS, code).result,
    ) - Vec3::new(
        eval_from_code(pos - Vec3::X * EPS, code).result,
        eval_from_code(pos - Vec3::Y * EPS, code).result,
        eval_from_code(pos - Vec3::Z * EPS, code).result,
    ) / (2.0 * EPS)
}

#[spirv(compute(threads(4, 4, 4)))]
pub fn main(
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(push_constant)] push: &QueryVolumePushConst,
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] code: &[Word],
    #[cfg(not(target_arch = "spirv"))]
    #[spirv(storage_buffer, descriptor_set = 0, binding = 1)]
    query_volume: &mut [QueryVoxel],
    #[cfg(target_arch = "spirv")]
    #[spirv(descriptor_set = 0, binding = 1)]
    query_volume: &Image!(3D, format=rgba16f, sampled=false),
) {
    //first of all check which voxel we have to work on. If it is outside the query volume,
    //return early
    #[cfg(not(target_arch = "spirv"))]
    let idx = {
        let idx = push.global_index_to_1d_index(id);
        if idx >= query_volume.len() {
            return;
        }
        idx
    };

    //On the gpu we can return whenever the voxel id exeeds the resolution of the image
    #[cfg(target_arch = "spirv")]
    if id.max_element() > push.resolution {
        return;
    }

    //get the voxels location in worldspace and calculate its distance to the next surface as well
    //as the local lipschitz bound(which is currently just a normal).
    let eval_location = push.global_index_to_location(id);

    let local_lipschitz = local_lipschitz(code, eval_location);

    //Write back on gpu by indexing directly
    #[cfg(not(target_arch = "spirv"))]
    {
        query_volume[idx].bound = local_lipschitz.into();
        query_volume[idx].dist = eval_from_code(eval_location, code).result;
    }
    #[cfg(target_arch = "spirv")]
    {
        //write out bound and dist
        let result = Vec4::new(
            local_lipschitz.x,
            local_lipschitz.y,
            local_lipschitz.z,
            eval_from_code(eval_location, code).result,
        );
        unsafe {
            query_volume.write(id, result);
        }
    }
}

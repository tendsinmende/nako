/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![cfg_attr(
    target_arch = "spirv",
    feature(register_attr),
    register_attr(spirv),
    no_std
)]
// HACK(eddyb) can't easily see warnings otherwise from `spirv-builder` builds.
#![deny(warnings)]

use shader_shared::glam::{IVec2, UVec2, UVec3, Vec3Swizzles, Vec4};
use shader_shared::push_constants::LayerCombinePushConst;
use shader_shared::spirv_std;
use shader_shared::spirv_std::Image;

//Note this is needed to compile on cpu
#[cfg(not(target_arch = "spirv"))]
use shader_shared::spirv_std::macros::spirv;

#[spirv(compute(threads(8, 8, 1)))]
pub fn main(
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(push_constant)] push: &LayerCombinePushConst,
    #[spirv(descriptor_set = 0, binding = 0)] dst_image: &Image!(2D, format=rgba32f, sampled=false),
    #[spirv(descriptor_set = 0, binding = 1)] bg_image: &Image!(2D, format=rgba32f, sampled=false),
    #[spirv(descriptor_set = 0, binding = 2)] fg_image: &Image!(2D, format=rgba32f, sampled=false),
) {
    let pixel_coord = id.xy();
    let dst_size = dst_image.query_size::<UVec2>();

    let bg: Vec4 = bg_image.read(pixel_coord);

    if (pixel_coord.x as i32) < push.origin[0]
        || (pixel_coord.y as i32) < push.origin[1]
        || (pixel_coord.x as i32) > (push.origin[0] + push.extent[0] as i32)
        || (pixel_coord.y as i32) > (push.origin[1] + push.extent[1] as i32)
    {
        if pixel_coord.x >= dst_size.x || pixel_coord.y >= dst_size.y {
            return; //do not work outside of image
        } else {
            //Just push the bg through
            unsafe {
                dst_image.write(pixel_coord, bg);
            }
        }
        return;
    } else {
        let fg: Vec4 = fg_image.read(
            (pixel_coord.as_ivec2() - IVec2::new(push.origin[0] as i32, push.origin[1] as i32))
                .as_uvec2(),
        );
        let px = bg.lerp(fg, fg.w);
        unsafe {
            dst_image.write(pixel_coord, px);
        }
    }
}

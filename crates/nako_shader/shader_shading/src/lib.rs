/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![cfg_attr(
    target_arch = "spirv",
    feature(register_attr),
    register_attr(spirv),
    no_std
)]
// HACK(eddyb) can't easily see warnings otherwise from `spirv-builder` builds.
//#![deny(warnings)]

use shader_shared::glam::{UVec2, UVec3, Vec3, Vec3Swizzles, Vec4, Vec4Swizzles};
use shader_shared::push_constants::ShadingPushConst;
use shader_shared::spirv_std;
use shader_shared::spirv_std::Image;

//Note this is needed to compile on cpu
#[cfg(not(target_arch = "spirv"))]
use shader_shared::spirv_std::macros::spirv;

fn uncharted2_tonemap_partial(x: Vec3) -> Vec3 {
    let a = 0.15;
    let b = 0.50;
    let c = 0.10;
    let d = 0.20;
    let e = 0.02;
    let f = 0.30;
    return ((x * (a * x + c * b) + d * e) / (x * (a * x + b) + d * f)) - e / f;
}

fn uncharted2_filmic(v: Vec3) -> Vec3 {
    let exposure_bias = 2.0;
    let curr = uncharted2_tonemap_partial(v * exposure_bias);

    let w = Vec3::splat(11.2);
    let while_scale = Vec3::ONE / uncharted2_tonemap_partial(w);
    curr * while_scale
}

#[spirv(compute(threads(8, 8, 1)))]
pub fn main(
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(push_constant)] push: &ShadingPushConst,
    #[spirv(descriptor_set = 0, binding = 0)] in_albedo: &Image!(2D, format=rgba8, sampled=false),
    #[spirv(descriptor_set = 0, binding = 1)] in_direct_light: &Image!(2D, format=rgba32f, sampled=false),
    #[spirv(descriptor_set = 0, binding = 2)] out_shaded: &Image!(2D, format=rgba32f, sampled=false),
) {
    let pixel_coord = id.xy();

    let img_size = out_shaded.query_size::<UVec2>();
    if img_size.x < pixel_coord.x || img_size.y < pixel_coord.y {
        return;
    }

    let albedo: Vec4 = in_albedo.read(pixel_coord);

    let light: Vec4 = in_direct_light.read(pixel_coord);
    let mut shaded = albedo.xyz() * light.xyz();
    shaded = uncharted2_filmic(shaded);
    //gamma correction
    shaded = shaded.powf(2.2 / push.gamma);

    unsafe {
        out_shaded.write(pixel_coord, shaded.extend(albedo.w));
    }
}

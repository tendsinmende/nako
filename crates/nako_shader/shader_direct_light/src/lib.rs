/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![cfg_attr(
    target_arch = "spirv",
    feature(register_attr),
    register_attr(spirv),
    no_std
)]
// HACK(eddyb) can't easily see warnings otherwise from `spirv-builder` builds.
#![deny(warnings)]

use shader_shared::glam::{const_vec3, UVec2, UVec3, Vec3, Vec3Swizzles, Vec4, Vec4Swizzles};
use shader_shared::nako_instruction::Word;
use shader_shared::push_constants::DirectLightConst;
use shader_shared::ray_tracing::trace_soft_shadow;
use shader_shared::spirv_std;
use shader_shared::spirv_std::Image;

//Note this is needed to compile on cpu
#[cfg(not(target_arch = "spirv"))]
use shader_shared::spirv_std::macros::spirv;

pub const LIGHT_LOCATION: Vec3 = const_vec3!([10.0, 100.0, -105.0]);
pub const AMBIENT_LIGHT: Vec3 = const_vec3!([0.06, 0.05, 0.05]);
pub const LIGHT_COLOR: Vec3 = const_vec3!([1.0, 0.85, 0.95]);
pub const LIGHT_POWER: f32 = 10.0;
pub const SHADOW_SHARPNESS: f32 = 17.0;
pub const LIGHT_RADIUS: f32 = 1000.0;

///calculates the lights radiance based on a distance and a radius.
///shamlessly stolen from epics paper: Real Shading in Unreal Engine 4
///Source: `<https://cdn2.unrealengine.com/Resources/files/2013SiggraphPresentationsNotes-26915738.pdf>` figure (9)
pub fn radiance(distance: f32, radius: f32) -> f32 {
    let inv_square_att = 1.0 / (radius * radius);
    let square_distance = distance * distance;
    let factor = square_distance * inv_square_att;
    let smoothfactor = (1.0 - factor * factor).clamp(0.0, 1.0);
    smoothfactor * smoothfactor
}

#[spirv(compute(threads(8, 8, 1)))]
pub fn main(
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(push_constant)] push: &DirectLightConst,
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] code: &[Word],
    #[spirv(descriptor_set = 0, binding = 1)] out_direct: &Image!(2D, format=rgba32f, sampled=false),
    #[spirv(descriptor_set = 0, binding = 2)] in_nrm_depth: &Image!(2D, format=rgba32f, sampled=false),
) {
    let coord = id.xy();
    let img_size = out_direct.query_size::<UVec2>();
    if img_size.x < coord.x || img_size.y < coord.y {
        //outside of image, can early return
        return;
    }

    let mut pixel_coord = coord;
    pixel_coord.y = img_size.y - 1 - pixel_coord.y;

    if code.len() < 2 || code.len() > 100000 {
        unsafe {
            out_direct.write(pixel_coord, Vec4::new(1.0, 0.0, 0.0, 0.0));
        }
        return;
    }

    let inp: Vec4 = in_nrm_depth.read(pixel_coord);
    //didnt hit anything on the primary ray
    if inp.w == f32::INFINITY {
        unsafe {
            out_direct.write(pixel_coord, Vec4::new(0.0, 0.0, 1.0, 1.0));
        }
        return;
    }

    let ray = push.camera.primary_ray(id.xy());
    let hitloc = ray.point_on_ray(inp.w);

    let nrm = inp.xyz();
    let light_dir = (LIGHT_LOCATION - hitloc).normalize();
    //check if we are facing the light, if not we can return early
    let n_dot_l = light_dir.dot(nrm.normalize());
    let light_dist = (LIGHT_LOCATION - hitloc).length();

    if n_dot_l < 0.0 || light_dist > LIGHT_RADIUS {
        unsafe {
            out_direct.write(
                pixel_coord,
                Vec4::new(AMBIENT_LIGHT.x, AMBIENT_LIGHT.y, AMBIENT_LIGHT.z, 1.0),
            );
        }
        return;
    }

    //Seems like we actually have to calculate something.
    //Get origin of shadow ray
    let origin = hitloc + (nrm * (inp.w).min(2.0));

    let shadow = trace_soft_shadow(code, origin, LIGHT_LOCATION, SHADOW_SHARPNESS);

    let radiance = radiance(light_dist, LIGHT_RADIUS);
    let direct_light = (LIGHT_COLOR * LIGHT_POWER * shadow * radiance).max(AMBIENT_LIGHT);
    let light = AMBIENT_LIGHT.lerp(direct_light, n_dot_l);

    unsafe {
        out_direct.write(pixel_coord, light.extend(1.0));
    }
}

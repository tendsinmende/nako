/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![cfg_attr(
    target_arch = "spirv",
    feature(register_attr),
    register_attr(spirv),
    no_std
)]
// HACK(eddyb) can't easily see warnings otherwise from `spirv-builder` builds.
#![deny(warnings)]

use shader_shared::glam::{UVec2, UVec3, Vec2, Vec3Swizzles, Vec4};
use shader_shared::push_constants::{
    ResolveConst, SAMPLING_FLAG_NONE_SAMPLING, SAMPLING_FLAG_OVER_SAMPLING,
    SAMPLING_FLAG_UNDER_SAMPLING,
};
use shader_shared::spirv_std;
use shader_shared::spirv_std::Image;

//Note this is needed to compile on cpu
#[cfg(not(target_arch = "spirv"))]
use shader_shared::spirv_std::macros::spirv;

fn over_sampling(
    over_sample_count: u32,
    image: &Image!(2D, format=rgba32f, sampled=false),
    coord: UVec2,
) -> Vec4 {
    //based on the over sampling count gather nXn pixel around our coord and average them (for now, maybe something more intelligent later)
    let mut px = Vec4::ZERO;
    let super_sampling_coord = coord * UVec2::splat(over_sample_count);
    let mut x = 0;

    //NOTE using while because of some compiler bugs
    while x < over_sample_count {
        let mut y = 0;
        while y < over_sample_count {
            let new: Vec4 = image.read(super_sampling_coord + UVec2::new(x, y));
            px += new;
            y += 1;
        }
        x += 1;
    }

    px / (over_sample_count * over_sample_count) as f32
}

///caluclates pixel by greedy picking.
//TODO reimplement AMD's fidelity fx here
fn under_sampling(
    under_sample_count: u32,
    image: &Image!(2D, format=rgba32f, sampled=false),
    coord: UVec2,
) -> Vec4 {
    let sample_coord = (coord.as_vec2() / Vec2::splat(under_sample_count as f32))
        .floor()
        .max(Vec2::ZERO)
        .as_uvec2();
    image.read(sample_coord)
}

#[spirv(compute(threads(8, 8, 1)))]
pub fn main(
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(push_constant)] push: &ResolveConst,
    #[spirv(descriptor_set = 0, binding = 0)] in_src: &Image!(2D, format=rgba32f, sampled=false),
    #[spirv(descriptor_set = 0, binding = 1)] out_resolved: &Image!(2D, format=rgba32f, sampled=false),
) {
    let pixel_coord = id.xy();

    let img_size = out_resolved.query_size::<UVec2>();
    if img_size.x < pixel_coord.x || img_size.y < pixel_coord.y {
        return;
    }

    //Check resolve type
    if (push.sampling_flags & SAMPLING_FLAG_OVER_SAMPLING) > 0 {
        let sample: Vec4 = over_sampling(push.sampling_param, in_src, pixel_coord);
        unsafe {
            out_resolved.write(pixel_coord, sample);
        }
    } else if (push.sampling_flags & SAMPLING_FLAG_UNDER_SAMPLING) > 0 {
        let sample = under_sampling(push.sampling_param, in_src, pixel_coord);
        unsafe {
            out_resolved.write(pixel_coord, sample);
        }
    } else if (push.sampling_flags & SAMPLING_FLAG_NONE_SAMPLING) > 0 {
        //This shouldn't happen since the frame builder should optimize that out. However, for compatibility we add it.
        let sample: Vec4 = in_src.read(pixel_coord);
        unsafe {
            out_resolved.write(pixel_coord, sample * Vec4::new(1.0, 0.2, 0.2, 1.0));
        }
    } else {
        //Error state
        unsafe {
            out_resolved.write(pixel_coord, Vec4::new(1.0, 0.0, 0.0, 1.0));
        }
    }
}

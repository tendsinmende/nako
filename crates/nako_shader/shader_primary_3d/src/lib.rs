/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![cfg_attr(
    target_arch = "spirv",
    feature(register_attr),
    register_attr(spirv),
    no_std
)]
// HACK(eddyb) can't easily see warnings otherwise from `spirv-builder` builds.
#![deny(warnings)]

use shader_shared::glam::{UVec3, Vec3, Vec3Swizzles, Vec4};
use shader_shared::push_constants::Primary3dConst;
use shader_shared::ray_tracing::{get_normal, trace};
use shader_shared::spirv_std;
use shader_shared::spirv_std::Image;

//Note this is needed to compile on cpu
#[cfg(not(target_arch = "spirv"))]
use shader_shared::spirv_std::macros::spirv;

pub const MAX_PRIMARY_TRACE: f32 = 5000.0;

#[spirv(compute(threads(8, 8, 1)))]
pub fn main(
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(push_constant)] push: &Primary3dConst,
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] code: &[u32],
    #[spirv(descriptor_set = 0, binding = 1)] out_albedo: &Image!(2D, format=rgba8, sampled=false),
    #[spirv(descriptor_set = 0, binding = 2)] out_nrm_depth: &Image!(2D, format=rgba32f, sampled=false),
) {
    let mut pixel_coord = id.xy();
    if push.camera.img_width < id.x || push.camera.img_height < id.y {
        return; //early return, outside of image
    }

    pixel_coord.y = push.camera.img_height - 1 - pixel_coord.y;

    //Check for faulty code
    if code.len() < 2 || code.len() > 1_000_000 {
        unsafe {
            out_albedo.write(pixel_coord, Vec4::new(0.0, 0.0, 0.0, 0.0));
            out_nrm_depth.write(pixel_coord, Vec4::new(0.0, 0.0, 0.0, 0.0));
        }
        return;
    }

    let ray = push.camera.primary_ray(id.xy());
    let res = trace(code, &ray, MAX_PRIMARY_TRACE);

    //Ignore I guess
    let albedo = if res.is_invalid() {
        Vec3::new(1.0, 0.0, 1.0)
    } else {
        Vec3::new(
            res.result.slot_v0[0],
            res.result.slot_v0[1],
            res.result.slot_v0[2],
        )
    };

    let alpha = if res.has_hit {
        res.result.slot_v0[3]
    } else {
        0.0
    };

    unsafe {
        out_albedo.write(pixel_coord, albedo.extend(alpha));
    }

    //Query normal and write out
    let mut normal = Vec3::ZERO;
    if res.has_hit {
        normal = get_normal(code, res.hitloc);
    }

    unsafe {
        out_nrm_depth.write(pixel_coord, normal.extend(res.t));
    }
}

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
#![cfg_attr(
    target_arch = "spirv",
    feature(register_attr),
    register_attr(spirv),
    no_std
)]
// HACK(eddyb) can't easily see warnings otherwise from `spirv-builder` builds.
#![deny(warnings)]

use shader_shared::glam::{UVec3, Vec3, Vec3Swizzles, Vec4};
use shader_shared::push_constants::Primary3dSegTraConst;
use shader_shared::ray_tracing::{get_normal, segment_tracing, bound_segment_tracing, SegmentTraceResult};
use shader_shared::spirv_std::Image;
use shader_shared::{spirv_std, BBox};

//Note this is needed to compile on cpu
#[cfg(not(target_arch = "spirv"))]
use shader_shared::query_volume::QueryVoxel;
#[cfg(not(target_arch = "spirv"))]
use shader_shared::spirv_std::macros::spirv;

pub const MAX_PRIMARY_TRACE: f32 = 5000.0;

#[spirv(compute(threads(8, 8, 1)))]
pub fn main(
    #[spirv(global_invocation_id)] id: UVec3,
    #[spirv(push_constant)] push: &Primary3dSegTraConst,
    #[spirv(storage_buffer, descriptor_set = 0, binding = 0)] code: &[u32],

    #[cfg(not(target_arch = "spirv"))]
    #[spirv(descriptor_set = 0, binding = 1)]
    query_volume: &[QueryVoxel],

    #[cfg(target_arch = "spirv")]
    #[spirv(descriptor_set = 0, binding = 1)]
    query_volume: &Image!(3D, format=rgba16f, sampled=false),
    #[spirv(descriptor_set = 0, binding = 2)] out_albedo: &Image!(2D, format=rgba8, sampled=false),
    #[spirv(descriptor_set = 0, binding = 3)] out_nrm_depth: &Image!(2D, format=rgba32f, sampled=false),
) {
    let coord = id.xy();
    let mut pixel_coord = coord;
    if push.camera.img_width < id.x || push.camera.img_height < id.y {
        return; //early return, outside of image
    }

    pixel_coord.y = push.camera.img_height - 1 - pixel_coord.y;

    //Check for faulty code
    if code.len() < 2 || code.len() > 1_000_000 {
        unsafe {
            out_albedo.write(pixel_coord, Vec4::new(1.0, 0.0, 0.0, 0.0));
            out_nrm_depth.write(pixel_coord, Vec4::new(1.0, 0.0, 0.0, 0.0));
        }
        return;
    }

    let ray = push.camera.primary_ray(pixel_coord);

    let (mut mint, mut maxt): (f32, f32) = (1.0, MAX_PRIMARY_TRACE);
    //Prepare the rays min and max distance. We can optimize if the volume encloses the sdf compleatly.
    //in that case we won't have to sample outside the volume. Therefore any sdf calculation is only a texture fetch.
    let res = if push.volume.sdf_enclosed() {
        //check the bounds min/max and setup mint and maxt
        let (is_intersecting, bmin, bmax) = BBox {
            min: Vec3::from(push.volume.min),
            max: Vec3::from(push.volume.max),
        }
        .ray_intersections(&ray);

        if is_intersecting{
            mint = mint.max(bmin);
            maxt = maxt.min(bmax);

            //query volume encloses sdf, we can therefore use the optimized version of segment tracing
            bound_segment_tracing(&push.volume, query_volume, code, &ray, mint, maxt)
        }else{
            //if we are not intersecting the sdf query volume in any way,
            //bound the volume encloses the sdf,
            //we can be sure to not intersect at all.
            SegmentTraceResult::NO_INTERSECTION
        }
    }else{
        //bound is infinit, therefore use segment tracing with fallback strategy
        segment_tracing(&push.volume, query_volume, code, &ray, mint, maxt)
    };


        
    //Ignore I guess
    let albedo = if res.is_invalid() {
        Vec3::new(
            res.result.slot_v0[0],
            res.result.slot_v0[1],
            res.result.slot_v0[2],
        )
    } else {
        Vec3::new(
            res.result.slot_v0[0],
            res.result.slot_v0[1],
            res.result.slot_v0[2],
        )
    };

    let alpha = if res.has_hit {
        res.result.slot_v0[3]
    } else {
        0.0
    };
    
    unsafe {
        out_albedo.write(coord, albedo.extend(alpha));
    }

    //Query normal and write out
    let mut normal = Vec3::ZERO;
    if res.has_hit {
        normal = get_normal(code, res.hitloc);
    }

    unsafe {
        out_nrm_depth.write(coord, normal.extend(res.t));
    }
}

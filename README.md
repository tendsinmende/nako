<div align="center">

# Nako

**[Signed distance field](https://en.wikipedia.org/wiki/Signed_distance_function) framework for Rust.**

Provides an instruction set (`nako_instructions`), compiler (`nako`), and renderer (`render/nakorender`).

</div>

## Disclaimer

The project is paused indefinitely. While testing it in real-world applications, it became apparent that the concept of a GPU-side 
interpreter for SDF-bytecode becomes slow for more complex SDFs. I therefore started working on a SDF ⟶ SPIR-V compiler instead, that 
generates GPU executable code in a Just-In-Time fashion. 

Nako might be continued as a frontend to [that](https://gitlab.com/tendsinmende/vola) project in the future.

## Description

The project is build upon the `nako` main crate that lets you define signed distance functions. It also contains several already defined standard primitives 
as well as transformations, that should get you started. 

`nako` itself makes not assumption over the context the SDF is used in. It merely provides you with a standard way of converting those
functions into a defined byte-code. For rendering have a look at `nakorender` which provides two standard renderers.

1. Software rendering on the CPU into an image via the `CpuBackend`
2. Gpu based rendering via the `MarpBackend` using vulkan.


Defining an SDF is done through an operator stream that can be optimized by nako for execution on the cpu or gpu. If you want to implement your own renderer, have a look at the gpu shaders to get an idea.

## Before adding Nako to your dependencies

When using the `nakorender` crate, make sure you put the `rust-toolchain` file into your working directory. Nako's Vulkan renderer currently needs a specific rust compiler version
and some additional components to work.

The first time compiling `nakorender` might take **really long** like 10min -20min long. Not sure why, but that's the price to pay for using rust shaders at the moment.

## Note on versions
The current *stable* branch is v0.3.0. The `main` branch does not use GLSL at all and experiments with [segment tracing](https://hal.archives-ouvertes.fr/hal-02507361/) the SDF using a voxel acceleration volume.

## Overview

The main idea of Nako is to give a user the ability to easily draw any shape to the screen. The idea is not new and already covered by libraries like SDL.
However, drawing complex shapes usually becomes a nightmare to manage for the user. Usually because the shapes are based on vertices. Nako instead is build on the concept of Signed-Distance-Fields. Those can be combined in creative ways to draw complex shapes.

Nako itself only defines a way to create SDF functions which are defined by a custom instruction set. Any interpreter on that instruction set in turn can display (or just calculate) the generated SDFs. Therefore, it is easy to create a GPU executor, or integrate the system into an already programmed engine.

To understand the concept a little better, have a look at `nakorender/src/cpu` as well as the `serialize` functions of the operations within `nako/src/operations` directory.

## Nako Std
The `nako_std` crate defines some useful additional data structures on top of nako. For instance button and other interface elements as well as structures to generate character sdfs from fonts.

It however does not make an assumption over the used renderer as well.

## Building and running
### Prerequisite
When using the GPU renderer, make sure that you have a Vulkan capable graphics card (anything after ~2015 should be fine).

### Building
Building and using the crates is the usual
```shell
cargo build --release
```

### Running the demo
Running the `layers` crate (which contains a small demo) is done through
```shell
cargo run --bin layers --release
```

## Documentation
The crates are mostly documented, however since they are not hosted on crates.io yet, you'll have to build the docs yourself via:
```
cargo doc --open
```

## FAQ
### Help I get rustc errors!
Make sure you have the same `rust-toolchain` file in your project's root directory as nako. It it still doesn't work, try upgrading all `rust-gpu` dependencies to the latest commit, otherwise file an issue please!

### Renderer crashes on startup
When using the `MarpBackend` and having the `validation` feature activated you'll have to have lunarG's Validation layers installed.

### Renderer did not find shaders on startup
This can happen if the build script for `nakorender` did not execute properly. In that case run
```
cargo run --bin nako_shader_builder --release -- --glsl
```
if using nako locally. Otherwise, please file an issue.

## Features
There are several features that can be activated or deactivated depending on the intended usage of the crates.

### Nako
- serialize: Enables `serde` support for most of nako's public structs. Is enabled by default

### Nakorender
- cpu_backend: Enables a CPU backend, that renders the picture on the CPU. Disabled by default.
- validation: Enables debug layers on Vulkan for validation. Disabled by default.
- glsl_evaluation: falls back to GLSL shader based SDF evaluation for primary and secondary rays. Might be more stable.
- watch_shaders: enables watching shader files at runtime and hot reloading when changing them. Can be used with the `NAKO_SHADER_CRATE` variable to live code shaders used in nako. If not in use the build script compiles shaders once.


## Variables
### Nakorender
- `NAKO_SHADER_CRATE`: if set overwrites where Nako searches for the shader crate. Assumes that a modified version of the `crates/nako_shader` crate is located at the given path.

## License
The whole project is licensed under MPL v2.0, all contributions will be licensed the same.

